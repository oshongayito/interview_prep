

//Idea 1 (divide by all possible numbers)

int N = 5000;
 
bool isPrime( int i ) {
    for( int j = 2; j < i; j++ ) {
        if( i % j == 0 ) // i is divisible by j, so i is not a prime
            return false;
    }
    // No integer less than i, divides i, so, i is a prime
    return true;
}
 
int main() {
    for( int i = 2; i <= N; i++ ) {
        if( isPrime(i) == true )
            printf("%d ", i);
    }
    return 0;
}

// Idea 2 (divide up to square root)
/*b >= a/b

or, b2 >= a

or, b >= sqrt(a)

so, bminimum = sqrt(a)
*/


int N = 5000;
 
bool isPrime( int i ) {
    int sqrtI = int( sqrt( (double) i) );
    // dont write "for(int j = 2; j <= sqrt(i); j++)" because sqrt is a slow
    // function. So, dont calculate it all the time, calculate it only once
    for( int j = 2; j <= sqrtI; j++ ) {
        if( i % j == 0 ) // i is divisible by j, so i is not a prime
            return false;
    }
    // No integer less than i, divides i, so, i is a prime
    return true;
}
 
int main() {
    for( int i = 2; i <= N; i++ ) {
        if( isPrime(i) == true )
            printf("%d ", i);
    }
    return 0;
}

//Idea 3 (no need to take even numbers)

int N = 5000;
 
bool isPrime( int i ) {
    int sqrtI = int( sqrt( (double) i) );
    for( int j = 3; j <= sqrtI; j += 2 ) { // j += 2 is given, condition (2)
        if( i % j == 0 ) // i is divisible by j, so i is not a prime
            return false;
    }
    return true;
}
 
int main() {
    printf("2 "); // 2 is the only even prime, so, print it
    for( int i = 3; i <= N; i += 2 ) { // i += 2 is given here, condition (1)
        if( isPrime(i) == true )
            printf("%d ", i);
    }
    return 0;
}

//Idea 4 (Sieve of Eratosthenes)

int N = 5000;
int status[5001];
// status[i] = 0, if i is prime
// status[i] = 1, if i is not a prime
int main() {
    int i, j;
    // initially we think that all are primes, so change the status
    for( i = 2; i <= N; i++ )
    status[i] = 0;
 
    for( i = 2; i <= N; i++ ) {
        if( status[i] == 0 ) {
            // so, i is a prime, so, discard all the multiples
            // j = 2 * i is the first multiple, then j += i, will find the
            // next multiple
            for( j = 2 * i; j <= N; j += i )
                status[j] = 1; // status of the multiple is 1
        }
    }
    // print the primes
    for( i = 2; i <= N; i++ ) {
        if( status[i] == 0 ) {
            // so, i is prime
            printf("%d ", i);
        }
    }
    return 0;
}

//Idea 5 (again, no even numbers)

int N = 5000;
int status[5001];
 
// status[i] = 0, if i is prime
// status[i] = 1, if i is not a prime
 
int main() {
    int i, j;
    // initially we think that all are primes
    for( i = 2; i <= N; i++ )
    status[i] = 0;
 
    for( i = 3; i <= N; i += 2 ) {
        if( status[i] == 0 ) {
            // so, i is a prime, so, discard all the multiples
            // 3 * i is odd, since i is odd. And j += 2 * i, so, the next odd
            // number which is multiple of i will be found
            for( j = 3 * i; j <= N; j += 2 * i )
                status[j] = 1; // status of the multiple is 1
        }
    }
    // print the primes
    printf("2 ");
    for( i = 3; i <= N; i += 2 ) {
        if( status[i] == 0 ) {
            // so, i is prime
            printf("%d ", i);
        }
    }
    return 0;
}

//Idea 6 (modified discarding technique)

int N = 5000, status[5001];
int main() {
    int i, j, sqrtN;
    for( i = 2; i <= N; i++ ) status[i] = 0;
    sqrtN = int( sqrt((double) N )); // have to check primes up to (sqrt(N))
    for( i = 3; i <= sqrtN; i += 2 ) {
        if( status[i] == 0 ) {
            // so, i is a prime, so, discard all the multiples
            // j = i * i, because it’s the first number to be colored
            for( j = i * i; j <= N; j += i + i )
                status[j] = 1; // status of the multiple is 1
        }
    }
    // print the primes
    printf("2 ");
    for( i = 3; i <= N; i += 2 ) {
        if( status[i] == 0 ) printf("%d ", i);
    }
    return 0;
}


//Idea 7 (saving memory)

int N = 5000, status[2501];
int main() {
    int i, j, sqrtN;
    for( i = 2; i <= N >> 1; i++ ) status[i] = 0;
    sqrtN = int( sqrt((double)N) ); // have to check primes up to (sqrt(N))
    for( i = 3; i <= sqrtN; i += 2 ) {
        if( status[i>>1] == 0 ) {
            // so, i is a prime, so, discard all the multiples
            // j = i * i, because it’s the first number to be colored
            for( j = i * i; j <= N; j += i + i )
                status[j>>1] = 1; // status of the multiple is 1
        }
    }
    // print the primes
    printf("2 ");
    for( i = 3; i <= N; i += 2 ) {
        if( status[i>>1] == 0 )
            printf("%d ", i);
    }
    return 0;
}
