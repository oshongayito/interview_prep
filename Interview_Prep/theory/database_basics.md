### RDMS (relational database management systems): MySQL, MSSQL, Oracle etc. ###
--------------------------------------------------------------------------
Follows **ACID** properties

**A (Atomicity):** Either the entire transaction takes place at once or doesn’t happen at all.

**C(Consistency):** Consistency ensures that a transaction can only bring the database from one valid state to another, maintaining database invariants: any data written to the database must be valid according to all defined rules, including constraints, cascades, triggers, and any combination thereof. This prevents database corruption by an illegal transaction, but does not guarantee that a transaction is correct. Referential integrity guarantees the primary key - foreign key relationship.

**I(Isolation):** Transactions are often executed concurrently (e.g., reading and writing to multiple tables at the same time). Isolation ensures that concurrent execution of transactions leaves the database in the same state that would have been obtained if the transactions were executed sequentially. Isolation is the main goal of concurrency control; Isolatin ensures that transactions take place in isolation and changes are visible only after a they have been made to the main memory.

**D(Durability):** This property ensures that once the transaction has completed execution, the updates and modifications to the database are stored in and written to disk and they persist even if system failure occurs. 



### NoSQL ###
----------
Follows **CAP theorem**

 **Consistency: ** Assuming you have a storage system which has more than one machine, consistency implies that the data is same across the cluster, so you can read or write to/from any node and get the same data.

 **Eventual consistency : ** Exactly what the name suggests. In a cluster, if multiple machines store the same data, an eventual consistent model implies that all machines will have the same data eventually. Its possible that at a given instance, those machines have different versions of the same data ( temporarily inconsistent ) but they will eventually reach a state where they have the same data.

 **Availability: ** In the context of a database cluster, Availability refers to the ability to always respond to queries ( read or write ) irrespective of nodes going down.

 **Partition Tolerance: ** In the context of a database cluster, cluster continues to function even if there is a “partition” (communications break) between two nodes (both nodes are up, but can’t communicate).

 **CAP Theorem ** states that in a distributed system, it is impossible to simultaneously guarantee all of the following:

•  Consistency
•  Availability
•  Partition Tolerance



### Database Normalization ###
---------------------------------
Database normalization is the process of structuring a relational database[clarification needed] in accordance with a series of so-called normal forms in order to reduce data redundancy and improve data integrity.


### What are the different types of languages that are available in the DBMS? ###
---------------------------------------------------------------------------------------------------

Ans: Basically, there are 3 types of languages in the DBMS as mentioned below:

DDL: DDL is Data Definition Language which is used to define the database and schema structure by using some set of SQL Queries like CREATE, ALTER, TRUNCATE, DROP and RENAME.

DCL: DCL is Data Control Language which is used to control the access of the users inside the database by using some set of SQL Queries like GRANT and REVOKE.

DML: DML is Data Manipulation Language which is used to do some manipulations in the database like Insertion, Deletion, etc. by using some set of SQL Queries like SELECT, INSERT, DELETE and UPDATE.



### What are the main differences between Primary key and Unique Key? ###
---------------------------------------------------------------------------------------------------

Ans: Given below are few differences:

The main difference between the Primary key and Unique key is that the Primary key can never have a null value while the Unique key may consist of null value.
In each table, there can be only one primary key while there can be more than one unique key in a table.




### What is the use of DROP command and what are the differences between DROP, TRUNCATE and DELETE commands? ###
---------------------------------------------------------------------------------------------------

Ans: DROP command is a DDL command which is used to drop/delete the existing table, database, index or view from the database.

The major difference between DROP, TRUNCATE and DELETE commands are:

DROP and TRUNCATE commands are the DDL Commands which are used to delete tables from the database and once the table gets deleted, all the privileges and indexes that are related to the table also get deleted. These 2 operations cannot be rolled back and so should be used with great care.

DELETE Command, on the other hand, is a DML Command which is also used to delete rows from the table and this can be rolled back.


### MySQL vs PostgreSQL ###
---------------------------------------------------------------------------------------------------

Better Licensing in PostgreSQL (free MIT style license), specially when supplying a whole application.

Better data integrity check in postgreSQL.

JSON and NoSQL support in postgreSQL




