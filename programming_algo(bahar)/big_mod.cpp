long long big_mod(long long b,long long p,long long m)
{
	long long n;
	if(p==0) return 1;
	else if(p%2==0)
	{
		n=big_mod(b,p/2,m);
		 return (n*n)%m;
	} 
	else return ((b%m)*big_mod(b,p-1,m))%m;
}

long long bigmod(long long b,long long p,long long m)
{
    if(b==0) return 0;
    long long x,power;
    x=1;
    power=b%m;
    while(p)
    {
       if(p%2==1)
            x=(x*power)%m;
        power=(power*power)%m;
        p=p/2;
    }
    return x;
}
