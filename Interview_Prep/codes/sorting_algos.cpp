#include<iostream>
#include<vector>

using namespace std;


/*
Comparison between Quick Sort and Merge Sort:



Quicksort: 
1. better suited for arrays. 
2. Do not require extra spaces
3. random access is good, cache use makes it better
4. Sorting linked list is not good with it. because nodes in linked list are not in contiguous memory locations like arrays


Merge Sort:
1. Requires extra space for arrays
2. However, good for sorting linked list
3. For linked list, do not require extra memory, because insertion can be done in O(1) time and space

Reference: https://www.geeksforgeeks.org/quick-sort/

*/

/*
 * QuickSort worst case time complexity O(N^2)
 * average case O(N * LogN)
 * Space Complexity O(1)
 */
pair<int,int> partition(vector<int>&nums, int start, int end){
	int pivot = nums[(start+end)/2]; //mid element
	int left=start, right = end;
	while(left <= right){
		while(nums[left]<pivot)left++; // Move to the right until we find a value which is greater or equal
		while(nums[right]>pivot)right--; // Move to the left until we find a value which is smaller or equal
		
		// At this point, left, right are not in their actual positions, need to swap
		if(left <= right){
			swap(nums[left++], nums[right--]);
		}
	}
	return {left, right};
}

void quick_sort(vector<int>&nums, int start, int end){
	if(start >= end)return;
	auto pivot_index = partition(nums, start, end);
	quick_sort(nums, start, pivot_index.second);
	quick_sort(nums, pivot_index.first, end);
}



/*
This merge function will be called n-1 times.
However, the average complexity of this merge function is logN.
Because, each time it will merge different sizes of arrays. If you go through an example with N=8, 
you will find that total work is 24 units or (8 * log8) units
Therefore, the total complexity of merge function is O(N * LogN)

Reference: https://www.reddit.com/r/learnprogramming/comments/2c41ia/how_many_times_will_merge_and_mergesort_be_run_in/
*/
void merge(vector<int>&nums, int start, int mid, int end){
	vector<int>temp(end-start+1);
	int i=0, left, right;
	for(left=start, right=mid+1;left<=mid && right<=end;){
		if(nums[right]<nums[left]){
			temp[i++] = nums[right++];
		}
		else{
			temp[i++]=nums[left++];
		}
	}
	// take the remaining items (if any) (the remaining will be already ordered)
	// consider the following 2 cases:
	// Case 1 : left > mid && right <= end (at this point the left array had been consumed, and the right array is not consumed totally)
	//           as we only have the right portion, that one should be array (considering the assumption of merge sort where we merge 2 arrays of left and right where both are sorted)
	// Case 2: right > end && left <= mid (the same as case 1, but the left array needs to processed in this case)
	while(i<=(end-start) && (right<=end || left<=mid)){
		temp[i++] = right <= end ? nums[right++] : nums[left++];
	}

	for(int i=start, j=0;i<=end;i++,j++){
		nums[i] = temp[j];
	}
}


/*
Time: O(N*LogN)
Space: O(N)
*/
void merge_sort(vector<int>&nums, int start, int end){
	if(start == end)return;

	int mid = (start+end)/2;

	merge_sort(nums, start, mid);
	merge_sort(nums, mid+1, end);

	merge(nums, start, mid, end);

}

// Time complexity O(N^2)
void selection_sort(vector<int>&nums){
	for(int i=0;i<nums.size();i++){

		//select the minimum from the right
		int min_id = i;
		for(int j=i+1;j<nums.size();j++){
			if(nums[j] < nums[min_id]){
				min_id = j;
			}
		}
		swap(nums[min_id], nums[i]);
	}
}


//Time Complexity O(N^2)
void insertion_sort(vector<int>&nums){
	for(int i=1;i<nums.size();i++){
		int key = nums[i];
		//find all the elements greater than the current element, and swap them one position right
		int j=i-1;
		for(;j>=0 && nums[j]>key;j--){
			swap(nums[j],nums[j+1]);
		}
		nums[j+1]=key;
	}
}

//Time Complexity O(N^2)
//better suited when input is almost sorted
void bubble_sort(vector<int>&nums){
	for(int i=0;i<nums.size();i++){
		bool swapped = false;
		for(int j=0;j<nums.size()-1;j++){
			if(nums[j] > nums[j+1]){
				swap(nums[j], nums[j+1]);
				swapped=true;
			}
		}
		if(!swapped)break;
	}
}

int main(){
	vector<int>arr = {4,5,3,1,3,2,4,3,39,40,23,200,100};
	vector<int>nums = arr;
	cout<<"unsorted:"<<endl;
	for(auto num:nums)cout<<num<<" ";
	cout<<endl;
	selection_sort(nums);
	cout<<"selection sort:"<<endl;
	for(auto num:nums)cout<<num<<" ";
	cout<<endl;
	nums = arr;
	bubble_sort(nums);
	cout<<"bubble sort:"<<endl;
	for(auto num:nums)cout<<num<<" ";
	cout<<endl;

	nums = arr;
	merge_sort(nums, 0, nums.size()-1);
	cout<<"merge sort:"<<endl;
	for(auto num:nums)cout<<num<<" ";
	cout<<endl;
	

	nums = arr;
	quick_sort(nums, 0, nums.size()-1);
	cout<<"quick sort:"<<endl;
	for(auto num:nums)cout<<num<<" ";
	cout<<endl;

	return 0;
}