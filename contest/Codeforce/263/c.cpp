    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))

int main()
{
    ll n,i,j,k,score=0,temp;
    vc<ll> arr;

    cin>>n;
    arr.resize(n);
    for(i=0;i<n;i++)
    {
        cin>>arr[i];
        score+=arr[i];
    }
    temp=score;
    sort(arr.begin(),arr.end());

    for(i=0;i<n-1;i++)
    {
        score+=(temp-arr[i]);
        score+=arr[i];
        temp=temp-arr[i];
    }

    cout<<score<<endl;
    return 0;
}
