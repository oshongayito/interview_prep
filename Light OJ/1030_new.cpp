#include<iostream>
#include<cstdio>
#include<cmath>
#include<vector>
#include<algorithm>
#include<cstring>
#include <iomanip>

using namespace std;

#define ll long long
#define vc vector
#define pb push_back

void precision_double(int x)
{
    std::cout << std::setprecision(x) << std::fixed;
}

int main()
{
    int n,i,j,d,l,t;
    vc<double>golds;
    vc<double>arr;
    vc<double>c_sum;
    cin>>t;

    for(l=1;l<=t;l++){
    cin>>n;
    golds.resize(n);
    arr.resize(n);
    c_sum.resize(n);

    for(i=0;i<n;i++)cin>>arr[i];

    for(i=n-1,j=0;i>=0;i--,j++)
    {
        if(j==0)
        {
            golds[j]=arr[i];
            c_sum[j]=arr[i];
        }
        else if(j<6)
        {
            golds[j]=arr[i]+((c_sum[j-1])/(double)(j));
            c_sum[j]=golds[j]+c_sum[j-1];
        }
        else
        {

            golds[j]=arr[i]+((c_sum[j-1]-c_sum[j-1-6])/(double)(6));
            c_sum[j]=golds[j]+c_sum[j-1];
        }
        //cout<<j<<": "<<golds[j]<<endl;
    }
    precision_double(8);
    cout<<"Case "<<l<<": "<<golds[n-1]<<endl;

    arr.clear();
    golds.clear();
    c_sum.clear();

    }

    return 0;
}
