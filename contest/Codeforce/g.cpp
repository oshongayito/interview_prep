#include<iostream>
#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#define pi (2*acos(0.0))
#define pb push_back
#define MAX_SIZE 1000

using namespace std;

typedef struct point
{
    double x;
    double y;
}PT;
typedef struct line
{
    PT a;
    PT b;
}LN;
//typedef struct PT PT;
//const double PI = 2.0*acos(0.0);
const double EPS = 1e-9;
map < pair<double,double> , int>  mp;


PT sub(PT a, PT b)
{
PT r;
r.x= a.x-b.x; r.y=a.y-b.y;
return r;
}
PT add(PT a,PT b)
{
PT r;
r.x= b.x+a.x; r.y= b.y+a.y;
return r;
}
PT mult(PT a, double sc)
{
PT r;
r.x=a.x*sc; r.y=a.y*sc;
return r;

}



double trap(PT a, PT b)
{
return (0.5*(b.x - a.x)*(b.y + a.y));
}

double triarea(PT a, PT b, PT c)
{
return fabs(trap(a,b)+trap(b,c)+trap(c,a));
}

int intersection( PT p1, PT p2, PT p3, PT p4, PT &r )
// two lines given by p1->p2, p3->p4 r is the intersection point
// return -1 if two lines are parallel
{
    double d = (p4.y - p3.y)*(p2.x-p1.x) - (p4.x - p3.x)*(p2.y - p1.y);
if( fabs( d ) < EPS ) return -1;
// might need to do something special!!!
double ua, ub;
ua = (p4.x - p3.x)*(p1.y-p3.y) - (p4.y-p3.y)*(p1.x-p3.x);
ua /= d;
// ub = (p2.x - p1.x)*(p1.y-p3.y) - (p2.y-p1.y)*(p1.x-p3.x);
//ub /= d;
r = add(p1,mult(sub(p2,p1),ua));
//r = p1 + (p2-p1)*ua;
return 0;
}


int pAndSeg(PT& p1, PT& p2, PT& p)
// the relation of the point p and the segment p1->p2.
// 1 if point is on the segment; 0 if not on the line; -1 if on the line but not on the segment
{
double s=triarea(p, p1, p2);
if(s>EPS) return(0);
double sg=(p.x-p1.x)*(p.x-p2.x);
if(sg>EPS) return(-1);
sg=(p.y-p1.y)*(p.y-p2.y);
if(sg>EPS) return(-1);
return(1);
}

int main()
{
    int i,j,k,l,f=0,t,tl,res=0;
    double a1,a2,b1,b2,x1,x2,y1,y2,p,q,r,s,tx,ty;
    vector<LN>lines;
    pair< double , double > pr;
    PT A,B,C,D,P,Q,R;
    LN M;

    cin>>t;
    for(i=1;i<=t;i++){
        //cout<<endl;
        cin>>tl;
    for(j=0;j<tl;j++)
    {
        cin>>M.a.x>>M.a.y>>M.b.x>>M.b.y;
        lines.pb(M);
    }
    res = tl;
    //cout<<res<<endl;
    for(j=0;j<tl;j++)
    {
        for(k=j+1;k<tl;k++)
        {
            if(intersection(lines[j].a, lines[j].b, lines[k].a, lines[k].b,R)==0)
            {
                //cout<<" # ";
                if(pAndSeg(lines[j].a,lines[j].b,R)==1 && pAndSeg(lines[k].a,lines[k].b,R)==1 && mp[make_pair(R.x,R.y)]==0)
                {
                    mp[make_pair(R.x,R.y)]=1;

                    res+=2;
                    //cout<<res<<endl;
                }
            }
        }
    }



   cout<<res<<endl;
   if(i<t)cout<<endl;
   mp.clear();
   lines.clear();
   res=0;

    //f=0;

    }




    return 0;

}

