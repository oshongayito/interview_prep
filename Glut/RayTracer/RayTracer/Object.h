#ifndef _OBJECT_H
#define _OBJECT_H

#include "Ray.h"
#include "customtypes.h"
#include "Color.h"

class Object {
	public:

	Object ();

	// method functions
	virtual Color getColor () { return Color (0.0, 0.0, 0.0, 0); }

	virtual Vector3 getNormalAt(Vector3 intersection_position) {
		return Vector3 (0, 0, 0);
	}

	virtual double findIntersection(Ray ray) {
		return 0;
	}

};

Object::Object () {}

#endif

