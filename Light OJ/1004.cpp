#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<string.h>
#define ll long long
using namespace std;

ll dp[200][100],arr[200][100];
int main()
{
    ll n,i,j,t,x,y,z,a,b;

    cin>>t;
    for(i=1;i<=t;i++)
    {
        cin>>n;
        for(x=0;x<(2*n-1);x++)
        {
            if(x<n){
                for(y=0;y<=x;y++){
                    cin>>arr[x][y];
                    dp[x][y]=arr[x][y];
                }
            }
            else{
                for(z=0;z<(2*n-x-1);z++){
                    cin>>arr[x][z];
                    dp[x][z]=arr[x][z];
                }
            }

        }



        for(x=1;x<(2*n-1) && n>1;x++)
        {
            if(x<n){
                for(y=0;y<=x;y++){
                    if(y==0){
                        if(arr[x][y]+dp[x-1][y]>dp[x][y])dp[x][y]=arr[x][y]+dp[x-1][y];
                    }
                    if(y==x){
                        if(arr[x][y]+dp[x-1][y-1]>dp[x][y])dp[x][y]=arr[x][y]+dp[x-1][y-1];
                    }
                    else{
                        if(arr[x][y]+dp[x-1][y]>dp[x][y])dp[x][y]=arr[x][y]+dp[x-1][y];
                        if(arr[x][y]+dp[x-1][y-1]>dp[x][y])dp[x][y]=arr[x][y]+dp[x-1][y-1];
                    }

                }
            }
            else{
                for(z=0;z<(2*n-x-1);z++){
                    if(arr[x][z]+dp[x-1][z]>dp[x][z])dp[x][z]=arr[x][z]+dp[x-1][z];
                    if(arr[x][z]+dp[x-1][z+1]>dp[x][z])dp[x][z]=arr[x][z]+dp[x-1][z+1];
                }
            }
        }
        if(n==1)cout<<"Case "<<i<<": "<<dp[0][0]<<endl;
        else cout<<"Case "<<i<<": "<<dp[2*n-2][0]<<endl;

        for(j=0;j<(2*n-1);j++){
            memset(dp[j],0,100);
            memset(arr[j],0,100);
        }

    }



    return 0;
}
