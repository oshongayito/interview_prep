#include<iostream>
#include<cstdio>
#include<set>
#include<queue>
#include<string>
#include<cstring>
#include<vector>
#include<map>
#include<cstdlib>
#include<climits>
using namespace std;


class edge{
	public:
	int to;
	int weight;
	edge(){}
	edge(int tt,int ww)
	{
		to=tt;
		weight=ww;
	}
    bool operator<(const edge &f) const
	{
		 return weight>f.weight;
	}
};

int dijkstra(int source,int destination,vector< vector<edge> > &graph)
{
	vector<int> distance(graph.size(),INT_MAX);
	distance[source]=0;
	priority_queue<edge> q; 
		
	q.push(edge(source,0));

	while(!q.empty())
	{
		int node=q.top().to;
		int dist=q.top().weight;
		q.pop();
		if(dist>distance[node]) continue;
		if(node==destination) break;
	
		
		for(int i=0;i<(int)graph[node].size();i++)
		{
			int to=graph[node][i].to;
			int weight=graph[node][i].weight;
			
			if(distance[to]>weight+dist)
			{
				distance[to]=weight+dist;
				q.push(edge(to,distance[to]));
			}
		}
	}
	
	return distance[destination];
}

int main()
{
		
	int n,m,s,t,u,v,e,d;
	
	cin>>t;
	
	for(int p=1;p<=t;p++)
	{
    
    cin>>n>>m>>s>>d;
    
    vector< vector<edge> > graph(n);

    for(int i=0;i<m;i++)
    {
	    cin>>u>>v>>e;
	    
	    graph[u].push_back(edge(v,e));
	    graph[v].push_back(edge(u,e));
	    
	} 	
	
	
	int time=dijkstra(s,d,graph);
	
	if(time==INT_MAX) cout<<"Case #"<<p<<": unreachable"<<endl;
	else cout<<"Case #"<<p<<": "<<time<<endl;
	
	}
	
	return 0;
}
