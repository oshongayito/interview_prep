#include<iostream>
#include<cstdio>
#include<cmath>
#include<vector>

using namespace std;


int bigmod ( long long a, int p, int m );

int bigmod ( long long a, int p, int m )
{
    if ( p == 0 )return 1; // If power is 0 ( a ^ 0 ), return 1

    if ( p & 1 ) // If power is odd, a ^ 7 = a * a ^ 6
    {
        return ( ( a % m ) * ( bigmod ( a, p - 1, m ) ) ) % m; // Multipication      may exceed int range
    }
    else
    {
        long long c = bigmod ( a, p / 2, m ); // Multipication may exceed int range
        return ( c * c ) % m;
    }
}



int main()
{
    long long int n,m,j,k,cnt1=0,cnt2=0,x,i;
    cin>>n>>m;
    x = bigmod(3,n,m);
    x= x-1;
    x =(x+m)%m;
    cout<<x<<endl;
    return 0;
}

