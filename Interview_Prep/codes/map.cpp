#include<iostream>
#include<map>
#include<vector>

using namespace std;


int main(){
	map<int,int>myMap;
	myMap.insert(make_pair(0,2));
	myMap.insert(make_pair(1,2));
	myMap.insert(make_pair(2,2));
	myMap.insert(make_pair(4,2));
	myMap.insert(make_pair(5,2));
	myMap.insert({6,2});
	// myMap.insert({4,2});
	// myMap.insert({5,2});
	// myMap.insert({6,2});

	cout<<"lower bound of 2 "<<(*(myMap.lower_bound(2))).first<<endl; //lower bound will get the key with value >= to the search key
	cout<<"upper bound of 2 "<<(*(myMap.upper_bound(2))).first<<endl; //upper bound will get the key with value > to the search key

	return 0;
}


