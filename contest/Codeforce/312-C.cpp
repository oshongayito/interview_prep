#include<iostream>
#include<vector>
#include<cmath>

using namespace std;


int gcd(int u, int v){
    return (v != 0)?gcd(v, u%v):u;
}

int main()
{
    int n,gcdVal=1,min=1000000009,maxElement=-1,count=0;
    vector<int> arr;


    cin>>n;
    arr.resize(n);
    for(int i=0;i<n;i++)
    {
        cin>>arr[i];
        if(i==0)gcdVal=arr[i];
        else gcdVal=gcd(gcdVal,arr[i]);
        if(arr[i]>maxElement)maxElement=arr[i];
    }

    //cout<<gcdVal<<endl;
    while(gcdVal<=maxElement)
    {
        count=0;
        for(int i=0;i<arr.size();i++)
        {
            if(arr[i]%gcdVal!=0)break;
            if(arr[i]>gcdVal)
            count+=(int)(log(((arr[i]/gcdVal)))/log(2));
            else count+=(int)(log((gcdVal/arr[i]))/log(2));
            cout<<count<<" "<<gcdVal<<endl;
        }
    cout<<endl;
        //cout<<count<<endl;
        if(count!=0 && count<min)min=count;
        cout<<count<<" "<<min<<endl;
        gcdVal*=2;

    }
    cout<<min<<endl;
    return 0;
}
