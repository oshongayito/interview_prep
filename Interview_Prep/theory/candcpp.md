
## Differences ##
1. C is a procedural language, C++ supports both procedural and OOP

2. C++ supports reference, C doesn't


###What are the differences between references and pointers?###

#### Similarities:####
1. Both references and pointers can be used to change local variables of one function insid
2. Both of them can also be used to save copying of big objects when passed as arguments to functions or returned from functions, to get efficiency gain.

#### Differences: ####
1. Once a reference is created, it cannot be later made to reference another object; it cannot be reseated. This is often done with pointers.
2. References cannot be NULL. Pointers are often made NULL to indicate that they are not pointing to any valid thing.
3. A reference must be initialized when declared. There is no such restriction with pointers


### What are virtual functions – Write an example? ###
Virtual functions are used with inheritance, they are called according to the type of object pointed or referred, not according to the type of pointer or reference. In other words, virtual functions are resolved late, at runtime. Virtual keyword is used to make a function virtual.

Following things are necessary to write a C++ program with runtime polymorphism (use of virtual functions)
1. A base class and a derived class.
2. A function with same name in base class and derived class.
3. A pointer or reference of base class type pointing or referring to an object of derived class.



### What are the major Differences between JAVA and C++? ###

1. Java has automatic garbage collection whereas C++ has destructors
2. Java does not support pointers, templates, unions, operator overloading, structures etc.
3. C++ has no in built support for threads,whereas in Java there is a Thread class that you inherit to create a new thread
4. C++ support multiple inheritance, method overloading and operator overloading but JAVA only has method overloading.
5. Java is interpreted and hence platform independent whereas C++ isn’t.


### Static in C++ ###

We can define class members static using static keyword. When we declare a member of a class as static it means no matter how many objects of the class are created, there is only one copy of the static member. A static member is shared by all objects of the class. All static data is initialized to zero when the first object is created, if no other initialization is present.



###References:###
1. https://www.geeksforgeeks.org/commonly-asked-c-interview-questions-set-1/
2. https://www.geeksforgeeks.org/commonly-asked-c-interview-questions-set-2/

