    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000

int main()
{

freopen("InputFile.txt","r",stdin);//redirects standard input
freopen("output.txt","w",stdout);//redirects standard output


    ll n,max=-1;
    vc< vc<ll> >arr;
    vc< vc<ll> >dp;


    while(!(cin>>n).eof()){
    arr.resize(n);
    dp.resize(n);
    for(int i=0;i<n;i++)
    {
        arr[i].resize(i+1);
        dp[i].resize(i+1);
    }

    for(int i=0;i<n;i++)
    {
        for(int j=0;j<i+1;j++)
        {
            cin>>arr[i][j];
            if(i==0)dp[i][j]=arr[i][j];
            else
            {
                if(j==0)dp[i][j]=dp[i-1][j]+arr[i][j];
                else if(j==arr[i].size()-1)dp[i][j]=dp[i-1][j-1]+arr[i][j];
                else
                {
                    if(dp[i-1][j-1]>dp[i-1][j])
                        dp[i][j]=arr[i][j]+dp[i-1][j-1];
                    else dp[i][j]=arr[i][j]+dp[i-1][j];
                }
            }

            if(dp[i][j]>max)max=dp[i][j];
        }
        //cout<<max<<endl;
    }

    cout<<max<<endl;
    arr.clear();
    dp.clear();
    max=-1;
    }
    return 0;
}
