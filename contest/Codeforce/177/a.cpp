
    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))


int main()
{
    ll res,n,k;
    vector< pair<int,int> >arr;

    cin>>n>>k;
    arr.resize(n);
    for(int i=0;i<n;i++)
    {
        cin>>arr[i].first>>arr[i].second;
    }
    res=0;
    for(int i=0;i<n;i++)
    {
        res+=(arr[i].second-arr[i].first+1);
    }
    int j;
    for(j=0;(res%k)!=0;j++)
    {
        res++;
    }
    cout<<j<<endl;
    return 0;
}
