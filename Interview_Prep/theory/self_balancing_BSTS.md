## Red Black Tree ##
Properties:

1) Every node has a color either red or black.

2) Root of tree is always black.

3) There are no two adjacent red nodes (A red node cannot have a red parent or red child).

4) Every path from a node (including root) to any of its descendant NULL node has the same number of black nodes.


pros: (i) good when frequent insetion, deletion is needed. In this situations, AVL, and Splay doesn't perform better than RB Tree

Example Applications: 
	(i) As universal data structures
	(ii) map, multiset in c++ uses RB Tree

## AVL Tree ##

AVL tree is a self-balancing Binary Search Tree (BST) where the difference between heights of left and right subtrees cannot be more than one for all nodes.

Insertion: https://www.geeksforgeeks.org/avl-tree-set-1-insertion/
Deletion: https://www.geeksforgeeks.org/avl-tree-set-2-deletion/

Cons: (i)increased number of rotations involved when insertion or deletion is required
Pros: (ii) good for lookups

Example Applications: 
	(i) when frequent lookup required
	(ii) database transactions

## Splay Tree ##

Example Applications: 
	(i) When same element is requested again again (locality in requests)
	(ii) Cache, garbage collection algos