    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)



template<class T>    ///Binary Index Tree Class...
class BIT
{
      //T *tree;
      int maxVal;
      public:
      T *tree;
      BIT(int N)
      {
              tree = new T[N+1];
              memset(tree,0,sizeof(T)*(N+1));
              maxVal = N;
      }
      void update(int idx, T val)
      {
           while (idx <= maxVal)
           {
                 tree[idx] += val;
                 idx += (idx & -idx);
           }
      }

      void decrease(int idx, T val)
      {
           while (idx <= maxVal)
           {
                 tree[idx] -= val;
                 idx += (idx & -idx);
           }
      }
      //Returns the cumulative frequency of index idx
      T read(int idx)
      {
        T sum=0;
        while (idx>0)
        {
              sum += tree[idx];
              idx -= (idx & (-idx));
        }
        return sum;
      }
};

int main()
{
    int t,n,q,i,j,x,y,p,r,k;
    vc<int>sacks,cum;
    scanf("%d",&t);
    for(k=1;k<=t;k++){

    scanf("%d%d",&n,&q);

    sacks.resize(n);
    cum.resize(n);
    BIT<int> B(n);

    for(i=1;i<=n;i++){

        scanf("%d",&sacks[i-1]);
        //cin>>sacks[i-1];
        if(i==1)
        cum[i-1]=sacks[i-1];
        else cum[i-1]=sacks[i-1]+cum[i-2];

        //B.update(i,sacks[i-1]);
    }

    for(i=1;i<=n;i++){
        int id= i-(i&(-i))+1;
        if(id==1)
        B.tree[i]=cum[i-1];
        else B.tree[i]=cum[i-1]-cum[id-2];
    }

    //cout<<"Case "<<k<<":"<<endl;
    printf("Case %d:\n",k);
    for(i=1;i<=q;i++){
        //cin>>y;
        scanf("%d",&y);

        if(y==1){
                //cin>>p;
                scanf("%d",&p);
                B.decrease(p+1,sacks[p]);
                //cout<<sacks[p]<<endl;
                printf("%d\n",sacks[p]);
                sacks[p]=0;
        }
        else if(y==2){
                //cin>>p>>r;
                scanf("%d%d",&p,&r);
                B.update(p+1,r);
                sacks[p]+=r;
        }
        else{
            //cin>>p>>r;
            scanf("%d%d",&p,&r);
            //cout<<B.read(p+1,r+1)<<endl;

            if(p>0)
            printf("%d\n",B.read(r+1)-B.read(p));
            else printf("%d\n",B.read(r+1));
        }
    }

    //sacks.clear();
    }

    return 0;

}
