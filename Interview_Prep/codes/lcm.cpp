#include<iostream>
using namespace std;

int gcd(int a,int b)
{
    while(a!=b)
    {
        if(a>b)a=a-b;
        else b=b-a;
    }
    return a;
}

int lcm(int a,int b)
{
    return (long long int)((a*b)/gcd(a,b));
}

int main()
{
    int x,y;
    cin>>x>>y;
    cout<<gcd(x,y)<<" "<<lcm(x,y)<<endl;
    return 0;
}
