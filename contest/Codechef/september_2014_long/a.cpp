    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)
    #define MAX 1000000007

int main()
{
    ll t,res;

    string str;
    int i,j;
    cin>>t;

    while(t--)
    {
        cin>>str;

        res=1;
        for(i=0;i<str.size();i++)
        {
            if(i%2==0)
            {
                if(str[i]=='l')res= (2*res)%MAX;
                else res=((2*res)+2)%MAX;

            }
            else
            {
                if(str[i]=='l')res=((2*res)-1)%MAX;
                else res = ((2*res)+1)%MAX;

            }
        }
        cout<<res%MAX;
        if(t>=1)cout<<endl;
        str.clear();
    }


    return 0;
}
