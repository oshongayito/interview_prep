
#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#define pi (2*acos(0.0))
#define pb push_back

using namespace std;

double T,n;
vector<double> dis,cs,xc,yc;

void find_distance(int i, double r, double t)
{
    double y,z,a,b,c;
    double A,B,C,x;
    x=((360*T)/t);
    if(x>=360)x=x-((int)(x/360)*360);
    //cout<<i<<" # "<<x<<endl;
    y=r*cos((x*pi)/180);
    z=r*sin((x*pi)/180);
    if(i>0)
    {
        y+=xc[i-1];
        z+=yc[i-1];
    }
    //if(y<0)y*=-1;

    a=sqrt((z*z)+(y*y));
    dis.push_back(a);
    xc.push_back(y);
    yc.push_back(z);
}

int  main()
{
    int i,x,y;

	vector<double > r;
	vector<double > t;
	double a,b,c,B,h,area,res,rat;
	//cin>>t;
	//freopen("input.txt","r",stdin);
	while(scanf("%lf%lf",&n,&T)==2)
	{
		for(i=0;i<n;i++)
		{
		    cin>>x>>y;
		    r.push_back(x);
		    t.push_back(y);
		    if(i==0)cs.push_back(x);
		    else cs.push_back(cs[i-1]+x);
		}
		for(i=0;i<r.size();i++)find_distance(i,r[i],t[i]);

    for(i=0;i<dis.size();i++)
    {
        if(i>0)cout<<" ";
        printf("%.4lf",dis[i]);
    }
    r.clear();
    t.clear();
    dis.clear();
    cs.clear();
    xc.clear();
    yc.clear();
    cout<<endl;
/*
		//A = acos((b*b + c*c - a*a)/(2*b*c))*(180.0/pi);
		B = acos((a*a + c*c - b*b)/(2*a*c))*(180.0/pi);
		//C = acos((a*a + b*b - c*c)/(2*a*b))*(180.0/pi);
		h = c* sin((B*pi)/180);
		area = rat*(((0.5)*a*h)/(rat+1));

		res = sqrt((2*area*c)/(a*sin((B*pi)/180))) + 0.0000000001;


		printf("Case %d: %.10lf\n",i,res);*/
	}
	return 0;
}
