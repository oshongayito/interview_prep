#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <windows.h> 
#include<GL/glut.h>

#define BLACK 0, 0, 0
#define pi 2*acos((double)0.0)

using namespace std;

double cameraHeight;
double cameraAngle;
int drawgrid;
int drawaxes;
double angle;
GLuint texid1,texid2,texid3;
int num_texture = -1;

struct point
{
	double x,y,z;
};


void drawAxes()
{
	if(drawaxes==1)
	{
		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);{
			glVertex3f( 100,0,0);
			glVertex3f(-100,0,0);
			glVertex3f(0,-100,0);
			glVertex3f(0, 100,0);
			glVertex3f(0,0, 100);
			glVertex3f(0,0,-100);
		}glEnd();
	}
}


void drawGrid()
{
	int i;
	if(drawgrid==1)
	{
		glColor3f(0.6, 0.6, 0.6);	//grey
		//glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);{
			for(i=-8;i<=8;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -90, 0);
				glVertex3f(i*10,  90, 0);

				//lines parallel to X-axis
				glVertex3f(-90, i*10, 0);
				glVertex3f( 90, i*10, 0);

				//lines parallel to Z-axis
				/*
				glVertex3f(0, i*10, -90);
				glVertex3f(0, i*10, 90);
				*/
			}
		}glEnd();
	}
}


void drawSquare(float a)
{
	glBegin(GL_QUADS);{
		glVertex3f( a, a,2);
		glVertex3f( a,-a,2);
		glVertex3f(-a,-a,2);
		glVertex3f(-a, a,2);
	}glEnd();
}


void drawss()
{
	glPushMatrix();{
		glRotatef(angle,0,0,1);
		glTranslatef(75,0,0);
		glRotatef(2*angle,0,0,1);


		glPushMatrix();{
			glRotatef(angle,0,0,1);
			glTranslatef(25,0,0);
			glRotatef(3*angle,0,0,1);
			glColor3f(0.0, 0.0, 1.0);
			drawSquare(5);

		}glPopMatrix();



		glColor3f(1.0, 0.0, 0.0);
		drawSquare(10.0);
	}glPopMatrix();

}

//draws half sphere
void drawsphere(float radius,int slices,int stacks)
{
	struct point points[100][100];
	int i,j;
	double h,r;

	//radius = 40;
	//stacks=slices=40;

	double th,tr;
	double cos_val;
	h=(double) radius;
	int count= 0;//-(stacks/2);
	for(i=0;i<=stacks;i++)
	{
		h=radius*sin(((double)i/(double)stacks)*(pi/2));
		cos_val = cos(((double)i/(double)stacks)*(pi/2));
		//r=(radius-h)* cos_val * cos_val;
		r=(radius)* cos_val * cos_val;		

		for(j=0;j<=slices;j++)
		{
			points[i][j].x= r * cos( ((double)j/(double)slices) *2*pi);
			points[i][j].y= r * sin( ((double)j/(double)slices) *2*pi);
			points[i][j].z=count;//h;
		}
		count+=2 ;
		

	}

	for(i=0;i<stacks;i++)
	{
		for(j=0;j<slices;j++)
		{
			//glColor3f((double)i/(double)stacks,(double)i/(double)stacks,(double)i/(double)stacks);
			//if ( i%2 == 0 )
				glColor3f((double)i/(double)stacks,(double)i/(double)stacks,0.0);
				//glColor3f(1.0,1.0,1.0);

			glBegin(GL_QUADS);{
				glVertex3f(points[i][j].x,points[i][j].y,points[i][j].z);
				glVertex3f(points[i][j+1].x,points[i][j+1].y,points[i][j+1].z);
				glVertex3f(points[i+1][j+1].x,points[i+1][j+1].y,points[i+1][j+1].z);
				glVertex3f(points[i+1][j].x,points[i+1][j].y,points[i+1][j].z);

			}glEnd();
		}
	}	
}


//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
void drawTexture(GLuint texid,
				 GLfloat x1,GLfloat y1,GLfloat z1,
				 GLfloat x2,GLfloat y2,GLfloat z2,
				 GLfloat x3,GLfloat y3,GLfloat z3,
				 GLfloat x4,GLfloat y4,GLfloat z4
				 )
{
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); glVertex3f(x1,y1,z1);
			glTexCoord2f(1,0); glVertex3f(x2,y2,z2);
			glTexCoord2f(1,1); glVertex3f(x3,y3,z3);
			glTexCoord2f(0,1); glVertex3f(x4,y4,z4);
		glEnd();
		glDisable(GL_TEXTURE_2D);
}

int LoadBitmap(char *filename)
{
    int i, j=0;
    FILE *l_file;
    unsigned char *l_texture;

    BITMAPFILEHEADER fileheader;
    BITMAPINFOHEADER infoheader;
    RGBTRIPLE rgb;

    num_texture++;

    if( (l_file = fopen(filename, "rb"))==NULL) return (-1);

    fread(&fileheader, sizeof(fileheader), 1, l_file);

    fseek(l_file, sizeof(fileheader), SEEK_SET);
    fread(&infoheader, sizeof(infoheader), 1, l_file);

    l_texture = (byte *) malloc(infoheader.biWidth * infoheader.biHeight * 4);
    memset(l_texture, 0, infoheader.biWidth * infoheader.biHeight * 4);

 for (i=0; i < infoheader.biWidth*infoheader.biHeight; i++)
    {
            fread(&rgb, sizeof(rgb), 1, l_file);

            l_texture[j+0] = rgb.rgbtRed;
            l_texture[j+1] = rgb.rgbtGreen;
            l_texture[j+2] = rgb.rgbtBlue;
            l_texture[j+3] = 255;
            j += 4;
    }
    fclose(l_file);

    glBindTexture(GL_TEXTURE_2D, num_texture);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, infoheader.biWidth, infoheader.biHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);
     gluBuild2DMipmaps(GL_TEXTURE_2D, 4, infoheader.biWidth, infoheader.biHeight, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);

    free(l_texture);

    return (num_texture);

}

void drawInnerCylinder()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{					
			glRotatef(i*40,0,0,1);

			glColor3f(0.20, 0.60, 0.80);
			glBegin(GL_QUADS);{
				glVertex3f(0.1925,-4.6020,5.9534);
				glVertex3f(0.1925,-4.6020,34.2124);
				glVertex3f(1.6491,-4.0018,34.2124);				
				glVertex3f(1.6491,-4.0018,5.9534);
			}glEnd();
			glColor3f(0.80, 0.60, 0.20);
			glBegin(GL_QUADS);{
				glVertex3f(1.6491,-4.0018,5.9534);
				glVertex3f(1.6491,-4.0018,34.2124);
				glVertex3f(3.1056,-3.4016,34.2124);
				glVertex3f(3.1056,-3.4016,5.9534);
			}glEnd();
		}glPopMatrix();
	
	}
}
void drawPetal_top()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{					
			glRotatef(i*40,0,0,1);

			glColor3f(0.40, 0.50, 0.60);
			glBegin(GL_POLYGON);{
				glVertex3f(0.1925,-4.6020,34.2124);
				glVertex3f(0.0,0.0,36.3171);
				glVertex3f(3.1056,-3.4016,34.2124);				
			}glEnd();
		}glPopMatrix();
	
	}
}
void drawPetal_0()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{					
			glRotatef(i*40,0,0,1);

			glColor3f(0.10, 0.20, 0.30);
			glBegin(GL_QUADS);{
				glVertex3f(0.9033,-21.5959,5.9534);
				glVertex3f(0.1925,-4.602,34.2124);
				glVertex3f(3.1056,-3.4016,34.2124);
				glVertex3f(14.5736,-15.9628,5.9534);
			}glEnd();
		}glPopMatrix();
	
	}
}
void drawPetal_3_rightback()
{
	
	//triangles
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_TRIANGLES);{		
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-11.8783,25.4035,6.479);
		glVertex3f(-11.9547,25.5084,6.5799);		
	}glEnd();		
	
	
	glBegin(GL_TRIANGLES);{		
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-11.9547,25.5084,6.5799);		
		glVertex3f(-12.0369,25.6201,6.6678);		
	}glEnd();		

	glBegin(GL_TRIANGLES);{		
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.0369,25.6201,6.6678);		
		glVertex3f(-12.1243,25.7378,6.7421);		
	}glEnd();		
	
	glBegin(GL_TRIANGLES);{		
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.1243,25.7378,6.7421);		
		glVertex3f(-12.216,25.8603,6.8021);			
	}glEnd();		

	glBegin(GL_TRIANGLES);{		
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.216,25.8603,6.8021);			
		glVertex3f(-12.3111,25.9864,6.8471);			
	}glEnd();		

	glBegin(GL_TRIANGLES);{		
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.3111,25.9864,6.8471);			
		glVertex3f(-12.4088,26.1151,6.8768);			
	}glEnd();		

	// ------------------------------------col-1
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-11.8783,25.4035,6.479);
		glVertex3f(-12.105,24.63,6.9663);
		glVertex3f(-12.1836,24.7379,7.0706);
		glVertex3f(-11.9157,25.455,6.531);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.105,24.63,6.9663);
		glVertex3f(-12.3381,23.8349,7.414);
		glVertex3f(-12.4614,23.9948,7.571);
		glVertex3f(-12.1836,24.7379,7.0706);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-12.3381,23.8349,7.414);
		glVertex3f(-12.5769,23.0199,7.8212);
		glVertex3f(-12.7486,23.2365,8.0308);
		glVertex3f(-12.4614,23.9948,7.571);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.5769,23.0199,7.8212);
		glVertex3f(-12.821,22.187,8.1869);
		glVertex3f(-13.0442,22.4618,8.449);
		glVertex3f(-12.7486,23.2365,8.0308);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-12.821,22.187,8.1869);
		glVertex3f(-13.0699,21.3381,8.5102);
		glVertex3f(-13.3477,21.6725,8.8244);
		glVertex3f(-13.0442,22.4618,8.449);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.0699,21.3381,8.5102);
		glVertex3f(-13.3228,20.4752,8.7904);
		glVertex3f(-13.6582,20.8705,9.1562);
		glVertex3f(-13.3477,21.6725,8.8244);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-13.3228,20.4752,8.7904);
		glVertex3f(-13.5792,19.6003,9.0286);
		glVertex3f(-13.9749,20.0575,9.4435);
		glVertex3f(-13.6582,20.8705,9.1562);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.5792,19.6003,9.0286);
		glVertex3f(-13.8385,18.7155,9.219);
		glVertex3f(-14.297,19.2356,9.6856);
		glVertex3f(-13.9749,20.0575,9.4435);		
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-13.8385,18.7155,9.219);
		glVertex3f(-14.1001,17.8229,9.3663);
		glVertex3f(-14.6237,18.4066,9.8818);
		glVertex3f(-14.297,19.2356,9.6856);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.1001,17.8229,9.3663);
		glVertex3f(-14.3635,16.9244,9.4686);
		glVertex3f(-14.9541,17.5726,10.0317);
		glVertex3f(-14.6237,18.4066,9.8818);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-14.3635,16.9244,9.4686);
		glVertex3f(-14.6279,16.0222,9.5255);
		glVertex3f(-15.2873,16.7353,10.1349);
		glVertex3f(-14.9541,17.5726,10.0317);		
	}glEnd();		
	//+======================+++++++++	
	//--------------------------column: 2
	
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-11.9157,25.455,6.531);
		glVertex3f(-12.1836,24.7379,7.0706);
		glVertex3f(-12.35,24.9554,7.26);
		glVertex3f(-11.9951,25.5634,6.6255);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.1836,24.7379,7.0706);		
		glVertex3f(-12.4614,23.9948,7.571);
		glVertex3f(-12.7222,24.3309,7.855);
		glVertex3f(-12.1836,24.7379,7.0706);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.4614,23.9948,7.571);
		
		glVertex3f(-12.7486,23.2365,8.0308);
		glVertex3f(-13.1106,23.6914,8.4092);
		glVertex3f(-12.7222,24.3309,7.855);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.7486,23.2365,8.0308);
		
		glVertex3f(-13.0442,22.4618,8.449);
		glVertex3f(-13.5142,23.0383,8.9208);
		glVertex3f(-13.1106,23.6914,8.4092);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.0442,22.4618,8.449);
		
		glVertex3f(-13.3477,21.6725,8.8244);
		glVertex3f(-13.9317,22.3733,9.3886);
		glVertex3f(-13.5142,23.0383,8.9208);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.3477,21.6725,8.8244);
		
		glVertex3f(-13.6582,20.8705,9.1562);
		glVertex3f(-14.3621,21.6979,9.8113);
		glVertex3f(-13.9317,22.3733,9.3886);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.6582,20.8705,9.1562);
		
		glVertex3f(-13.9749,20.0575,9.4435);
		glVertex3f(-14.804,21.0137,10.1876);
		glVertex3f(-14.3621,21.6979,9.8113);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.9749,20.0575,9.4435);
		
		glVertex3f(-14.297,19.2356,9.6856);
		glVertex3f(-15.2563,20.3223,10.5166);
		glVertex3f(-14.804,21.0137,10.1876);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.297,19.2356,9.6856);
		
		glVertex3f(-14.6237,18.4066,9.8818);
		glVertex3f(-15.7176,19.6254,10.7973);
		glVertex3f(-15.2563,20.3223,10.5166);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.6237,18.4066,9.8818);
		
		glVertex3f(-14.9541,17.5726,10.0317);
		glVertex3f(-16.1865,18.9247,11.0289);
		glVertex3f(-15.7176,19.6254,10.7973);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.9541,17.5726,10.0317);
		
		glVertex3f(-15.2873,16.7353,10.1349);
		glVertex3f(-16.6618,18.2217,11.2107);
		glVertex3f(-16.1865,18.9247,11.0289);
		
	}glEnd();		
	//--------------------------column: 3
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-11.9951,25.5634,6.6255);
		glVertex3f(-12.35,24.9554,7.26);
		glVertex3f(-12.5275,25.1888,7.422);
		glVertex3f(-12.08,25.6783,6.7067);
		
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.35,24.9554,7.26);
		
		glVertex3f(-12.7222,24.3309,7.855);
		glVertex3f(-12.9995,24.6861,8.0971);
		glVertex3f(-12.5275,25.1888,7.422);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.7222,24.3309,7.855);
		
		glVertex3f(-13.1106,23.6914,8.4092);
		glVertex3f(-13.4948,24.1714,8.7302);
		glVertex3f(-12.9995,24.6861,8.0971);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.1106,23.6914,8.4092);
		
		glVertex3f(-13.5142,23.0383,8.9208);
		glVertex3f(-14.0118,23.646,9.3194);
		glVertex3f(-13.4948,24.1714,8.7302);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.5142,23.0383,8.9208);
		glVertex3f(-13.9317,22.3733,9.3886);
		glVertex3f(-14.549,23.1112,9.863);
		glVertex3f(-14.0118,23.646,9.3194);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.9317,22.3733,9.3886);
		glVertex3f(-14.3621,21.6979,9.8113);
		glVertex3f(-15.1046,22.5682,10.3594);
		glVertex3f(-14.549,23.1112,9.863);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.3621,21.6979,9.8113);
		glVertex3f(-14.804,21.0137,10.1876);
		glVertex3f(-15.6771,22.0183,10.8072);
		glVertex3f(-15.1046,22.5682,10.3594);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.804,21.0137,10.1876);
		glVertex3f(-15.2563,20.3223,10.5166);
		glVertex3f(-16.2647,21.4629,11.2051);
		glVertex3f(-15.6771,22.0183,10.8072);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.2563,20.3223,10.5166);
		glVertex3f(-15.7176,19.6254,10.7973);
		glVertex3f(-16.8656,20.9033,11.5518);
		glVertex3f(-16.2647,21.4629,11.2051);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.7176,19.6254,10.7973);
		glVertex3f(-16.1865,18.9247,11.0289);
		glVertex3f(-17.4779,20.3408,11.8464);
		glVertex3f(-16.8656,20.9033,11.5518);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-16.1865,18.9247,11.0289);
		glVertex3f(-16.6618,18.2217,11.2107);
		glVertex3f(-18.0998,19.7769,12.0879);
		glVertex3f(-17.4779,20.3408,11.8464);		
	}glEnd();		
	//--------------------------column: 4
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.08,25.6783,6.7067);
		glVertex3f(-12.5275,25.1888,7.422);
		glVertex3f(-12.7144,25.4327,7.5552);
		glVertex3f(-12.1696,25.7985,6.7739);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.5275,25.1888,7.422);
		glVertex3f(-12.9995,24.6861,8.0971);
		glVertex3f(-13.291,25.057,8.295);
		glVertex3f(-12.7144,25.4327,7.5552);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.9995,24.6861,8.0971);
		glVertex3f(-13.4948,24.1714,8.7302);
		glVertex3f(-13.8977,24.6722,8.9909);
		glVertex3f(-13.291,25.057,8.295);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.4948,24.1714,8.7302);
		glVertex3f(-14.0118,23.646,9.3194);
		glVertex3f(-14.5326,24.2794,9.641);
		glVertex3f(-13.8977,24.6722,8.9909);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.0118,23.646,9.3194);
		glVertex3f(-14.549,23.1112,9.863);
		glVertex3f(-15.1938,23.8795,10.2432);
		glVertex3f(-14.5326,24.2794,9.641);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.549,23.1112,9.863);
		glVertex3f(-15.1046,22.5682,10.3594);
		glVertex3f(-15.879,23.4734,10.7956);
		glVertex3f(-15.1938,23.8795,10.2432);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.1046,22.5682,10.3594);
		glVertex3f(-15.6771,22.0183,10.8072);
		glVertex3f(-16.5601,23.0771,11.2806);
		glVertex3f(-15.879,23.4734,10.7956);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.6771,22.0183,10.8072);
		glVertex3f(-16.2647,21.4629,11.2051);
		glVertex3f(-17.313,22.6469,11.7447);
		glVertex3f(-16.5601,23.0771,11.2806);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-16.2647,21.4629,11.2051);
		glVertex3f(-16.8656,20.9033,11.5518);
		glVertex3f(-18.0572,22.2285,12.1385);
		glVertex3f(-17.313,22.6469,11.7447);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.8656,20.9033,11.5518);
		glVertex3f(-17.4779,20.3408,11.8464);
		glVertex3f(-18.8164,21.808,12.4767);
		glVertex3f(-18.0572,22.2285,12.1385);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-17.4779,20.3408,11.8464);
		glVertex3f(-18.0998,19.7769,12.0879);
		glVertex3f(-19.5882,21.3865,12.7583);
		glVertex3f(-18.8164,21.808,12.4767);
	}glEnd();		
	
	//--------------------------column: 5
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.1696,25.7985,6.7739);
		glVertex3f(-12.7144,25.4327,7.5552);
		glVertex3f(-12.909,25.685,7.6585);
		glVertex3f(-12.2631,25.923,6.8265);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.7144,25.4327,7.5552);
		glVertex3f(-13.291,25.057,8.295);
		glVertex3f(-13.5939,25.4402,8.4468);
		glVertex3f(-12.909,25.685,7.6585);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.291,25.057,8.295);
		glVertex3f(-13.8977,24.6722,8.9909);
		glVertex3f(-14.3256,25.1892,9.189);
		glVertex3f(-13.5939,25.4402,8.4468);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.8977,24.6722,8.9909);
		glVertex3f(-14.5326,24.2794,9.641);
		glVertex3f(-15.0719,24.9326,9.8826);
		glVertex3f(-14.3256,25.1892,9.189);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.5326,24.2794,9.641);
		glVertex3f(-15.1938,23.8795,10.2432);
		glVertex3f(-15.8602,24.671,10.5256);
		glVertex3f(-15.0719,24.9326,9.8826);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.1938,23.8795,10.2432);
		glVertex3f(-15.879,23.4734,10.7956);
		glVertex3f(-16.6781,24.4052,11.1158);
		glVertex3f(-15.8602,24.671,10.5256);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.879,23.4734,10.7956);
		glVertex3f(-16.5601,23.0771,11.2806);
		glVertex3f(-17.5227,24.1358,11.6513);
		glVertex3f(-16.6781,24.4052,11.1158);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.5601,23.0771,11.2806);
		glVertex3f(-17.313,22.6469,11.7447);
		glVertex3f(-18.3915,23.8635,12.1306);
		glVertex3f(-17.5227,24.1358,11.6513);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-17.313,22.6469,11.7447);
		glVertex3f(-18.0572,22.2285,12.1385);
		glVertex3f(-19.2814,23.5889,12.5519);
		glVertex3f(-18.3915,23.8635,12.1306);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.0572,22.2285,12.1385);
		glVertex3f(-18.8164,21.808,12.4767);
		glVertex3f(-20.1879,23.3128,12.9141);
		glVertex3f(-19.2814,23.5889,12.5519);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-18.8164,21.808,12.4767);
		glVertex3f(-19.5882,21.3865,12.7583);
		glVertex3f(-21.1134,23.0358,13.2159);
		glVertex3f(-20.1879,23.3128,12.9141);
	}glEnd();		
	
	//--------------------------column: 6
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.2631,25.923,6.8265);
		glVertex3f(-12.909,25.685,7.6585);
		glVertex3f(-13.1095,25.9434,7.7308);
		glVertex3f(-12.2631,25.923,6.8265);
		
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.909,25.685,7.6585);
		glVertex3f(-13.5939,25.4402,8.4468);
		glVertex3f(-13.9053,25.8323,8.5512);
		glVertex3f(-13.1095,25.9434,7.7308);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.5939,25.4402,8.4468);
		glVertex3f(-14.3256,25.1892,9.189);
		glVertex3f(-14.7446,25.7176,9.3225);
		glVertex3f(-13.9053,25.8323,8.5512);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.3256,25.1892,9.189);
		glVertex3f(-15.0719,24.9326,9.8826);
		glVertex3f(-15.6246,25.5996,10.0421);
		glVertex3f(-14.7446,25.7176,9.3225);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.0719,24.9326,9.8826);
		glVertex3f(-15.8602,24.671,10.5256);
		glVertex3f(-16.5422,25.4786,10.7077);
		glVertex3f(-15.6246,25.5996,10.0421);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.8602,24.671,10.5256);
		glVertex3f(-16.6781,24.4052,11.1158);
		glVertex3f(-17.4945,25.355,11.317);
		glVertex3f(-16.5422,25.4786,10.7077);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-16.6781,24.4052,11.1158);
		glVertex3f(-17.5227,24.1358,11.6513);
		glVertex3f(-18.4783,25.2292,11.8681);
		glVertex3f(-17.4945,25.355,11.317);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-17.5227,24.1358,11.6513);
		glVertex3f(-18.3915,23.8635,12.1306);
		glVertex3f(-19.4903,25.1013,12.3591);
		glVertex3f(-18.4783,25.2292,11.8681);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-18.3915,23.8635,12.1306);
		glVertex3f(-19.2814,23.5889,12.5519);
		glVertex3f(-20.5271,24.9719,12.7884);
		glVertex3f(-19.4903,25.1013,12.3591);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.2814,23.5889,12.5519);
		glVertex3f(-20.1879,23.3128,12.9141);
		glVertex3f(-21.5853,24.8413,13.1545);
		glVertex3f(-20.5271,24.9719,12.7884);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-20.1879,23.3128,12.9141);
		glVertex3f(-21.1134,23.0358,13.2159);
		glVertex3f(-22.6613,24.7098,13.4564);
		glVertex3f(-21.5853,24.8413,13.1545);
	}glEnd();		
	
	//--------------------------column: 7
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.2631,25.923,6.8265);
		glVertex3f(-13.1095,25.9434,7.7308);
		glVertex3f(-13.2114,26.0741,7.7551);
		glVertex3f(-12.4088,26.1151,6.8768);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.1095,25.9434,7.7308);
		glVertex3f(-13.9053,25.8323,8.5512);
		glVertex3f(-14.0634,26.0305,8.5853);
		glVertex3f(-13.2114,26.0741,7.7551);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.9053,25.8323,8.5512);
		glVertex3f(-14.7446,25.7176,9.3225);
		glVertex3f(-14.9621,25.9846,9.3647);
		glVertex3f(-14.0634,26.0305,8.5853);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.7446,25.7176,9.3225);
		glVertex3f(-15.6246,25.5996,10.0421);
		glVertex3f(-15.9044,25.9364,10.0906);
		glVertex3f(-14.9621,25.9846,9.3647);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.6246,25.5996,10.0421);
		glVertex3f(-16.5422,25.4786,10.7077);
		glVertex3f(-16.8871,25.8861,10.7606);
		glVertex3f(-15.9044,25.9364,10.0906);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.5422,25.4786,10.7077);
		glVertex3f(-17.4945,25.355,11.317);
		glVertex3f(-17.9069,25.834,11.3724);
		glVertex3f(-16.8871,25.8861,10.7606);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-17.4945,25.355,11.317);
		glVertex3f(-18.4783,25.2292,11.8681);
		glVertex3f(-18.9604,25.7801,11.9241);
		glVertex3f(-17.9069,25.834,11.3724);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.4783,25.2292,11.8681);
		glVertex3f(-19.4903,25.1013,12.3591);
		glVertex3f(-20.0441,25.7247,12.4136);
		glVertex3f(-18.9604,25.7801,11.9241);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-19.4903,25.1013,12.3591);
		glVertex3f(-20.5271,24.9719,12.7884);
		glVertex3f(-21.1543,25.668,12.8394);
		glVertex3f(-20.0441,25.7247,12.4136);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.5271,24.9719,12.7884);
		glVertex3f(-21.5853,24.8413,13.1545);
		glVertex3f(-22.2873,25.61,13.2001);
		glVertex3f(-21.1543,25.668,12.8394);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-21.5853,24.8413,13.1545);
		glVertex3f(-22.6613,24.7098,13.4564);
		glVertex3f(-23.4393,25.5512,13.4945);
		glVertex3f(-22.2873,25.61,13.2001);
	}glEnd();		
	//--------------------------column: 8
}
void drawPetal_3_leftback()
{
	//triangles
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_TRIANGLES);{		
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.8633,14.5985,6.8768);
		glVertex3f(-24.7426,14.491,6.8471);		
	}glEnd();		
	
	glBegin(GL_TRIANGLES);{		
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.7426,14.491,6.8471);		
		glVertex3f(-24.6243,14.3863,6.8021);			
	}glEnd();		

	glBegin(GL_TRIANGLES);{		
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.6243,14.3863,6.8021);			
		glVertex3f(-24.5094,14.2853,6.7421);			
	}glEnd();		

	glBegin(GL_TRIANGLES);{		
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.5094,14.2853,6.7421);			
		glVertex3f(-24.3989,14.189,6.6678);	
	}glEnd();		

	glBegin(GL_TRIANGLES);{		
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.3989,14.189,6.6678);	
		glVertex3f(-24.2939,14.0983,6.5799);			
	}glEnd();		

	glBegin(GL_TRIANGLES);{		
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.2939,14.0983,6.5799);			
		glVertex3f(-24.1953,14.0139,6.479);			
	}glEnd();		
	

	//===================================================================================== col-1
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-24.8633,14.5985,6.8768);
		glVertex3f(-24.7597,15.3953,7.7551);
		glVertex3f(-24.5158,15.1728,7.6985);
		glVertex3f(-24.7426,14.491,6.8471);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.7597,15.3953,7.7551);
		glVertex3f(-24.6497,16.2414,8.5853);
		glVertex3f(-24.2803,15.8967,8.505);
		glVertex3f(-24.5158,15.1728,7.6985);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.6497,16.2414,8.5853);
		glVertex3f(-24.5337,17.1337,9.3647);
		glVertex3f(-24.0368,16.6603,9.2639);
		glVertex3f(-24.2803,15.8967,8.505);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.5337,17.1337,9.3647);
		glVertex3f(-24.412,18.0693,10.0906);
		glVertex3f(-23.786,17.4611,9.9728);
		glVertex3f(-24.0368,16.6603,9.2639);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.412,18.0693,10.0906);
		glVertex3f(-24.2852,19.0451,10.7606);
		glVertex3f(-23.5285,18.2963,10.6293);
		glVertex3f(-23.786,17.4611,9.9728);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.2852,19.0451,10.7606);
		glVertex3f(-24.1535,20.0577,11.3724);
		glVertex3f(-23.2651,19.1633,11.2314);
		glVertex3f(-23.5285,18.2963,10.6293);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.1535,20.0577,11.3724);
		glVertex3f(-24.0175,21.1031,11.9241);
		glVertex3f(-22.9965,20.0591,11.7771);
		glVertex3f(-23.2651,19.1633,11.2314);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.0175,21.1031,11.9241);
		glVertex3f(-23.8776,22.1799,12.4136);
		glVertex3f(-22.7234,20.9809,12.2647);
		glVertex3f(-22.9965,20.0591,11.7771);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.8776,22.1799,12.4136);
		glVertex3f(-23.7343,23.2823,12.8394);
		glVertex3f(-22.4467,21.9255,12.6925);
		glVertex3f(-22.7234,20.9809,12.2647);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.7343,23.2823,12.8394);
		glVertex3f(-23.588,24.4073,13.2001);
		glVertex3f(-22.167,22.8898,13.0591);
		glVertex3f(-22.4467,21.9255,12.6925);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.588,24.4073,13.2001);
		glVertex3f(-23.4393,25.5512,13.4945);
		glVertex3f(-21.8854,23.8707,13.3634);
		glVertex3f(-22.167,22.8898,13.0591);
	}glEnd();		
	//--------------------------------------------------------------------------------column: 2
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-24.7426,14.491,6.8471);
		glVertex3f(-24.5158,15.1728,7.6985);
		glVertex3f(-24.2764,14.9557,7.6107);
		glVertex3f(-24.6243,14.3863,6.8021);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.5158,15.1728,7.6985);
		glVertex3f(-24.2803,15.8967,8.505);
		glVertex3f(-23.9174,15.5598,8.3767);
		glVertex3f(-24.2764,14.9557,7.6107);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.2803,15.8967,8.505);
		glVertex3f(-24.0368,16.6603,9.2639);
		glVertex3f(-23.5483,16.1968,9.0979);
		glVertex3f(-23.9174,15.5598,8.3767);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.0368,16.6603,9.2639);
		glVertex3f(-23.786,17.4611,9.9728);
		glVertex3f(-23.1698,16.8645,9.772);
		glVertex3f(-23.5483,16.1968,9.0979);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.786,17.4611,9.9728);
		glVertex3f(-23.5285,18.2963,10.6293);
		glVertex3f(-22.7832,17.5608,10.3968);
		glVertex3f(-23.1698,16.8645,9.772);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.5285,18.2963,10.6293);
		glVertex3f(-23.2651,19.1633,11.2314);
		glVertex3f(-22.3892,18.2835,10.9704);
		glVertex3f(-22.7832,17.5608,10.3968);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.2651,19.1633,11.2314);
		glVertex3f(-22.9965,20.0591,11.7771);
		glVertex3f(-21.9891,19.0301,11.4911);
		glVertex3f(-22.3892,18.2835,10.9704);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.9965,20.0591,11.7771);
		glVertex3f(-22.7234,20.9809,12.2647);
		glVertex3f(-21.5837,19.7984,11.9571);
		glVertex3f(-21.9891,19.0301,11.4911);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.7234,20.9809,12.2647);
		glVertex3f(-22.4467,21.9255,12.6925);
		glVertex3f(-21.1742,20.5857,12.3671);
		glVertex3f(-21.5837,19.7984,11.9571);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.4467,21.9255,12.6925);
		glVertex3f(-22.167,22.8898,13.0591);
		glVertex3f(-20.7617,21.3895,12.7198);
		glVertex3f(-21.1742,20.5857,12.3671);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.167,22.8898,13.0591);
		glVertex3f(-21.8854,23.8707,13.3634);
		glVertex3f(-20.3471,22.2071,13.014);
		glVertex3f(-20.7617,21.3895,12.7198);
	}glEnd();		
	//--------------------------------------------------------------------------------column: 3
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-24.6243,14.3863,6.8021);
		glVertex3f(-24.2764,14.9557,7.6107);
		glVertex3f(-24.0437,14.7459,7.4923);
		glVertex3f(-24.5094,14.2853,6.7421);
		
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.2764,14.9557,7.6107);
		glVertex3f(-23.9174,15.5598,8.3767);
		glVertex3f(-23.5643,15.2337,8.2017);
		glVertex3f(-24.0437,14.7459,7.4923);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.9174,15.5598,8.3767);
		glVertex3f(-23.5483,16.1968,9.0979);
		glVertex3f(-23.0724,15.7474,8.8683);
		glVertex3f(-23.5643,15.2337,8.2017);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.5483,16.1968,9.0979);
		glVertex3f(-23.1698,16.8645,9.772);
		glVertex3f(-22.5693,16.2852,9.49);
		glVertex3f(-23.0724,15.7474,8.8683);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.1698,16.8645,9.772);
		glVertex3f(-22.7832,17.5608,10.3968);
		glVertex3f(-22.056,16.8454,10.0651);
		glVertex3f(-22.5693,16.2852,9.49);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.7832,17.5608,10.3968);
		glVertex3f(-22.3892,18.2835,10.9704);
		glVertex3f(-21.534,17.4264,10.5918);
		glVertex3f(-22.056,16.8454,10.0651);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.3892,18.2835,10.9704);
		glVertex3f(-21.9891,19.0301,11.4911);
		glVertex3f(-21.0046,18.0263,11.0685);
		glVertex3f(-21.534,17.4264,10.5918);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.9891,19.0301,11.4911);
		glVertex3f(-21.5837,19.7984,11.9571);
		glVertex3f(-20.4689,18.6431,11.4939);
		glVertex3f(-21.0046,18.0263,11.0685);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.5837,19.7984,11.9571);
		glVertex3f(-21.1742,20.5857,12.3671);
		glVertex3f(-19.9285,19.275,11.8665);
		glVertex3f(-20.4689,18.6431,11.4939);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.1742,20.5857,12.3671);
		glVertex3f(-20.7617,21.3895,12.7198);
		glVertex3f(-19.3846,19.9199,12.1853);
		glVertex3f(-19.9285,19.275,11.8665);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.7617,21.3895,12.7198);
		glVertex3f(-20.3471,22.2071,13.014);
		glVertex3f(-18.8386,20.5758,12.4493);
		glVertex3f(-19.3846,19.9199,12.1853);
	}glEnd();		
	
	//--------------------------------------------------------------------------------column: 4
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.5094,14.2853,6.7421);
		glVertex3f(-24.0437,14.7459,7.4923);
		glVertex3f(-23.8198,14.5454,7.3445);
		glVertex3f(-24.3989,14.189,6.6678);		
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.0437,14.7459,7.4923);
		glVertex3f(-23.5643,15.2337,8.2017);
		glVertex3f(-23.2243,14.9215,7.9815);
		glVertex3f(-23.8198,14.5454,7.3445);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.5643,15.2337,8.2017);
		glVertex3f(-23.0724,15.7474,8.8683);
		glVertex3f(-22.6138,15.3162,8.5771);
		glVertex3f(-23.2243,14.9215,7.9815);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.0724,15.7474,8.8683);
		glVertex3f(-22.5693,16.2852,9.49);
		glVertex3f(-21.9897,15.7284,9.1295);
		glVertex3f(-22.6138,15.3162,8.5771);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.5693,16.2852,9.49);
		glVertex3f(-22.056,16.8454,10.0651);
		glVertex3f(-21.3537,16.1567,9.6374);
		glVertex3f(-21.9897,15.7284,9.1295);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.056,16.8454,10.0651);
		glVertex3f(-21.534,17.4264,10.5918);
		glVertex3f(-20.7073,16.6,10.0991);
		glVertex3f(-21.3537,16.1567,9.6374);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.534,17.4264,10.5918);
		glVertex3f(-21.0046,18.0263,11.0685);
		glVertex3f(-20.0519,17.0568,10.5134);
		glVertex3f(-20.7073,16.6,10.0991);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.0046,18.0263,11.0685);
		glVertex3f(-20.4689,18.6431,11.4939);
		glVertex3f(-19.3893,17.5259,10.8791);
		glVertex3f(-20.0519,17.0568,10.5134);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.4689,18.6431,11.4939);
		glVertex3f(-19.9285,19.275,11.8665);
		glVertex3f(-18.7209,18.0056,11.1951);
		glVertex3f(-19.3893,17.5259,10.8791);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.9285,19.275,11.8665);
		glVertex3f(-19.3846,19.9199,12.1853);
		glVertex3f(-18.0485,18.4947,11.4606);
		glVertex3f(-18.7209,18.0056,11.1951);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.3846,19.9199,12.1853);
		glVertex3f(-18.8386,20.5758,12.4493);
		glVertex3f(-17.3737,18.9916,11.6747);
		glVertex3f(-18.0485,18.4947,11.4606);
	}glEnd();		
	//--------------------------------------------------------------------------------column: 5
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.3989,14.189,6.6678);		
		glVertex3f(-23.8198,14.5454,7.3445);
		glVertex3f(-23.6067,14.356,7.1686);		
		glVertex3f(-24.2939,14.0983,6.5799);
		
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.8198,14.5454,7.3445);
		glVertex3f(-23.2243,14.9215,7.9815);
		glVertex3f(-22.9003,14.6259,7.7181);
		glVertex3f(-23.6067,14.356,7.1686);	
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.2243,14.9215,7.9815);
		glVertex3f(-22.6138,15.3162,8.5771);
		glVertex3f(-22.1764,14.9072,8.227);
		glVertex3f(-22.9003,14.6259,7.7181);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.6138,15.3162,8.5771);
		glVertex3f(-21.9897,15.7284,9.1295);
		glVertex3f(-21.4366,15.1992,8.6938);
		glVertex3f(-22.1764,14.9072,8.227);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.9897,15.7284,9.1295);
		glVertex3f(-21.3537,16.1567,9.6374);
		glVertex3f(-20.6828,15.501,9.1175);
		glVertex3f(-21.4366,15.1992,8.6938);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.3537,16.1567,9.6374);
		glVertex3f(-20.7073,16.6,10.0991);
		glVertex3f(-19.9166,15.8118,9.4986);
		glVertex3f(-20.6828,15.501,9.1175);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.7073,16.6,10.0991);
		glVertex3f(-20.0519,17.0568,10.5134);
		glVertex3f(-19.14,16.1307,9.8307);
		glVertex3f(-19.9166,15.8118,9.4986);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.0519,17.0568,10.5134);
		glVertex3f(-19.3893,17.5259,10.8791);
		glVertex3f(-18.3547,16.4568,10.1185);
		glVertex3f(-19.14,16.1307,9.8307);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.3893,17.5259,10.8791);
		glVertex3f(-18.7209,18.0056,11.1951);
		glVertex3f(-17.5627,16.7892,10.3592);
		glVertex3f(-18.3547,16.4568,10.1185);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.7209,18.0056,11.1951);
		glVertex3f(-18.0485,18.4947,11.4606);
		glVertex3f(-16.7657,17.1269,10.5522);
		glVertex3f(-17.5627,16.7892,10.3592);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.0485,18.4947,11.4606);
		glVertex3f(-17.3737,18.9916,11.6747);
		glVertex3f(-15.9658,17.4691,10.6971);
		glVertex3f(-16.7657,17.1269,10.5522);
	}glEnd();		
	//--------------------------------------------------------------------------------column: 6
	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.2939,14.0983,6.5799);
		glVertex3f(-23.6067,14.356,7.1686);	
		glVertex3f(-23.4065,14.1795,6.9663);
		glVertex3f(-24.1953,14.0139,6.479);	
		
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.6067,14.356,7.1686);	
		glVertex3f(-22.9003,14.6259,7.7181);
		glVertex3f(-22.5955,14.3498,7.414);
		glVertex3f(-23.4065,14.1795,6.9663);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.9003,14.6259,7.7181);
		glVertex3f(-22.1764,14.9072,8.227);
		glVertex3f(-21.7644,14.5242,7.8212);
		glVertex3f(-22.5955,14.3498,7.414);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.1764,14.9072,8.227);
		glVertex3f(-21.4366,15.1992,8.6938);
		glVertex3f(-20.915,14.7025,8.1869);
		glVertex3f(-21.7644,14.5242,7.8212);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.4366,15.1992,8.6938);
		glVertex3f(-20.6828,15.501,9.1175);
		glVertex3f(-20.0492,14.8842,8.5102);
		glVertex3f(-20.915,14.7025,8.1869);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.6828,15.501,9.1175);
		glVertex3f(-19.9166,15.8118,9.4986);
		glVertex3f(-19.1692,15.069,8.7904);
		glVertex3f(-20.0492,14.8842,8.5102);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.9166,15.8118,9.4986);
		glVertex3f(-19.14,16.1307,9.8307);
		glVertex3f(-18.277,15.2563,9.0268);
		glVertex3f(-19.1692,15.069,8.7904);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.14,16.1307,9.8307);
		glVertex3f(-18.3547,16.4568,10.1185);
		glVertex3f(-17.3746,15.4457,9.219);
		glVertex3f(-18.277,15.2563,9.0268);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.3547,16.4568,10.1185);
		glVertex3f(-17.5627,16.7892,10.3592);
		glVertex3f(-16.4643,15.6368,9.3663);
		glVertex3f(-17.3746,15.4457,9.219);
	}glEnd();		
	
	glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-17.5627,16.7892,10.3592);
		glVertex3f(-16.7657,17.1269,10.5522);
		glVertex3f(-15.548,15.8291,9.4686);
		glVertex3f(-16.4643,15.6368,9.3663);
	}glEnd();

	glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.7657,17.1269,10.5522);
		glVertex3f(-15.9658,17.4691,10.6971);
		glVertex3f(-14.6279,16.0222,9.5255);
		glVertex3f(-15.548,15.8291,9.4686);
	}glEnd();		
	//--------------------------------------------------------------------------------column: 7
	
}
void drawPetal_3_leftback_side()
{
	
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_QUADS);{
		
		glVertex3f(-24.8968,14.0059,5.8376);
		glVertex3f(-24.7721,14.7273,6.7564);
		glVertex3f(-24.8633,14.5985,6.8768);
		glVertex3f(-24.9601,13.8534,5.9534);

	}glEnd();		

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.7721,14.7273,6.7564);
		glVertex3f(-24.6719,15.5182,7.6262);
		glVertex3f(-24.7597,15.3953,7.7551);
		glVertex3f(-24.8633,14.5985,6.8768);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.6719,15.5182,7.6262);
		glVertex3f(-24.5655,16.3577,8.4483);
		glVertex3f(-24.6497,16.2414,8.5853);
		glVertex3f(-24.7597,15.3953,7.7551);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.5655,16.3577,8.4483);
		glVertex3f(-24.4533,17.2431,9.2198);
		glVertex3f(-24.5337,17.1337,9.3647);
		glVertex3f(-24.6497,16.2414,8.5853);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.4533,17.2431,9.2198);
		glVertex3f(-24.3359,18.1713,9.9382);
		glVertex3f(-24.412,18.0693,10.0906);
		glVertex3f(-24.5337,17.1337,9.3647);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.3359,18.1713,9.9382);
		glVertex3f(-24.2137,19.1393,10.6011);
		glVertex3f(-24.2852,19.0451,10.7606);
		glVertex3f(-24.412,18.0693,10.0906);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.2137,19.1393,10.6011);
		glVertex3f(-24.0872,20.1438,11.2063);
		glVertex3f(-24.1535,20.0577,11.3724);
		glVertex3f(-24.2852,19.0451,10.7606);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.0872,20.1438,11.2063);
		glVertex3f(-23.958,21.1813,11.7512);
		glVertex3f(-24.0175,21.1031,11.9241);
		glVertex3f(-24.1535,20.0577,11.3724);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.958,21.1813,11.7512);
		glVertex3f(-23.8228,22.25,12.2362);
		glVertex3f(-23.8776,22.1799,12.4136);
		glVertex3f(-24.0175,21.1031,11.9241);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.8228,22.25,12.2362);
		glVertex3f(-23.6859,23.3431,12.6568);
		glVertex3f(-23.7343,23.2823,12.8394);
		glVertex3f(-23.8776,22.1799,12.4136);

	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
				glVertex3f(-23.6859,23.3431,12.6568);
		glVertex3f(-23.5464,24.4586,13.013);
		glVertex3f(-23.588,24.4073,13.2001);
		glVertex3f(-23.7343,23.2823,12.8394);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.5464,24.4586,13.013);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-23.4393,25.5512,13.4945);
		glVertex3f(-23.588,24.4073,13.2001);
	}glEnd();		
}
void drawPetal_3_leftface()
{
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_QUADS);{		
		glVertex3f(-14.6107,16.0808,9.4607);
		glVertex3f(-15.3754,16.8306,9.9671);
		glVertex3f(-24.8968,14.0059,5.8376);
		glVertex3f(-24.7721,14.7273,6.7564);
	}glEnd();		

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.3754,16.8306,9.9671);
		glVertex3f(-15.9954,17.501,10.4806);
		glVertex3f(-24.6719,15.5182,7.6262);
		glVertex3f(-24.8968,14.0059,5.8376);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);

	glBegin(GL_QUADS);{
		glVertex3f(-15.9954,17.501,10.4806);
		glVertex3f(-16.6829,18.2446,10.9933);
		glVertex3f(-24.5655,16.3577,8.4483);
		glVertex3f(-24.6719,15.5182,7.6262);

		
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.6829,18.2446,10.9933);
		glVertex3f(-17.3864,19.0053,11.4565);
		glVertex3f(-24.4533,17.2431,9.2198);
		glVertex3f(-24.5655,16.3577,8.4483);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-17.3864,19.0053,11.4565);
		glVertex3f(-18.1507,19.8308,11.8961);
		glVertex3f(-24.3359,18.1713,9.9382);
		glVertex3f(-24.4533,17.2431,9.2198);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.1507,19.8308,11.8961);
		glVertex3f(-18.8992,20.6085,12.2483);
		glVertex3f(-24.2137,19.1393,10.6011);
		glVertex3f(-24.3359,18.1713,9.9382);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.8992,20.6085,12.2483);
		glVertex3f(-19.6242,21.4254,12.5598);
		glVertex3f(-24.0872,20.1438,11.2063);
		glVertex3f(-24.2137,19.1393,10.6011);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.6242,21.4254,12.5598);
		glVertex3f(-20.3867,22.23,12.8092);
		glVertex3f(-23.958,21.1813,11.7512);
		glVertex3f(-24.0872,20.1438,11.2063);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.3867,22.23,12.8092);
		glVertex3f(-21.1359,23.0583,13.0124);
		glVertex3f(-23.8228,22.25,12.2362);
		glVertex3f(-23.958,21.1813,11.7512);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.1359,23.0583,13.0124);
		glVertex3f(-21.8784,23.8631,13.1548);
		glVertex3f(-23.6859,23.3431,12.6568);
		glVertex3f(-23.8228,22.25,12.2362);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.8784,23.8631,13.1548);
		glVertex3f(-22.5968,24.64,13.2407);
		glVertex3f(-23.5464,24.4586,13.013);
		glVertex3f(-23.6859,23.3431,12.6568);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.5968,24.64,13.2407);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-23.5464,24.4586,13.013);
	}glEnd();		
}
void drawPetal_3_rightback_side()
{
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.4088,26.1151,6.8768);
		glVertex3f(-11.8155,26.1022,5.8376);
		glVertex3f(-12.5445,26.0343,6.7564);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.4088,26.1151,6.8768);
		glVertex3f(-13.2114,26.0741,7.7551);
		glVertex3f(-13.3408,25.9962,7.6262);
		glVertex3f(-11.8155,26.1022,5.8376);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.2114,26.0741,7.7551);
		glVertex3f(-14.0634,26.0305,8.5853);
		glVertex3f(-14.186,25.9557,8.4483);
		glVertex3f(-13.3408,25.9962,7.6262);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.0634,26.0305,8.5853);
		glVertex3f(-14.9621,25.9846,9.3647);
		glVertex3f(-15.0774,25.913,9.2198);
		glVertex3f(-14.186,25.9557,8.4483);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.9621,25.9846,9.3647);
		glVertex3f(-15.9044,25.9364,10.0906);
		glVertex3f(-16.012,25.8685,9.9382);
		glVertex3f(-15.0774,25.913,9.2198);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.9044,25.9364,10.0906);
		glVertex3f(-16.8871,25.8861,10.7606);
		glVertex3f(-16.9866,25.8223,10.6011);
		glVertex3f(-16.012,25.8685,9.9382);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.8871,25.8861,10.7606);
		glVertex3f(-17.9069,25.834,11.3724);
		glVertex3f(-17.9979,25.7746,11.2063);
		glVertex3f(-16.9866,25.8223,10.6011);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-17.9069,25.834,11.3724);
		glVertex3f(-18.9604,25.7801,11.9241);
		glVertex3f(-19.0423,25.7268,11.7512);
		glVertex3f(-17.9979,25.7746,11.2063);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.9604,25.7801,11.9241);
		glVertex3f(-20.0441,25.7247,12.4136);
		glVertex3f(-20.1182,25.6756,12.2362);
		glVertex3f(-19.0423,25.7268,11.7512);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.0441,25.7247,12.4136);
		glVertex3f(-21.1543,25.668,12.8394);
		glVertex3f(-21.2188,25.6245,12.6568);
		glVertex3f(-20.1182,25.6756,12.2362);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.1543,25.668,12.8394);
		glVertex3f(-22.2873,25.61,13.2001);
		glVertex3f(-22.3417,25.5726,13.013);
		glVertex3f(-21.2188,25.6245,12.6568);
		
	}glEnd();		

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.2873,25.61,13.2001);
		glVertex3f(-23.4393,25.5512,13.4945);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-22.3417,25.5726,13.013);
		//2->first, 3->last & before 3 ->2
		
	}glEnd();		
}
void drawPetal_3_rightface()
{
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-11.8155,26.1022,5.8376);
		glVertex3f(-12.5445,26.0343,6.7564);
		glVertex3f(-14.6107,16.0808,9.4607);
		glVertex3f(-15.3754,16.8306,9.9671);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.5445,26.0343,6.7564);
		glVertex3f(-13.3408,25.9962,7.6262);
		glVertex3f(-15.9954,17.501,10.4806);
		glVertex3f(-14.6107,16.0808,9.4607);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.3408,25.9962,7.6262);
		glVertex3f(-14.186,25.9557,8.4483);
		glVertex3f(-16.6829,18.2446,10.9933);
		glVertex3f(-15.9954,17.501,10.4806);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.186,25.9557,8.4483);
		glVertex3f(-15.0774,25.913,9.2198);
		glVertex3f(-17.3864,19.0053,11.4565);
		glVertex3f(-16.6829,18.2446,10.9933);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.0774,25.913,9.2198);
		glVertex3f(-16.012,25.8685,9.9382);
		glVertex3f(-18.1507,19.8308,11.8961);
		glVertex3f(-17.3864,19.0053,11.4565);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.012,25.8685,9.9382);
		glVertex3f(-16.9866,25.8223,10.6011);
		glVertex3f(-18.8992,20.6085,12.2483);
		glVertex3f(-18.1507,19.8308,11.8961);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.9866,25.8223,10.6011);
		glVertex3f(-17.9979,25.7746,11.2063);
		glVertex3f(-19.6242,21.4254,12.5598);
		glVertex3f(-18.8992,20.6085,12.2483);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-17.9979,25.7746,11.2063);
		glVertex3f(-19.0423,25.7268,11.7512);
		glVertex3f(-20.3867,22.23,12.8092);
		glVertex3f(-19.6242,21.4254,12.5598);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.0423,25.7268,11.7512);
		glVertex3f(-20.1182,25.6756,12.2362);
		glVertex3f(-21.1359,23.0583,13.0124);
		glVertex3f(-20.3867,22.23,12.8092);
	}glEnd();		
	
	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.1182,25.6756,12.2362);
		glVertex3f(-21.2188,25.6245,12.6568);
		glVertex3f(-21.8784,23.8631,13.1548);
		glVertex3f(-21.1359,23.0583,13.0124);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.2188,25.6245,12.6568);
		glVertex3f(-22.3417,25.5726,13.013);
		glVertex3f(-22.5968,24.64,13.2407);
		glVertex3f(-21.8784,23.8631,13.1548);		
	}glEnd();		

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.3417,25.5726,13.013);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-22.5968,24.64,13.2407);
	}glEnd();		
}

void drawPetal_3()
{
	for(int i=0; i<9; i++)
	{		
		glPushMatrix();{					
			glRotatef(i*40,0,0,1);			
			drawPetal_3_rightback();
			drawPetal_3_leftback();
			drawPetal_3_leftback_side();
			drawPetal_3_leftface();
			drawPetal_3_rightback_side();
			drawPetal_3_rightface();
		}glPopMatrix();	
	}
}
void drawPetal_2_rightface()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{					
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// right part of a petal face
						
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8174,17.4753,5.9536);
				glVertex3f(12.8459,17.4527,6.8079);
				glVertex3f(13.2772,17.3175,6.8079);
				glVertex3f(13.2846,17.3378,5.9536);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8459,17.4527,6.8079);
				glVertex3f(12.8121,17.4430,7.975);
				glVertex3f(13.267,17.2897,7.975);
				glVertex3f(13.2772,17.3175,6.8079);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8121,17.4430,7.975);
				glVertex3f(12.7393,17.4329,8.7476);
				glVertex3f(13.2603,17.2713,8.7476);
				glVertex3f(13.267,17.2897,7.975);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.7393,17.4329,8.7476);
				glVertex3f(12.7598,17.4101,9.2870);
				glVertex3f(13.2557,17.2585,9.2870);
				glVertex3f(13.2603,17.2713,8.7476);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.7598,17.4101,9.2870);
				glVertex3f(12.6838,17.3988,9.8639);
				glVertex3f(13.2365,17.2403,9.7977);
				glVertex3f(13.2557,17.2585,9.2870);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.6838,17.3988,9.8639);
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(13.2049,17.2199,10.2509);
				glVertex3f(13.2365,17.2403,9.7977);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(13.1569,17.1888,10.9394);
				glVertex3f(13.2049,17.2199,10.2509);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(12.3406,17.3526,11.8870);
				glVertex3f(13.0403,17.1418,11.9161);
				glVertex3f(13.1569,17.1888,10.9394);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.3406,17.3526,11.8870);
				glVertex3f(12.1210,17.3297,12.8261);
				glVertex3f(12.9117,17.0959,12.8444);
				glVertex3f(13.0403,17.1418,11.9161);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.1210,17.3297,12.8261);
				glVertex3f(11.8872,17.3103,13.6246);
				glVertex3f(12.7962,17.0680,13.4018);
				glVertex3f(12.9117,17.0959,12.8444);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.8872,17.3103,13.6246);
				glVertex3f(11.6185,17.2905,14.4584);
				glVertex3f(12.5751,17.0101,14.4584);
				glVertex3f(12.7962,17.0680,13.4018);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.6185,17.2905,14.4584);
				glVertex3f(11.3391,17.2732,15.1985);
				glVertex3f(12.3584,16.9667,15.2306);
				glVertex3f(12.5751,17.0101,14.4584);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.3391,17.2732,15.1985);
				glVertex3f(11.033,17.2544,15.9525);
				glVertex3f(12.13,16.9209,16.0443);
				glVertex3f(12.3584,16.9667,15.2306);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.033,17.2544,15.9525);
				glVertex3f(10.725,17.2445,16.6159);
				glVertex3f(11.9247,16.8907,16.6267);
				glVertex3f(12.13,16.9209,16.0443);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.725,17.2445,16.6159);
				glVertex3f(10.4384,17.2282,17.2260);
				glVertex3f(11.7518,16.8605,17.1108);
				glVertex3f(11.9247,16.8907,16.6267);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.4384,17.2282,17.2260);
				glVertex3f(10.2728,17.2260,17.5224);
				glVertex3f(11.5788,16.8303,17.5949);
				glVertex3f(11.7518,16.8605,17.1108);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.2728,17.2260,17.5224);
				glVertex3f(9.9836,17.2170,18.0356);
				glVertex3f(11.3529,16.7995,18.1157);
				glVertex3f(11.5788,16.8303,17.5949);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.9836,17.2170,18.0356);
				glVertex3f(9.6249,17.2059,18.6719);
				glVertex3f(11.1117,16.7666,18.6719);
				glVertex3f(11.3529,16.7995,18.1157);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.6249,17.2059,18.6719);
				glVertex3f(9.1190,17.2273,19.3169);
				glVertex3f(10.9246,16.7410,19.1031);
				glVertex3f(11.1117,16.7666,18.6719);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.1190,17.2273,19.3169);
				glVertex3f(8.8102,17.1978,19.8957);
				glVertex3f(10.5571,16.6995,19.8147);
				glVertex3f(10.9246,16.7410,19.1031);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.8102,17.1978,19.8957);
				glVertex3f(8.4037,17.1981,20.4395);
				glVertex3f(10.2850,16.6702,20.343);
				glVertex3f(10.5571,16.6995,19.8147);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4037,17.1981,20.4395);
				glVertex3f(8.3079,17.1987,20.5624);
				glVertex3f(10.1709,16.6558,20.5624);
				glVertex3f(10.2850,16.6702,20.343);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.3079,17.1987,20.5624);
				glVertex3f(7.9858,17.2008,20.9756);
				glVertex3f(9.8755,16.6287,21.0507);
				glVertex3f(10.1709,16.6558,20.5624);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9858,17.2008,20.9756);
				glVertex3f(7.688,17.2013,21.3566);
				glVertex3f(9.5838,16.6019,21.533);
				glVertex3f(9.8755,16.6287,21.0507);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.688,17.2013,21.3566);
				glVertex3f(7.1801,17.2254,21.8695);
				glVertex3f(9.3215,16.5778,21.9667);
				glVertex3f(9.5838,16.6019,21.533);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1801,17.2254,21.8695);
				glVertex3f(6.6392,17.2254,22.5136);
				glVertex3f(8.9177,16.5488,22.5437);
				glVertex3f(9.3215,16.5778,21.9667);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.6392,17.2254,22.5136);
				glVertex3f(6.2435,17.2407,22.9001);
				glVertex3f(8.6841,16.5320,22.8774);
				glVertex3f(8.9177,16.5488,22.5437);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.2435,17.2407,22.9001);
				glVertex3f(5.9082,17.2548,23.2197);
				glVertex3f(8.5380,16.5215,23.0861);
				glVertex3f(8.6841,16.5320,22.8774);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.9082,17.2548,23.2197);
				glVertex3f(5.3810,17.2755,23.7211);
				glVertex3f(7.8324,16.4827,23.9940);
				glVertex3f(8.5380,16.5215,23.0861);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3810,17.2755,23.7211);
				glVertex3f(5.3383,17.1581,24.1537);
				glVertex3f(7.5229,16.4659,24.3786);
				glVertex3f(7.8324,16.4827,23.9940);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3383,17.1581,24.1537);
				glVertex3f(5.2503,16.9163,25.0453);
				glVertex3f(6.7827,16.4441,25.1717);
				glVertex3f(7.5229,16.4659,24.3786);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2503,16.9163,25.0453);
				glVertex3f(5.1994,16.7765,25.5605);
				glVertex3f(6.2472,16.4209,25.7944);
				glVertex3f(6.7827,16.4441,25.1717);
			}glEnd();
			
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.1994,16.7765,25.5605);
				glVertex3f(5.1348,16.599,26.2148);
				glVertex3f(5.7332,16.4209,26.2434);
				glVertex3f(6.2472,16.4209,25.7944);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.1348,16.599,26.2148);
				glVertex3f(5.1025,16.5103,26.5420);
				glVertex3f(5.381,16.4131,26.6131);
				glVertex3f(5.7332,16.4209,26.2434);
			}glEnd();
			
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(5.1025,16.5103,26.5420);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.381,16.4131,26.6131);
			}glEnd();		
			
			//------------------- face of a petal end

		}glPopMatrix();	
	}
}
void drawPetal_2_leftface()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{					
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left part of a petal face
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9067,21.6765,5.9534);
				glVertex3f(-0.9177,21.6462,7.2216);
				glVertex3f(-0.4616,21.5103,7.2216);
				glVertex3f(-0.4435,21.5295,5.9534);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9177,21.6462,7.2216);
				glVertex3f(-0.9375,21.5919,9.4936);
				glVertex3f(-0.3556,21.4027,9.5259);
				glVertex3f(-0.4616,21.5103,7.2216);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9375,21.5919,9.4936);
				glVertex3f(-0.8943,21.4690,11.1525);
				glVertex3f(-0.159,21.2391,11.0836);
				glVertex3f(-0.3556,21.4027,9.5259);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.8943,21.4690,11.1525);
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.0361,21.0767,12.5058);
				glVertex3f(-0.159,21.2391,11.0836);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(0.3133,20.7896,14.0929);
				glVertex3f(-0.0361,21.0767,12.5058);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(0.76,20.4386,15.6422);
				glVertex3f(0.3133,20.7896,14.0929);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(1.4338,19.9352,17.4426);
				glVertex3f(0.76,20.4386,15.6422);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(2.4082,19.2361,19.4869);
				glVertex3f(1.4338,19.9352,17.4426);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(3.3408,18.6234,20.9393);
				glVertex3f(2.4082,19.2361,19.4869);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(4.3924,17.8982,22.5534);
				glVertex3f(3.3408,18.6234,20.9393);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(5.381,17.2755,23.7211);
				glVertex3f(4.3924,17.8982,22.5534);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(5.3013,17.0566,24.5281);
				glVertex3f(5.381,17.2755,23.7211);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(4.129,17.0735,25.7615);
				glVertex3f(5.1796,16.722,25.7615);
				glVertex3f(5.3013,17.0566,24.5281);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(4.129,17.0735,25.7615);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.1796,16.722,25.7615);
			}glEnd();
			//------------------- face of a petal end

		}glPopMatrix();	
	}
}
void drawPetal_2_rightback()
{
	for(int i=0; i<1; i++)
	{
		glPushMatrix();{					
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left back part of a petal face

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.7677,27.9475,8.183);
				glVertex3f(8.5208,27.9148,7.8667);				
			}glEnd();		

			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.5208,27.9148,7.8667);
				glVertex3f(8.3098,27.8663,7.5216);				
			}glEnd();		

			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.3098,27.8663,7.5216);				
				glVertex3f(8.1379,27.8026,7.1532);				
			}glEnd();		

			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.1379,27.8026,7.1532);				
				glVertex3f(8.0078,27.7244,6.7674);				
			}glEnd();		

			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.0078,27.7244,6.7674);				
				glVertex3f(7.9215,27.6327,6.3704);				
			}glEnd();		

			
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(7.9215,27.6327,6.3704);				
				glVertex3f(7.8803,27.5289,5.9686);//last				
			}glEnd();		
			// -------------------------------------------------------------------------------col-1
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.7677,27.9475,8.1830);
				glVertex3f(8.6470,27.5694,10.3582);
				glVertex3f(8.4380,27.5656,10.0903);
				glVertex3f(8.640,27.93310,8.0288);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.6470,27.5694,10.3582);
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(8.4380,27.5656,10.0903);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(7.8917,26.4047,14.0401);
				glVertex3f(8.1879,27.0545,12.0989);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(7.9870,25.5183,16.5042);
				glVertex3f(7.5515,25.6224,15.9003);
				glVertex3f(7.8917,26.4047,14.0401);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9870,25.5183,16.5042);
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.1700,24.7146,17.667);
				glVertex3f(7.5515,25.6224,15.9003);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(6.7501,23.6892,19.3286);
				glVertex3f(7.1700,24.7146,17.667);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(6.3055,22.22534,20.9061);
				glVertex3f(6.7501,23.6892,19.3286);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(5.7484,21.2735,22.3183);
				glVertex3f(6.3055,22.22534,20.9061);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(5.2319,19.9499,23.6042);
				glVertex3f(5.7484,21.2735,22.3183);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(4.6909,18.5466,24.7533);
				glVertex3f(5.2319,19.9499,23.6042);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(4.1290,17.0735,25.7615);
				glVertex3f(4.6909,18.5466,24.7533);
			}glEnd();		
			//col=2
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.640,27.93310,8.0288);
				glVertex3f(8.4380,27.5656,10.0903);

				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(8.5208,27.9148,7.8667);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				glVertex3f(8.4380,27.5656,10.0903);
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(7.9210,27.0652,11.6987);
				glVertex3f(8.2435,27.5553,9.8093);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(7.8917,26.4047,14.0401);
				
				glVertex3f(7.5553,26.4491,13.5223);
				glVertex3f(7.9210,27.0652,11.6987);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.8917,26.4047,14.0401);
				glVertex3f(7.5515,25.6224,15.9003);
				
				glVertex3f(7.1486,25.7127,15.2679);
				glVertex3f(7.5553,26.4491,13.5223);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.5515,25.6224,15.9003);
				glVertex3f(7.1700,24.7146,17.667);
				

				glVertex3f(6.7034,24.8623,16.9246);
				glVertex3f(7.1486,25.7127,15.2679);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1700,24.7146,17.667);
				glVertex3f(6.7501,23.6892,19.3286);
				

				glVertex3f(6.2225,23.9053,18.4823);
				glVertex3f(6.7034,24.8623,16.9246);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7501,23.6892,19.3286);
				glVertex3f(6.3055,22.22534,20.9061);
				

				glVertex3f(5.5906,22.7548,19.9831);
				glVertex3f(6.2225,23.9053,18.4823);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.3055,22.22534,20.9061);
				glVertex3f(5.7484,21.2735,22.3183);


				glVertex3f(5.0451,21.6080,21.3062);
				glVertex3f(5.5906,22.7548,19.9831);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.7484,21.2735,22.3183);
				glVertex3f(5.2319,19.9499,23.6042);
				

				glVertex3f(4.4737,20.3799,22.5077);
				glVertex3f(5.0451,21.6080,21.3062);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2319,19.9499,23.6042);
				glVertex3f(4.6909,18.5466,24.7533);
				

				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(4.4737,20.3799,22.5077);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.6909,18.5466,24.7533);
				glVertex3f(4.1290,17.0735,25.7615);
				

				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(3.8798,19.0793,23.5829);
			}glEnd();		
			
			//col=3
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.5208,27.9148,7.8667);

				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(8.4106,27.8925,7.6974);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(7.9210,27.0652,11.6987);
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(8.0643,27.5384,9.5163);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9210,27.0652,11.6987);
				
				glVertex3f(7.5553,26.4491,13.5223);
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(7.6757,27.0666,11.2820);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.5553,26.4491,13.5223);
				
				glVertex3f(7.1486,25.7127,15.2679);
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(7.2468,26.4815,12.9832);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1486,25.7127,15.2679);
				

				glVertex3f(6.7034,24.8623,16.9246);
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(6.7797,25.7882,14.6096);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7034,24.8623,16.9246);
				

				glVertex3f(6.2225,23.9053,18.4823);
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(6.277,24.9925,16.1515);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.2225,23.9053,18.4823);
				

				glVertex3f(5.5906,22.7548,19.9831);
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(5.7412,24.1011,17.6003);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5906,22.7548,19.9831);


				glVertex3f(5.0451,21.6080,21.3062);
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(4.9956,22.9798,19.0224);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.0451,21.6080,21.3062);
				

				glVertex3f(4.4737,20.3799,22.5077);
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(4.3989,21.9176,20.2465);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.4737,20.3799,22.5077);
				

				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(3.7788,20.7825,21.3583);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(3.1383,19.5823,22.3538);
				
			}glEnd();		
			
			//col=4
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4106,27.8925,7.6974);
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(8.3098,27.8663,7.5216);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(7.9010,27.5149,9.2126);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(7.4528,27.0586,10.8504);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(6.9671,26.5013,12.4251);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(6.4461,25.848,13.928);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(5.8921,25.1039,15.3507);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(5.3077,24.2747,16.6861);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(4.4532,23.1801,18.0232);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(3.8117,22.1992,19.1435);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(3.1491,21.1539,20.1604);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(2.4683,20.0510,21.0708);
			}glEnd();			

			//col=5

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.3098,27.8663,7.5216);
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(8.2188,27.8363,7.3399);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(7.7544,27.4847,8.8995);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(7.2532,27.0408,10.4057);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(6.7173,26.5082,11.8504);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(6.1488,25.8913,13.226);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(5.5499,25.1950,14.5257);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(4.9232,24.4244,15.7434);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(3.9652,23.3534,16.9896);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(3.2853,22.4499,18.0016);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(2.5865,21.4904,18.9188);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(1.8716,20.4809,19.7386);
			}glEnd();			

			//col=6
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.2188,27.8363,7.3399);
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(8.1379,27.8026,7.1532);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(7.0775,27.0131,9.9497);
				
				glVertex3f(7.6248,27.4479,8.5781);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(6.4982,26.5017,11.2612);
				
				glVertex3f(7.0775,27.0131,9.9497);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(5.8888,25.9175,12.5063);
				
				glVertex3f(6.4982,26.5017,11.2612);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(5.2517,25.265,13.6796);
				
				glVertex3f(5.8888,25.9175,12.5063);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(4.5891,24.5486,14.7761);
				
				glVertex3f(5.2517,25.265,13.6796);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(3.533,23.4977,15.9259);
				
				glVertex3f(4.5891,24.5486,14.7761);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(2.821,22.667,16.8254);
				
				glVertex3f(3.533,23.4977,15.9259);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(2.0924,21.7887,17.6382);
				
				glVertex3f(2.821,22.667,16.8254);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(1.3108,20.8513,18.3386);
				
				glVertex3f(2.0924,21.7887,17.6382);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(0.5953,19.9095,18.9972);
				
				glVertex3f(1.3108,20.8513,18.3386);
			}glEnd();			


			//col=7
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.1379,27.8026,7.1532);
				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(8.0675,27.7653,6.9621);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				
				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(7.0775,27.0131,9.9497);
				glVertex3f(6.9265,26.9755,9.4844);
				
				glVertex3f(7.5128,27.4045,8.2499);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.0775,27.0131,9.9497);
				glVertex3f(6.4982,26.5017,11.2612);
				glVertex3f(6.3105,26.4815,10.6601);
				
				glVertex3f(6.9265,26.9755,9.4844);
				
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.4982,26.5017,11.2612);
				glVertex3f(5.8888,25.9175,12.5063);
				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(6.3105,26.4815,10.6601);
				
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(5.8888,25.9175,12.5063);
				glVertex3f(5.2517,25.265,13.6796);
				glVertex3f(4.9982,25.3128,12.816);
				
				glVertex3f(5.6671,25.926,11.7721);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2517,25.265,13.6796);
			
				glVertex3f(4.5891,24.5486,14.7761);
				glVertex3f(4.3063,24.646,13.7879);
				
				glVertex3f(4.9982,25.3128,12.816);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(4.5891,24.5486,14.7761);
				glVertex3f(3.533,23.4977,15.9259);
				glVertex3f(3.158,23.6113,14.8363);
				
				glVertex3f(4.3063,24.646,13.7879);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				
				glVertex3f(3.533,23.4977,15.9259);
				glVertex3f(2.821,22.667,16.8254);
				glVertex3f(2.4203,22.8482,15.6197);
				
				glVertex3f(3.158,23.6113,14.8363);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.821,22.667,16.8254);
				glVertex3f(2.0924,21.7887,17.6382);
				glVertex3f(1.6681,22.0457,16.3239);
				
				glVertex3f(2.4203,22.8482,15.6197);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(2.0924,21.7887,17.6382);
				glVertex3f(1.3108,20.8513,18.3386);
				glVertex3f(0.9037,21.2082,16.9479);
				
				glVertex3f(1.6681,22.0457,16.3239);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.3108,20.8513,18.3386);
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(0.1295,20.3398,17.491);
				
				glVertex3f(0.9037,21.2082,16.9479);
			}glEnd();			


			//col=8
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.0675,27.7653,6.9621);
				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(7.4188,27.3546,7.916);
				glVertex3f(8.0078,27.7244,6.7674);



			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				
				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(6.9265,26.9755,9.4844);
				glVertex3f(6.8006,26.928,9.0114);
				
				glVertex3f(7.4188,27.3546,7.916);

				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.9265,26.9755,9.4844);
				glVertex3f(6.3105,26.4815,10.6601);
				glVertex3f(6.155,26.4475,10.0494);
				
				glVertex3f(6.8006,26.928,9.0114);

				
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.3105,26.4815,10.6601);
				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(5.4843,25.9163,11.0261);
				
				glVertex3f(6.155,26.4475,10.0494);

				
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				
				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(4.9982,25.3128,12.816);
				glVertex3f(4.7904,25.3377,11.9382);
				
				glVertex3f(5.4843,25.9163,11.0261);

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9982,25.3128,12.816);
				glVertex3f(4.3063,24.646,13.7879);
				glVertex3f(4.0757,24.7154,12.783);
				
				glVertex3f(4.7904,25.3377,11.9382);

				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(4.3063,24.646,13.7879);
				glVertex3f(3.158,23.6113,14.8363);
				glVertex3f(2.8411,23.6927,13.7254);
				
				glVertex3f(4.0757,24.7154,12.783);

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				
				glVertex3f(3.158,23.6113,14.8363);
				glVertex3f(2.4203,22.8482,15.6197);
				glVertex3f(2.0842,22.9914,14.3894);
				
				glVertex3f(2.8411,23.6927,13.7254);

				
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4203,22.8482,15.6197);
				glVertex3f(1.6681,22.0457,16.3239);
				glVertex3f(1.3147,22.2589,14.9813);
				
				glVertex3f(2.0842,22.9914,14.3894);

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(1.6681,22.0457,16.3239);
				glVertex3f(0.9037,21.2082,16.9479);
				glVertex3f(0.5348,21.4986,15.5005);
				
				glVertex3f(1.3147,22.2589,14.9813);

			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.9037,21.2082,16.9479);
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(-0.2531,20.7141,15.947);
				
				glVertex3f(0.5348,21.4986,15.5005);

			}glEnd();			


			//col=9
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.0078,27.7244,6.7674);
				glVertex3f(7.4188,27.3546,7.916);
				glVertex3f(7.3432,27.2984,7.5779);
				glVertex3f(7.9591,27.6802,6.5699);



			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				
				glVertex3f(7.4188,27.3546,7.916);
				glVertex3f(6.8006,26.928,9.0114);
				glVertex3f(6.7003,26.8706,8.5328);
				
				glVertex3f(7.3432,27.2984,7.5779);

				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.8006,26.928,9.0114);
				glVertex3f(6.155,26.4475,10.0494);
				glVertex3f(6.0322,26.3996,9.4316);
				
				glVertex3f(6.7003,26.8706,8.5328);

				
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(6.155,26.4475,10.0494);
				glVertex3f(5.4843,25.9163,11.0261);

				glVertex3f(5.3411,25.8882,10.2714);
				
				glVertex3f(6.0322,26.3996,9.4316);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				
				glVertex3f(5.4843,25.9163,11.0261);
				glVertex3f(4.7904,25.3377,11.9382);
				glVertex3f(4.629,25.3393,11.05);
				
				glVertex3f(5.3411,25.8882,10.2714);

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.7904,25.3377,11.9382);
				glVertex3f(4.0757,24.7154,12.783);
				glVertex3f(3.898,24.7559,11.7653);
				
				glVertex3f(4.629,25.3393,11.05);

				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(4.0757,24.7154,12.783);
				glVertex3f(2.8411,23.6927,13.7254);
				glVertex3f(2.5835,23.7406,12.5976);
				
				glVertex3f(3.898,24.7559,11.7653);

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				
				glVertex3f(2.8411,23.6927,13.7254);
				glVertex3f(2.0842,22.9914,14.3894);
				glVertex3f(1.8137,23.0951,13.1395);
				
				glVertex3f(2.5835,23.7406,12.5976);

				
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

					glVertex3f(2.0842,22.9914,14.3894);

					glVertex3f(1.3147,22.2589,14.9813);
				

				glVertex3f(1.0331,22.4261,13.6157);
				
				glVertex3f(1.8137,23.0951,13.1395);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(1.3147,22.2589,14.9813);
				glVertex3f(0.5348,21.4986,15.5005);
				glVertex3f(0.244,21.7364,14.0263);
				
				glVertex3f(1.0331,22.4261,13.6157);

			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.5348,21.4986,15.5005);
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(-0.5516,21.0288,14.3715);
				
				glVertex3f(0.244,21.7364,14.0263);

			}glEnd();			

			//col=10
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9591,27.6802,6.5699);
				glVertex3f(7.3432,27.2984,7.5779);
				glVertex3f(7.2862,27.236,7.2368);
				glVertex3f(7.9215,27.6327,6.3704);


			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				
				glVertex3f(7.3432,27.2984,7.5779);
				glVertex3f(6.7003,26.8706,8.5328);
				glVertex3f(6.6259,26.8035,8.0505);
				
				glVertex3f(7.2862,27.236,7.2368);

				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7003,26.8706,8.5328);
				glVertex3f(6.0322,26.3996,9.4316);
				glVertex3f(5.9425,26.3379,8.8092);
				
				glVertex3f(6.6259,26.8035,8.0505);

				
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
					glVertex3f(6.0322,26.3996,9.4316);

				glVertex3f(5.3411,25.8882,10.2714);
				glVertex3f(5.2379,25.8415,9.511);
				
				glVertex3f(5.9425,26.3379,8.8092);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				
				glVertex3f(5.3411,25.8882,10.2714);
				glVertex3f(4.629,25.3393,11.05);
				glVertex3f(4.5143,25.3171,10.1547);
				
				glVertex3f(5.2379,25.8415,9.511);

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.629,25.3393,11.05);
				glVertex3f(3.898,24.7559,11.7653);
				glVertex3f(3.7737,24.7671,10.7389);
				glVertex3f(4.5143,25.3171,10.1547);
				

				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(3.898,24.7559,11.7653);
				glVertex3f(2.5835,23.7406,12.5976);
				glVertex3f(2.3858,23.7543,11.4575);
				
				glVertex3f(3.7737,24.7671,10.7389);

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				
				
				glVertex3f(2.5835,23.7406,12.5976);
				glVertex3f(1.8137,23.0951,13.1395);
				glVertex3f(1.6095,23.158,11.8751);
				
				glVertex3f(2.3858,23.7543,11.4575);
				
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.8137,23.0951,13.1395);
				

				glVertex3f(1.0331,22.4261,13.6157);
				glVertex3f(0.8241,22.5455,12.2329);
				
				glVertex3f(1.6095,23.158,11.8751);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(1.0331,22.4261,13.6157);
				glVertex3f(0.244,21.7364,14.0263);
				glVertex3f(0.0318,21.9191,12.5313);
				
				glVertex3f(0.8241,22.5455,12.2329);

			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				
				glVertex3f(0.244,21.7364,14.0263);
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(-0.7655,21.2812,12.7711);
				
				glVertex3f(0.0318,21.9191,12.5313);

			}glEnd();			

			//col=11
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9215,27.6327,6.3704);
				glVertex3f(7.2862,27.236,7.2368);
				glVertex3f(7.2481,27.1676,6.8942);
				glVertex3f(7.8952,27.5822,6.1697);


			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				
				glVertex3f(7.2862,27.236,7.2368);
				glVertex3f(6.6259,26.8035,8.0505);
				glVertex3f(6.5778,26.7269,7.5662);
				
				glVertex3f(7.2481,27.1676,6.8942);

				

				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.6259,26.8035,8.0505);
				glVertex3f(5.9425,26.3379,8.8092);
				glVertex3f(5.8862,26.2624,8.1845);
				
				glVertex3f(6.5778,26.7269,7.5662);


				
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(5.9425,26.3379,8.8092);
				glVertex3f(5.2379,25.8415,9.511);
				glVertex3f(5.1752,25.7764,8.748);
				
				glVertex3f(5.8862,26.2624,8.1845);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				
				glVertex3f(5.2379,25.8415,9.511);
				glVertex3f(4.5143,25.3171,10.1547);
				glVertex3f(4.4469,25.2711,9.2559);
				
				glVertex3f(5.1752,25.7764,8.748);

				

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.5143,25.3171,10.1547);
				glVertex3f(3.7737,24.7671,10.7389);
				glVertex3f(3.7032,24.7486,9.708);
				
				glVertex3f(4.4469,25.2711,9.2559);


				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(3.7737,24.7671,10.7389);
				glVertex3f(2.3858,23.7543,11.4575);
				glVertex3f(2.2486,23.7331,10.3098);
				
				glVertex3f(3.7032,24.7486,9.708);


			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				
				
				glVertex3f(2.3858,23.7543,11.4575);
				glVertex3f(1.6095,23.158,11.8751);
				glVertex3f(1.4722,23.1791,10.6014);
				
				glVertex3f(2.2486,23.7331,10.3098);

				
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.6095,23.158,11.8751);
				glVertex3f(0.8241,22.5455,12.2329);
				glVertex3f(0.6882,22.6159,10.8385);
				
				glVertex3f(1.4722,23.1791,10.6014);

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(0.8241,22.5455,12.2329);
				glVertex3f(0.0318,21.9191,12.5313);
				glVertex3f(-0.1012,22.0452,11.0218);
				
				glVertex3f(0.6882,22.6159,10.8385);


			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				
				glVertex3f(0.0318,21.9191,12.5313);
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.8943,21.469,11.1525);
				
				glVertex3f(-0.1012,22.0452,11.0218);


			}glEnd();			

			//col=12
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.8952,27.5822,6.1697);
				glVertex3f(7.2481,27.1676,6.8942);
				glVertex3f(7.229,27.0935,6.5513);
				glVertex3f(7.8803,27.5289,5.9686);


			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{				
				
				glVertex3f(7.2481,27.1676,6.8942);
				glVertex3f(6.5778,26.7269,7.5662);
				glVertex3f(6.5561,26.641,7.082);
				
				glVertex3f(7.229,27.0935,6.5513);

				

				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.5778,26.7269,7.5662);
				glVertex3f(5.8862,26.2624,8.1845);
				glVertex3f(5.8635,26.1736,7.56);
				
				glVertex3f(6.5561,26.641,7.082);


				
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(5.8862,26.2624,8.1845);
				glVertex3f(5.1752,25.7764,8.748);
				glVertex3f(5.1531,25.693,7.9851);
				
				glVertex3f(5.8635,26.1736,7.56);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				
				glVertex3f(5.1752,25.7764,8.748);
				glVertex3f(4.4469,25.2711,9.2559);
				glVertex3f(4.4269,25.2013,8.3573);
				
				glVertex3f(5.1531,25.693,7.9851);

				

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.4469,25.2711,9.2559);
				glVertex3f(3.7032,24.7486,9.708);
				glVertex3f(3.6867,24.7002,8.6767);
				
				glVertex3f(4.4269,25.2013,8.3573);


				
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(3.7032,24.7486,9.708);
				glVertex3f(2.2486,23.7331,10.3098);
				glVertex3f(2.1725,23.6769,9.1591);
				
				glVertex3f(3.6867,24.7002,8.6767);


			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				
				
				glVertex3f(2.2486,23.7331,10.3098);
				glVertex3f(1.4722,23.1791,10.6014);
				glVertex3f(1.4022,23.1581,9.3236);
				
				glVertex3f(2.1725,23.6769,9.1591);

				
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.4722,23.1791,10.6014);
				glVertex3f(0.6882,22.6159,10.8385);
				glVertex3f(0.6258,22.6364,9.4382);
				
				glVertex3f(1.4022,23.1581,9.3236);

			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				
				glVertex3f(0.6882,22.6159,10.8385);
				glVertex3f(-0.1012,22.0452,11.0218);
				glVertex3f(-0.1427,22.1202,9.5215);
				glVertex3f(0.6258,22.6364,9.4382);
				

			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				
				glVertex3f(-0.1012,22.0452,11.0218);
				glVertex3f(-0.8943,21.469,11.1525);
				glVertex3f(-0.938,21.5905,9.5226);
				
				glVertex3f(-0.1427,22.1202,9.5215);


			}glEnd();			

			//col=13		
			
			//------------------- face of a petal end

		}glPopMatrix();	
	}
}
void drawPetal_2_leftback()
{
	for(int i=0; i<1; i++)
	{
		glPushMatrix();{					
			//glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left back part of a petal face
			
			

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.2724,27.1032,5.968);
				glVertex3f(9.2962,27.2124,6.3711);				
			}glEnd();		

			//glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.2962,27.2124,6.3711);				
				glVertex3f(9.2756,27.3368,6.7684);				
			}glEnd();		

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.2756,27.3368,6.7684);				
				glVertex3f(9.211,27.4746,7.1543);				
			}glEnd();		

			//glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.211,27.4746,7.1543);				
				glVertex3f(9.1035,27.6237,7.5226);				
			}glEnd();		

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.1035,27.6237,7.5226);				
				glVertex3f(8.9549,27.782,7.8674);				
			}glEnd();		

			//glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.9549,27.782,7.8674);				
				glVertex3f(8.7677,27.9475,8.183);				
			}glEnd();		
			//=========================================================== col-1
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.2724,27.1032,5.9688);
				glVertex3f(9.5686,26.3782,6.5527);
				glVertex3f(9.6,26.5286,7.2393);
				glVertex3f(9.2962,27.2124,6.3711);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.5686,26.3782,6.5527);
				glVertex3f(9.8729,25.627,7.0852);
				glVertex3f(9.9038,25.8013,8.0559);
				glVertex3f(9.6,26.5286,7.2393);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.8729,25.627,7.0852);
				glVertex3f(10.1848,24.8524,7.5657);
				glVertex3f(10.2075,25.0337,8.8185);
				glVertex3f(9.9038,25.8013,8.0559);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.1848,24.8524,7.5657);
				glVertex3f(10.504,24.057,7.997);
				glVertex3f(10.511,24.229,9.5253);
				glVertex3f(10.2075,25.0337,8.8185);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.504,24.057,7.997);
				glVertex3f(10.8299,23.2435,8.3693);
				glVertex3f(10.8141,23.3904,10.1747);
				glVertex3f(10.511,24.229,9.5253);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.8299,23.2435,8.3693);
				glVertex3f(11.1622,22.4144,8.6925);
				glVertex3f(11.1169,22.5212,10.7656);
				glVertex3f(10.8141,23.3904,10.1747);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.1622,22.4144,8.6925);
				glVertex3f(11.8432,20.7198,9.1831);
				glVertex3f(11.6955,20.9065,11.4967);
				glVertex3f(11.1169,22.5212,10.7656);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.8432,20.7198,9.1831);
				glVertex3f(12.1907,19.859,9.3519);
				glVertex3f(12.002,19.9787,11.9224);
				glVertex3f(11.6955,20.9065,11.4967);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.1907,19.859,9.3519);
				glVertex3f(12.542,18.9925,9.4706);
				glVertex3f(12.307,19.0323,12.2887);
				glVertex3f(12.002,19.9787,11.9224);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.542,18.9925,9.4706);
				glVertex3f(12.8942,18.1269,9.5471);
				glVertex3f(12.6104,18.0703,12.5959);
				glVertex3f(12.307,19.0323,12.2887);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8942,18.1269,9.5471);
				glVertex3f(13.2529,17.2509,9.5625);
				glVertex3f(12.9117,17.0959,12.8444);
				glVertex3f(12.6104,18.0703,12.5959);
			}glEnd();		
			//=========================================================col=2
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.2962,27.2124,6.3711);
				glVertex3f(9.6,26.5286,7.2393);
				glVertex3f(9.555,26.7015,7.9193);
				glVertex3f(9.2756,27.3368,6.7684);				
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(9.6,26.5286,7.2393);
				glVertex3f(9.9038,25.8013,8.0559);
				glVertex3f(9.8254,26.0031,9.0185);
				glVertex3f(9.555,26.7015,7.9193);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.9038,25.8013,8.0559);
				glVertex3f(10.2075,25.0337,8.8185);
				glVertex3f(10.0869,25.245,10.0615);
				glVertex3f(9.8254,26.0031,9.0185);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.2075,25.0337,8.8185);
				glVertex3f(10.511,24.229,9.5253);
				glVertex3f(10.3398,24.4311,11.0445);
				glVertex3f(10.0869,25.245,10.0615);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.511,24.229,9.5253);
				glVertex3f(10.8141,23.3904,10.1747);
				glVertex3f(10.5845,23.5652,11.9642);
				glVertex3f(10.3398,24.4311,11.0445);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.8141,23.3904,10.1747);
				glVertex3f(11.1169,22.5212,10.7656);
				glVertex3f(10.8213,22.6514,12.8175);
				glVertex3f(10.5845,23.5652,11.9642);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.1169,22.5212,10.7656);
				glVertex3f(11.6955,20.9065,11.4967);
				glVertex3f(11.2626,21.1152,13.775);
				glVertex3f(10.8213,22.6514,12.8175);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.6955,20.9065,11.4967);
				glVertex3f(12.002,19.9787,11.9224);
				glVertex3f(11.4897,20.1122,14.45);
				glVertex3f(11.2626,21.1152,13.775);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.002,19.9787,11.9224);
				glVertex3f(12.307,19.0323,12.2887);
				glVertex3f(11.7098,19.076,15.0536);
				glVertex3f(11.4897,20.1122,14.45);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.307,19.0323,12.2887);
				glVertex3f(12.6104,18.0703,12.5959);
				glVertex3f(11.9232,18.0109,15.5851);
				glVertex3f(11.7098,19.076,15.0536);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.6104,18.0703,12.5959);
				glVertex3f(12.9117,17.0959,12.8444);
				glVertex3f(12.13,16.9209,16.0443);
				glVertex3f(11.9232,18.0109,15.5851);
			}glEnd();		
			//==============================================================col=3
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.2756,27.3368,6.7684);				
				glVertex3f(9.555,26.7015,7.9193);
				glVertex3f(9.4345,26.8946,8.5818);				
				glVertex3f(9.211,27.4746,7.1543);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(9.555,26.7015,7.9193);
				glVertex3f(9.8254,26.0031,9.0185);
				glVertex3f(9.6391,26.2297,9.9574);
				glVertex3f(9.4345,26.8946,8.5818);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.8254,26.0031,9.0185);
				glVertex3f(10.0869,25.245,10.0615);
				glVertex3f(9.8253,25.484,11.2743);
				glVertex3f(9.6391,26.2297,9.9574);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.0869,25.245,10.0615);
				glVertex3f(10.3398,24.4311,11.0445);
				glVertex3f(9.9938,24.6616,12.5263);
				glVertex3f(9.8253,25.484,11.2743);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.3398,24.4311,11.0445);
				glVertex3f(10.5845,23.5652,11.9642);
				glVertex3f(10.1455,23.7673,13.7076);
				glVertex3f(9.9938,24.6616,12.5263);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.5845,23.5652,11.9642);
				glVertex3f(10.8213,22.6514,12.8175);
				glVertex3f(10.2813,22.806,14.8135);
				glVertex3f(10.1455,23.7673,13.7076);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.8213,22.6514,12.8175);
				glVertex3f(11.2626,21.1152,13.775);
				glVertex3f(10.553,21.3477,15.9786);
				glVertex3f(10.2813,22.806,14.8135);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.2626,21.1152,13.775);
				glVertex3f(11.4897,20.1122,14.45);
				glVertex3f(10.6644,20.2639,16.8902);
				glVertex3f(10.553,21.3477,15.9786);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.4897,20.1122,14.45);
				glVertex3f(11.7098,19.076,15.0536);
				glVertex3f(10.7628,19.1312,17.716);
				glVertex3f(10.6644,20.2639,16.8902);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.7098,19.076,15.0536);
				glVertex3f(11.9232,18.0109,15.5851);
				glVertex3f(10.8493,17.9551,18.454);
				glVertex3f(10.7628,19.1312,17.716);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.9232,18.0109,15.5851);
				glVertex3f(12.13,16.9209,16.0443);
				glVertex3f(10.9246,16.741,19.1031);
				glVertex3f(10.8493,17.9551,18.454);
			}glEnd();		
			//==============================================================col=4
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.211,27.4746,7.1543);
				glVertex3f(9.4345,26.8946,8.5818);				
				glVertex3f(9.2406,27.1053,9.2159);
				glVertex3f(9.1035,27.6237,7.5226);
				
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(9.4345,26.8946,8.5818);				
				glVertex3f(9.6391,26.2297,9.9574);
				glVertex3f(9.3482,26.4788,10.8572);
				glVertex3f(9.2406,27.1053,9.2159);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.6391,26.2297,9.9574);
				glVertex3f(9.8253,25.484,11.2743);
				glVertex3f(9.4275,25.7486,12.4368);
				glVertex3f(9.3482,26.4788,10.8572);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.8253,25.484,11.2743);
				glVertex3f(9.9938,24.6616,12.5263);
				glVertex3f(9.4795,24.9196,13.9457);
				glVertex3f(9.4275,25.7486,12.4368);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.9938,24.6616,12.5263);
				glVertex3f(10.1455,23.7673,13.7076);
				glVertex3f(9.5056,23.9974,15.3758);
				glVertex3f(9.4795,24.9196,13.9457);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.1455,23.7673,13.7076);
				glVertex3f(10.2813,22.806,14.8135);
				glVertex3f(9.5074,22.9882,16.7195);
				glVertex3f(9.5056,23.9974,15.3758);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.2813,22.806,14.8135);
				glVertex3f(10.553,21.3477,15.9786);
				glVertex3f(9.5811,21.6083,18.0696);
				glVertex3f(9.5074,22.9882,16.7195);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.553,21.3477,15.9786);
				glVertex3f(10.6644,20.2639,16.8902);
				glVertex3f(9.5429,20.4415,19.2007);
				glVertex3f(9.5811,21.6083,18.0696);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.6644,20.2639,16.8902);
				glVertex3f(10.7628,19.1312,17.716);
				glVertex3f(9.4857,19.2094,20.2293);
				glVertex3f(9.5429,20.4415,19.2007);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.7628,19.1312,17.716);
				glVertex3f(10.8493,17.9551,18.454);
				glVertex3f(9.4113,17.9191,21.1521);
				glVertex3f(9.4857,19.2094,20.2293);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.8493,17.9551,18.454);
				glVertex3f(10.9246,16.741,19.1031);
				glVertex3f(9.3215,16.5778,21.9667);
				glVertex3f(9.4113,17.9191,21.1521);
			}glEnd();		
			//==============================================================col=5
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.1035,27.6237,7.5226);
				glVertex3f(9.2406,27.1053,9.2159);
				glVertex3f(8.9766,27.3311,9.8114);
				glVertex3f(8.9549,27.782,7.8674);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(9.2406,27.1053,9.2159);
				glVertex3f(9.3482,26.4788,10.8572);
				glVertex3f(8.958,26.748,11.7031);
				glVertex3f(8.9766,27.3311,9.8114);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.3482,26.4788,10.8572);
				glVertex3f(9.4275,25.7486,12.4368);
				glVertex3f(8.9007,26.0374,13.5297);
				glVertex3f(8.958,26.748,11.7031);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.4275,25.7486,12.4368);
				glVertex3f(9.4795,24.9196,13.9457);
				glVertex3f(8.8064,25.2051,15.2792);
				glVertex3f(8.9007,26.0374,13.5297);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.4795,24.9196,13.9457);
				glVertex3f(9.5056,23.9974,15.3758);
				glVertex3f(8.6769,24.2577,16.9405);
				glVertex3f(8.8064,25.2051,15.2792);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.5056,23.9974,15.3758);
				glVertex3f(9.5074,22.9882,16.7195);
				glVertex3f(8.5147,23.2026,18.5035);
				glVertex3f(8.6769,24.2577,16.9405);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.5074,22.9882,16.7195);
				glVertex3f(9.5811,21.6083,18.0696);
				glVertex3f(8.3661,21.9034,20.0121);
				glVertex3f(8.5147,23.2026,18.5035);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.5811,21.6083,18.0696);
				glVertex3f(9.5429,20.4415,19.2007);
				glVertex3f(8.1478,20.6554,21.342);
				glVertex3f(8.3661,21.9034,20.0121);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.5429,20.4415,19.2007);
				glVertex3f(9.4857,19.2094,20.2293);
				glVertex3f(7.9046,19.3257,22.551);
				glVertex3f(8.1478,20.6554,21.342);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.4857,19.2094,20.2293);
				glVertex3f(9.4113,17.9191,21.1521);
				glVertex3f(7.6393,17.9231,23.6341);
				glVertex3f(7.9046,19.3257,22.551);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.4113,17.9191,21.1521);
				glVertex3f(9.3215,16.5778,21.9667);
				glVertex3f(7.3546,16.4568,24.5877);
				glVertex3f(7.6393,17.9231,23.6341);
			}glEnd();		
			//==============================================================col=6
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.9549,27.782,7.8674);
				glVertex3f(8.9766,27.3311,9.8114);
				glVertex3f(8.647,27.5694,10.3582);
				glVertex3f(8.7677,27.9475,8.183);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(8.9766,27.3311,9.8114);
				glVertex3f(8.958,26.748,11.7031);
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.647,27.5694,10.3582);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.958,26.748,11.7031);
				glVertex3f(8.9007,26.0374,13.5297);
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(8.4754,27.0348,12.4809);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.9007,26.0374,13.5297);
				glVertex3f(8.8064,25.2051,15.2792);
				glVertex3f(7.987,25.5183,16.5042);
				glVertex3f(8.2546,26.3489,14.5346);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.8064,25.2051,15.2792);
				glVertex3f(8.6769,24.2577,16.9405);
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.987,25.5183,16.5042);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.6769,24.2577,16.9405);
				glVertex3f(8.5147,23.2026,18.5035);
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(7.6752,24.5508,18.3755);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.5147,23.2026,18.5035);
				glVertex3f(8.3661,21.9034,20.0121);
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(7.3223,23.4549,20.1358);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.3661,21.9034,20.0121);
				glVertex3f(8.1478,20.6554,21.342);
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(6.9316,22.2404,21.7735);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.1478,20.6554,21.342);
				glVertex3f(7.9046,19.3257,22.551);
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(6.5067,20.9174,23.2788);
			}glEnd();		
			
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9046,19.3257,22.551);
				glVertex3f(7.6393,17.9231,23.6341);
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(6.0514,19.4966,24.6435);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.6393,17.9231,23.6341);
				glVertex3f(7.3546,16.4568,24.5877);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.5694,17.9891,25.8609);
			}glEnd();		
			//==============================================================col=7
			
		}glPopMatrix();	
	}
}
void drawPetal_2_leftface_midsquare()
{
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(-0.4616,21.5103,7.2216);
				glVertex3f(-0.4432,21.4529,9.1098);
				glVertex3f(-0.1562,21.3485,8.8746);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.4432,21.4529,9.1098);
				glVertex3f(-0.3796,21.3899,9.8859);
				glVertex3f(0.3113,21.1332,10.0332);
				glVertex3f(-0.1562,21.3485,8.8746);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.3796,21.3899,9.8859);
				glVertex3f(-0.1562,21.1852,11.7266);
				glVertex3f(1.7951,20.5125,11.9807);
				glVertex3f(0.3113,21.1332,10.0332);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.1562,21.1852,11.7266);
				glVertex3f(0.0798,20.9864,13.0263);
				glVertex3f(3.8515,19.7466,13.2329);
				glVertex3f(1.7951,20.5125,11.9807);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.0798,20.9864,13.0263);
				glVertex3f(0.2196,20.8686,13.626);
				glVertex3f(6.0217,19.0358,13.626);
				glVertex3f(3.8515,19.7466,13.2329);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.2196,20.8686,13.626);
				glVertex3f(0.4273,20.6695,14.64);
				glVertex3f(5.9796,18.92,14.6701);
				glVertex3f(6.0217,19.0358,13.626);
			}glEnd();

			//--------------------------------------------------------
}
void drawPetal_2_rightface_midsquare()
{
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(12.8215,17.4492,7.2349);
				glVertex3f(12.4726,17.4869,8.9036);
				glVertex3f(12.7899,17.4212,8.7476);
			}glEnd();
			
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.4726,17.4869,8.9036);
				glVertex3f(11.9586,17.5711,10.0706);
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(12.7899,17.4212,8.7476);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.9586,17.5711,10.0706);
				glVertex3f(11.2482,17.7051,11.1214);
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(12.6088,17.3965,10.2298);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.2482,17.7051,11.1214);
				glVertex3f(10.678,17.8227,11.74);
				glVertex3f(12.3719,17.3566,11.74);
				glVertex3f(12.5402,17.3729,10.9394);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.678,17.8227,11.74);
				glVertex3f(8.6061,18.3093,13.1093);
				glVertex3f(12.0465,17.3254,13.0833);
				glVertex3f(12.3719,17.3566,11.74);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.6061,18.3093,13.1093);
				glVertex3f(6.0217,19.0358,13.626);
				glVertex3f(11.8872,17.3103,13.626);
				glVertex3f(12.0465,17.3254,13.0833);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.0217,19.0358,13.626);
				glVertex3f(5.9796,18.92,14.6701);
				glVertex3f(11.5222,17.2863,14.7155);
				glVertex3f(11.8872,17.3103,13.626);
			}glEnd();

			//--------------------------------------------------------
}
void drawPetal_2_face_bottom_circle()
{

}
void drawPetal_2_leftface_upper_glass()
{

}
void drawPetal_2()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{					
			glRotatef(i*40,0,0,1);
			drawPetal_2_leftface();
			drawPetal_2_rightface();
			drawPetal_2_rightback();
			drawPetal_2_leftback();
			drawPetal_2_leftface_midsquare();
			drawPetal_2_leftface_upper_glass();//NOT IMPLEMENTED YET
			drawPetal_2_rightface_midsquare();
			drawPetal_2_face_bottom_circle();//NOT IMPLEMENTED YET
			
		}glPopMatrix();	
	}
}
void drawPetal_1_rightback()
{	
	//--------------------------column: 0
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.8403,-4.7016,13.284);
		glVertex3f(-18.8268,-4.4563,15.4193);
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-19.705,-5.2887,13.2596);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.8268,-4.4563,15.4193);
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-17.75626,-4.8517,17.2742);
		glVertex3f(-18.752,-5.0751,15.2765);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-16.6489,-3.9292,19.6101);
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-17.75626,-4.8517,17.2742);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.6489,-3.9292,19.6101);
		glVertex3f(-15.4835,-3.6575,21.663);
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-16.7053,-4.5867,19.2118);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.4835,-3.6575,21.663);
		glVertex3f(-14.2731,-3.3588,23.6861);
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-15.6159,-4.3117,21.1207);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.2731,-3.3588,23.6861);
		glVertex3f(-13.0141,-3.0524,25.678);
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-14.4784,-4.0245,23.0102);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.0141,-3.0524,25.678);
		glVertex3f(-11.7048,-2.7327,27.6427);
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-13.2929,-3.7261,24.8814);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-11.7048,-2.7327,27.6427);
		glVertex3f(-10.3556,-2.4062,29.5636);
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-12.0666,-3.4195,26.7173);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.3556,-2.4062,29.5636);
		glVertex3f(-8.9574,-2.0678,31.455);
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-10.7881,-3.0938,28.5454);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-8.9574,-2.0678,31.455);
		glVertex3f(-7.5147,-1.7186,33.3106);
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-9.4796,-2.7649,30.3253);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-7.5147,-1.7186,33.3106);
		glVertex3f(-6.0282,-1.3589,35.1293);
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-8.1315,-2.4264,32.0735);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-6.0282,-1.3589,35.1293);
		glVertex3f(-4.4989,-0.9888,36.9101);
		glVertex3f(-5.3194,-1.7214,35.4705);
		glVertex3f(-6.7445,-2.0785,33.7888);
	}glEnd();
	//--------------------------column: 1
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.705,-5.2887,13.2596);
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-18.583,-6.6951,14.5753);
		glVertex3f(-19.3978,-6.6308,12.9811);
		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-17.7613,-6.5654,16.2535);
		glVertex3f(-18.583,-6.6951,14.5753);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-16.926,-6.3416,17.7525);
		glVertex3f(-17.7613,-6.5654,16.2535);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-16.0612,-6.145,19.2105);
		glVertex3f(-16.926,-6.3416,17.7525);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-15.1335,-5.8648,20.7555);
		glVertex3f(-16.0612,-6.145,19.2105);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-14.1947,-5.6165,22.2283);
		glVertex3f(-15.1335,-5.8648,20.7555);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-13.1743,-5.3434,23.7565);
		glVertex3f(-14.1947,-5.6165,22.2283);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-12.1293,-5.065,25.2601);
		glVertex3f(-13.1743,-5.3434,23.7565);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-11.0822,-4.7881,26.706);
		glVertex3f(-12.1293,-5.065,25.2601);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-10.0079,-4.5048,28.1302);
		glVertex3f(-11.0822,-4.7881,26.706);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-8.907,-4.2149,29.5321);
		glVertex3f(-10.0079,-4.5048,28.1302);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-5.3194,-1.7214,35.4705);
		glVertex3f(-7.78,-3.9188,30.9111);
		glVertex3f(-8.907,-4.2149,29.5321);
	}glEnd();
	//--------------------------column: 2
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.3978,-6.6308,12.9811);
		glVertex3f(-18.583,-6.6951,14.5753);
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-19.1521,-7.715,12.5178);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.583,-6.6951,14.5753);
		glVertex3f(-17.7613,-6.5654,16.2535);
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-18.4077,-8.1401,13.6087);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7613,-6.5654,16.2535);
		glVertex3f(-16.926,-6.3416,17.7525);
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-17.7381,-8.053,15.0332);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.926,-6.3416,17.7525);
		glVertex3f(-16.0612,-6.145,19.2105);
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-17.0562,-7.8517,16.2322);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.0612,-6.145,19.2105);
		glVertex3f(-15.1335,-5.8648,20.7555);
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-16.347,-7.6192,17.4575);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.1335,-5.8648,20.7555);
		glVertex3f(-14.1947,-5.6165,22.2283);
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-15.5995,-7.3933,18.6788);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.1947,-5.6165,22.2283);
		glVertex3f(-13.1743,-5.3434,23.7565);
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-14.8388,-7.1544,19.8907);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1743,-5.3434,23.7565);
		glVertex3f(-12.1293,-5.065,25.2601);
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-13.9911,-6.8918,21.18);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.1293,-5.065,25.2601);
		glVertex3f(-11.0822,-4.7881,26.706);
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-13.1265,-6.6248,22.4544);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-11.0822,-4.7881,26.706);
		glVertex3f(-10.0079,-4.5048,28.1302);
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-12.2784,-6.3665,23.6616);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.0079,-4.5048,28.1302);
		glVertex3f(-8.907,-4.2149,29.5321);
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-11.4115,-6.1036,24.8542);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-8.907,-4.2149,29.5321);
		glVertex3f(-7.78,-3.9188,30.9111);
		glVertex3f(-9.6231,-5.5648,27.1936);
		glVertex3f(-10.5263,-5.8363,26.0317);
	}glEnd();
	//--------------------------column: 3
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.1521,-7.715,12.5178);
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-18.8067,-9.2603,11.3508);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-18.3082,-9.4045,12.211);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-17.8175,-9.3855,13.2702);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-17.3158,-9.2325,14.1256);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-16.8041,-9.0877,14.9583);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-16.2556,-8.9184,15.8423);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-15.7049,-8.7466,16.7121);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-15.0636,-8.5511,17.6805);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-14.4293,-8.3589,18.6155);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-13.8152,-8.1749,19.4974);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-13.1905,-7.9883,20.3711);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-9.6231,-5.5648,27.1936);
		glVertex3f(-12.0487,-7.7309,21.7581);
		glVertex3f(-12.5557,-7.7992,21.2364);
	}glEnd();
	//--------------------------column: 4
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.8067,-9.2603,11.3508);
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-18.6771,-9.8512,10.6575);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-18.2568,-9.9142,11.4855);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-17.8288,-9.9066,12.4349);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-17.3864,-9.7639,13.1925);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.935,-9.62,13.9435);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-16.4663,-9.5006,14.6469);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-16.0035,-9.3536,15.3746);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-15.4121,-9.1349,16.3398);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-14.8458,-8.9545,17.19);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-14.308,-8.7849,17.9786);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-13.7621,-8.6134,18.7611);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-12.0487,-7.7309,21.7581);
		glVertex3f(-12.7191,-8.3296,20.1298);
		glVertex3f(-13.2081,-8.44,19.537);
	}glEnd();
	//--------------------------column: 5
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.6771,-9.8512,10.6575);
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-18.5843,-10.2815,9.9958);
		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-18.2294,-10.3563,10.6732);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.8686,-10.3674,11.4891);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-17.4943,-10.2515,12.1212);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-17.1136,-10.1341,12.7497);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-16.706,-10.0082,13.4056);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-16.3133,-9.8879,14.0259);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-15.8234,-9.7356,14.7801);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-15.3416,-9.5866,15.5088);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-14.8888,-9.4478,16.1802);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-14.4301,-9.3077,16.8473);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-12.7191,-8.3296,20.1298);
		glVertex3f(-13.5314,-9.0551,18.0755);
		glVertex3f(-13.9657,-9.1661,17.5097);
	}glEnd();
	//--------------------------column: 6
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.5843,-10.2815,9.9958);
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-18.499,-10.6862,9.1601);		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-18.2285,-10.7262,9.7639);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.9422,-10.7662,10.3985);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.6456,-10.6937,10.8742);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-17.3451,-10.6202,11.3479);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-17.0227,-10.5409,11.8462);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-16.7151,-10.4657,12.3146);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-16.3229,-10.368,12.8998);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-15.9397,-10.2727,13.464);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-15.5839,-10.1849,13.9798);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-15.2246,-10.0962,14.4929);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-13.5314,-9.0551,18.0755);
		glVertex3f(-14.4958,-9.9163,15.5107);
		glVertex3f(-14.8619,-10.0066,15.0032);
	}glEnd();
	//--------------------------column: 7
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.499,-10.6862,9.1601);		
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.4483,-10.9382,8.3984);
		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-18.2434,-11.0054,8.8216);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-18.0265,-11.0762,9.2643);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.8032,-11.0507,9.5898);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.5778,-11.0248,9.9141);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-17.3354,-10.9963,10.2582);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-17.1066,-10.9695,10.5796);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-16.8084,-10.9331,10.9924);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-16.5188,-10.8976,11.3896);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-15.2246,-10.0962,14.4929);		
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-16.2534,-10.8652,11.7498);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-15.9862,-10.8324,12.1085);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-14.4958,-9.9163,15.5107);
		glVertex3f(-15.4185,-10.7403,12.9037);
		glVertex3f(-15.7172,-10.7991,12.4659);
	}glEnd();
	//--------------------------column: 8
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.4483,-10.9382,8.3984);
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.4167,-11.1796,7.1859);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.307,-11.3658,7.4421);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-18.1473,-11.3509,7.6847);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-18.0166,-11.3953,7.7537);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-17.8899,-11.4432,7.8332);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-17.7586,-11.4995,7.9146);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.6367,-11.5514,8.0093);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.4818,-11.6289,8.1123);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.3365,-11.7067,8.2133);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.2534,-10.8652,11.7498);		
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-17.2203,-11.7896,8.2613);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-17.1235,-11.8889,8.2483);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-15.4185,-10.7403,12.9037);
		glVertex3f(-16.8775,-12.0432,8.4274);
		glVertex3f(-16.991,-11.9575,8.3693);
	}glEnd();
	//--------------------------column: 9
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.4167,-11.1796,7.1859);
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.3861,-11.3106,5.972);
		glVertex3f(-18.4215,-11.1571,5.9751);
		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.3534,-11.4654,5.972);
		glVertex3f(-18.3861,-11.3106,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-18.2775,-11.5914,5.972);
		glVertex3f(-18.3534,-11.4654,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-18.22,-11.6869,5.972);
		glVertex3f(-18.2775,-11.5914,5.972);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-18.1503,-11.8028,5.972);
		glVertex3f(-18.22,-11.6869,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-18.0861,-11.9093,5.972);
		glVertex3f(-18.1503,-11.8028,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.9975,-12.0564,5.972);
		glVertex3f(-18.0861,-11.9093,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.9127,-12.1973,5.972);
		glVertex3f(-17.9975,-12.0564,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.8373,-12.3225,5.972);
		glVertex3f(-17.9127,-12.1973,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-17.7619,-12.4478,5.972);
		glVertex3f(-17.8373,-12.3225,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-17.6864,-12.573,5.972);
		glVertex3f(-17.7619,-12.4478,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-16.8775,-12.0432,8.4274);
		glVertex3f(-17.611,-12.6983,5.972);
		glVertex3f(-17.6864,-12.573,5.972);
	}glEnd();
	//--------------------END
}
void drawPetal_1_rightback_texture()
{	
	//--------------------------column: 0
	{
		
		
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); glVertex3f(-18.8268,-4.4563,15.4193);
			glTexCoord2f(1,0); glVertex3f(-17.7431,-4.1941,17.5674);
			glTexCoord2f(1,1); glVertex3f(-17.75626,-4.8517,17.2742);
			glTexCoord2f(0,1); glVertex3f(-18.752,-5.0751,15.2765);
		glEnd();
		glDisable(GL_TEXTURE_2D);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-17.7431,-4.1941,17.5674);
			glTexCoord2f(1,0); 
			glVertex3f(-16.6489,-3.9292,19.6101);
			glTexCoord2f(1,1); 
			glVertex3f(-16.7053,-4.5867,19.2118);
			glTexCoord2f(0,1); 
			glVertex3f(-17.75626,-4.8517,17.2742);
		glEnd();
		glDisable(GL_TEXTURE_2D);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-16.6489,-3.9292,19.6101);
			glTexCoord2f(1,0); 
			glVertex3f(-15.4835,-3.6575,21.663);
			glTexCoord2f(1,1); 
			glVertex3f(-15.6159,-4.3117,21.1207);
			glTexCoord2f(0,1); 
			glVertex3f(-16.7053,-4.5867,19.2118);
		glEnd();
		glDisable(GL_TEXTURE_2D);

			glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-15.4835,-3.6575,21.663);
			glTexCoord2f(1,0); 
			glVertex3f(-14.2731,-3.3588,23.6861);
			glTexCoord2f(1,1); 
			glVertex3f(-14.4784,-4.0245,23.0102);
			glTexCoord2f(0,1); 
			glVertex3f(-15.6159,-4.3117,21.1207);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		
				glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-14.2731,-3.3588,23.6861);
			glTexCoord2f(1,0); 
			glVertex3f(-13.0141,-3.0524,25.678);
			glTexCoord2f(1,1); 
			glVertex3f(-13.2929,-3.7261,24.8814);
			glTexCoord2f(0,1); 
			glVertex3f(-14.4784,-4.0245,23.0102);
		glEnd();
		glDisable(GL_TEXTURE_2D);

					glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-13.0141,-3.0524,25.678);
			glTexCoord2f(1,0); 
			glVertex3f(-11.7048,-2.7327,27.6427);
			glTexCoord2f(1,1); 
			glVertex3f(-12.0666,-3.4195,26.7173);
			glTexCoord2f(0,1); 
			glVertex3f(-13.2929,-3.7261,24.8814);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-11.7048,-2.7327,27.6427);
			glTexCoord2f(1,0); 
			glVertex3f(-10.3556,-2.4062,29.5636);
			glTexCoord2f(1,1); 
			glVertex3f(-10.7881,-3.0938,28.5454);
			glTexCoord2f(0,1); 
			glVertex3f(-12.0666,-3.4195,26.7173);
		glEnd();
		glDisable(GL_TEXTURE_2D);

			glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-10.3556,-2.4062,29.5636);
			glTexCoord2f(1,0); 
			glVertex3f(-8.9574,-2.0678,31.455);
			glTexCoord2f(1,1); 
			glVertex3f(-9.4796,-2.7649,30.3253);
			glTexCoord2f(0,1); 
			glVertex3f(-10.7881,-3.0938,28.5454);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		
				glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-8.9574,-2.0678,31.455);
			glTexCoord2f(1,0); 
			glVertex3f(-7.5147,-1.7186,33.3106);
			glTexCoord2f(1,1); 
			glVertex3f(-8.1315,-2.4264,32.0735);
			glTexCoord2f(0,1); 
			glVertex3f(-9.4796,-2.7649,30.3253);
		glEnd();
		glDisable(GL_TEXTURE_2D);

					glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-7.5147,-1.7186,33.3106);
			glTexCoord2f(1,0); 
			glVertex3f(-6.0282,-1.3589,35.1293);
			glTexCoord2f(1,1); 
			glVertex3f(-6.7445,-2.0785,33.7888);
			glTexCoord2f(0,1); 
			glVertex3f(-8.1315,-2.4264,32.0735);
		glEnd();
		glDisable(GL_TEXTURE_2D);
		
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);			
			glTexCoord2f(0,0); 
			glVertex3f(-6.0282,-1.3589,35.1293);
			glTexCoord2f(1,0); 
			glVertex3f(-4.4989,-0.9888,36.9101);
			glTexCoord2f(1,1); 
			glVertex3f(-5.3194,-1.7214,35.4705);
			glTexCoord2f(0,1); 
			glVertex3f(-6.7445,-2.0785,33.7888);
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	//--------------------------column: 1
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.705,-5.2887,13.2596);
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-18.583,-6.6951,14.5753);
		glVertex3f(-19.3978,-6.6308,12.9811);
		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-17.7613,-6.5654,16.2535);
		glVertex3f(-18.583,-6.6951,14.5753);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-16.926,-6.3416,17.7525);
		glVertex3f(-17.7613,-6.5654,16.2535);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-16.0612,-6.145,19.2105);
		glVertex3f(-16.926,-6.3416,17.7525);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-15.1335,-5.8648,20.7555);
		glVertex3f(-16.0612,-6.145,19.2105);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-14.1947,-5.6165,22.2283);
		glVertex3f(-15.1335,-5.8648,20.7555);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-13.1743,-5.3434,23.7565);
		glVertex3f(-14.1947,-5.6165,22.2283);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-12.1293,-5.065,25.2601);
		glVertex3f(-13.1743,-5.3434,23.7565);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-11.0822,-4.7881,26.706);
		glVertex3f(-12.1293,-5.065,25.2601);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-10.0079,-4.5048,28.1302);
		glVertex3f(-11.0822,-4.7881,26.706);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-8.907,-4.2149,29.5321);
		glVertex3f(-10.0079,-4.5048,28.1302);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-5.3194,-1.7214,35.4705);
		glVertex3f(-7.78,-3.9188,30.9111);
		glVertex3f(-8.907,-4.2149,29.5321);
	}glEnd();
	//--------------------------column: 2
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-19.3978,-6.6308,12.9821);
		glTexCoord2f(1,0); 
		glVertex3f(-18.583,-6.6951,14.5763);
		glTexCoord2f(1,1); 
		glVertex3f(-18.4077,-8.1401,13.6097);
		glTexCoord2f(0,1); 
		glVertex3f(-19.1521,-7.715,12.5188);
	glEnd();
	glDisable(GL_TEXTURE_2D);

	
		glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-18.583,-6.6951,14.5763);
		glTexCoord2f(1,0); 
		glVertex3f(-17.7613,-6.5654,16.2545);
		glTexCoord2f(1,1); 
		glVertex3f(-17.7381,-8.053,15.0342);
		glTexCoord2f(0,1); 
		glVertex3f(-18.4077,-8.1401,13.6097);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	
			glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-17.7613,-6.5654,16.2545);
		glTexCoord2f(1,0); 
		glVertex3f(-16.926,-6.3416,17.7535);
		glTexCoord2f(1,1); 
		glVertex3f(-17.0562,-7.8517,16.2332);
		glTexCoord2f(0,1); 
		glVertex3f(-17.7381,-8.053,15.0342);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	
				glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-16.926,-6.3416,17.7535);
		glTexCoord2f(1,0); 
		glVertex3f(-16.0612,-6.145,19.2115);
		glTexCoord2f(1,1); 
		glVertex3f(-16.347,-7.6192,17.4585);
		glTexCoord2f(0,1); 
		glVertex3f(-17.0562,-7.8517,16.2332);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	
				glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-16.0612,-6.145,19.2115);
		glTexCoord2f(1,0); 
		glVertex3f(-15.1335,-5.8648,20.7565);
		glTexCoord2f(1,1); 
		glVertex3f(-15.5995,-7.3933,18.6798);
		glTexCoord2f(0,1); 
		glVertex3f(-16.347,-7.6192,17.4585);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	
				glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-15.1335,-5.8648,20.7565);
		glTexCoord2f(1,0); 
		glVertex3f(-14.1947,-5.6165,22.2293);
		glTexCoord2f(1,1); 
		glVertex3f(-14.8388,-7.1544,19.8917);
		glTexCoord2f(0,1); 
		glVertex3f(-15.5995,-7.3933,18.6798);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	
				glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-14.1947,-5.6165,22.2293);
		glTexCoord2f(1,0); 
		glVertex3f(-13.1743,-5.3434,23.7575);
		glTexCoord2f(1,1); 
		glVertex3f(-13.9911,-6.8918,21.181);
		glTexCoord2f(0,1); 
		glVertex3f(-14.8388,-7.1544,19.8917);
	glEnd();
	glDisable(GL_TEXTURE_2D);

					glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-13.1743,-5.3434,23.7575);
		glTexCoord2f(1,0); 
		glVertex3f(-12.1293,-5.065,25.2611);
		glTexCoord2f(1,1); 
		glVertex3f(-13.1265,-6.6248,22.4554);
		glTexCoord2f(0,1); 
		glVertex3f(-13.9911,-6.8918,21.181);
	glEnd();
	glDisable(GL_TEXTURE_2D);

						glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-12.1293,-5.065,25.2611);
		glTexCoord2f(1,0); 
		glVertex3f(-11.0822,-4.7881,26.707);
		glTexCoord2f(1,1); 
		glVertex3f(-12.2784,-6.3665,23.6626);
		glTexCoord2f(0,1); 
		glVertex3f(-13.1265,-6.6248,22.4554);
	glEnd();
	glDisable(GL_TEXTURE_2D);

							glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-11.0822,-4.7881,26.707);
		glTexCoord2f(1,0); 
		glVertex3f(-10.0079,-4.5048,28.1312);
		glTexCoord2f(1,1); 
		glVertex3f(-11.4115,-6.1036,24.8552);
		glTexCoord2f(0,1); 
		glVertex3f(-12.2784,-6.3665,23.6626);
	glEnd();
	glDisable(GL_TEXTURE_2D);

								glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-10.0079,-4.5048,28.1312);
		glTexCoord2f(1,0); 
		glVertex3f(-8.907,-4.2149,29.5331);
		glTexCoord2f(1,1); 
		glVertex3f(-10.5263,-5.8363,26.0327);
		glTexCoord2f(0,1); 
		glVertex3f(-11.4115,-6.1036,24.8552);
	glEnd();
	glDisable(GL_TEXTURE_2D);

									glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);			
		glTexCoord2f(0,0); 
		glVertex3f(-8.907,-4.2149,29.5331);
		glTexCoord2f(1,0); 
		glVertex3f(-7.78,-3.9188,30.9121);
		glTexCoord2f(1,1); 
		glVertex3f(-9.6231,-5.5648,27.1946);
		glTexCoord2f(0,1); 
		glVertex3f(-10.5263,-5.8363,26.0327);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	//--------------------------column: 3
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.1521,-7.715,12.5178);
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-18.8067,-9.2603,11.3508);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-18.3082,-9.4045,12.211);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-17.8175,-9.3855,13.2702);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-17.3158,-9.2325,14.1256);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-16.8041,-9.0877,14.9583);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-16.2556,-8.9184,15.8423);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-15.7049,-8.7466,16.7121);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-15.0636,-8.5511,17.6805);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-14.4293,-8.3589,18.6155);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-13.8152,-8.1749,19.4974);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-13.1905,-7.9883,20.3711);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-9.6231,-5.5648,27.1936);
		glVertex3f(-12.0487,-7.7309,21.7581);
		glVertex3f(-12.5557,-7.7992,21.2364);
	}glEnd();
	//--------------------------column: 4
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.8067,-9.2603,11.3508);
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-18.6771,-9.8512,10.6575);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-18.2568,-9.9142,11.4855);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-17.8288,-9.9066,12.4349);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-17.3864,-9.7639,13.1925);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.935,-9.62,13.9435);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-16.4663,-9.5006,14.6469);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-16.0035,-9.3536,15.3746);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-15.4121,-9.1349,16.3398);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-14.8458,-8.9545,17.19);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-14.308,-8.7849,17.9786);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-13.7621,-8.6134,18.7611);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-12.0487,-7.7309,21.7581);
		glVertex3f(-12.7191,-8.3296,20.1298);
		glVertex3f(-13.2081,-8.44,19.537);
	}glEnd();
	//--------------------------column: 5
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.6771,-9.8512,10.6575);
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-18.5843,-10.2815,9.9958);
		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-18.2294,-10.3563,10.6732);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.8686,-10.3674,11.4891);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-17.4943,-10.2515,12.1212);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-17.1136,-10.1341,12.7497);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-16.706,-10.0082,13.4056);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-16.3133,-9.8879,14.0259);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-15.8234,-9.7356,14.7801);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-15.3416,-9.5866,15.5088);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-14.8888,-9.4478,16.1802);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-14.4301,-9.3077,16.8473);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-12.7191,-8.3296,20.1298);
		glVertex3f(-13.5314,-9.0551,18.0755);
		glVertex3f(-13.9657,-9.1661,17.5097);
	}glEnd();
	//--------------------------column: 6
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.5843,-10.2815,9.9958);
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-18.499,-10.6862,9.1601);		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-18.2285,-10.7262,9.7639);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.9422,-10.7662,10.3985);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.6456,-10.6937,10.8742);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-17.3451,-10.6202,11.3479);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-17.0227,-10.5409,11.8462);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-16.7151,-10.4657,12.3146);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-16.3229,-10.368,12.8998);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-15.9397,-10.2727,13.464);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-15.5839,-10.1849,13.9798);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-15.2246,-10.0962,14.4929);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-13.5314,-9.0551,18.0755);
		glVertex3f(-14.4958,-9.9163,15.5107);
		glVertex3f(-14.8619,-10.0066,15.0032);
	}glEnd();
	//--------------------------column: 7
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.499,-10.6862,9.1601);		
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.4483,-10.9382,8.3984);
		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-18.2434,-11.0054,8.8216);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-18.0265,-11.0762,9.2643);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.8032,-11.0507,9.5898);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.5778,-11.0248,9.9141);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-17.3354,-10.9963,10.2582);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-17.1066,-10.9695,10.5796);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-16.8084,-10.9331,10.9924);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-16.5188,-10.8976,11.3896);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-15.2246,-10.0962,14.4929);		
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-16.2534,-10.8652,11.7498);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-15.9862,-10.8324,12.1085);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-14.4958,-9.9163,15.5107);
		glVertex3f(-15.4185,-10.7403,12.9037);
		glVertex3f(-15.7172,-10.7991,12.4659);
	}glEnd();
	//--------------------------column: 8
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.4483,-10.9382,8.3984);
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.4167,-11.1796,7.1859);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.307,-11.3658,7.4421);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-18.1473,-11.3509,7.6847);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-18.0166,-11.3953,7.7537);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-17.8899,-11.4432,7.8332);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-17.7586,-11.4995,7.9146);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.6367,-11.5514,8.0093);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.4818,-11.6289,8.1123);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.3365,-11.7067,8.2133);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.2534,-10.8652,11.7498);		
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-17.2203,-11.7896,8.2613);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-17.1235,-11.8889,8.2483);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-15.4185,-10.7403,12.9037);
		glVertex3f(-16.8775,-12.0432,8.4274);
		glVertex3f(-16.991,-11.9575,8.3693);
	}glEnd();
	//--------------------------column: 9
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.4167,-11.1796,7.1859);
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.3861,-11.3106,5.972);
		glVertex3f(-18.4215,-11.1571,5.9751);
		
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.3534,-11.4654,5.972);
		glVertex3f(-18.3861,-11.3106,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-18.2775,-11.5914,5.972);
		glVertex3f(-18.3534,-11.4654,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-18.22,-11.6869,5.972);
		glVertex3f(-18.2775,-11.5914,5.972);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-18.1503,-11.8028,5.972);
		glVertex3f(-18.22,-11.6869,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-18.0861,-11.9093,5.972);
		glVertex3f(-18.1503,-11.8028,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.9975,-12.0564,5.972);
		glVertex3f(-18.0861,-11.9093,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.9127,-12.1973,5.972);
		glVertex3f(-17.9975,-12.0564,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.8373,-12.3225,5.972);
		glVertex3f(-17.9127,-12.1973,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-17.7619,-12.4478,5.972);
		glVertex3f(-17.8373,-12.3225,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-17.6864,-12.573,5.972);
		glVertex3f(-17.7619,-12.4478,5.972);
	}glEnd();
	
	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-16.8775,-12.0432,8.4274);
		glVertex3f(-17.611,-12.6983,5.972);
		glVertex3f(-17.6864,-12.573,5.972);
	}glEnd();
	//--------------------END
}

void drawPetal_1()
{
	//texid1 =LoadBitmap("textures/lotus_temple-1-petal-2/translucent_glass.bmp");
	//texid1 =LoadBitmap("textures/red_sandstone.bmp");
	texid1 =LoadBitmap("red_sandstone.bmp");
	//texid1 =LoadBitmap("translucent_glass.bmp");
	/*drawTexture(texid1,
				0,0,0,
				0,0,30,
				30,0,30,
				30,0,0
				);*/

	for(int i=0; i<9; i++)
	{
		glPushMatrix();{					
			glRotatef(i*40,0,0,1);
			//drawPetal_1_rightback();
			drawPetal_1_rightback_texture();
			
			
			
		}glPopMatrix();
	
	}
}
void drawUpperPetals()
{
	//drawInnerCylinder();
	//drawPetal_top();
	//drawPetal_0();
	//drawPetal_1();
	//drawPetal_2();
	drawPetal_3();
	
	
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^


void keyboardListener(unsigned char key, int x,int y){
	switch(key){

		case '1':
			drawgrid=1-drawgrid;
			break;

		default:
			break;
	}
}


void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			cameraHeight -= 3.0;
			break;
		case GLUT_KEY_UP:		// up arrow key
			cameraHeight += 3.0;
			break;

		case GLUT_KEY_RIGHT:
			cameraAngle += 0.03;
			break;
		case GLUT_KEY_LEFT:
			cameraAngle -= 0.03;
			break;

		case GLUT_KEY_PAGE_UP:
			break;
		case GLUT_KEY_PAGE_DOWN:
			break;

		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
			break;
		case GLUT_KEY_END:
			break;

		default:
			break;
	}
}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				drawaxes=1-drawaxes;
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}



void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?

	//gluLookAt(100,100,100,	0,0,0,	0,0,1);
	gluLookAt(100*cos(cameraAngle), 100*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	//gluLookAt(0,-1,150,	0,0,0,	0,0,1);


	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);


	/****************************
	/ Add your objects from here
	****************************/
	//add objects=====================================================================================================
	drawAxes();
	drawGrid();
	glColor3f(1,0,0);
	//drawsphere(20,20,20);
	//glRotatef(180,1,0,0);
	//drawsphere(20,20,20);
	//drawss();
	//clipping example
	double coef[4];
	coef[0] = -1;	//-1.x
	coef[1] = 0;	//0.y
	coef[2] = 0;	//0.z
	coef[3] = 10;	//10
	//standard format:: ax + by + cz + d >= 0
	/// the cutting plane equation: x = 10
	/// we will keep the points with
	//		x <= 10
	//OR	-1.x + 0.y + 1.z + 10 >= 0
	glClipPlane(GL_CLIP_PLANE0,coef);

	//now we enable the clip plane
	
	//glEnable(GL_CLIP_PLANE0);{
		//drawsphere(20,20,20);
		//glRotatef(180,1,0,0);
		//drawsphere(20,20,20);
	//}glDisable(GL_CLIP_PLANE0);
	
	drawUpperPetals();
	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}



void animate(){
	angle+=0.05;
	//codes for any changes in Models, Camera
	glutPostRedisplay();
}

void init(){
	//initialize global variable
	//codes for initialization
	//parser("coordinates/petal_2.txt");
	drawgrid=0;
	drawaxes=1;
	cameraHeight=100.0;
	cameraAngle=1.0;
	angle=0;

	//clear the screen
	glClearColor(0,0,0,0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(80,	1,	1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){
	glutInit(&argc,argv);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
