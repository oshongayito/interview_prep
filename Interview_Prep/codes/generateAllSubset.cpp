#include<iostream>
#include<vector>

using namespace std;

vector< vector<int>> all_subsets(vector<int>src)
{
    vector< vector<int>>ret;

    int n=src.size();
    int N=1<<n; // the size will be 2^n

    for(int i=0;i<N;i++)
    {
        vector<int>set;
        for(int j=0;j<n;j++)
        {
            if(i&(1<<j))set.push_back(src[j]);
        }
        ret.push_back(set);
    }
    return ret;
}
int main()
{
    vector<int>src;
    vector< vector<int>>all;


    src.push_back(1);
    src.push_back(2);
    src.push_back(3);

    all=all_subsets(src);

    for(int i=0;i<all.size();i++)
    {
        for(int j=0;j<all[i].size();j++)
        {
            cout<<all[i][j];
        }
        cout<<endl;
    }

    return 0;
}
