
#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#define pi (2*acos(0.0))
#define pb push_back

using namespace std;


int  main()
{

    long long int ax,ay,bx,by,cx,cy,dx,dy,a,b,c,x,y,z,i,j,k,t,area;

    cin>>t;
    for(i=1;i<=t;i++){
    cin>>ax>>ay>>bx>>by>>cx>>cy;
    dx = (ax+cx-bx);
    dy = (ay+cy-by);


    area= ((ax-bx)*dy - (ay-by)*dx - ay*(ax-bx) + ax*(ay-by));
    if(area<0)area*=-1;

    cout<<"Case "<<i<<": "<<dx<<" "<<dy<<" "<<area<<endl;


    }
	return 0;
}
