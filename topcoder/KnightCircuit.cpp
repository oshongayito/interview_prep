#include <set>
#include <map>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>
#include <vector>
#include <string>
#include <cctype>
#include <cstdio>
#include <iomanip>
#include <sstream>
#include <cstdlib>
#include <climits>
#include <complex>
#include <numeric>
#include <valarray>
#include <iostream>
#include <string.h>
#include <algorithm>
using namespace std;



class KnightCircuit {
	public:
	long long maxSize(int w, int h, int a, int b) {
		
	}
};






// BEGIN CUT HERE
namespace moj_harness {
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				cerr << "Illegal input! Test case " << casenum << " does not exist." << endl;
			}
			return;
		}
		
		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}
		
		if (total == 0) {
			cerr << "No test cases run." << endl;
		} else if (correct < total) {
			cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << endl;
		} else {
			cerr << "All " << total << " tests passed!" << endl;
		}
	}
	
	int verify_case(int casenum, const long long &expected, const long long &received, clock_t elapsed) { 
		cerr << "Example " << casenum << "... "; 
		
		string verdict;
		vector<string> info;
		char buf[100];
		
		if (elapsed > CLOCKS_PER_SEC / 200) {
			sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}
		
		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}
		
		cerr << verdict;
		if (!info.empty()) {
			cerr << " (";
			for (int i=0; i<(int)info.size(); ++i) {
				if (i > 0) cerr << ", ";
				cerr << info[i];
			}
			cerr << ")";
		}
		cerr << endl;
		
		if (verdict == "FAILED") {
			cerr << "    Expected: " << expected << endl; 
			cerr << "    Received: " << received << endl; 
		}
		
		return verdict == "PASSED";
	}

	int run_test_case(int casenum) {
		switch (casenum) {
		case 0: {
			int w                     = 1;
			int h                     = 1;
			int a                     = 2;
			int b                     = 1;
			long long expected__      = 1;

			clock_t start__           = clock();
			long long received__      = KnightCircuit().maxSize(w, h, a, b);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 1: {
			int w                     = 3;
			int h                     = 20;
			int a                     = 1;
			int b                     = 3;
			long long expected__      = 11;

			clock_t start__           = clock();
			long long received__      = KnightCircuit().maxSize(w, h, a, b);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 2: {
			int w                     = 100000;
			int h                     = 100000;
			int a                     = 1;
			int b                     = 2;
			long long expected__      = 10000000000LL;

			clock_t start__           = clock();
			long long received__      = KnightCircuit().maxSize(w, h, a, b);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 3: {
			int w                     = 3;
			int h                     = 3;
			int a                     = 1;
			int b                     = 2;
			long long expected__      = 8;

			clock_t start__           = clock();
			long long received__      = KnightCircuit().maxSize(w, h, a, b);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 4: {
			int w                     = 30;
			int h                     = 30;
			int a                     = 8;
			int b                     = 4;
			long long expected__      = 64;

			clock_t start__           = clock();
			long long received__      = KnightCircuit().maxSize(w, h, a, b);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 5: {
			int w                     = 32;
			int h                     = 34;
			int a                     = 6;
			int b                     = 2;
			long long expected__      = 136;

			clock_t start__           = clock();
			long long received__      = KnightCircuit().maxSize(w, h, a, b);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 6: {
			int w                     = ;
			int h                     = ;
			int a                     = ;
			int b                     = ;
			long long expected__      = ;

			clock_t start__           = clock();
			long long received__      = KnightCircuit().maxSize(w, h, a, b);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 7: {
			int w                     = ;
			int h                     = ;
			int a                     = ;
			int b                     = ;
			long long expected__      = ;

			clock_t start__           = clock();
			long long received__      = KnightCircuit().maxSize(w, h, a, b);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 8: {
			int w                     = ;
			int h                     = ;
			int a                     = ;
			int b                     = ;
			long long expected__      = ;

			clock_t start__           = clock();
			long long received__      = KnightCircuit().maxSize(w, h, a, b);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}

int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(atoi(argv[i]));
	}
}
// END CUT HERE
