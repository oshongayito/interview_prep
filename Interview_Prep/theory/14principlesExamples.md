### Why Amazon? ###
------------------------------

**Answer**

1. Back in my country, I worked in a startup to solve a problem of online payment. However, our customer base was limited to only our country’s people. Company like Amazon have a global scope. The works that amazon employees do, impacts lives of millions.
2.  Along with having meaningful and large impact on customers, I also intend to grow my skillset as a software engineer. At amazon the most talented people’s are working and developing complex and scalable systems which allows the engineers gain hands-on experiences and being pioneer in their respective fields.
3. I am a good customer of amazon and like their products (e-commerce, prime video, kindle etc.). They brought unique solutions to some complex problems: made e-commerce easier than ever, kindle: just like the physical books, aws: cutting off the loads of managing warehouses. I know amazon will keep doing amazing things and bring even more positive impacts on people’s lives. I want to be a part of the existing and next big things they are about to do.
4. Finally, I believe that when someone is given the freedom to take ownership, he/she can deliver to his level best. Amazon expects it’s employees to take full ownership of one’s job and religiously preaches this. Therefore, I think, amazon is the right place for me to work.


### Why Huawei? ###
-----------------------

** Answer **

1. Back in my country, I worked in a startup to solve a problem of online payment. However, our customer base was limited to only our country’s people. Company like Huawei have a global scope. The works that huawei employees do, impacts lives of millions.
2.  Along with having meaningful and large impact on customers, I also intend to grow my skillset as a software engineer. At huawei the most talented people’s are working and developing complex and scalable systems which allows the engineers gain hands-on experiences and being pioneer in their respective fields.
3. I guess the GaussDB product is still in it's growth phase. and lots of R&D works are going on right now. Hence, there are lots of opportunities to contribute and experience the underlying platform development. 

### What is your biggest weakness? ###
-----------------------------------------
**Answer 1: ** I am not socially that extrovert. Although, I am open and friendly with the people I know, I do not approach to strangers that much. This limits my chances to explore or expand my network. For example, in a recent conference, I didn't socialize with people that much. However, one of my lab mate in the previous conference socialized with people, and also got a job interview from there. I am trying to improve this skill. Now in my office I try to eat my lunch with others and paticipate in the chats there.

**Answer 2: ** Being caught up in the details. Sometimes while solving very complicated problems, I get caught up in the details. This actually makes it hard to solve the problem and also takes a lot of time. **(Example)** Recently I worked on a problem to make a specific SELECT type transaction two-phase aware. The problem was complicated because there were no such thing in the existing codebase for the SELECT operations, and also the codebase was very new for me. To solve this problem I looked at the existing SQL queries (drop, update) which were already two-phase aware. I found that there were no single API call to achieve this rather there were multiple parts which were used in different positions in the codepath. Therefore, I had to take those specific parts and plug those in my own codepath. However, while fitting this in my purpose, I made several mistakes by focusing on some parts which were not relevant for my purpose at all. I spent quite a bit of time on those parts. At one point I took a step back and looked at the bigger picture to connect the different parts and trying to understand which were relevant and which not. This allowed me to tackle the problem in a better way. The learning for me was that it's very easy to get caught up in the details specifically for complicated problems. However, it's a very good skill to always take a moment to step back and look at the bigger picture to realign the directions.

### What is your biggest strengths? ###

**Answer 1:** Taking ownership and responsibility of my tasks. Examples: 1. iPay admin panel (development, testing, deployment, customer support). 2. XLog, and new xact related issues in Huawei.

**Anser 2:** Embracing new challenges, and technologies. Examples: 1. Coming to Canada, getting into Bioinformatics research. 2. Joining Huawei, learning and developing distributes system, and database.

**Question: ** Talk about a time you were criticized and how you handled it

**Answer 1:** **(Situation)** Recently, I was criticized for a binary classification modeling problem. Originally the dataset was multi class dataset. **(Task)** I converted the dataset into binary by taking one class in one group, and all the others in another group. I did this using only one class. **(Action)** My supervisor criticized me for not considering all the binary grouping.  **(Result)** There was a reason for what I didn't do the all grouping. The reason is to divide the groups in case control. However, I listened to him, and apologized for not going for the all combinations.


** Answer 2: ** **(Situation)** Recently, I was criticized for not listening properly. My team lead gave me a task. I was struggling at some point, then he described one possible solution approach. However, I didn't directly followd his approach in the beginning, as I was trying to investigate one of my own solution which was bugging me. **(Task)** The task was to create a new internal transaction which will be triggered by another transaction. Currently there were no such thing in the codebase. **(Action)** I acually tried my solution and then found that it's not working. After that I actually followed my supervisor's solution. At that point, he said that he already mentioned it earlier and I should have followed him at that point. **(Result/Impact,Lesson)** Although following my idea is not a bad idea, I think I should have made my stance clear to my supervisor. I could have told him why I want to try my approach first. That could have lead to a fruitful / constructive discussion. I learned that I need to improve my communication. After that event I always try to discuss my solution approach in detail with my supervisor before implementing them.


### Questions to Ask ###
--------------------------
1. What a typical day at amazon look like?
2. How is someone appreciated if he/she delivers something amzaing or exceptional?
3. How Amazon makes sure that everyone is delivering their level best? Is there any training or something like that?
4. How long is the sprint?
5. Is there daily scrum?


## 14 Principles ##

### 1. Customer Obsession ###
----------------
**Question:**
 Give me an example of a time when you did not meet a client’s expectation. What happened, and how did you attempt to rectify the situation? What did you learn from it?

**Answer:**

1. Caries Deadline. **(Situation)** At the end of March, my prof told me that dentistry department wants to collaborate a project with us. **(Task)** However, they wanted the project to be completed within a month. I had a less busy time window at that time, and that's why I committed to work on that project. However, after working for a week on that project, another paper review came unexpectedly with a lot of adjustments. I had the review under consideration, however I couldn't imagine that it would be that large. **(Action)** I talked with dentistry departments lab and told them I need to postpone the work for next weeks and described them the whole situation. They allowed me to do so. After I was done with the paper review, I started to work on the dentistry departments project. This time it was in my mind that I have to make up for the delay. That's why I started working full time on that project. **(Results)** Within two weeks I came up with publishable results with some nicely polished visualizations (plots). Although I was one week late to deliver, I delivered more than they expected. However, I learned a very important lesson: I should have estimated the large workload from the review beforehand while giving commitment to the customers.

2. Creative Circle website deadline. 


**Question:** Tell the story of last time you had to apologize to someone.

**Answer:** 
**(Situation)** After the completion of algorithm course project, the course instructor told me that the project is publishable, if polished. I let my thesis supervisor know about this and he gave me permission to work on the project. **(Task)** I started working on the project and went almost 90% after two months. It was time to write the paper, and submit it to a conference. At that time my supervisor told me that he wants his name in the paper too. The alg course instructor said that it's not possible, because in alg paper the author names goes alphabetically, and evereone's contributions are thought to be equal. My mistake was that I didn't know this special rule, and also I thought that supervisor's name goes automatically (which is actually a very common rule in our university). **(Action)** Because of this conflict the professor halted the project. I apologized to both of the professor for not being conscious enough about this situation beforehand. I should have made clear about my thesis supervisor's expectations. **(Result)** The project halted, however I learned that it's always the best to make sure that the people who you are collaborating with are on the same page. 


### 2. Ownership ###
----------------

** Questions ** Tell me about a time when you had to work on a project with unclear responsibilities

**Answer 1:**

Online algorithm project: team project. however my partner was sick and couldn't contribute at that moment. I stepped up did as much as I can: studied related papers and summarized them, designed and analyzed algorithm advice complexity etc. Later on she joined and worked


**Answer 2:** 
CN/DN merge project: Team project. The project was half done by some other team from China. They left the project to start something else. We took over that. However, there were no details on what needs to be done. Therefore, we were unaware about our responsibilities. We tackled the project in an explorative manner. At first, we clarified our goals. Then we started testing the project with very basic SQL operations like: SELECT, INSERT, UPDATE, two-phase commit, DDL commands etc. We also ran the TPC-C benchmarks to identify concurrency issues. Once we had a rough idea about what needs to be done, my supervisor made a list of todos and also made a time/resource estimation for the project. 


 ** Question** Tell me about a time when you had to leave a task unfinished.

**Answer 1:**

 ** Creative Circle website. **
**(Situation)** Me with some of my friends started an organization to organize math olympiads and science workshops in my home town Bhola, in Bangladesh. We have been organizing programs since 2011. **(Task)** However, we don't have any website of our organization so far. I planned to build a website few months ago. 
As, we all were busy with our own tasks, (study, jobs etc.), we planned to outsource the project. **(Action)** I talked with a guy and discussed with my cofounders to go on with the project. However, after they finished the work, we didn't like the output. It was not what we were expecting. **(Result)** We paid them a fraction of the money, but didn't launch the website. After that we planned to do the job by ourselves. However, 3 months have been passed, and we couldn't finish the job yet. I am really disappointed about this. And I hope once, I am done with my thesis, I will get a small window. And I will finish the project at that time before our next events of Creative Circle.

** Answer 2: **
MSc thesis publication (IBD disease, gene expression, microbiome data, multi-view learning approach, Supervised Deep Canonical Correlation Analysis)

**Question** Do you collaborate well?

**Answer:**

Project with dentistry department, they have little ideas about computational tasks, therefore I had to visualize the results as simply as possible. I even had to describe what classification means and what it does. I had to describe what accuracy and area under curves mean. Even I had to describe correlations and different classification methods such as random forests and logistic regression in simple words.




### 3. Invent and Simplify ###
----------------------------

** Question **  Tell me about a time when you invented something.

**Answer 1:**
Two-phase commit for select type transactions. Actually making a plugin function for two-phase commit.

** Answer 2:**
New internal subtransaction with xid from DN. 
Challenges: 
1. no existing solution
2. Asserts preventing calls
3. No call for RecordTransactionCommit
4. Resource owner has to be different otherwise internal commit will remove the resourceowner


** Question ** Tell me about a time when you gave a simple solution to a complex problem.

**Answer:**

1. Using youtube APIs in VidSplit project instead of designing our own video player or using some anularjs libraries.

2. Using a weighted sum of CCA loss and cros entropy loss (classification loss) of classification in DCCA(S), instead of modifying the CCA loss function to include classification (as it was more complex for me)

3. Using subtransaction instead of regular transaciton to create a new type of internal transaction which will have new xid from gtm.



### 4. Are right a lot ###
----------------------------
**Question** Tell about a time when u were wrong.

**Answer:**

1. Algo project. **(Situation)** In my algorithm course project, I was doing analysis on the number of advice required to emulate the optimal algorithm. We had an upper and lower bound on the number of customers per day. **(Task)** To calculate the advice bits, I had to divide the range between groups. I divided them based on the middle point at first. However, when I plotted the competitive ratio of the different strategies, it turned out that the mid points strategy gives similar or worse results compared to using no advice at all. Then I realized I was doing something very wrong. It turned out that the range had an uneven balance, as the cost of buying a product and or not buying or buying less amount was different. **(Action)** Then, I tweaked the strategy by taking the ratio of upper bound and lower bound instead of the mid point. **(Result)** This gave us better results. I learned that, it is always good to verify your assumptions with little testing than investing a lot of time.

2. XLog buffer record with data or not. **(Situation)** Recently, I developed a feature to record a xlog entry for a deleted buffers (moved buffer). The xlog entry should be capable of reinitializing the buffer with zero values. Therefore, we didn't need to store the original data segment of the buffer in the xlog record. **(Task)** My supervisor asked whether I was saving the data or not. **(Action)** I went through the code, and did a quick skim through one of the existing API of recording a buffer in xlog. From the quick skim in the code, I found that for our case the data will not be saved. Then I told my supervisor about that. ** (Result)** However, after that feature was deployed, my supervisor one day debugged the code and found that the data was storing with the record. Actually, I was wrong in the beginning. **(Lesson)** I learned that it's always better to debug and make sure that the feature is behaving as expected.


### 5. Learn and be Curious ###
------------------------------
**Question: ** Tell me about a time when you had a change in paradigm, why you changed the paradigm and how did you cope with that?

**Answer:** 

1. Moving to a new place like Canada, was a huge paradigm shift for me. I faced and still facing a lot of challenges. I am learning new things almost everyday. Back home, I didn’t have to do laundry, cook, face -30 degree. But here I am on my own. I am learning and adopting myself with the challenges. And also giving my best to the study and my career.

2. Moving to bioinformatics research from pure computer science research. I had to learn many biological things read the very basics. I realized that in biology you need to have detailed and step by step interpretable analysis. Here interpretation matters more than good results, as at the end of the day you will build your medications based on the details. I struggled a lot in the beginning. However, I realized that I had to take this pain and learn, because in real world every tasks will have many new things like this. 

3. Moving to database. Huge paradigm shift.


### 6. Hire and Develop the Best ###
------------------------------
**Question:** Tell me about a time when you mentored someone.

**Answer:**
**(Situation) ** A few months ago, I mentored a grade 12 student (Angelo) on his science project for Manitoba Science Symposium. **(Task)** The symposium was related to biology. As my thesis is on bioinformatics, I told him to do something related to bioinformatics. Angelo had very little knowledge on Bioinformatics, and programming. **(Action)** I came up with a project plan to work with a publicly available dataset to do prediction of asthma disease based on some features of individuals. I arranged weekly or bi-weekly meetings to monitor the progress and also guide him to the right direction. I gave him some lectures on the basics of machine learning (random forest, SVM, linear reression), python programming, evaluation metrics etc. **(Result)** Angelo participated in the MSSS 2019 competition, and won the best individual biology award.


### 7. Insist on Highest Standards ###
------------------------------
**Question:** Tell me about a time when you couldn’t meet your own expectations on a project.

**Answer:**
**(Situation)** I will not talk about a project, but about my masters program. I think I should have done better than what I achieved so far, although it is not finished yet. **(Task)** In my bachelor, my cgpa was not excellent. So, I wanted to cover for this in masters. Hence, I focused on my study really hard from the beginning. I tried my level best to focus on ths study only. **(Action)** I attended all the classes, tried to  do novel works in the course projects. **(Result)** It paid off though. My cgpa is better than my BSC. I have published 2 articles so far. However, I could not get satisfactory results in my thesis. I tried different DNN models for my thesis. The dataset that I worked on was really small for DNN. My fault was that I relied on one dataset only, and didn't try to find a better dataset myself.


### 8. Think Big ###
------------------------------
**Qustion:** Tell me about a time when you went way beyond the scope of the project and delivered.

**Answer 1:**

Algo project, parallel project (published).


** Answer 2:**
Investigating internal transaction for split. I was asked to investigate and propose solutions. However, besides doing investigation and proposing the solutions, I also delivered the fully working solution within a week.


### 9. Bias for Action ###
----------------------------

**Qustion:** Describe a time when you saw some problem and took the initiative to correct it rather than waiting for someone else to do it.

**Answer:** 
** Refactored the whole admin panel project in iPay ** 
**(Situation)** At iPay I developed the admin panel fron end. As it was a startup, everything was very fast paced. In every week, we had enough new features to implement or bugs to fix. Therefore, I had little time to refactor the code. **(Task & Action)** At the end of my time at iPay, I had to made the documentation of the project. A new member was assigned to hand over the project. So, along with working on regular features and bug fix, I mentored the new employee to take over the project. I also wrote the documentation. However, I knew that the code is not well factored which is a problem for the company. So I stepped up and managed extra time to refactor the whole code base from the beginning. **(Result)** Although it took some of my extra efforts, it paid off more, as it helped the new employee to understand and work on the project easily.


**Answer 2:**
Using SQL loop instead of python loop for test cases. Python loop to insert uses a lot of SQL inserts. This in turn takes a lot of time. However, SQL loop uses a single transaction and avoids a lot of overheads. This improved the run time of the test cases. Besides, it also made the log file look clean which was very helpful for analysis.


### 10. Frugality ###
----------------------------
**Question** Tell me about a time when you had to work with limited time or resources.

**Answer:**

1. At iPay, I was mainly working on the front end application. However, as it was a startup, there were not that much people in the company. As time passed by, we had several modules in our team than we could handle. And my team lead had to work in some other modules as well. At that time I had to step up and take responsibilites of her backend application well. I also worked on the weekly deployment, waited late hours till our modules were successfully deployed. When our compnay grew, more people joined at situation became easier. However, i learned that at difficult and situations like this we can use limited resource to achieve the goal as well.


### 11. Earn Trust ###
------------------------------
**Question:** Tell me about a time when you had to tell someone a harsh truth.

**Answer:**

1. **(Situation)** One of friend was staying at my place for few days, as he lost his job. He was then applying for some other positions. two months passed by but he didn't get any jobs. **(Task)** I was observing him on that time. He was only applying for jobs, but not trying to improve himself. **(Action)** Then one day I sat with him, and told him that he was not being serious about the job and passing time in idle. He should think carefully about what he wants in his career. He started to think about the job seriously at that time. **(Result)** After some days he figured out the main reason behind his failure is the lack of passion in the jobs he was looking for. Then he switched his track to teaching and found interest in that. He is now happy with his job,  self dependent and also helping his family.


### 12. Dive Deep ###
----------------------------
** Question** Give me two examples of when you did more than what was required in any job experience

1. iPay: helped in deployment and health checks even if it was the responsibility of the automation team

2. Algorithm course project. The requirements of the course project was to demonstrate that We have learned the concepts properly. However, I pushed myself to go beyond and deliver something novel. Even though the course was finished and the project was not related to my thesis work, I am managing my free time to get to the deepest level of this project and publish in a good algorithm conference.

3. Creating a new internal transaction for split. Goal was investigation and solution proposal. However, I implemented the fully working solution too which merged with the main code within 2 weeks.

**Questions: ** Tell me about something that you learnt recently in your role.

**Answer:**

1. Visualization project. I was planning to build a new interactive visualization method for high-dimensional multi-omics data. I searched for different technologies, and then found that Unity is the best thing to achieve my goal. However, I had very little knowledge on that. Therefore, I had to learn Unity from scratch. I did that in two weeks. I learned how to create a scene, how to create a 2D image from pixel values, how to interact with mouse clicks, how to zoom in/out etc. 

2. Creating subtransaction which are capable of getting new xid. Also learned about freespace map, two-phase commit, cn/dn merge.


### 13. Have Backbone; Disagree, and Commit ###
--------------------------------------------

**Question** Give me an example of a time you faced a conflict while working on a team. How did you handle that?

**Question** If your direct manager was instructing you to do something you disagreed with, how would you handle it?

**Answer:**

 **(Situation)** Submitting Parallel Paper for the fourth time during my thesis's climax time (also goes to disagree, but commit principle). **(Task)** It was end of February this year. I submitted a paper for 3 times previously, which my professor wanted me to submit for the fourth time. However, I did not like this proposal, because I had to focus on my thesis at that time and that project was not directly related to my thesis. However, my professor insisted me on this and assured me that it's gonna pay me off. **(Action)** Although, I was not happy, I committed and trusted my supervisor on this. I worked for next 3 weeks full time on that paper and submitted it. **(Results)** At the end of April, the paper got accepted at BMC journal. And it also turned out that with the modifications I did based on the comments of the reviewers, the paper became more relevant to my thesis topic. And so I ended up adding that to my thesis as well. 


### 14. Deliver Results ###
------------------------------
**Question:** What is the most difficult situation you have ever faced in your life? How did you handle it?

**Answer:**

1. Coming to canada and facing my first winter was the most difficult situation that I can remember at this moment. I came to winnipeg, which in late august, when the temperature was around 7 to 15 degrees. In dhaka it was always 20 to 30 degrees. Besides, my course started from September. I had to study. I had to cook by my own. I was far far away from family and friends. From october the temperature went below 0, and started to snow. I couldn't stay outside for more than 10 minutes. **Against all these odds I didn't compromise a little with my job: my study.** I studied even harder and secured A+ and A in my first two courses of that semester.


**Question:** Give me an example of a time when you were 75% of the way through a project, and you had to pivot strategy–how were you able to make that into a success story?

**Answer:** 

**(Situation)** Here I will consider 75% completion in terms of time not progress of the project. In my parallel computing course project, I planned to implement a GPU based parallel algorithm of LCS on real world DNA sequence data. I did the research, figured out our methodology, written proposal. **(Task)** However, at the end of the semester, when I was about to do the implementation, I tried to start coding on the lab machines. However, it turned out that the machines didn't support latest version of CUDA which I needed for my case. I tried several things and leanred that I had to take help from the technical team for the upgradation. However, I didn't have that much time in my hand. **(Action)** Then I tried to think if I can change my strategy or not. After some careful thinking and research I found that the CPU based parallel implementations are doable and no body did that for the specific algorithm I targeted. **(Result)** I went for MPI, OpenMP based CPU implementations of the algorithm and completed my project successfully. And also it was a novel work, I published it to BMC journal eventually.



**Question:** Give me an example of a time when you made a data-driven decision?

** Answer: ** Admin panel role, permission issue. At first it was okay. Then the number of roles, and activities increased in the production db. At that point the page was loading slowly (taking almost a minute to load which was problematic for the admin). I grouped the activities by their types and then only displayed the groups in the beginning. This improved timing significantly. 


**Question:** Tell me about a challenge you’ve recently faced in your role. How did you tackle it, what was the outcome, and what did you learn?





**Question:** Tell me about a setback that you faced recently.

** Answer:** Dentistry project. Not able to include in my thesis.



**Question:** Tell me about your greatest achievements.
MSc result, 3 publication (1 in BMC, 1 in JDR)








