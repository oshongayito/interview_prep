
#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#define pi (2*acos(0.0))

using namespace std;

int  main()
{
	int t,i;
	double x,y,z,a,b,c,p,q,r,A,B,C,h,area,res;
	cin>>t;
	for(i=1;i<=t;i++)
	{
		cin>>x>>y>>z;
		a = y+z;
		b = z+x;
		c = x+y;
		A = acos((b*b + c*c - a*a)/(2*b*c))*(180.0/pi);
		B = acos((a*a + c*c - b*b)/(2*a*c))*(180.0/pi);
		C = acos((a*a + b*b - c*c)/(2*a*b))*(180.0/pi);
		h = b* sin((C*pi)/180);
		area = (0.5)*a*h;
		p = (A/360)*(pi*x*x);
		q = (B/360)*(pi*y*y);
		r = (C/360)*(pi*z*z);
		res = area - (p+q+r) + 0.0000000001;
		printf("Case %d: %.10lf\n",i,res);
	}
	return 0;
}
