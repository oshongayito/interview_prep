#include<iostream>
#include<cstdio>
#include<algorithm>
#include<string>
#include<vector>
#include<utility>
#include<map>
#include<cmath>


using namespace std;

#define ll long long
#define pb push_back
#define md 1000000007


int main()
{
    ll x,y,z,i,j,k,ans,T;
     vector<ll> a;
    cin>>T;
    while(T--){
        a.resize(3);
    cin>>a[0]>>a[1]>>a[2];
    sort(a.begin(),a.end());
    ans = (((a[0]%md)*((a[1]-1)%md)))%md;
    ans = (ans*((a[2]-2)%md))%md;
    cout<<ans<<endl;
    //cout<<((((md-3)%md)*((md-2)%md)))%md+md<<endl;
    ans=0;
    a.clear();
    }
    return 0;
}
