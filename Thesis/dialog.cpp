#define WIN32_LEAN_AND_MEAN

#include <windows.h>

#include "resource.h"



BOOL CALLBACK DialogProc(HWND H,UINT M,WPARAM W,LPARAM L)
{
  switch(M)
  {
    case WM_INITDIALOG:
    {
      SetDlgItemText(H,ED_ABOUT,"\n\nThis is my about dialog.\nPress OK to Quit.");
      return TRUE;
    }
    case WM_COMMAND:
    {
      switch(LOWORD(W))
      {
        case BTN_OK:
        {
          EndDialog(H,0);
          return TRUE;
        }
      }
    }
  }
  return FALSE;
}

int APIENTRY WinMain(HINSTANCE hInst, HINSTANCE hPrev, LPSTR CmdStr, int CmdInt)
{
  return DialogBox(hInst,MAKEINTRESOURCE(DLG_ABOUT),0,DialogProc); // If you want a parent window to own the dialog box put its HWND as the 3rd parameter
}
