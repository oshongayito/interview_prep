#include<iostream>
#include<vector>
#include<string>

using namespace std;

#define ll long long

ll counter=0;

int numberOfOperations(string s,int start,char prev)
{
   // int res=0;
    int res=0;
   if(prev!='A')
   {
       res=counter;

        if(s[start]=='.')
        {
            if(prev!='.')
            {
                res+=(start>0?s[start-1]=='.'?1:0:0);
                res+=(start<s.size()-1?s[start+1]=='.'?1:0:0);

            }

        }
        else
        {
            if(prev=='.')
            {
                res-=(start>0?s[start-1]=='.'?1:0:0);
                res-=(start<s.size()-1?s[start+1]=='.'?1:0:0);

            }

        }
        counter=res;
        return res;
   }
    else
    for(int i=start;i<s.size();i++)
    {
        if(i==0)counter=0;
        else
        {
            if(s[i]=='.'&& s[i-1]=='.')counter++;
        }
    }
    return counter;
}

int main()
{
    ll  n,m;
    string str;

    cin>>n>>m;
    cin>>str;


    int res=numberOfOperations(str,0,'A');

    for(int t=0;t<m;t++)
    {
        int x;
        char y,prev;
        cin>>x>>y;
        prev=str[x-1];

        str[x-1]=y;

        cout<<numberOfOperations(str,x-1,prev)<<endl;

    }
    return 0;
}

