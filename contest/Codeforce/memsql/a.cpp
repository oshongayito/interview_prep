    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)

vc< pair<ll,ll> > v,vv;

bool chk(int n){
	ll x,xx,y,yy,ar=0,minx=99999999999,miny=99999999999,maxx=-1,maxy=-1,i=0;
	while(n){
		if((n&1)==0){
			n>>=1;
			i++;
			continue;
		}
		x = v[i].first;
		xx = vv[i].first;
		y = v[i].second;
		yy = vv[i].second;
		ar+=(ll)abs(x-xx)*abs(y-yy);
		minx = min(minx,min(x,xx));
		miny = min(miny,min(y,yy));
		maxx = max(maxx,max(x,xx));
		maxy = max(maxy,max(y,yy));
		n>>=1;
		i++;

	}
		//~ cout<<ar<<" "<<maxx<<" "<<minx<<" "<<maxy<<" "<<miny<<endl;
	if(ar==(ll)(maxx-minx)*(maxy-miny) && maxx-minx == maxy-miny){
		return true;
	}
	return false;
}

int main()
{
    int n;

    cin>>n;

    v.resize(n);
    vv.resize(n);
    for(int i=0;i<n;i++)
    {
        cin>>v[i].first>>v[i].second>>vv[i].first>>vv[i].second;
    }

    int i;
    for(i=1;i<(1<<n);i++)
    {
        if(chk(i)){cout<<"YES"<<endl;break;}
    }
    if(i==(1<<n))cout<<"NO"<<endl;

    return 0;

}
