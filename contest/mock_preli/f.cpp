    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))


int main()
{
    int t,n,y;
    vc<int>router_loc_x;
    vc<int>router_loc_y;
    vc<int>router_rad;
    vc<int>person_loc_x;
    vc<int>person_loc_y;

    cin>>t;

    for(int k=0;k<t;k++)
    {
        cin>>n>>y;
        router_loc_x.resize(n);
        router_loc_y.resize(n);
        router_rad.resize(n);
        person_loc_x.resize(y);
        person_loc_y.resize(y);


        for(int i=0;i<n;i++)
        {
            cin>>router_loc_x[i]>>router_loc_y[i]>>router_rad[i];
        }
        for(int i=0;i<y;i++)
        {
            cin>>person_loc_x[i]>>person_loc_y[i];
        }


        bool flag=false;
        cout<<"Case "<<k+1<<":"<<endl;
        for(int i=0;i<y;i++)
        {
            for(int j=0;j<n;j++)
            {
                if(person_loc_x[i]<=router_loc_x[j]+router_rad[j] && person_loc_x[i]>=router_loc_x[j]-router_rad[j] && person_loc_y[i]<=router_loc_y[j]+router_rad[j] && person_loc_y[i]>=router_loc_y[j]-router_rad[j] )
                {
                    flag=true;
                    break;
                }

            }
            if(flag)
            {
                cout<<"Yes"<<endl;
            }
            else
            {
                cout<<"No"<<endl;
            }
            flag=false;
        }


    }
    return 0;
}
