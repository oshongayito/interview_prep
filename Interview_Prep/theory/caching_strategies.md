## Caching ###

### Offline Scenario ###
--------------------------
If we know all the requests up front (offline problem), Furthest In Future (FIF) works optimally. FIF removes the item which will be requested furthest in future.

### Online Scenario ###
-------------------------
For online problem (where we do not know the next request), there is no optimal algorithm. But we have heuristic algorithms like followings:

1. LFU (Least Frequently Used):
	Keeps track of the frequency of access of elements in cache and removes the one with lowest frequency.
	Cons: (i) Not fast enough
		  (ii) Can not adapt if the request pattern changes.

2. LRU (has two versions: LRU 2, and 2 Queue)
	Removes the least recently used. Can be implemented with doubly link list and a hash.
	pros: (i) fast
		  (ii) adaptive
	cons: (i) extra overhead for keeping track of the least recently used item

3. Adaptive Replacement Cache (ARC):
	Combines LFU and LRU
	Pros: (i) One of the bests in terms of performance.
		  (ii) Fast and adaptive

4. MRU (Most Recently Used)
	Opposite of LRU
	Pros: (i) when request pattern is very unpredictable, maintaining LRU is costly. MRU is better suited in this situation.
		  (ii) low overhead

5. FIFO (First In First Out)
	pros: (i) low overhead
		  (ii) fast
	cons: (i) not adaptive to changes in request pattern


### Distributed System Caching ###
-------------------------------------
A server (main node) can be made dedicated for maintaining the cache directory (where a specific item is). 
Several servers can hold the cache items.

Main node will receive and serve the requests. If cache miss occurs, main node will look up in DB and store in one of the servers.

Reference: http://javalandscape.blogspot.com/2009/01/cachingcaching-algorithms-and-caching.html 

This is a very good read








