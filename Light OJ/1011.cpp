    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)
    #define clear(a) memset(a,0,sizeof(a))

    #define MAX 10000000
    int memo[16][65538];
    int n;
    int arr[17][17];

int dp(int mask,int row)
{
    if(row>=n)return 0;
    if(memo[row][mask])return memo[row][mask];
    int res=0;
    for(int i=n-1;i>=0;i--)
    {
        if(mask&(1<<i))continue;
        res=max(dp((1<<i)|mask,row+1)+arr[row][n-i-1],res);
    }
    memo[row][mask]=res;
    return res;
}

int main()
{
    int t,i,j,k;
    //int maxb[17],sorted[17][17];
    bool vis[17];
    int mask;
    int res=0;
    //cout<<(1<<0);

    cin>>t;
    for(k=1;k<=t;k++){
//    memset(maxb,0,17);
  //  memset(vis,0,17);
    cin>>n;
    for(i=0;i<n;i++)
        for(j=0;j<n;j++){cin>>arr[i][j];}
    res=0;
    for(i=n-1;i>=0;i--)
    {
        mask=1<<i;
        res=max(dp(mask,1)+arr[0][n-i-1],res);
    }
    cout<<"Case "<<k<<": "<<res<<endl;
    clear(memo);
    clear(arr);


    }

 /*   for(i=0;i<n;i++)sort(arr[i]+0,arr[i]+n);

    for(i=0;i<n;i++)
        for(j=0;j<n;j++)
        {
            if(arr[i][j]>maxb[i]);
            maxb[i]=arr[i][j];
        }
    for(i=0;i<n;i++)
        for(j=0,max=-1;j<n;j++)
        {
            if(arr[i][j]+(maxbb-sorted[i][n-1])>max){max=arr[i][j]+(maxbb-sorted[i][n-1]);}
        }
   */
    return 0;
}
