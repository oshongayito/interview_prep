
int LIS(vector<int>A){
	vector<int>DP;
	int n = A.size();

	if(n==0){
		return 0;
	}

	int res = 1;
	DP.resize(n,0);
	DP[0] = 1;

	for(int i=1; i<n; i++){

		for(int j=0; j<i; j++){
			if(A[j] < A[i] && (1+DP[j] > DP[i])){
				DP[i] = 1+DP[j];
			}
		}
		if(DP[i] == 0){
			DP[i] = 1;
		}

		if(DP[i] > res){
			res = DP[i];
		}

	}

	return res;
}