    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)

    ll mod=1000003;
    ll fact[1000001];

    void init()
    {
        for(int i=0;i<=1000000;i++)
        {
            if(i==0)fact[i]=1;
            else
            {
                fact[i]=((fact[i-1]%mod)*(i%mod))%mod;
            }
        }
    }


    /* This function calculates (a^b)%MOD */
int pows(int a, int b, int MOD) {
int x = 1, y = a;
    while(b > 0) {
        if(b%2 == 1) {
            x=(x*y);
            if(x>MOD) x%=MOD;
        }
        y = (y*y);
        if(y>MOD) y%=MOD;
        b /= 2;
    }
    return x;
}

/* this function calculates (aInv)%m */
int modInverse(int a, int m) {
    return pows(a,m-2,m);
}
/*
int modInverse(int a, int m) {
    a %= m;
    for(int x = 1; x < m; x++) {
        if((a*x) % m == 1) return x;
    }
}*/

int main()
{
    ll n,t,a,b,r;
    init();
    cin>>t;
    for(int j=0;j<t;j++)
    {
        cin>>n>>r;
        cout<<"Case "<<j+1<<": "<<fact[n]<<" "<<modInverse(fact[r],mod)<<" "<<modInverse(fact[n-r],mod)<<" "<<(((fact[n])%mod)*((modInverse((fact[r]*fact[n-r])%mod,mod))%mod))%mod<<endl;
    }

    return 0;
}
