#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<windows.h>
#include<GL/glut.h>

using namespace std;

#include<vector>
#include<iostream>
#include<fstream>
#include<string>


#define pi 2*acos(0)
#define PI acos(-1)

#define TRANSPARENCY_FACTOR 0.8


#include "color.h"
#include "camera.h"


double cameraHeight;
double cameraAngle;
int drawgrid;
int drawaxes;
double angle;
float dfx=100,dfy=100,dfz=50,dr=1.0,dg=1.0,db=1.0,diff_rad=50,diff_delta=5,diff_ang=0;
float sx=100,sy=100,sz=50,ax=1.0,ay=1.0,az=1.0;




Camera cam;
double delta=3.0;

GLuint texid1,texid2,texid3,large_marble,grass,floors,floors2,floors3,water;
int num_texture = -1;


vector< vector< vector<double> > >points;


struct point
{
	double x,y,z;
};

bool flag[100][100];


void drawAxes()
{
	if(drawaxes==1)
	{
		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);{
			glVertex3f( 100,0,0);
			glVertex3f(-100,0,0);
			glVertex3f(0,-100,0);
			glVertex3f(0, 100,0);
			glVertex3f(0,0, 100);
			glVertex3f(0,0,-100);
		}glEnd();
	}
}


void drawGrid()
{
	int i;
	if(drawgrid==1)
	{
		glColor3f(0.6, 0.6, 0.6);	//grey
		glBegin(GL_LINES);{
			for(i=-8;i<=8;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -90, 0);
				glVertex3f(i*10,  90, 0);

				//lines parallel to X-axis
				glVertex3f(-90, i*10, 0);
				glVertex3f( 90, i*10, 0);
			}
		}glEnd();
	}
}


void drawSquare(float a)
{
	glBegin(GL_QUADS);{
		glVertex3f( a, a,2);
		glVertex3f( a,-a,2);
		glVertex3f(-a,-a,2);
		glVertex3f(-a, a,2);
	}glEnd();
}


void drawss()
{
	glPushMatrix();{
		glRotatef(angle,0,0,1);
		glTranslatef(75,0,0);
		glRotatef(2*angle,0,0,1);


		glPushMatrix();{
			glRotatef(angle,0,0,1);
			glTranslatef(25,0,0);
			glRotatef(3*angle,0,0,1);
			glColor3f(0.0, 0.0, 1.0);
			drawSquare(5);

		}glPopMatrix();



		glColor3f(1.0, 0.0, 0.0);
		drawSquare(10.0);
	}glPopMatrix();

}


void drawTexture(GLuint texid,
				 GLfloat x1,GLfloat y1,GLfloat z1,
				 GLfloat x2,GLfloat y2,GLfloat z2,
				 GLfloat x3,GLfloat y3,GLfloat z3,
				 GLfloat x4,GLfloat y4,GLfloat z4
				 )
{
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0); glVertex3f(x1,y1,z1+0.01);
			glTexCoord2f(1,0); glVertex3f(x2,y2,z2+0.01);
			glTexCoord2f(1,1); glVertex3f(x3,y3,z3+0.01);
			glTexCoord2f(0,1); glVertex3f(x4,y4,z4+0.01);
		glEnd();
		glDisable(GL_TEXTURE_2D);
}

///another try



int LoadBitmap(char *filename)
{
    int i, j=0;
    FILE *l_file;
    unsigned char *l_texture;

    BITMAPFILEHEADER fileheader;
    BITMAPINFOHEADER infoheader;
    RGBTRIPLE rgb;

    num_texture++;

    if( (l_file = fopen(filename, "rb"))==NULL) return (-1);

    fread(&fileheader, sizeof(fileheader), 1, l_file);

    fseek(l_file, sizeof(fileheader), SEEK_SET);
    fread(&infoheader, sizeof(infoheader), 1, l_file);

    l_texture = (byte *) malloc(infoheader.biWidth * infoheader.biHeight * 4);
    memset(l_texture, 0, infoheader.biWidth * infoheader.biHeight * 4);

 for (i=0; i < infoheader.biWidth*infoheader.biHeight; i++)
    {
            fread(&rgb, sizeof(rgb), 1, l_file);

            l_texture[j+0] = rgb.rgbtRed;
            l_texture[j+1] = rgb.rgbtGreen;
            l_texture[j+2] = rgb.rgbtBlue;
            l_texture[j+3] = 255;
            j += 4;
    }
    fclose(l_file);

    glBindTexture(GL_TEXTURE_2D, num_texture);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, infoheader.biWidth, infoheader.biHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);
     gluBuild2DMipmaps(GL_TEXTURE_2D, 4, infoheader.biWidth, infoheader.biHeight, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);

    free(l_texture);

    return (num_texture);

}

void func(void)
{
    large_marble=LoadBitmap("large_marble.bmp");
	texid1 =LoadBitmap("translucent_glass.bmp");   /*here bkg1.bmp is the bitmap image to be used as texture, texid is global varible declared to uniquely  identify this particular image*/

	floors =LoadBitmap("red_sandstone.bmp");
	floors2 =LoadBitmap("red_sandstone2.bmp");
	floors3 =LoadBitmap("red_sandstone3.bmp");


	water =LoadBitmap("Water_Pool.bmp");
	grass = LoadBitmap("Vegetation_Grass.bmp");

//    texid2 =LoadBitmap("Cladding_Siding_White.bmp");
//	texid3=LoadBitmap("Metal_Panel.bmp");
//	texid4=LoadBitmap("Asphalt_New.bmp");
//	texid5=LoadBitmap("Stone_Brushed_Khaki.bmp");
//	texid6=LoadBitmap("j2.bmp");
//	texid7=LoadBitmap("sky-clouds-3wax.bmp");
}

/// texture try

GLuint LoadTexture( string filename )
{



	GLuint texture;

	int width, height;

	unsigned char * data;

	FILE * file;

	file = fopen( filename.c_str(), "rb" );

	cout<<"aisi"<<endl;


	if ( file == NULL ){cout<<"ret"; return 0;}
	width = 1024;
	height = 1024;
	//width = 256;
	//height = 256;

	data = (unsigned char *)malloc( width * height * 3 );
	//int size = fseek(file,);


	fread( data, width * height * 3, 1, file );
	fclose( file );


	for(int i = 0; i < width * height ; ++i)
	{
		int index = i*3;
		unsigned char B,R;
		B = data[index];
		R = data[index+2];

		data[index] = R;
		data[index+2] = B;

	}


	glGenTextures( 1, &texture );
	glBindTexture( GL_TEXTURE_2D, texture );
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST );


	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT );
	gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height,GL_RGB, GL_UNSIGNED_BYTE, data );
	free( data );

	cout<<"hoise "<<texture<<endl;

	return texture;
}

///

//draws half sphere
void drawsphere(float radius,int slices,int stacks)
{
	struct point points[100][100];
		int i,j;
	double h,r;
	for(i=0;i<=stacks;i++)
	{
		h=radius*sin(((double)i/(double)stacks)*(pi/2));
		//r=cos(((double)i/(double)stacks)*(pi))*sqrt(radius*radius-h*h);
		r=exp(-((h*h)/45))*sqrt(radius*radius-h*h);
        //if(i > (stacks/2))r=(r/h);
		for(j=0;j<=slices;j++)
		{
			points[i][j].x=r*cos(((double)j/(double)slices)*2*pi);
			points[i][j].y=r*sin(((double)j/(double)slices)*2*pi);
			points[i][j].z=h*4;
		}

	}
	for(i=0;i<stacks;i++)
	{
		for(j=0;j<slices;j++)
		{
			//glColor3f((double)i/(double)stacks,(double)i/(double)stacks,(double)i/(double)stacks);
			//glColor3f(0.6,0.6,0.6);
			if((j%(((i+1)))>=0 )&&(j%(((i+1)))<=5))glColor3f(0.5,0.5,0.5);
            else glColor3f(1,1,1);


			glBegin(GL_QUADS);{

				glVertex3f(points[i][j].x,points[i][j].y,points[i][j].z);
				glVertex3f(points[i][j+1].x,points[i][j+1].y,points[i][j+1].z);
				glVertex3f(points[i+1][j+1].x,points[i+1][j+1].y,points[i+1][j+1].z);
				glVertex3f(points[i+1][j].x,points[i+1][j].y,points[i+1][j].z);

			}glEnd();
		}

	}
}

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

void drawUpperFloor()//complete
{
    glPushMatrix();{



            glColor3f(1.0, 1.0, 1.0);




                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                25.4094,32.9892,5.9534,
                                24.0915,34.0066,5.9534,
                                24.0915,34.0066,5.9534
                                );

			glBegin(GL_TRIANGLES);{

                glVertex3f(0.0,0.0,5.9534);
                glVertex3f(25.4094,32.9892,5.9534);
                glVertex3f(24.0915,34.0066,5.9534);




			}glEnd();






                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                24.0915,34.0066,5.9534,
                                22.2059,32.0151,5.9534,
                                22.2059,32.0151,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(24.0915,34.0066,5.9534);
                glVertex3f(22.2059,32.0151,5.9534);




			}glEnd();



                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                22.2059,32.0151,5.9534,
                                17.4366,29.3635,5.9534,
                                17.4366,29.3635,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(22.2059,32.0151,5.9534);
                glVertex3f(17.4366,29.3635,5.9534);




			}glEnd();



                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                17.4366,29.3635,5.9534,
                                14.7498,28.8129,5.9534,
                                14.7498,28.8129,5.9534
                                );
                                			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(17.4366,29.3635,5.9534);
                glVertex3f(14.7498,28.8129,5.9534);




			}glEnd();


                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                14.7498,28.8129,5.9534,
                                12.0071,28.8168,5.9534,
                                12.0071,28.8168,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(14.7498,28.8129,5.9534);
                glVertex3f(12.0071,28.8168,5.9534);




			}glEnd();





                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                12.0071,28.8168,5.9534,
                                9.3219,29.3749,5.9534,
                                9.3219,29.3749,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(12.0071,28.8168,5.9534);
                glVertex3f(9.3219,29.3749,5.9534);




			}glEnd();
                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                9.3219,29.3749,5.9534,
                                6.8049,30.4643,5.9534,
                                6.8049,30.4643,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(9.3219,29.3749,5.9534);
                glVertex3f(6.8049,30.4643,5.9534);




			}glEnd();

                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                6.8049,30.4643,5.9534,
                                4.5600,32.0399,5.9534,
                                4.5600,32.0399,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(6.8049,30.4643,5.9534);
                glVertex3f(4.5600,32.0399,5.9534);




			}glEnd();

                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                4.5600,32.0399,5.9534,
                                2.6799,34.0368,5.9534,
                                2.6799,34.0368,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(4.5600,32.0399,5.9534);
                glVertex3f(2.6799,34.0368,5.9534);




			}glEnd();


                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                2.6799,34.0368,5.9534,
                                1.2423,36.3724,5.9534,
                                1.2423,36.3724,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(2.6799,34.0368,5.9534);
                glVertex3f(1.2423,36.3724,5.9534);




			}glEnd();




                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                1.2423,36.3724,5.9534,
                                0.3064,38.9505,5.9534,
                                0.3064,38.9505,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(1.2423,36.3724,5.9534);
                glVertex3f(0.3064,38.9505,5.9534);




			}glEnd();

                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                0.3064,38.9505,5.9534,
                                -0.0892,41.6656,5.9534,
                                -0.0892,41.6656,5.9534
                                );
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(0.3064,38.9505,5.9534);
                glVertex3f(-0.0892,41.6656,5.9534);




			}glEnd();

                    drawTexture(floors,
                            0.0,0.0,5.9534,
                                -0.0892,41.6656,5.9534,
                                -1.7403,41.6040,5.9534,
                                -1.7403,41.6040,5.9534);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(-0.0892,41.6656,5.9534);
               glVertex3f(-1.7403,41.6040,5.9534);




			}glEnd();



	}glPopMatrix();


}


void calculate_points()
{
    ifstream  data("E:\\CODES\\Glut\\Offline_0905060\\relling.txt");
string line;
double temp;

     vector<double>x;
      // x.resize(3);
    vector< vector<double> >y;
    //y.resize(4);


     for(int i=0,j=0;std::getline(data,line);i++)
    {
       //cout<<line<<endl;
         if(line==""){i--;continue;}

       temp = atof(line.c_str());
       x.push_back((double)temp);

   // cout<<line<<" "<<temp<<" "<<i<<endl;

       if(i%3==2)
       {
           y.push_back(x);
           x.clear();
        //   x.resize(3);
           j++;
           if(j%4==0)
           {
               points.push_back(y);
               y.clear();
          //     y.resize(4);
           }

       }

    }

    for(int i=0;i<points.size() && false;i++)
    {
        for(int j=0;j<points[i].size();j++)
        {
            for(int k=0;k<3;k++)
            {
                cout<<points[i][j][k]<<" ";
            }
            cout<<endl;
        }
        cout<<endl;
    }

}


void drawRelling()//no texture
{



for(int i=0;i<(int)points.size()-1;i++)
{
    for(int j=0;j<(int)points[i].size();j++)
    {
        glPushMatrix();{




            glColor3f(0.65, 0.16, 0.16);
			glBegin(GL_QUADS);{


                glVertex3f(points[i][j][0],points[i][j][1],points[i][j][2]);

                glVertex3f(points[i+1][j][0],points[i+1][j][1],points[i+1][j][2]);
                glVertex3f(points[i+1][(j+1)%4][0],points[i+1][(j+1)%4][1],points[i+1][(j+1)%4][2]);

                glVertex3f(points[i][(j+1)%4][0],points[i][(j+1)%4][1],points[i][(j+1)%4][2]);


			}glEnd();

				}glPopMatrix();


    }

}




}

void drawStairSideInside()//complete
{
    glPushMatrix();{




            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(29.4997,41.0298,5.2489);
                glVertex3f(29.766,41.3755,4.6635);
                glVertex3f(29.6449,41.2183,4.6635);

                glVertex3f(29.4997,41.0298,5.0505);



			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(24.6,34.667,5.2489);
                glVertex3f(29.4997,41.0298,5.2489);
                glVertex3f(29.4997,41.0298,5.0505);

                glVertex3f(24.6,34.667,5.0505);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(24.0915,34.0066,6.1519);
                glVertex3f(24.6,34.667,5.2489);

                glVertex3f(24.6,34.667,5.0505);
                glVertex3f(24.0915,34.0066,5.9534);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(24.0915,34.0066,6.1519);
                glVertex3f(22.2059,32.0151,6.1519);
                glVertex3f(22.2059,32.0151,5.9534);
                glVertex3f(24.0915,34.0066,5.9534);


			}glEnd();




            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(22.2059,32.0151,6.1519);

                glVertex3f(17.4366,29.3635,6.1519);
                glVertex3f(17.4366,29.3635,5.9534);
                glVertex3f(22.2059,32.0151,5.9534);





			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(17.4366,29.3635,6.1519);

			    glVertex3f(14.7498,28.8129,6.1519);
			    glVertex3f(14.7498,28.8129,5.9534);
			    glVertex3f(17.4366,29.3635,5.9534);





			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(14.7498,28.8129,6.1519);

			    glVertex3f(12.0071,28.8168,6.1519);
			    glVertex3f(12.0071,28.8168,5.9534);
                glVertex3f(14.7498,28.8129,5.9534);




			}glEnd();






            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(12.0071,28.8168,6.1519);

                glVertex3f(9.3219,29.3749,6.1519);

                glVertex3f(9.3219,29.3749,5.9534);

                glVertex3f(12.0071,28.8168,5.9534);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(9.3219,29.3749,6.1519);

                glVertex3f(6.8049,30.4643,6.1519);

                glVertex3f(6.8049,30.4643,5.9534);

                glVertex3f(9.3219,29.3749,5.9534);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(6.8049,30.4643,6.1519);

                glVertex3f(4.5600,32.0399,6.1519);
                glVertex3f(4.5600,32.0399,5.9534);
glVertex3f(6.8049,30.4643,5.9534);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(4.5600,32.0399,6.1519);

                glVertex3f(2.6799,34.0368,6.1519);

                glVertex3f(2.6799,34.0368,5.9534);

glVertex3f(4.5600,32.0399,5.9534);



			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(2.6799,34.0368,6.1519);

                glVertex3f(1.2423,36.3724,6.1519);

                glVertex3f(1.2423,36.3724,5.9534);

glVertex3f(2.6799,34.0368,5.9534);



			}glEnd();





            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(1.2423,36.3724,6.1519);

                glVertex3f(0.3064,38.9505,6.1519);
                glVertex3f(0.3064,38.9505,5.9534);

glVertex3f(1.2423,36.3724,5.9534);



			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(0.3064,38.9505,6.1519);


                glVertex3f(-0.0892,41.6656,6.1519);
                glVertex3f(-0.0892,41.6656,5.9534);

                glVertex3f(0.3064,38.9505,5.9534);



			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(-0.0892,41.6656,6.1519);
                glVertex3f(-0.1220,42.4984,5.1795);
                glVertex3f(-0.1220,42.4984,5.0505);
                glVertex3f(-0.0892,41.6656,5.9534);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(-0.1220,42.4984,5.1795);
                glVertex3f(-0.4492,50.53,5.1795);
                glVertex3f(-0.4492,50.53,5.0505);
                glVertex3f(-0.1220,42.4984,5.0505);




			}glEnd();


			            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(-0.4492,50.53,5.1795);
                glVertex3f(-0.467,50.9633,4.6635);

                glVertex3f(-0.4589,50.7651,4.6635);
                glVertex3f(-0.4492,50.53,5.0505);




			}glEnd();




	}glPopMatrix();


}



void drawInnerCircle2()//complete
{
    glPushMatrix();{

///center flower

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(0.0,0.0,4.6635);
                glVertex3f(1.3931,4.3903,4.6635);
                glVertex3f(3.8892,2.4677,4.6635);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(0.0,0.0,4.6635);
                glVertex3f(3.8892,2.4677,4.6635);
                glVertex3f(4.5656,-0.6096,4.6635);


			}glEnd();


///circle 1

//1
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(6.4595,11.1881,4.6635);

			   glVertex3f(9.1351,9.1351,4.6635);
			   glVertex3f(9.1351,9.1351,0.5259);

                glVertex3f(6.4595,11.1881,0.5259);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(9.1351,9.1351,4.6635);

			    glVertex3f(11.1881,6.4595,4.6635);
			    glVertex3f(11.1881,6.4595,0.5259);

			    glVertex3f(9.1351,9.1351,0.5259);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(11.1881,6.4595,4.6635);
			    glVertex3f(12.4878,3.3437,4.6635);
			    glVertex3f(12.4878,3.3437,0.5259);
			    glVertex3f(11.1881,6.4595,0.5259);


			}glEnd();





///circle 2
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(9.6200,16.6624,4.6635);
			    glVertex3f(13.6048,13.6048,4.6635);
			    glVertex3f(13.6048,13.6048,0.5259);
			    glVertex3f(9.6200,16.6624,0.5259);

			}glEnd();

			//1
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(13.6048,13.6048,4.6635);
			    glVertex3f(16.6624,9.6200,4.6635);
			    glVertex3f(16.6624,9.6200,0.5259);
			    glVertex3f(13.6048,13.6048,0.5259);

			}glEnd();

			//1
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(16.6624,9.6200,4.6635);
			    glVertex3f(18.5845,4.9797,4.6635);
			    glVertex3f(18.5845,4.9797,0.5259);
			    glVertex3f(16.6624,9.6200,0.5259);

			}glEnd();



	}glPopMatrix();


}


void drawStairSide()//complete
{
     glPushMatrix();{

//1
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

                glVertex3f(29.7660,41.3755,4.6645);
                glVertex3f(29.4997,41.0298,5.2489);
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(29.6089,41.4968,4.6635);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(29.6089,41.4968,4.6635);
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(29.3425,41.1509,5.0505);
                glVertex3f(29.4878,41.3396,4.7925);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(29.6089,41.4968,4.6635);
                glVertex3f(29.4878,41.3396,4.7925);
                glVertex3f(29.4878,41.3396,4.6635);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(29.4152,41.2452,4.7925);
                glVertex3f(29.4878,41.3396,4.7925);
                glVertex3f(29.4878,41.3396,4.6635);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(29.4878,41.3396,4.7925);
                glVertex3f(29.4152,41.2452,4.9215);
                glVertex3f(29.4152,41.2452,4.7925);



			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(29.3425,41.1509,4.9215);
                glVertex3f(29.4152,41.2452,4.9215);
                glVertex3f(29.4152,41.2452,4.7925);



			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(29.4152,41.2452,4.9215);
                glVertex3f(29.3425,41.1509,5.0505);
                glVertex3f(29.3425,41.1509,4.9215);



			}glEnd();


//2
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(29.4997,41.0298,5.2489);
                glVertex3f(24.6,34.667,5.2489);
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(29.3425,41.1509,5.2489);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(23.9336,34.1284,5.9534);
                glVertex3f(24.4429,34.7882,5.0505);

			}glEnd();

									            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(23.9336,34.1284,5.9534);
                glVertex3f(23.9336,34.1284,4.6635);
                glVertex3f(24.2691,34.5636,4.0682);


			}glEnd();





            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(29.3425,41.1509,5.0505);


			}glEnd();







					            glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_LINE);{
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(24.4429,34.7882,5.2489);


			}glEnd();


		            glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_LINE);{
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(24.4429,34.7882,5.0505);


			}glEnd();

					            glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_LINE);{
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(29.3425,41.1509,5.0505);


			}glEnd();


					            glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_LINE);{
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(29.3425,41.1509,5.0505);


			}glEnd();




            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(29.4152,41.2452,4.7925);
                glVertex3f(26.2293,37.1090,4.0682);
                glVertex3f(26.2293,37.1090,1.9547);
                glVertex3f(29.4878,41.3396,4.6635);



			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(29.3425,41.1509,5.0505);
                glVertex3f(26.2293,37.1090,4.0682);
                glVertex3f(29.3425,41.1509,4.9215);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(29.3425,41.1509,5.0505);
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(26.2293,37.1090,4.0682);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(29.3425,41.1509,4.9215);
                                glVertex3f(26.2293,37.1090,4.0682);
                                glVertex3f(29.4152,41.2452,4.7925);





			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(26.2293,37.1090,4.0682);
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(24.2691,34.5636,4.0682);

			}glEnd();






//3
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(24.6,34.667,5.2489);
                glVertex3f(24.0915,34.0066,6.1519);
                glVertex3f(23.9336,34.1248,6.1519);
                glVertex3f(24.4429,34.7882,5.2489);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(23.9336,34.1248,6.1519);
            glVertex3f(23.9336,34.1248,4.6635);
            glVertex3f(24.4429,34.7882,4.0682);




			}glEnd();



//4

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(24.0915,34.0066,6.1519);
                glVertex3f(22.2059,32.0151,6.1519);
                glVertex3f(22.0758,32.1663,6.1519);
                glVertex3f(23.9336,34.1248,6.1519);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(23.9336,34.1248,6.1519);
                glVertex3f(22.0758,32.1663,6.1519);
                glVertex3f(22.0758,32.1663,4.6635);
                glVertex3f(23.9336,34.1248,4.6635);




			}glEnd();

//5


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(22.2059,32.0151,6.1519);
                glVertex3f(19.9566,30.4458,6.1519);
                glVertex3f(19.8597,30.6202,6.1519);
                glVertex3f(22.0758,32.1663,6.1519);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(22.0758,32.1663,6.1519);
                glVertex3f(19.8597,30.6202,6.1519);
                glVertex3f(19.8597,30.6202,4.6635);
                glVertex3f(22.0758,32.1663,4.6635);

			}glEnd();



//6

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(19.9566,30.4458,6.1519);
                glVertex3f(17.4366,29.3635,6.1519);
                glVertex3f(17.3769,29.5539,6.1519);

                glVertex3f(19.8597,30.6202,6.1519);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(19.8597,30.6202,6.1519);
                glVertex3f(17.3769,29.5539,6.1519);
                glVertex3f(17.3769,29.5539,4.6635);
                glVertex3f(19.8597,30.6202,4.6635);





			}glEnd();


//7


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(17.4366,29.3635,6.1519);
                glVertex3f(14.7498,28.8129,6.1519);
                glVertex3f(14.7298,29.0114,6.1519);
                glVertex3f(17.3769,29.5539,6.1519);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(17.3769,29.5539,6.1519);
                glVertex3f(14.7298,29.0114,6.1519);
                glVertex3f(14.7298,29.0114,4.6635);
                glVertex3f(17.3769,29.5539,4.6635);




			}glEnd();

//8

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(14.7498,28.8129,6.1519);
                glVertex3f(12.0071,28.8168,6.1519);
                glVertex3f(12.0277,29.0152,6.1519);
                glVertex3f(14.7298,29.0114,6.1519);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(14.7298,29.0114,6.1519);
                glVertex3f(12.0277,29.0152,6.1519);
                glVertex3f(12.0277,29.0152,4.6635);
                glVertex3f(14.7298,29.0114,4.6635);




			}glEnd();



//9


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(12.0071,28.8168,6.1519);
                glVertex3f(9.3219,29.3749,6.1519);
                glVertex3f(9.3821,29.5651,6.1519);
                glVertex3f(12.0277,29.0152,6.1519);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

                glVertex3f(12.0277,29.0152,6.1519);
                glVertex3f(9.3821,29.5651,6.1519);

               glVertex3f(9.3821,29.5651,4.6635);

                glVertex3f(12.0277,29.0152,4.6635);


			}glEnd();



//10
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(9.3219,29.3749,6.1519);
                glVertex3f(6.8049,30.4643,6.1519);
                glVertex3f(6.9023,30.6384,6.1519);
                glVertex3f(9.3821,29.5651,6.1519);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			                    glVertex3f(9.3821,29.5651,6.1519);
                glVertex3f(6.9023,30.6384,6.1519);

                glVertex3f(6.9023,30.6384,4.6635);

			                    glVertex3f(9.3821,29.5651,4.6635);


			}glEnd();


//11
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(6.8049,30.4643,6.1519);
                glVertex3f(4.5600,32.0399,6.1519);
                glVertex3f(4.6906,32.1907,6.1519);
                glVertex3f(6.9023,30.6384,6.1519);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			                    glVertex3f(6.9023,30.6384,6.1519);
                glVertex3f(4.6906,32.1907,6.1519);

                glVertex3f(4.6906,32.1907,4.6635);

			                    glVertex3f(6.9023,30.6384,4.6635);


			}glEnd();


//12
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(4.5600,32.0399,6.1519);
                glVertex3f(2.6799,34.0368,6.1519);
                glVertex3f(2.8383,34.1581,6.1519);
                glVertex3f(4.6906,32.1907,6.1519);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(4.6906,32.1907,6.1519);
                glVertex3f(2.8383,34.1581,6.1519);

                glVertex3f(2.8383,34.1581,4.6635);
                glVertex3f(4.6906,32.1907,4.6635);



			}glEnd();




//13

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(2.6799,34.0368,6.1519);
                glVertex3f(1.2423,36.3724,6.1519);
                glVertex3f(1.4219,36.4592,6.1519);
                glVertex3f(2.8383,34.1581,6.1519);




			}glEnd();




            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			                    glVertex3f(2.8383,34.1581,6.1519);
                glVertex3f(1.4219,36.4592,6.1519);

                glVertex3f(1.4219,36.4592,4.6635);

			                    glVertex3f(2.8383,34.1581,4.6635);


			}glEnd();




//14




            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

                glVertex3f(1.2423,36.3724,6.1519);
                glVertex3f(0.3064,38.9505,6.1519);
                glVertex3f(0.4999,38.9991,6.1519);
                glVertex3f(1.4219,36.4592,6.1519);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

                glVertex3f(1.4219,36.4592,6.1519);
                glVertex3f(0.4999,38.9991,6.1519);

                glVertex3f(0.4999,38.9991,4.6635);

                glVertex3f(1.4219,36.4592,4.6635);


			}glEnd();


//15
            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

                glVertex3f(0.3064,38.9505,6.1519);
                glVertex3f(-0.0892,41.6656,6.1519);
               glVertex3f(0.1102,41.673,6.1519);
                glVertex3f(0.4999,38.9991,6.1519);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

                glVertex3f(0.4999,38.9991,6.1519);
               glVertex3f(0.1102,41.673,6.1519);

               glVertex3f(0.1102,41.673,4.6635);

                glVertex3f(0.4999,38.9991,4.6635);


			}glEnd();


//16 disturb

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

               glVertex3f(-0.0892,41.6656,6.1519);
                glVertex3f(-0.122,42.4984,5.1795);
               glVertex3f(0.0763,42.5058,5.1795);
               glVertex3f(0.1102,41.673,6.1519);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

               glVertex3f(0.1102,41.673,6.1519);
           glVertex3f(0.0763,42.5058,5.1795);

           glVertex3f(0.0763,42.5058,4.6635);

               glVertex3f(0.1102,41.673,4.0730);


			}glEnd();


//17

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

                glVertex3f(-0.122,42.4984,5.1795);
               glVertex3f(-0.4492,50.5270,5.1795);
               glVertex3f(-0.251,50.5351,5.1795);
               glVertex3f(0.0763,42.5058,5.1795);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

               glVertex3f(0.0763,42.5058,5.1795);
               glVertex3f(-0.251,50.5351,5.1795);
               glVertex3f(-0.2607,50.7730,4.6635);
               glVertex3f(0.0763,42.5058,4.0730);





			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{

               glVertex3f(-0.2607,50.7730,4.6635);
               glVertex3f(-0.0431,45.4353,4.0730);
               glVertex3f(0.0763,42.5058,4.0730);





			}glEnd();

													               glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{

               glVertex3f(-0.2607,50.7730,4.6635);
               glVertex3f(-0.0431,45.4353,1.9547);
               glVertex3f(-0.0431,45.4353,4.0730);





			}glEnd();



//18

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{

               glVertex3f(-0.4492,50.5270,5.1795);
               glVertex3f(-0.467,50.9633,4.6635);
               glVertex3f(-0.2687,50.9713,4.6635);
               glVertex3f(-0.251,50.5351,5.1795);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(-0.251,50.5351,5.1795);
                glVertex3f(-0.2687,50.9713,4.6635);
               glVertex3f(-0.2607,50.773,4.6635);



			}glEnd();





	}glPopMatrix();


}


void drawStair()//complete
{
     glPushMatrix();{


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -50.0735,8.3633,4.6635,

                                -50.0762,8.3637,4.7925,
                                -50.5188,5.0641,4.7925,
                                -50.5185,5.0641,4.6635

                               );

                               			glBegin(GL_QUADS);{
                glVertex3f(-50.0735,8.3633,4.6635);
                glVertex3f(-50.0762,8.3637,4.7925);
                glVertex3f(-50.5188,5.0641,4.7925);
                glVertex3f(-50.5185,5.0641,4.6635);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -50.0762,8.3637,4.7925,

                                -49.9585,8.3473,4.7921,
                                -50.4003,5.0483,4.7921,
                                -50.5188,5.0641,4.7925

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-50.0762,8.3637,4.7925);
                glVertex3f(-49.9585,8.3473,4.7921);
                glVertex3f(-50.4003,5.0483,4.7921);
                glVertex3f(-50.5188,5.0641,4.7925);


			}glEnd();

            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -49.9585,8.3473,4.7921,

                                -49.9585,8.3478,4.9215,
                                -50.4007,5.0484,4.9215,
                                -50.4003,5.0483,4.7921

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-49.9585,8.3473,4.7921);
                glVertex3f(-49.9585,8.3478,4.9215);
                glVertex3f(-50.4007,5.0484,4.9215);
                glVertex3f(-50.4003,5.0483,4.7921);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -49.9585,8.3478,4.9215,

                                -49.8404,8.3319,4.9215,
                                -50.2819,5.0325,4.9215,
                                -50.4007,5.0484,4.9215

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-49.9585,8.3478,4.9215);
                glVertex3f(-49.8404,8.3319,4.9215);
                glVertex3f(-50.2819,5.0325,4.9215);

                glVertex3f(-50.4007,5.0484,4.9215);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -49.8404,8.3319,4.9215,

                                -49.8404,8.3319,5.0505,
                                -50.2827,5.0326,5.0505,
                                -50.2819,5.0325,4.9215

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-49.8404,8.3319,4.9215);
                glVertex3f(-49.8404,8.3319,5.0505);
                glVertex3f(-50.2827,5.0326,5.0505);
                glVertex3f(-50.2819,5.0325,4.9215);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -49.8404,8.3319,5.0505,

                                -41.874,7.2596,5.0505,
                                -42.3224,3.9707,5.0505,
                                -50.2827,5.0326,5.0505

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-49.8404,8.3319,5.0505);
                glVertex3f(-41.874,7.2596,5.0505);
                glVertex3f(-42.3224,3.9707,5.0505);
                glVertex3f(-50.2827,5.0326,5.0505);


			}glEnd();

            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.874,7.2596,5.0505,

                                -41.874,7.2596,5.1795,
                                -42.3224,3.9707,5.1795,
                                -42.3224,3.9707,5.0505

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.874,7.2596,5.0505);
                glVertex3f(-41.874,7.2596,5.1795);
                glVertex3f(-42.3224,3.9707,5.1795);
                glVertex3f(-42.3224,3.9707,5.0505);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.874,7.2596,5.1795,

                                -41.756,7.2436,5.1795,
                                -42.2044,3.955,5.1795,
                                -42.3224,3.9707,5.1795

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.874,7.2596,5.1795);
                glVertex3f(-41.756,7.2436,5.1795);
                glVertex3f(-42.2044,3.955,5.1795);
                glVertex3f(-42.3224,3.9707,5.1795);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.756,7.2436,5.1795,

                                -41.756,7.2436,5.3085,
                                -42.2044,3.955,5.3085,
                                -42.2044,3.955,5.1795

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.756,7.2436,5.1795);
                glVertex3f(-41.756,7.2436,5.3085);
                glVertex3f(-42.2044,3.955,5.3085);
                glVertex3f(-42.2044,3.955,5.1795);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.756,7.2436,5.3085,

                                -41.638,7.2275,5.3085,
                                -42.0864,3.9392,5.3085,
                                -42.2044,3.955,5.3085

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.756,7.2436,5.3085);
                glVertex3f(-41.638,7.2275,5.3085);
                glVertex3f(-42.0864,3.9392,5.3085);
                glVertex3f(-42.2044,3.955,5.3085);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.638,7.2275,5.3085,

                                -41.638,7.2275,5.4375,
                                -42.0864,3.9392,5.4375,
                                -42.0864,3.9392,5.3085

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.638,7.2275,5.3085);
                glVertex3f(-41.638,7.2275,5.4375);
                glVertex3f(-42.0864,3.9392,5.4375);
                glVertex3f(-42.0864,3.9392,5.3085);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.638,7.2275,5.4375,

                                -41.52,7.2115,5.4375,
                                -41.9684,3.9235,5.4375,
                                -42.0864,3.9392,5.4375

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.638,7.2275,5.4375);
                glVertex3f(-41.52,7.2115,5.4375);
                glVertex3f(-41.9684,3.9235,5.4375);
                glVertex3f(-42.0864,3.9392,5.4375);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.52,7.2115,5.4375,

                                -41.52,7.2115,5.5665,
                                -41.9684,3.9235,5.5665,
                                -41.9684,3.9235,5.4375

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.52,7.2115,5.4375);
                glVertex3f(-41.52,7.2115,5.5665);
                glVertex3f(-41.9684,3.9235,5.5665);
                glVertex3f(-41.9684,3.9235,5.4375);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.52,7.2115,5.5665,

                                -41.4020,7.1954,5.5665,
                                -41.8503,3.9078,5.5665,
                                -41.9684,3.9235,5.5665

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.52,7.2115,5.5665);
                glVertex3f(-41.4020,7.1954,5.5665);
                glVertex3f(-41.8503,3.9078,5.5665);
                glVertex3f(-41.9684,3.9235,5.5665);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.4020,7.1954,5.5665,

                                -41.4020,7.1954,5.6955,
                                -41.8503,3.9078,5.6955,
                                -41.8503,3.9078,5.5665

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.4020,7.1954,5.5665);
                glVertex3f(-41.4020,7.1954,5.6955);
                glVertex3f(-41.8503,3.9078,5.6955);
                glVertex3f(-41.8503,3.9078,5.5665);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.4020,7.1954,5.6955,

                                -41.284,7.1794,5.6955,
                                -41.7323,3.892,5.6955,
                                -41.8503,3.9078,5.6955

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.4020,7.1954,5.6955);
                glVertex3f(-41.284,7.1794,5.6955);
                glVertex3f(-41.7323,3.892,5.6955);
                glVertex3f(-41.8503,3.9078,5.6955);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.284,7.1794,5.6955,

                                -41.284,7.1794,5.8244,
                                -41.7323,3.892,5.8244,
                                -41.7323,3.892,5.6955

                               );

                               			glBegin(GL_QUADS);{
                glVertex3f(-41.284,7.1794,5.6955);
                glVertex3f(-41.284,7.1794,5.8244);
                glVertex3f(-41.7323,3.892,5.8244);
                glVertex3f(-41.7323,3.892,5.6955);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.284,7.1794,5.8244,

                                -41.1661,7.1633,5.8244,
                                -41.6143,3.8763,5.8244,
                                -41.7323,3.892,5.8244

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.284,7.1794,5.8244);
                glVertex3f(-41.1661,7.1633,5.8244);
                glVertex3f(-41.6143,3.8763,5.8244);
                glVertex3f(-41.7323,3.892,5.8244);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.1661,7.1633,5.8244,

                                -41.1661,7.1633,5.9534,
                                -41.6143,3.8763,5.9534,
                                -41.6143,3.8763,5.8244

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.1661,7.1633,5.8244);
                glVertex3f(-41.1661,7.1633,5.9534);
                glVertex3f(-41.6143,3.8763,5.9534);
                glVertex3f(-41.6143,3.8763,5.8244);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,
                                -41.1661,7.1633,5.9534,

                                -41.0481,7.1473,5.9534,
                                -41.4963,3.8605,5.9534,
                                -41.6143,3.8763,5.9534

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.1661,7.1633,5.9534);
                glVertex3f(-41.0481,7.1473,5.9534);
                glVertex3f(-41.4963,3.8605,5.9534);
                glVertex3f(-41.6143,3.8763,5.9534);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors2,

                                -41.5229,3.6629,4.6635,
                                -41.0481,3.7535,5.9534,
                                -40.7183,7.1374,5.9534,
                                -41.0208,7.345,4.6635

                               );
                               			glBegin(GL_QUADS);{
                glVertex3f(-41.5229,3.6629,4.6635);
                glVertex3f(-41.0481,3.7535,5.9534);
                glVertex3f(-40.7183,7.1374,5.9534);
                glVertex3f(-41.0208,7.345,4.6635);

			}glEnd();








	}glPopMatrix();

     }


void drawBase_innerPillar()//complete
{

     glPushMatrix();{


//pillar thick


            glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors3,
                                28.3303,-5.0232,1.9547,
                                28.3303,-5.0232,4.6635,
                                28.7455,-2.6683,4.6635,
                               28.7455,-2.6683,1.9547
                                );			glBegin(GL_QUADS);{
                glVertex3f(28.3303,-5.0232,1.9547);
                glVertex3f(28.3303,-5.0232,4.6635);
                glVertex3f(28.7455,-2.6683,4.6635);
                glVertex3f(28.7455,-2.6683,1.9547);



			}glEnd();


            glColor3f(1.0, 1.0, 1.0);




                    drawTexture(floors3,
                                27.3042,-4.8423,1.9547,
                                27.3042,-4.8423,4.6635,
                                28.3303,-5.0232,4.6635,
                           28.3303,-5.0232,1.9547
                                );			glBegin(GL_QUADS);{
                glVertex3f(27.3042,-4.8423,1.9547);
                glVertex3f(27.3042,-4.8423,4.6635);
                glVertex3f(28.3303,-5.0232,4.6635);
                glVertex3f(28.3303,-5.0232,1.9547);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);




                    drawTexture(floors3,
                                28.7455,-2.6683,1.9547,
                                28.7455,-2.6683,4.6635,
                                27.7195,-2.4874,4.6635,
                               27.7195,-2.4874,1.9547
                                );
			glBegin(GL_QUADS);{
                glVertex3f(28.7455,-2.6683,1.9547);
                glVertex3f(28.7455,-2.6683,4.6635);
                glVertex3f(27.7195,-2.4874,4.6635);
                glVertex3f(27.7195,-2.4874,1.9547);

			}glEnd();


            glColor3f(1.0, 1.0, 1.0);




                    drawTexture(floors3,
                                27.7195,-2.4874,1.9547,
                                27.7195,-2.4874,4.6635,
                                27.3042,-4.8423,4.6635,
                               27.3042,-4.8423,1.9547
                                );
			glBegin(GL_QUADS);{
                glVertex3f(27.7195,-2.4874,1.9547);
                glVertex3f(27.7195,-2.4874,4.6635);
                glVertex3f(27.3042,-4.8423,4.6635);
                glVertex3f(27.3042,-4.8423,1.9547);



			}glEnd();



//pillar thin and tall


/*
glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(-1.9391,42.3806,1.9547);
                glVertex3f(-1.6417,42.3924,1.9547);
                glVertex3f(-1.6101,41.6044,1.9547);
                glVertex3f(-1.9090,41.5924,1.9547);



			}glEnd();

			glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(-1.9391,42.3806,4.6635);
                glVertex3f(-1.6417,42.3924,4.6635);
                glVertex3f(-1.6102,41.6044,4.6635);
                glVertex3f(-1.9092,41.5971,4.6635);


			}glEnd();
*/


			//start
            glColor3f(1.0, 1.0, 1.0);




                    drawTexture(floors3,
                                -1.9391,42.3806,1.9547,
                                -1.9391,42.3806,4.6635,
                                -1.9092,41.5971,4.6635,
                               -1.9090,41.5924,1.9547
                                );			glBegin(GL_QUADS);{
                glVertex3f(-1.9391,42.3806,1.9547);
                glVertex3f(-1.9391,42.3806,4.6635);
                glVertex3f(-1.9092,41.5971,4.6635);
                glVertex3f(-1.9090,41.5924,1.9547);



			}glEnd();
            glColor3f(1.0, 1.0, 1.0);
                    drawTexture(floors3,
                                -1.6417,42.3924,1.9547,
                                -1.6417,42.3924,4.6635,
                                -1.9391,42.3806,4.6635,
                               -1.9391,42.3806,1.9547
                                );
			glBegin(GL_QUADS);{
                glVertex3f(-1.6417,42.3924,1.9547);
                glVertex3f(-1.6417,42.3924,4.6635);
                glVertex3f(-1.9391,42.3806,4.6635);
                glVertex3f(-1.9391,42.3806,1.9547);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
                    drawTexture(floors3,
                                -1.6101,41.6044,1.9547,
                                -1.6102,41.6044,4.6635,
                                -1.6417,42.3924,4.6635,
                                -1.6417,42.3924,1.9547
                                );
			glBegin(GL_QUADS);{
                glVertex3f(-1.6101,41.6044,1.9547);
                glVertex3f(-1.6102,41.6044,4.6635);

                glVertex3f(-1.6417,42.3924,4.6635);
                glVertex3f(-1.6417,42.3924,1.9547);



			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
                    drawTexture(floors3,
                                -1.9090,41.5924,1.9547,
                                -1.9092,41.5971,4.6635,
                                -1.6102,41.6044,4.6635,
                               -1.6101,41.6044,1.9547
                                );
			glBegin(GL_QUADS);{
                glVertex3f(-1.9090,41.5924,1.9547);
                glVertex3f(-1.9092,41.5971,4.6635);
                glVertex3f(-1.6102,41.6044,4.6635);

                glVertex3f(-1.6101,41.6044,1.9547);


			}glEnd();




		}glPopMatrix();
}

void drawBaseLowerFloor()//complete
{
         glPushMatrix();{


  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                40.2348,-21.0336,1.9547,
                                41.1874,-19.2996,1.9547,
                                41.1874,-19.2996,1.9547

                               );

                glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(40.2348,-21.0336,1.9547);
                glVertex3f(41.1874,-19.2996,1.9547);
            }glEnd();



  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                40.1503,-18.8904,1.9547,
                                35.4776,-16.0489,1.9547,
                                35.4776,-16.0489,1.9547

                               );

                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(40.1503,-18.8904,1.9547);
				glVertex3f(35.4776,-16.0489,1.9547);
			}glEnd();


  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                35.4776,-16.0489,1.9547,
                                33.5702,-14.1350,1.9547,
                                33.5702,-14.1350,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(35.4776,-16.0489,1.9547);
				glVertex3f(33.5702,-14.1350,1.9547);
			}glEnd();


  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                33.5702,-14.1350,1.9547,
                                32.0889,-11.8751,1.9547,
                                32.0889,-11.8751,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(33.5702,-14.1350,1.9547);
				glVertex3f(32.0889,-11.8751,1.9547);
			}glEnd();



  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                32.0889,-11.8751,1.9547,
                                31.095,-9.3624,1.9547,
                                31.095,-9.3624,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(32.0889,-11.8751,1.9547);
				glVertex3f(31.095,-9.3624,1.9547);
			}glEnd();


  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                31.095,-9.3624,1.9547,
                                30.6296,-6.7007,1.9547,
                                30.6296,-6.7007,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(31.095,-9.3624,1.9547);
				glVertex3f(30.6296,-6.7007,1.9547);
			}glEnd();


  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                30.6296,-6.7007,1.9547,
                                30.7117,-3.9998,1.9547,
                                30.7117,-3.9998,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(30.6296,-6.7007,1.9547);
				glVertex3f(30.7117,-3.9998,1.9547);
			}glEnd();


  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                30.7117,-3.9998,1.9547,
                                31.3380,-1.3713,1.9547,
                                31.3380,-1.3713,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(30.7117,-3.9998,1.9547);
				glVertex3f(31.3380,-1.3713,1.9547);
			}glEnd();



  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                31.3380,-1.3713,1.9547,
                                32.4828,1.0764,1.9547,
                                32.4828,1.0764,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(31.3380,-1.3713,1.9547);
				glVertex3f(32.4828,1.0764,1.9547);
			}glEnd();


  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                32.4828,1.0764,1.9547,
                                34.0986,3.2422,1.9547,
                                34.0986,3.2422,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(32.4828,1.0764,1.9547);
				glVertex3f(34.0986,3.2422,1.9547);
			}glEnd();



  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                34.0986,3.2422,1.9547,
                                38.46,6.3857,1.9547,
                                38.46,6.3857,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(34.0986,3.2422,1.9547);
				glVertex3f(38.46,6.3857,1.9547);
			}glEnd();


  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                38.46,6.3857,1.9547,
                                41.0256,7.2337,1.9547,
                                41.0256,7.2337,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(38.46,6.3857,1.9547);
				glVertex3f(41.0256,7.2337,1.9547);
			}glEnd();



  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                41.0256,7.2337,1.9547,
                                43.6904,7.7032,1.9547,
                                43.6904,7.7032,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(41.0256,7.2337,1.9547);
				glVertex3f(43.6904,7.7032,1.9547);
			}glEnd();

  glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                0.0,0.0,1.9547,
                                44.8116,7.8034,1.9547,
                                44.3418,9.7497,1.9547,
                                44.3418,9.7497,1.9547

                               );
                			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(44.8116,7.8034,1.9547);
				glVertex3f(44.3418,9.7497,1.9547);
			}glEnd();







		}glPopMatrix();

}

void drawBase1_leftface()//complete
{
		glPushMatrix();{
	//		glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left part of a petal face

//1st layer


 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(grass,
                                53.4968,-28.8032,0.3473,
                                48.8869,-27.2255,4.6635,
                                50.2543,-24.6173,4.6635,
                                54.1767,-27.5063,0.3473

                                );

                                			glBegin(GL_QUADS);{
				glVertex3f(53.4968,-28.8032,0.3473);
				glVertex3f(48.8869,-27.2255,4.6635);
				glVertex3f(50.2543,-24.6173,4.6635);
				glVertex3f(54.1767,-27.5063,0.3473);

			}glEnd();

 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(grass,
                                54.1767,-27.5063,0.3473,
                                50.2543,-24.6173,4.6635,
                                66.0919,-8.8239,4.6635,
                                72.1067,-9.6264,0.3473

                                );			glBegin(GL_QUADS);{
				glVertex3f(54.1767,-27.5063,0.3473);
				glVertex3f(50.2543,-24.6173,4.6635);

                glVertex3f(66.0919,-8.8239,4.6635);
				glVertex3f(72.1067,-9.6264,0.3473);


			}glEnd();


 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(grass,
                                72.1067,-9.6264,0.3473,
                                66.0919,-8.8239,4.6635,
                                54.9498,10.5679,4.6635,
                                59.4953,12.3225,0.3473

                                );			glBegin(GL_QUADS);{
				glVertex3f(72.1067,-9.6264,0.3473);
				glVertex3f(66.0919,-8.8239,4.6635);
				glVertex3f(54.9498,10.5679,4.6635);
				glVertex3f(59.4953,12.3225,0.3473);

			}glEnd();

//2nd layer

 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(grass,
                                48.8869,-27.2255,4.6635,
                                46.2973,-27.2255,4.6635,
                                48.1777,-23.0878,4.6635,
                                50.2543,-24.6173,4.6635


                                );				glBegin(GL_QUADS);{
				glVertex3f(48.8869,-27.2255,4.6635);
				glVertex3f(46.2973,-27.2255,4.6635);
				glVertex3f(48.1777,-23.0878,4.6635);
				glVertex3f(50.2543,-24.6173,4.6635);

			}glEnd();

 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(grass,
                                50.2543,-24.6173,4.6635,
                                48.1777,-23.0878,4.6635,
                                62.9031,-8.4035,4.6635,
                                66.0919,-8.8239,4.6635


                                );			glBegin(GL_QUADS);{
				glVertex3f(50.2543,-24.6173,4.6635);

				glVertex3f(48.1777,-23.0878,4.6635);
				glVertex3f(62.9031,-8.4035,4.6635);
				glVertex3f(66.0919,-8.8239,4.6635);

			}glEnd();

 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(grass,
                                66.0919,-8.8239,4.6635,
                                62.9031,-8.4035,4.6635,
                                52.5433,9.6390,4.6635,
                                54.9498,10.5679,4.6635


                                );			glBegin(GL_QUADS);{

				glVertex3f(66.0919,-8.8239,4.6635);
                glVertex3f(62.9031,-8.4035,4.6635);
				glVertex3f(52.5433,9.6390,4.6635);
				glVertex3f(54.9498,10.5679,4.6635);

			}glEnd();

//3rd layer

 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                46.2973,-27.2255,4.6635,
                                44.1035,-25.162,4.6635,
                                45.8320,-21.8613,4.6635,
                                48.1777,-23.0878,4.6635


                                );
                                			glBegin(GL_QUADS);{
				glVertex3f(46.2973,-27.2255,4.6635);
				glVertex3f(44.1035,-25.162,4.6635);
				glVertex3f(45.8320,-21.8613,4.6635);
				glVertex3f(48.1777,-23.0878,4.6635);

			}glEnd();

 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                48.1777,-23.0878,4.6635,
                                45.8320,-21.8613,4.6635,
                                59.7539,-7.9782,4.6635,
                                62.9031,-8.4035,4.6635


                                );			glBegin(GL_QUADS);{
				glVertex3f(48.1777,-23.0878,4.6635);
				glVertex3f(45.8320,-21.8613,4.6635);
				glVertex3f(59.7539,-7.9782,4.6635);
				glVertex3f(62.9031,-8.4035,4.6635);

			}glEnd();

 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                62.9031,-8.4035,4.6635,
                                59.7539,-7.9782,4.6635,
                                50.1503,9.1157,4.6635,
                                52.5433,9.6390,4.6635


                                );			glBegin(GL_QUADS);{

                glVertex3f(62.9031,-8.4035,4.6635);

				glVertex3f(59.7539,-7.9782,4.6635);
				glVertex3f(50.1503,9.1157,4.6635);
				glVertex3f(52.5433,9.6390,4.6635);

			}glEnd();

//4th layer (grass)


 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(grass,
                                45.8320,-21.8613,4.6635,
                                41.0999,-19.3869,1.9547,
                                53.3916,-7.1294,1.9547,
                                59.7539,-7.9782,4.6635


                                );			glBegin(GL_QUADS);{
				glVertex3f(45.8320,-21.8613,4.6635);
				glVertex3f(41.0999,-19.3869,1.9547);
				glVertex3f(53.3916,-7.1294,1.9547);
				glVertex3f(59.7539,-7.9782,4.6635);

			}glEnd();

                    drawTexture(grass,
                                59.7539,-7.9782,4.6635,
                                53.3916,-7.1294,1.9547,
                                44.7376,7.9322,1.9547,
                                50.1503,9.1157,4.6635


                                );			glBegin(GL_QUADS);{

				glVertex3f(59.7539,-7.9782,4.6635);

				glVertex3f(53.3916,-7.1294,1.9547);
				glVertex3f(44.7376,7.9322,1.9547);
				glVertex3f(50.1503,9.1157,4.6635);

			}glEnd();

//5th layer (near water)

 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                41.0999,-19.3869,1.9547,
                                40.1503,-18.8904,1.9051,
                                52.115,-6.959,1.9051,
                                53.3916,-7.1294,1.9547


                                );			glBegin(GL_QUADS);{

				glVertex3f(41.0999,-19.3869,1.9547);
				glVertex3f(40.1503,-18.8904,1.9051);
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(53.3916,-7.1294,1.9547);

			}glEnd();

 glColor3f(1.0, 1.0, 1.0);

                    drawTexture(floors,
                                53.3916,-7.1294,1.9547,
                                52.115,-6.959,1.9051,
                                43.6904,7.7032,1.9051,
                                44.7376,7.9322,1.9547


                                );			glBegin(GL_QUADS);{


				glVertex3f(53.3916,-7.1294,1.9547);

				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(43.6904,7.7032,1.9051);
				glVertex3f(44.7376,7.9322,1.9547);

			}glEnd();




		}glPopMatrix();

}

void drawBase_water()//complete
{
        glPushMatrix();{

 glColor3f(0.8, 0.8, 0.8);

                    drawTexture(water ,
                    52.115,-6.959,1.9051,
                    40.1503,-18.8904,1.9051,
                    35.4776,-16.0489,1.9051,
                    35.4776,-16.0489,1.9051
                    );

 glColor3f(0.8, 0.8, 0.8);

                   drawTexture(water ,
                    52.115,-6.959,1.9051,
                    40.1503,-18.8904,1.9051,
                    35.4776,-16.0489,1.9051,
                    35.4776,-16.0489,1.9051
                    );

			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(40.1503,-18.8904,1.9051);
				glVertex3f(35.4776,-16.0489,1.9051);
			}glEnd();


                   drawTexture(water ,
                    52.115,-6.959,1.9051,
                    35.4776,-16.0489,1.9051,
                    33.5702,-14.1350,1.9547,
                    33.5702,-14.1350,1.9547
                    );

    		  //glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(35.4776,-16.0489,1.9051);
				glVertex3f(33.5702,-14.1350,1.9547);
			}glEnd();


    	             drawTexture(water ,
                    52.115,-6.959,1.9051,
                    33.5702,-14.1350,1.9547,
                    32.0889,-11.8751,1.9051,
                    32.0889,-11.8751,1.9051
                    );
	glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(33.5702,-14.1350,1.9547);
				glVertex3f(32.0889,-11.8751,1.9051);
			}glEnd();



    	             drawTexture(water ,
                              52.115,-6.959,1.9051,
                              32.0889,-11.8751,1.9051,
                              31.095,-9.3624,1.9051,
                              31.095,-9.3624,1.9051
                              	);

                glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(32.0889,-11.8751,1.9051);
				glVertex3f(31.095,-9.3624,1.9051);
			}glEnd();


    	             drawTexture(water ,
                              52.115,-6.959,1.9051,
                              31.095,-9.3624,1.9051,
                              30.6296,-6.7007,1.9051,
                              30.6296,-6.7007,1.9051
                              	);
                              				glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(31.095,-9.3624,1.9051);
				glVertex3f(30.6296,-6.7007,1.9051);
			}glEnd();

    	             drawTexture(water ,
                              52.115,-6.959,1.9051,
                              30.6296,-6.7007,1.9051,
                              30.7117,-3.9998,1.9547,
                              30.7117,-3.9998,1.9547
                              	);

			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(30.6296,-6.7007,1.9051);
				glVertex3f(30.7117,-3.9998,1.9547);
			}glEnd();

    	             drawTexture(water ,
                              52.115,-6.959,1.9051,
                              30.7117,-3.9998,1.9547,
                              31.3380,-1.3713,1.9051,
                              31.3380,-1.3713,1.9051
                              	);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(30.7117,-3.9998,1.9547);
				glVertex3f(31.3380,-1.3713,1.9051);
			}glEnd();


    	             drawTexture(water ,
                              52.115,-6.959,1.9051,
                              31.3380,-1.3713,1.9051,
                              32.4828,1.0764,1.9051,
                              32.4828,1.0764,1.9051
                              	);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(31.3380,-1.3713,1.9051);
				glVertex3f(32.4828,1.0764,1.9051);
			}glEnd();

    	             drawTexture(water ,
                              52.115,-6.959,1.9051,
                              32.4828,1.0764,1.9051,
                              34.0986,3.2422,1.9051,
                              34.0986,3.2422,1.9051
                              	);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(32.4828,1.0764,1.9051);
				glVertex3f(34.0986,3.2422,1.9051);
			}glEnd();


    	             drawTexture(water ,
                              52.115,-6.959,1.9051,
                              34.0986,3.2422,1.9051,
                              38.46,6.3857,1.9051,
                              38.46,6.3857,1.9051
                              	);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(34.0986,3.2422,1.9051);
				glVertex3f(38.46,6.3857,1.9051);
			}glEnd();

    	             drawTexture(water ,
                              52.115,-6.959,1.9051,
                              38.46,6.3857,1.9051,
                              41.0256,7.2337,1.9547,
                              41.0256,7.2337,1.9547
                              	);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(38.46,6.3857,1.9051);
				glVertex3f(41.0256,7.2337,1.9547);
			}glEnd();


    	             drawTexture(water ,
                              52.115,-6.959,1.9051,
                              41.0256,7.2337,1.9547,
                              43.6904,7.7032,1.9547,
                              43.6904,7.7032,1.9547
                              	);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(41.0256,7.2337,1.9547);
				glVertex3f(43.6904,7.7032,1.9547);
			}glEnd();






		}glPopMatrix();

}

void drawBase_1()//complete
{


	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			drawBase1_leftface();
			drawBase_water();
			//drawBase_innerfloor();
			drawBaseLowerFloor();
			drawBase_innerPillar();
			drawStairSide();
			drawStair();
			drawUpperFloor();
			drawStairSideInside();
			drawRelling();
			drawInnerCircle2();
			//drawPetal_2_rightface();
			//drawPetal_2_rightback();


			//------------------- back part of a petal

		}glPopMatrix();
	}


}


int LoadBitmaps(char *filename)
{
    int i, j=0;
    FILE *l_file;
    unsigned char *l_texture;

    BITMAPFILEHEADER fileheader;
    BITMAPINFOHEADER infoheader;
    RGBTRIPLE rgb;

    num_texture++;

    if( (l_file = fopen(filename, "rb"))==NULL) return (-1);

    fread(&fileheader, sizeof(fileheader), 1, l_file);

    fseek(l_file, sizeof(fileheader), SEEK_SET);
    fread(&infoheader, sizeof(infoheader), 1, l_file);

    l_texture = (byte *) malloc(infoheader.biWidth * infoheader.biHeight * 4);
    memset(l_texture, 0, infoheader.biWidth * infoheader.biHeight * 4);

 for (i=0; i < infoheader.biWidth*infoheader.biHeight; i++)
    {
            fread(&rgb, sizeof(rgb), 1, l_file);

            l_texture[j+0] = rgb.rgbtRed;
            l_texture[j+1] = rgb.rgbtGreen;
            l_texture[j+2] = rgb.rgbtBlue;
            l_texture[j+3] = 255;
            j += 4;
    }
    fclose(l_file);

    glBindTexture(GL_TEXTURE_2D, num_texture);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);

// glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    glTexImage2D(GL_TEXTURE_2D, 0, 4, infoheader.biWidth, infoheader.biHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);
     gluBuild2DMipmaps(GL_TEXTURE_2D, 4, infoheader.biWidth, infoheader.biHeight, GL_RGBA, GL_UNSIGNED_BYTE, l_texture);

    free(l_texture);

    return (num_texture);

}

void drawInnerCylinder()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.20, 0.60, 0.80);
			glBegin(GL_QUADS);{
				glVertex3f(0.1925,-4.6020,5.9534);
				glVertex3f(0.1925,-4.6020,34.2124);
				glVertex3f(1.6491,-4.0018,34.2124);
				glVertex3f(1.6491,-4.0018,5.9534);
			}glEnd();
			glColor3f(0.80, 0.60, 0.20);
			glBegin(GL_QUADS);{
				glVertex3f(1.6491,-4.0018,5.9534);
				glVertex3f(1.6491,-4.0018,34.2124);
				glVertex3f(3.1056,-3.4016,34.2124);
				glVertex3f(3.1056,-3.4016,5.9534);
			}glEnd();
		}glPopMatrix();

	}
}
void drawPetal_top()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.40, 0.50, 0.60);
			glBegin(GL_POLYGON);{
				glVertex3f(0.1925,-4.6020,34.2124);
				glVertex3f(0.0,0.0,36.3171);
				glVertex3f(3.1056,-3.4016,34.2124);
			}glEnd();
		}glPopMatrix();

	}
}



void drawPetal_0()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.10, 0.20, 0.30);
			glBegin(GL_QUADS);{
				glVertex3f(0.9033,-21.5959,5.9534);
				glVertex3f(0.1925,-4.602,34.2124);
				glVertex3f(3.1056,-3.4016,34.2124);
				glVertex3f(14.5736,-15.9628,5.9534);
			}glEnd();
		}glPopMatrix();

	}
}



void drawQuadT(float x1,float y1,float z1,
               float x2,float y2,float z2
               )
{
    float dim = 0.0009;
    glPushMatrix();
            glColor3f(1.0, 1.00, 1.0);
            glBegin(GL_QUADS);{
                glVertex3f(x1,y1,z1);
                glVertex3f(x2,y2,z2);
                glVertex3f(x2+dim,y2+dim,z2);
                glVertex3f(x1+dim,y1+dim,z1);
            }glEnd();
	glPopMatrix();
}


void drawPetal_3_right_transparent()
{
    glColor3f(1.0, 1.0, 1.0);

	/* b4 quad
	glEnable( GL_BLEND ); glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); glColor4f(1,1,1,TRANSPARENCY_FACTOR); glDepthMask(0);
	*/

	/* aftr quad
		glDepthMask(1); glColor4f(0,0,0,1); glDisable (GL_BLEND);
	*/
	//glEnable( GL_BLEND ); glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); glColor4f(1,1,1,TRANSPARENCY_FACTOR); glDepthMask(0);

	glEnable( GL_BLEND ); glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); glColor4f(1,1,1,TRANSPARENCY_FACTOR); glDepthMask(0);

	glBegin(GL_QUADS);{
		glVertex3f(-14.6279,16.0222,5.9534);
		glVertex3f(-14.6279,16.0222,9.5255);
		glVertex3f(-14.3635,16.9244,9.4686);
		glVertex3f(-14.3635,16.9244,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-14.3635,16.9244,5.9534);
		glVertex3f(-14.3635,16.9244,9.4686);
		glVertex3f(-14.1001,17.8229,9.3663);
		glVertex3f(-14.1001,17.8229,5.9534);
	}glEnd();



	glBegin(GL_QUADS);{
		glVertex3f(-14.1001,17.8229,5.9534);
		glVertex3f(-14.1001,17.8229,9.3663);
		glVertex3f(-13.8385,18.7155,9.219);
		glVertex3f(-13.8385,18.7155,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-13.8385,18.7155,5.9534);
		glVertex3f(-13.8385,18.7155,9.219);
		glVertex3f(-13.5792,19.6003,9.0268);
		glVertex3f(-13.5792,19.6003,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-13.5792,19.6003,5.9534);
		glVertex3f(-13.5792,19.6003,9.0268);
		glVertex3f(-13.3228,20.4752,8.7904);
		glVertex3f(-13.3228,20.4752,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-13.3228,20.4752,5.9534);
		glVertex3f(-13.3228,20.4752,8.7904);
		glVertex3f(-13.0699,21.3381,8.5102);
		glVertex3f(-13.0699,21.3381,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-13.0699,21.3381,5.9534);
		glVertex3f(-13.0699,21.3381,8.5102);
		glVertex3f(-12.821,22.187,8.1869);
		glVertex3f(-12.821,22.187,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-12.821,22.187,5.9534);
		glVertex3f(-12.821,22.187,8.1869);
		glVertex3f(-12.5769,23.0199,7.8212);
		glVertex3f(-12.5769,23.0199,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-12.5769,23.0199,5.9534);
		glVertex3f(-12.5769,23.0199,7.8212);
		glVertex3f(-12.3381,23.8349,7.414);
		glVertex3f(-12.3381,23.8349,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-12.3381,23.8349,5.9534);
		glVertex3f(-12.3381,23.8349,7.414);
		glVertex3f(-12.105,24.63,6.9663);
		glVertex3f(-12.105,24.63,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-12.105,24.63,5.9534);
		glVertex3f(-12.105,24.63,6.9663);
		glVertex3f(-11.8783,25.4035,6.479);
		glVertex3f(-11.8783,25.4035,5.9534);
	}glEnd();
	glDepthMask(1); glColor4f(0,0,0,1); glDisable (GL_BLEND);
	glBegin(GL_QUADS);{


		glVertex3f(-11.8783,25.4035,5.9534);
		glVertex3f(-11.8783,25.4035,6.479);
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-11.6585,26.1535,5.9534);

	}glEnd();

}

void drawPetal_3_left_transparent()
{

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.1953,14.0139,6.479);
		glVertex3f(-24.1953,14.0139,5.9534);
		glVertex3f(-24.9601,13.8534,5.9534);
	}glEnd();

	glEnable( GL_BLEND ); glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); glColor4f(1,1,1,TRANSPARENCY_FACTOR); glDepthMask(0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.1953,14.0139,5.9534);
		glVertex3f(-23.4065,14.1795,6.9663);
		glVertex3f(-23.4065,14.1795,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-23.4065,14.1795,5.9534);
		glVertex3f(-23.4065,14.1795,6.9663);
		glVertex3f(-22.5955,14.3498,7.414);
		glVertex3f(-22.5955,14.3498,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-22.5955,14.3498,5.9534);
		glVertex3f(-22.5955,14.3498,7.414);
		glVertex3f(-21.7644,14.5242,7.8212);
		glVertex3f(-21.7644,14.5242,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-21.7644,14.5242,5.9534);
		glVertex3f(-21.7644,14.5242,7.8212);
		glVertex3f(-20.915,14.7025,8.1869);
		glVertex3f(-20.915,14.7025,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-20.915,14.7025,5.9534);
		glVertex3f(-20.915,14.7025,8.1869);
		glVertex3f(-20.0492,14.8842,8.5102);
		glVertex3f(-20.0492,14.8842,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-20.0492,14.8842,5.9534);
		glVertex3f(-20.0492,14.8842,8.5102);
		glVertex3f(-19.1692,15.069,8.7904);
		glVertex3f(-19.1692,15.069,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-19.1692,15.069,5.9534);
		glVertex3f(-19.1692,15.069,8.7904);
		glVertex3f(-18.277,15.2563,9.0268);
		glVertex3f(-18.277,15.2563,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-18.277,15.2563,5.9534);
		glVertex3f(-18.277,15.2563,9.0268);
		glVertex3f(-17.3746,15.4457,9.219);
		glVertex3f(-17.3746,15.4457,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-17.3746,15.4457,5.9534);
		glVertex3f(-17.3746,15.4457,9.219);
		glVertex3f(-16.4643,15.6368,9.3663);
		glVertex3f(-16.4643,15.6368,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-16.4643,15.6368,5.9534);
		glVertex3f(-16.4643,15.6368,9.3663);
		glVertex3f(-15.548,15.8291,9.4686);
		glVertex3f(-15.548,15.8291,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-15.548,15.8291,5.9534);
		glVertex3f(-15.548,15.8291,9.4686);
		glVertex3f(-14.6279,16.0222,9.5255);
		glVertex3f(-14.6279,16.0222,5.9534);
	}glEnd();
	glDepthMask(1); glColor4f(0,0,0,1); glDisable (GL_BLEND);
}



void drawPetal_3_rightback()//complete
{

	//triangles
	glColor3f(1.0, 1.0, 1.0);
	drawTexture(large_marble,
             -11.6585,26.1535,5.9534,
             -11.8783,25.4035,6.479,
             -11.9547,25.5084,6.5799,
             -11.9547,25.5084,6.5799

             );
	glBegin(GL_TRIANGLES);{
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-11.8783,25.4035,6.479);
		glVertex3f(-11.9547,25.5084,6.5799);
	}glEnd();

	drawTexture(large_marble,
             -11.6585,26.1535,5.9534,
             -11.9547,25.5084,6.5799,
             -12.0369,25.6201,6.6678,
             -12.0369,25.6201,6.6678

             );
	glBegin(GL_TRIANGLES);{
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-11.9547,25.5084,6.5799);
		glVertex3f(-12.0369,25.6201,6.6678);
	}glEnd();

	drawTexture(large_marble,
             -11.6585,26.1535,5.9534,
             -12.0369,25.6201,6.6678,
             -12.1243,25.7378,6.7421,
             -12.1243,25.7378,6.7421

             );
	glBegin(GL_TRIANGLES);{
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.0369,25.6201,6.6678);
		glVertex3f(-12.1243,25.7378,6.7421);
	}glEnd();

	drawTexture(large_marble,
             -11.6585,26.1535,5.9534,
             -12.1243,25.7378,6.7421,
             -12.216,25.8603,6.8021,
             -12.216,25.8603,6.8021

             );
	glBegin(GL_TRIANGLES);{
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.1243,25.7378,6.7421);
		glVertex3f(-12.216,25.8603,6.8021);
	}glEnd();

	drawTexture(large_marble,
             -11.6585,26.1535,5.9534,
             -12.216,25.8603,6.8021,
             -12.3111,25.9864,6.8471,
             -12.3111,25.9864,6.8471

             );
	glBegin(GL_TRIANGLES);{
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.216,25.8603,6.8021);
		glVertex3f(-12.3111,25.9864,6.8471);
	}glEnd();

	drawTexture(large_marble,
             -11.6585,26.1535,5.9534,
             -12.3111,25.9864,6.8471,
             -12.4088,26.1151,6.8768,
             -12.4088,26.1151,6.8768

             );
	glBegin(GL_TRIANGLES);{
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.3111,25.9864,6.8471);
		glVertex3f(-12.4088,26.1151,6.8768);
	}glEnd();

	// ------------------------------------col-1
	glColor3f(1.0, 1.0, 1.0);

	drawTexture(large_marble,
		-11.8783,25.4035,6.479,
		-12.105,24.63,6.9663,
		-12.1836,24.7379,7.0706,
		-11.9157,25.455,6.531
             );
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-11.8783,25.4035,6.479);
		glVertex3f(-12.105,24.63,6.9663);
		glVertex3f(-12.1836,24.7379,7.0706);
		glVertex3f(-11.9157,25.455,6.531);
	}glEnd();

	drawTexture(large_marble,
		-12.105,24.63,6.9663,
		-12.3381,23.8349,7.414,
		-12.4614,23.9948,7.571,
		-12.1836,24.7379,7.0706
             );
             	glBegin(GL_QUADS);{
		glVertex3f(-12.105,24.63,6.9663);
		glVertex3f(-12.3381,23.8349,7.414);
		glVertex3f(-12.4614,23.9948,7.571);
		glVertex3f(-12.1836,24.7379,7.0706);
	}glEnd();

	drawTexture(large_marble,
		-12.3381,23.8349,7.414,
		-12.5769,23.0199,7.8212,
		-12.7486,23.2365,8.0308,
		-12.4614,23.9948,7.571
             );	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-12.3381,23.8349,7.414);
		glVertex3f(-12.5769,23.0199,7.8212);
		glVertex3f(-12.7486,23.2365,8.0308);
		glVertex3f(-12.4614,23.9948,7.571);
	}glEnd();

	drawTexture(large_marble,
		-12.5769,23.0199,7.8212,
		-12.821,22.187,8.1869,
		-13.0442,22.4618,8.449,
		-12.7486,23.2365,8.0308
		            );
             	glBegin(GL_QUADS);{
		glVertex3f(-12.5769,23.0199,7.8212);
		glVertex3f(-12.821,22.187,8.1869);
		glVertex3f(-13.0442,22.4618,8.449);
		glVertex3f(-12.7486,23.2365,8.0308);
	}glEnd();

	drawTexture(large_marble,
		-12.821,22.187,8.1869,
		-13.0699,21.3381,8.5102,
		-13.3477,21.6725,8.8244,
		-13.0442,22.4618,8.449
             );
             	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-12.821,22.187,8.1869);
		glVertex3f(-13.0699,21.3381,8.5102);
		glVertex3f(-13.3477,21.6725,8.8244);
		glVertex3f(-13.0442,22.4618,8.449);
	}glEnd();

	drawTexture(large_marble,
		-13.0699,21.3381,8.5102,
		-13.3228,20.4752,8.7904,
		-13.6582,20.8705,9.1562,
		-13.3477,21.6725,8.8244
             );
             	glBegin(GL_QUADS);{
		glVertex3f(-13.0699,21.3381,8.5102);
		glVertex3f(-13.3228,20.4752,8.7904);
		glVertex3f(-13.6582,20.8705,9.1562);
		glVertex3f(-13.3477,21.6725,8.8244);
	}glEnd();

	drawTexture(large_marble,
		-13.3228,20.4752,8.7904,
		-13.5792,19.6003,9.0286,
		-13.9749,20.0575,9.4435,
		-13.6582,20.8705,9.1562
             );	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-13.3228,20.4752,8.7904);
		glVertex3f(-13.5792,19.6003,9.0286);
		glVertex3f(-13.9749,20.0575,9.4435);
		glVertex3f(-13.6582,20.8705,9.1562);
	}glEnd();

	drawTexture(large_marble,
		-13.5792,19.6003,9.0286,
		-13.8385,18.7155,9.219,
		-14.297,19.2356,9.6856,
		-13.9749,20.0575,9.4435
             );	glBegin(GL_QUADS);{
		glVertex3f(-13.5792,19.6003,9.0286);
		glVertex3f(-13.8385,18.7155,9.219);
		glVertex3f(-14.297,19.2356,9.6856);
		glVertex3f(-13.9749,20.0575,9.4435);
	}glEnd();

	drawTexture(large_marble,
             		-13.8385,18.7155,9.219,
		-14.1001,17.8229,9.3663,
		-14.6237,18.4066,9.881,
		-14.297,19.2356,9.6856
             );	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-13.8385,18.7155,9.219);
		glVertex3f(-14.1001,17.8229,9.3663);
		glVertex3f(-14.6237,18.4066,9.8818);
		glVertex3f(-14.297,19.2356,9.6856);
	}glEnd();

	drawTexture(large_marble,
		-14.1001,17.8229,9.3663,
		-14.3635,16.9244,9.4686,
		-14.9541,17.5726,10.0317,
		-14.6237,18.4066,9.8818
             );	glBegin(GL_QUADS);{
		glVertex3f(-14.1001,17.8229,9.3663);
		glVertex3f(-14.3635,16.9244,9.4686);
		glVertex3f(-14.9541,17.5726,10.0317);
		glVertex3f(-14.6237,18.4066,9.8818);
	}glEnd();

	drawTexture(large_marble,
		//2->first, 3->last
		-14.3635,16.9244,9.4686,
		-14.6279,16.0222,9.5255,
		-15.2873,16.7353,10.1349,
		-14.9541,17.5726,10.0317
             );	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-14.3635,16.9244,9.4686);
		glVertex3f(-14.6279,16.0222,9.5255);
		glVertex3f(-15.2873,16.7353,10.1349);
		glVertex3f(-14.9541,17.5726,10.0317);
	}glEnd();
	//+======================+++++++++
	//--------------------------column: 2

	drawTexture(large_marble,
		-11.9157,25.455,6.531,
		-12.1836,24.7379,7.0706,
		-12.35,24.9554,7.26,
		-11.9951,25.5634,6.6255
             );
             	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-11.9157,25.455,6.531);
		glVertex3f(-12.1836,24.7379,7.0706);
		glVertex3f(-12.35,24.9554,7.26);
		glVertex3f(-11.9951,25.5634,6.6255);
	}glEnd();

	drawTexture(large_marble,
		-12.1836,24.7379,7.0706,
		-12.4614,23.9948,7.571,
		-12.7222,24.3309,7.855,
		-12.1836,24.7379,7.0706
             );	glBegin(GL_QUADS);{
		glVertex3f(-12.1836,24.7379,7.0706);
		glVertex3f(-12.4614,23.9948,7.571);
		glVertex3f(-12.7222,24.3309,7.855);
		glVertex3f(-12.1836,24.7379,7.0706);
	}glEnd();

	drawTexture(large_marble,
		-12.4614,23.9948,7.571,

		-12.7486,23.2365,8.0308,
		-13.1106,23.6914,8.4092,
		-12.7222,24.3309,7.855
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.4614,23.9948,7.571);

		glVertex3f(-12.7486,23.2365,8.0308);
		glVertex3f(-13.1106,23.6914,8.4092);
		glVertex3f(-12.7222,24.3309,7.855);
	}glEnd();

	drawTexture(large_marble,
		-12.7486,23.2365,8.0308,

		-13.0442,22.4618,8.449,
		-13.5142,23.0383,8.9208,
		-13.1106,23.6914,8.4092
             );	glBegin(GL_QUADS);{
		glVertex3f(-12.7486,23.2365,8.0308);

		glVertex3f(-13.0442,22.4618,8.449);
		glVertex3f(-13.5142,23.0383,8.9208);
		glVertex3f(-13.1106,23.6914,8.4092);
	}glEnd();

	drawTexture(large_marble,
		-13.0442,22.4618,8.449,

		-13.3477,21.6725,8.8244,
		-13.9317,22.3733,9.3886,
		-13.5142,23.0383,8.9208
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.0442,22.4618,8.449);

		glVertex3f(-13.3477,21.6725,8.8244);
		glVertex3f(-13.9317,22.3733,9.3886);
		glVertex3f(-13.5142,23.0383,8.9208);
	}glEnd();

	drawTexture(large_marble,
		-13.3477,21.6725,8.8244,

		-13.6582,20.8705,9.1562,
		-14.3621,21.6979,9.8113,
		-13.9317,22.3733,9.3886
             );	glBegin(GL_QUADS);{
		glVertex3f(-13.3477,21.6725,8.8244);

		glVertex3f(-13.6582,20.8705,9.1562);
		glVertex3f(-14.3621,21.6979,9.8113);
		glVertex3f(-13.9317,22.3733,9.3886);
	}glEnd();

	drawTexture(large_marble,
		-13.6582,20.8705,9.1562,

		-13.9749,20.0575,9.4435,
		-14.804,21.0137,10.1876,
		-14.3621,21.6979,9.8113
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.6582,20.8705,9.1562);

		glVertex3f(-13.9749,20.0575,9.4435);
		glVertex3f(-14.804,21.0137,10.1876);
		glVertex3f(-14.3621,21.6979,9.8113);
	}glEnd();

	drawTexture(large_marble,
		-13.9749,20.0575,9.4435,

		-14.297,19.2356,9.6856,
		-15.2563,20.3223,10.5166,
		-14.804,21.0137,10.1876
             );	glBegin(GL_QUADS);{
		glVertex3f(-13.9749,20.0575,9.4435);

		glVertex3f(-14.297,19.2356,9.6856);
		glVertex3f(-15.2563,20.3223,10.5166);
		glVertex3f(-14.804,21.0137,10.1876);
	}glEnd();

	drawTexture(large_marble,
		-14.297,19.2356,9.6856,

		-14.6237,18.4066,9.8818,
		-15.7176,19.6254,10.7973,
		-15.2563,20.3223,10.5166
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.297,19.2356,9.6856);

		glVertex3f(-14.6237,18.4066,9.8818);
		glVertex3f(-15.7176,19.6254,10.7973);
		glVertex3f(-15.2563,20.3223,10.5166);
	}glEnd();

	drawTexture(large_marble,
		-14.6237,18.4066,9.8818,

		-14.9541,17.5726,10.0317,
		-16.1865,18.9247,11.0289,
		-15.7176,19.6254,10.7973
             );	glBegin(GL_QUADS);{
		glVertex3f(-14.6237,18.4066,9.8818);

		glVertex3f(-14.9541,17.5726,10.0317);
		glVertex3f(-16.1865,18.9247,11.0289);
		glVertex3f(-15.7176,19.6254,10.7973);
	}glEnd();

	drawTexture(large_marble,
		-14.9541,17.5726,10.0317,

		-15.2873,16.7353,10.1349,
		-16.6618,18.2217,11.2107,
		-16.1865,18.9247,11.0289
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.9541,17.5726,10.0317);

		glVertex3f(-15.2873,16.7353,10.1349);
		glVertex3f(-16.6618,18.2217,11.2107);
		glVertex3f(-16.1865,18.9247,11.0289);

	}glEnd();
	//--------------------------column: 3

	drawTexture(large_marble,
		-11.9951,25.5634,6.6255,
		-12.35,24.9554,7.26,
		-12.5275,25.1888,7.422,
		-12.08,25.6783,6.7067
		            );
			glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-11.9951,25.5634,6.6255);
		glVertex3f(-12.35,24.9554,7.26);
		glVertex3f(-12.5275,25.1888,7.422);
		glVertex3f(-12.08,25.6783,6.7067);

	}glEnd();

	drawTexture(large_marble,
             		-12.35,24.9554,7.26,

		-12.7222,24.3309,7.855,
		-12.9995,24.6861,8.0971,
		-12.5275,25.1888,7.422
             );	glBegin(GL_QUADS);{
		glVertex3f(-12.35,24.9554,7.26);

		glVertex3f(-12.7222,24.3309,7.855);
		glVertex3f(-12.9995,24.6861,8.0971);
		glVertex3f(-12.5275,25.1888,7.422);
	}glEnd();

	drawTexture(large_marble,
		-12.7222,24.3309,7.855,

		-13.1106,23.6914,8.4092,
		-13.4948,24.1714,8.7302,
		-12.9995,24.6861,8.0971
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.7222,24.3309,7.855);

		glVertex3f(-13.1106,23.6914,8.4092);
		glVertex3f(-13.4948,24.1714,8.7302);
		glVertex3f(-12.9995,24.6861,8.0971);
	}glEnd();

	drawTexture(large_marble,
		-13.1106,23.6914,8.4092,

		-13.5142,23.0383,8.9208,
		-14.0118,23.646,9.3194,
		-13.4948,24.1714,8.7302
             );	glBegin(GL_QUADS);{
		glVertex3f(-13.1106,23.6914,8.4092);

		glVertex3f(-13.5142,23.0383,8.9208);
		glVertex3f(-14.0118,23.646,9.3194);
		glVertex3f(-13.4948,24.1714,8.7302);
	}glEnd();

	drawTexture(large_marble,
             		-13.5142,23.0383,8.9208,
		-13.9317,22.3733,9.3886,
		-14.549,23.1112,9.863,
		-14.0118,23.646,9.3194
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.5142,23.0383,8.9208);
		glVertex3f(-13.9317,22.3733,9.3886);
		glVertex3f(-14.549,23.1112,9.863);
		glVertex3f(-14.0118,23.646,9.3194);
	}glEnd();

	drawTexture(large_marble,
		-13.9317,22.3733,9.3886,
		-14.3621,21.6979,9.8113,
		-15.1046,22.5682,10.3594,
		-14.549,23.1112,9.863
             );	glBegin(GL_QUADS);{
		glVertex3f(-13.9317,22.3733,9.3886);
		glVertex3f(-14.3621,21.6979,9.8113);
		glVertex3f(-15.1046,22.5682,10.3594);
		glVertex3f(-14.549,23.1112,9.863);
	}glEnd();

	drawTexture(large_marble,
		-14.3621,21.6979,9.8113,
		-14.804,21.0137,10.1876,
		-15.6771,22.0183,10.8072,
		-15.1046,22.5682,10.3594
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.3621,21.6979,9.8113);
		glVertex3f(-14.804,21.0137,10.1876);
		glVertex3f(-15.6771,22.0183,10.8072);
		glVertex3f(-15.1046,22.5682,10.3594);
	}glEnd();

	drawTexture(large_marble,
		-14.804,21.0137,10.1876,
		-15.2563,20.3223,10.5166,
		-16.2647,21.4629,11.2051,
		-15.6771,22.0183,10.8072
             );	glBegin(GL_QUADS);{
		glVertex3f(-14.804,21.0137,10.1876);
		glVertex3f(-15.2563,20.3223,10.5166);
		glVertex3f(-16.2647,21.4629,11.2051);
		glVertex3f(-15.6771,22.0183,10.8072);
	}glEnd();

	drawTexture(large_marble,
		-15.2563,20.3223,10.5166,
		-15.7176,19.6254,10.7973,
		-16.8656,20.9033,11.5518,
		-16.2647,21.4629,11.2051
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.2563,20.3223,10.5166);
		glVertex3f(-15.7176,19.6254,10.7973);
		glVertex3f(-16.8656,20.9033,11.5518);
		glVertex3f(-16.2647,21.4629,11.2051);
	}glEnd();

	drawTexture(large_marble,
		-15.7176,19.6254,10.7973,
		-16.1865,18.9247,11.0289,
		-17.4779,20.3408,11.8464,
		-16.8656,20.9033,11.5518
             );	glBegin(GL_QUADS);{
		glVertex3f(-15.7176,19.6254,10.7973);
		glVertex3f(-16.1865,18.9247,11.0289);
		glVertex3f(-17.4779,20.3408,11.8464);
		glVertex3f(-16.8656,20.9033,11.5518);
	}glEnd();

	drawTexture(large_marble,
		-16.1865,18.9247,11.0289,
		-16.6618,18.2217,11.2107,
		-18.0998,19.7769,12.0879,
		-17.4779,20.3408,11.8464
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-16.1865,18.9247,11.0289);
		glVertex3f(-16.6618,18.2217,11.2107);
		glVertex3f(-18.0998,19.7769,12.0879);
		glVertex3f(-17.4779,20.3408,11.8464);
	}glEnd();
	//--------------------------column: 4


	drawTexture(large_marble,
		-12.08,25.6783,6.7067,
		-12.5275,25.1888,7.422,
		-12.7144,25.4327,7.5552,
		-12.1696,25.7985,6.7739
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.08,25.6783,6.7067);
		glVertex3f(-12.5275,25.1888,7.422);
		glVertex3f(-12.7144,25.4327,7.5552);
		glVertex3f(-12.1696,25.7985,6.7739);
	}glEnd();

	drawTexture(large_marble,
		-12.5275,25.1888,7.422,
		-12.9995,24.6861,8.0971,
		-13.291,25.057,8.295,
		-12.7144,25.4327,7.5552
             );	glBegin(GL_QUADS);{
		glVertex3f(-12.5275,25.1888,7.422);
		glVertex3f(-12.9995,24.6861,8.0971);
		glVertex3f(-13.291,25.057,8.295);
		glVertex3f(-12.7144,25.4327,7.5552);
	}glEnd();

	drawTexture(large_marble,
		-12.9995,24.6861,8.0971,
		-13.4948,24.1714,8.7302,
		-13.8977,24.6722,8.990,
		-13.291,25.057,8.295
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.9995,24.6861,8.0971);
		glVertex3f(-13.4948,24.1714,8.7302);
		glVertex3f(-13.8977,24.6722,8.9909);
		glVertex3f(-13.291,25.057,8.295);
	}glEnd();

	drawTexture(large_marble,
		-13.4948,24.1714,8.7302,
		-14.0118,23.646,9.3194,
		-14.5326,24.2794,9.641,
		-13.8977,24.6722,8.9909
             );	glBegin(GL_QUADS);{
		glVertex3f(-13.4948,24.1714,8.7302);
		glVertex3f(-14.0118,23.646,9.3194);
		glVertex3f(-14.5326,24.2794,9.641);
		glVertex3f(-13.8977,24.6722,8.9909);
	}glEnd();

	drawTexture(large_marble,
		-14.0118,23.646,9.3194,
		-14.549,23.1112,9.863,
		-15.1938,23.8795,10.2432,
		-14.5326,24.2794,9.641
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.0118,23.646,9.3194);
		glVertex3f(-14.549,23.1112,9.863);
		glVertex3f(-15.1938,23.8795,10.2432);
		glVertex3f(-14.5326,24.2794,9.641);
	}glEnd();

	drawTexture(large_marble,
		-14.549,23.1112,9.863,
		-15.1046,22.5682,10.3594,
		-15.879,23.4734,10.7956,
		-15.1938,23.8795,10.2432
             );	glBegin(GL_QUADS);{
		glVertex3f(-14.549,23.1112,9.863);
		glVertex3f(-15.1046,22.5682,10.3594);
		glVertex3f(-15.879,23.4734,10.7956);
		glVertex3f(-15.1938,23.8795,10.2432);
	}glEnd();

	drawTexture(large_marble,
		-15.1046,22.5682,10.3594,
		-15.6771,22.0183,10.8072,
		-16.5601,23.0771,11.2806,
		-15.879,23.4734,10.7956
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.1046,22.5682,10.3594);
		glVertex3f(-15.6771,22.0183,10.8072);
		glVertex3f(-16.5601,23.0771,11.2806);
		glVertex3f(-15.879,23.4734,10.7956);
	}glEnd();

	drawTexture(large_marble,
		-15.6771,22.0183,10.8072,
		-16.2647,21.4629,11.2051,
		-17.313,22.6469,11.7447,
		-16.5601,23.0771,11.2806
             );	glBegin(GL_QUADS);{
		glVertex3f(-15.6771,22.0183,10.8072);
		glVertex3f(-16.2647,21.4629,11.2051);
		glVertex3f(-17.313,22.6469,11.7447);
		glVertex3f(-16.5601,23.0771,11.2806);
	}glEnd();

	drawTexture(large_marble,
		-16.2647,21.4629,11.2051,
		-16.8656,20.9033,11.5518,
		-18.0572,22.2285,12.1385,
		-17.313,22.6469,11.7447
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-16.2647,21.4629,11.2051);
		glVertex3f(-16.8656,20.9033,11.5518);
		glVertex3f(-18.0572,22.2285,12.1385);
		glVertex3f(-17.313,22.6469,11.7447);
	}glEnd();

	drawTexture(large_marble,
		-16.8656,20.9033,11.5518,
		-17.4779,20.3408,11.8464,
		-18.8164,21.808,12.4767,
		-18.0572,22.2285,12.1385
             );	glBegin(GL_QUADS);{
		glVertex3f(-16.8656,20.9033,11.5518);
		glVertex3f(-17.4779,20.3408,11.8464);
		glVertex3f(-18.8164,21.808,12.4767);
		glVertex3f(-18.0572,22.2285,12.1385);
	}glEnd();

	drawTexture(large_marble,
    	-17.4779,20.3408,11.8464,
		-18.0998,19.7769,12.0879,
		-19.5882,21.3865,12.7583,
		-18.8164,21.808,12.4767
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-17.4779,20.3408,11.8464);
		glVertex3f(-18.0998,19.7769,12.0879);
		glVertex3f(-19.5882,21.3865,12.7583);
		glVertex3f(-18.8164,21.808,12.4767);
	}glEnd();

	//--------------------------column: 5
	drawTexture(large_marble,
		-12.1696,25.7985,6.7739,
		-12.7144,25.4327,7.5552,
		-12.909,25.685,7.6585,
		-12.2631,25.923,6.8265
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.1696,25.7985,6.7739);
		glVertex3f(-12.7144,25.4327,7.5552);
		glVertex3f(-12.909,25.685,7.6585);
		glVertex3f(-12.2631,25.923,6.8265);
	}glEnd();

	drawTexture(large_marble,
		-12.7144,25.4327,7.5552,
		-13.291,25.057,8.295,
		-13.5939,25.4402,8.4468,
		-12.909,25.685,7.6585
             );	glBegin(GL_QUADS);{
		glVertex3f(-12.7144,25.4327,7.5552);
		glVertex3f(-13.291,25.057,8.295);
		glVertex3f(-13.5939,25.4402,8.4468);
		glVertex3f(-12.909,25.685,7.6585);
	}glEnd();

	drawTexture(large_marble,
		-13.291,25.057,8.295,
		-13.8977,24.6722,8.9909,
		-14.3256,25.1892,9.189,
		-13.5939,25.4402,8.4468
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.291,25.057,8.295);
		glVertex3f(-13.8977,24.6722,8.9909);
		glVertex3f(-14.3256,25.1892,9.189);
		glVertex3f(-13.5939,25.4402,8.4468);
	}glEnd();

	drawTexture(large_marble,
		-13.8977,24.6722,8.9909,
		-14.5326,24.2794,9.641,
		-15.0719,24.9326,9.8826,
		-14.3256,25.1892,9.189
             );	glBegin(GL_QUADS);{
		glVertex3f(-13.8977,24.6722,8.9909);
		glVertex3f(-14.5326,24.2794,9.641);
		glVertex3f(-15.0719,24.9326,9.8826);
		glVertex3f(-14.3256,25.1892,9.189);
	}glEnd();

	drawTexture(large_marble,
		//2->first, 3->last & before 3 ->2
		-14.5326,24.2794,9.641,
		-15.1938,23.8795,10.2432,
		-15.8602,24.671,10.5256,
		-15.0719,24.9326,9.8826
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-14.5326,24.2794,9.641);
		glVertex3f(-15.1938,23.8795,10.2432);
		glVertex3f(-15.8602,24.671,10.5256);
		glVertex3f(-15.0719,24.9326,9.8826);
	}glEnd();

	drawTexture(large_marble,
		-15.1938,23.8795,10.2432,
		-15.879,23.4734,10.7956,
		-16.6781,24.4052,11.1158,
		-15.8602,24.671,10.5256
             );	glBegin(GL_QUADS);{
		glVertex3f(-15.1938,23.8795,10.2432);
		glVertex3f(-15.879,23.4734,10.7956);
		glVertex3f(-16.6781,24.4052,11.1158);
		glVertex3f(-15.8602,24.671,10.5256);
	}glEnd();

	drawTexture(large_marble,
		-15.879,23.4734,10.7956,
		-16.5601,23.0771,11.2806,
		-17.5227,24.1358,11.6513,
		-16.6781,24.4052,11.1158
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.879,23.4734,10.7956);
		glVertex3f(-16.5601,23.0771,11.2806);
		glVertex3f(-17.5227,24.1358,11.6513);
		glVertex3f(-16.6781,24.4052,11.1158);
	}glEnd();

	drawTexture(large_marble,
		-16.5601,23.0771,11.2806,
		-17.313,22.6469,11.7447,
		-18.3915,23.8635,12.1306,
		-17.5227,24.1358,11.6513
             );	glBegin(GL_QUADS);{
		glVertex3f(-16.5601,23.0771,11.2806);
		glVertex3f(-17.313,22.6469,11.7447);
		glVertex3f(-18.3915,23.8635,12.1306);
		glVertex3f(-17.5227,24.1358,11.6513);
	}glEnd();

	drawTexture(large_marble,
             -17.313,22.6469,11.7447,
             -18.0572,22.2285,12.1385,
             -19.2814,23.5889,12.5519,
             -18.3915,23.8635,12.1306

             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-17.313,22.6469,11.7447);
		glVertex3f(-18.0572,22.2285,12.1385);
		glVertex3f(-19.2814,23.5889,12.5519);
		glVertex3f(-18.3915,23.8635,12.1306);
	}glEnd();

	drawTexture(large_marble,
             -18.0572,22.2285,12.1385,
             -18.8164,21.808,12.4767,
             -20.1879,23.3128,12.9141,
             -19.2814,23.5889,12.5519

             );	glBegin(GL_QUADS);{
		glVertex3f(-18.0572,22.2285,12.1385);
		glVertex3f(-18.8164,21.808,12.4767);
		glVertex3f(-20.1879,23.3128,12.9141);
		glVertex3f(-19.2814,23.5889,12.5519);
	}glEnd();

	drawTexture(large_marble,
             -18.8164,21.808,12.4767,
             -19.5882,21.3865,12.7583,
             -21.1134,23.0358,13.2159,
             -20.1879,23.3128,12.9141

             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-18.8164,21.808,12.4767);
		glVertex3f(-19.5882,21.3865,12.7583);
		glVertex3f(-21.1134,23.0358,13.2159);
		glVertex3f(-20.1879,23.3128,12.9141);
	}glEnd();

	//--------------------------column: 6

	drawTexture(large_marble,
             -12.2631,25.923,6.8265,
             -12.909,25.685,7.6585,
             -13.1095,25.9434,7.7308,
             -12.2631,25.923,6.8265

             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.2631,25.923,6.8265);
		glVertex3f(-12.909,25.685,7.6585);
		glVertex3f(-13.1095,25.9434,7.7308);
		glVertex3f(-12.2631,25.923,6.8265);

	}glEnd();

	drawTexture(large_marble,
             -12.909,25.685,7.6585,
             -13.5939,25.4402,8.4468,
             -13.9053,25.8323,8.5512,
             -13.1095,25.9434,7.7308

             );	glBegin(GL_QUADS);{
		glVertex3f(-12.909,25.685,7.6585);
		glVertex3f(-13.5939,25.4402,8.4468);
		glVertex3f(-13.9053,25.8323,8.5512);
		glVertex3f(-13.1095,25.9434,7.7308);
	}glEnd();

	drawTexture(large_marble,
             -13.5939,25.4402,8.4468,
             -14.3256,25.1892,9.189,
             -14.7446,25.7176,9.3225,
             -13.9053,25.8323,8.5512

             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.5939,25.4402,8.4468);
		glVertex3f(-14.3256,25.1892,9.189);
		glVertex3f(-14.7446,25.7176,9.3225);
		glVertex3f(-13.9053,25.8323,8.5512);
	}glEnd();

	drawTexture(large_marble,
             -14.3256,25.1892,9.189,
             -15.0719,24.9326,9.8826,
             -15.6246,25.5996,10.0421,
             -14.7446,25.7176,9.3225

             );	glBegin(GL_QUADS);{
		glVertex3f(-14.3256,25.1892,9.189);
		glVertex3f(-15.0719,24.9326,9.8826);
		glVertex3f(-15.6246,25.5996,10.0421);
		glVertex3f(-14.7446,25.7176,9.3225);
	}glEnd();

	drawTexture(large_marble,
             -15.0719,24.9326,9.8826,
             -15.8602,24.671,10.5256,
             -16.5422,25.4786,10.7077,
             -15.6246,25.5996,10.0421

             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.0719,24.9326,9.8826);
		glVertex3f(-15.8602,24.671,10.5256);
		glVertex3f(-16.5422,25.4786,10.7077);
		glVertex3f(-15.6246,25.5996,10.0421);
	}glEnd();

	drawTexture(large_marble,
             -15.8602,24.671,10.5256,
             -16.6781,24.4052,11.1158,
             -17.4945,25.355,11.317,
             -16.5422,25.4786,10.7077

             );	glBegin(GL_QUADS);{
		glVertex3f(-15.8602,24.671,10.5256);
		glVertex3f(-16.6781,24.4052,11.1158);
		glVertex3f(-17.4945,25.355,11.317);
		glVertex3f(-16.5422,25.4786,10.7077);
	}glEnd();

	drawTexture(large_marble,
             -16.6781,24.4052,11.1158,
             -17.5227,24.1358,11.6513,
             -18.4783,25.2292,11.8681,
             -17.4945,25.355,11.317

             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-16.6781,24.4052,11.1158);
		glVertex3f(-17.5227,24.1358,11.6513);
		glVertex3f(-18.4783,25.2292,11.8681);
		glVertex3f(-17.4945,25.355,11.317);
	}glEnd();

	drawTexture(large_marble,
             -17.5227,24.1358,11.6513,
             -18.3915,23.8635,12.1306,
             -19.4903,25.1013,12.3591,
             -18.4783,25.2292,11.8681

             );	glBegin(GL_QUADS);{
		glVertex3f(-17.5227,24.1358,11.6513);
		glVertex3f(-18.3915,23.8635,12.1306);
		glVertex3f(-19.4903,25.1013,12.3591);
		glVertex3f(-18.4783,25.2292,11.8681);
	}glEnd();

	drawTexture(large_marble,
             -18.3915,23.8635,12.1306,
             -19.2814,23.5889,12.5519,
             -20.5271,24.9719,12.7884,
             -19.4903,25.1013,12.3591

             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-18.3915,23.8635,12.1306);
		glVertex3f(-19.2814,23.5889,12.5519);
		glVertex3f(-20.5271,24.9719,12.7884);
		glVertex3f(-19.4903,25.1013,12.3591);
	}glEnd();

	drawTexture(large_marble,
             -19.2814,23.5889,12.5519,
             -20.1879,23.3128,12.9141,
             -21.5853,24.8413,13.1545,
             -20.5271,24.9719,12.7884

             );	glBegin(GL_QUADS);{
		glVertex3f(-19.2814,23.5889,12.5519);
		glVertex3f(-20.1879,23.3128,12.9141);
		glVertex3f(-21.5853,24.8413,13.1545);
		glVertex3f(-20.5271,24.9719,12.7884);
	}glEnd();

	drawTexture(large_marble,
             -20.1879,23.3128,12.9141,
             -21.1134,23.0358,13.2159,
             -22.6613,24.7098,13.4564,
             -21.5853,24.8413,13.1545

             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-20.1879,23.3128,12.9141);
		glVertex3f(-21.1134,23.0358,13.2159);
		glVertex3f(-22.6613,24.7098,13.4564);
		glVertex3f(-21.5853,24.8413,13.1545);
	}glEnd();

	//--------------------------column: 7
		drawTexture(large_marble,
             -12.2631,25.923,6.8265,
             -13.1095,25.9434,7.7308,
             -13.2114,26.0741,7.7551,
             -12.4088,26.1151,6.8768

             );glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-12.2631,25.923,6.8265);
		glVertex3f(-13.1095,25.9434,7.7308);
		glVertex3f(-13.2114,26.0741,7.7551);
		glVertex3f(-12.4088,26.1151,6.8768);
	}glEnd();

		drawTexture(large_marble,
             -13.1095,25.9434,7.7308,
             -13.9053,25.8323,8.5512,
             -14.0634,26.0305,8.5853,
-13.2114,26.0741,7.7551
             );glBegin(GL_QUADS);{
		glVertex3f(-13.1095,25.9434,7.7308);
		glVertex3f(-13.9053,25.8323,8.5512);
		glVertex3f(-14.0634,26.0305,8.5853);
		glVertex3f(-13.2114,26.0741,7.7551);
	}glEnd();

	drawTexture(large_marble,
             -13.9053,25.8323,8.5512,
             -14.7446,25.7176,9.3225,
             -14.9621,25.9846,9.3647,
-14.0634,26.0305,8.5853
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-13.9053,25.8323,8.5512);
		glVertex3f(-14.7446,25.7176,9.3225);
		glVertex3f(-14.9621,25.9846,9.3647);
		glVertex3f(-14.0634,26.0305,8.5853);
	}glEnd();

	drawTexture(large_marble,
             -14.7446,25.7176,9.3225,
             -15.6246,25.5996,10.0421,
             -15.9044,25.9364,10.0906,
             -14.9621,25.9846,9.3647
             );	glBegin(GL_QUADS);{
		glVertex3f(-14.7446,25.7176,9.3225);
		glVertex3f(-15.6246,25.5996,10.0421);
		glVertex3f(-15.9044,25.9364,10.0906);
		glVertex3f(-14.9621,25.9846,9.3647);
	}glEnd();

	drawTexture(large_marble,
             -15.6246,25.5996,10.0421,
             -16.5422,25.4786,10.7077,
             -16.8871,25.8861,10.7606,
             -15.9044,25.9364,10.0906
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-15.6246,25.5996,10.0421);
		glVertex3f(-16.5422,25.4786,10.7077);
		glVertex3f(-16.8871,25.8861,10.7606);
		glVertex3f(-15.9044,25.9364,10.0906);
	}glEnd();

	drawTexture(large_marble,
             -16.5422,25.4786,10.7077,
             -17.4945,25.355,11.317,
             -17.9069,25.834,11.3724,
             -16.8871,25.8861,10.7606
             );	glBegin(GL_QUADS);{
		glVertex3f(-16.5422,25.4786,10.7077);
		glVertex3f(-17.4945,25.355,11.317);
		glVertex3f(-17.9069,25.834,11.3724);
		glVertex3f(-16.8871,25.8861,10.7606);
	}glEnd();

	drawTexture(large_marble,
             -17.4945,25.355,11.317,
             -18.4783,25.2292,11.8681,
             -18.9604,25.7801,11.9241,
             -17.9069,25.834,11.3724
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-17.4945,25.355,11.317);
		glVertex3f(-18.4783,25.2292,11.8681);
		glVertex3f(-18.9604,25.7801,11.9241);
		glVertex3f(-17.9069,25.834,11.3724);
	}glEnd();

	drawTexture(large_marble,
             -18.4783,25.2292,11.8681,
             -19.4903,25.1013,12.3591,
             -20.0441,25.7247,12.4136,
             -18.9604,25.7801,11.9241
             );	glBegin(GL_QUADS);{
		glVertex3f(-18.4783,25.2292,11.8681);
		glVertex3f(-19.4903,25.1013,12.3591);
		glVertex3f(-20.0441,25.7247,12.4136);
		glVertex3f(-18.9604,25.7801,11.9241);
	}glEnd();

	drawTexture(large_marble,
             -19.4903,25.1013,12.3591,
             -20.5271,24.9719,12.7884,
             -21.1543,25.668,12.8394,
             -20.0441,25.7247,12.4136
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-19.4903,25.1013,12.3591);
		glVertex3f(-20.5271,24.9719,12.7884);
		glVertex3f(-21.1543,25.668,12.8394);
		glVertex3f(-20.0441,25.7247,12.4136);
	}glEnd();

	drawTexture(large_marble,
             -20.5271,24.9719,12.7884,
             -21.5853,24.8413,13.1545,
             -22.2873,25.61,13.2001,
             -21.1543,25.668,12.8394
             );	glBegin(GL_QUADS);{
		glVertex3f(-20.5271,24.9719,12.7884);
		glVertex3f(-21.5853,24.8413,13.1545);
		glVertex3f(-22.2873,25.61,13.2001);
		glVertex3f(-21.1543,25.668,12.8394);
	}glEnd();

	drawTexture(large_marble,
             -21.5853,24.8413,13.1545,
             -22.6613,24.7098,13.4564,
             -23.4393,25.5512,13.4945,
             -22.2873,25.61,13.2001
             );	glBegin(GL_QUADS);{
		//2->first, 3->last & before 3 ->2
		glVertex3f(-21.5853,24.8413,13.1545);
		glVertex3f(-22.6613,24.7098,13.4564);
		glVertex3f(-23.4393,25.5512,13.4945);
		glVertex3f(-22.2873,25.61,13.2001);
	}glEnd();
	//--------------------------column: 8
}
void drawPetal_3_leftback()//complete
{
	//triangles

	glColor3f(1.0, 1.0, 1.0);
	drawTexture(large_marble,
		-24.9601,13.8534,5.9534,
		-24.8633,14.5985,6.8768,
		-24.7426,14.491,6.8471,
		-24.7426,14.491,6.8471

             );

	glBegin(GL_TRIANGLES);{
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.8633,14.5985,6.8768);
		glVertex3f(-24.7426,14.491,6.8471);
	}glEnd();

	drawTexture(large_marble,
		-24.9601,13.8534,5.9534,
		-24.7426,14.491,6.8471,
		-24.6243,14.3863,6.8021,
		-24.6243,14.3863,6.8021

             );

	glBegin(GL_TRIANGLES);{
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.7426,14.491,6.8471);
		glVertex3f(-24.6243,14.3863,6.8021);
	}glEnd();

	drawTexture(large_marble,
		-24.9601,13.8534,5.9534,
		-24.6243,14.3863,6.8021,
		-24.5094,14.2853,6.7421,
		-24.5094,14.2853,6.7421

             );

	glBegin(GL_TRIANGLES);{
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.6243,14.3863,6.8021);
		glVertex3f(-24.5094,14.2853,6.7421);
	}glEnd();

	drawTexture(large_marble,
		-24.9601,13.8534,5.9534,
		-24.5094,14.2853,6.7421,
		-24.3989,14.189,6.6678,
		-24.3989,14.189,6.6678

             );

	glBegin(GL_TRIANGLES);{
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.5094,14.2853,6.7421);
		glVertex3f(-24.3989,14.189,6.6678);
	}glEnd();

	drawTexture(large_marble,
		-24.9601,13.8534,5.9534,
		-24.3989,14.189,6.6678,
		-24.2939,14.0983,6.5799,
		-24.2939,14.0983,6.5799

             );

	glBegin(GL_TRIANGLES);{
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.3989,14.189,6.6678);
		glVertex3f(-24.2939,14.0983,6.5799);
	}glEnd();

	drawTexture(large_marble,
		-24.9601,13.8534,5.9534,
		-24.2939,14.0983,6.5799,
		-24.1953,14.0139,6.479,
		-24.1953,14.0139,6.479
             );

	glBegin(GL_TRIANGLES);{
		glVertex3f(-24.9601,13.8534,5.9534);
		glVertex3f(-24.2939,14.0983,6.5799);
		glVertex3f(-24.1953,14.0139,6.479);
	}glEnd();


	//===================================================================================== col-1
	glColor3f(1.0, 1.0, 1.0);

	drawTexture(large_marble,
		-24.8633,14.5985,6.8768,
		-24.7597,15.3953,7.7551,
		-24.5158,15.1728,7.6985,
		-24.7426,14.491,6.8471
             );
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-24.8633,14.5985,6.8768);
		glVertex3f(-24.7597,15.3953,7.7551);
		glVertex3f(-24.5158,15.1728,7.6985);
		glVertex3f(-24.7426,14.491,6.8471);
	}glEnd();


	drawTexture(large_marble,
		-24.7597,15.3953,7.7551,
		-24.6497,16.2414,8.5853,
		-24.2803,15.8967,8.505,
		-24.5158,15.1728,7.6985
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.7597,15.3953,7.7551);
		glVertex3f(-24.6497,16.2414,8.5853);
		glVertex3f(-24.2803,15.8967,8.505);
		glVertex3f(-24.5158,15.1728,7.6985);
	}glEnd();

	drawTexture(large_marble,
		-24.6497,16.2414,8.5853,
		-24.5337,17.1337,9.3647,
		-24.0368,16.6603,9.2639,
		-24.2803,15.8967,8.505
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.6497,16.2414,8.5853);
		glVertex3f(-24.5337,17.1337,9.3647);
		glVertex3f(-24.0368,16.6603,9.2639);
		glVertex3f(-24.2803,15.8967,8.505);
	}glEnd();

	drawTexture(large_marble,
		-24.5337,17.1337,9.3647,
		-24.412,18.0693,10.0906,
		-23.786,17.4611,9.9728,
		-24.0368,16.6603,9.2639
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.5337,17.1337,9.3647);
		glVertex3f(-24.412,18.0693,10.0906);
		glVertex3f(-23.786,17.4611,9.9728);
		glVertex3f(-24.0368,16.6603,9.2639);
	}glEnd();

	drawTexture(large_marble,
		-24.412,18.0693,10.0906,
		-24.2852,19.0451,10.7606,
		-23.5285,18.2963,10.6293,
		-23.786,17.4611,9.9728
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.412,18.0693,10.0906);
		glVertex3f(-24.2852,19.0451,10.7606);
		glVertex3f(-23.5285,18.2963,10.6293);
		glVertex3f(-23.786,17.4611,9.9728);
	}glEnd();

	drawTexture(large_marble,
		-24.2852,19.0451,10.7606,
		-24.1535,20.0577,11.3724,
		-23.2651,19.1633,11.2314,
		-23.5285,18.2963,10.6293
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.2852,19.0451,10.7606);
		glVertex3f(-24.1535,20.0577,11.3724);
		glVertex3f(-23.2651,19.1633,11.2314);
		glVertex3f(-23.5285,18.2963,10.6293);
	}glEnd();

	drawTexture(large_marble,
		-24.1535,20.0577,11.3724,
		-24.0175,21.1031,11.9241,
		-22.9965,20.0591,11.7771,
		-23.2651,19.1633,11.2314
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.1535,20.0577,11.3724);
		glVertex3f(-24.0175,21.1031,11.9241);
		glVertex3f(-22.9965,20.0591,11.7771);
		glVertex3f(-23.2651,19.1633,11.2314);
	}glEnd();

	drawTexture(large_marble,
		-24.0175,21.1031,11.9241,
		-23.8776,22.1799,12.4136,
		-22.7234,20.9809,12.2647,
		-22.9965,20.0591,11.7771
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.0175,21.1031,11.9241);
		glVertex3f(-23.8776,22.1799,12.4136);
		glVertex3f(-22.7234,20.9809,12.2647);
		glVertex3f(-22.9965,20.0591,11.7771);
	}glEnd();

	drawTexture(large_marble,
		-23.8776,22.1799,12.4136,
		-23.7343,23.2823,12.8394,
		-22.4467,21.9255,12.6925,
		-22.7234,20.9809,12.2647
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.8776,22.1799,12.4136);
		glVertex3f(-23.7343,23.2823,12.8394);
		glVertex3f(-22.4467,21.9255,12.6925);
		glVertex3f(-22.7234,20.9809,12.2647);
	}glEnd();

	drawTexture(large_marble,
		-23.7343,23.2823,12.8394,
		-23.588,24.4073,13.2001,
		-22.167,22.8898,13.0591,
		-22.4467,21.9255,12.6925
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.7343,23.2823,12.8394);
		glVertex3f(-23.588,24.4073,13.2001);
		glVertex3f(-22.167,22.8898,13.0591);
		glVertex3f(-22.4467,21.9255,12.6925);
	}glEnd();

	drawTexture(large_marble,
		-23.588,24.4073,13.2001,
		-23.4393,25.5512,13.4945,
		-21.8854,23.8707,13.3634,
		-22.167,22.8898,13.0591
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.588,24.4073,13.2001);
		glVertex3f(-23.4393,25.5512,13.4945);
		glVertex3f(-21.8854,23.8707,13.3634);
		glVertex3f(-22.167,22.8898,13.0591);
	}glEnd();
	//--------------------------------------------------------------------------------column: 2
	drawTexture(large_marble,
		-24.7426,14.491,6.8471,
		-24.5158,15.1728,7.6985,
		-24.2764,14.9557,7.6107,
		-24.6243,14.3863,6.8021
             );
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-24.7426,14.491,6.8471);
		glVertex3f(-24.5158,15.1728,7.6985);
		glVertex3f(-24.2764,14.9557,7.6107);
		glVertex3f(-24.6243,14.3863,6.8021);
	}glEnd();

	drawTexture(large_marble,
		-24.5158,15.1728,7.6985,
		-24.2803,15.8967,8.505,
		-23.9174,15.5598,8.3767,
		-24.2764,14.9557,7.6107
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.5158,15.1728,7.6985);
		glVertex3f(-24.2803,15.8967,8.505);
		glVertex3f(-23.9174,15.5598,8.3767);
		glVertex3f(-24.2764,14.9557,7.6107);
	}glEnd();

	drawTexture(large_marble,
		-24.2803,15.8967,8.505,
		-24.0368,16.6603,9.2639,
		-23.5483,16.1968,9.0979,
		-23.9174,15.5598,8.3767
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.2803,15.8967,8.505);
		glVertex3f(-24.0368,16.6603,9.2639);
		glVertex3f(-23.5483,16.1968,9.0979);
		glVertex3f(-23.9174,15.5598,8.3767);
	}glEnd();

	drawTexture(large_marble,
		-24.0368,16.6603,9.2639,
		-23.786,17.4611,9.9728,
		-23.1698,16.8645,9.772,
		-23.5483,16.1968,9.0979
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.0368,16.6603,9.2639);
		glVertex3f(-23.786,17.4611,9.9728);
		glVertex3f(-23.1698,16.8645,9.772);
		glVertex3f(-23.5483,16.1968,9.0979);
	}glEnd();

	drawTexture(large_marble,
		-23.786,17.4611,9.9728,
		-23.5285,18.2963,10.6293,
		-22.7832,17.5608,10.3968,
		-23.1698,16.8645,9.772
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.786,17.4611,9.9728);
		glVertex3f(-23.5285,18.2963,10.6293);
		glVertex3f(-22.7832,17.5608,10.3968);
		glVertex3f(-23.1698,16.8645,9.772);
	}glEnd();

	drawTexture(large_marble,
		-23.5285,18.2963,10.6293,
		-23.2651,19.1633,11.2314,
		-22.3892,18.2835,10.9704,
		-22.7832,17.5608,10.3968
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.5285,18.2963,10.6293);
		glVertex3f(-23.2651,19.1633,11.2314);
		glVertex3f(-22.3892,18.2835,10.9704);
		glVertex3f(-22.7832,17.5608,10.3968);
	}glEnd();

	drawTexture(large_marble,
		-23.2651,19.1633,11.2314,
		-22.9965,20.0591,11.7771,
		-21.9891,19.0301,11.4911,
		-22.3892,18.2835,10.9704
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.2651,19.1633,11.2314);
		glVertex3f(-22.9965,20.0591,11.7771);
		glVertex3f(-21.9891,19.0301,11.4911);
		glVertex3f(-22.3892,18.2835,10.9704);
	}glEnd();

	drawTexture(large_marble,
		-22.9965,20.0591,11.7771,
		-22.7234,20.9809,12.2647,
		-21.5837,19.7984,11.9571,
		-21.9891,19.0301,11.4911
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.9965,20.0591,11.7771);
		glVertex3f(-22.7234,20.9809,12.2647);
		glVertex3f(-21.5837,19.7984,11.9571);
		glVertex3f(-21.9891,19.0301,11.4911);
	}glEnd();

	drawTexture(large_marble,
		-22.7234,20.9809,12.2647,
		-22.4467,21.9255,12.6925,
		-21.1742,20.5857,12.3671,
		-21.5837,19.7984,11.9571
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.7234,20.9809,12.2647);
		glVertex3f(-22.4467,21.9255,12.6925);
		glVertex3f(-21.1742,20.5857,12.3671);
		glVertex3f(-21.5837,19.7984,11.9571);
	}glEnd();

	drawTexture(large_marble,
		-22.4467,21.9255,12.6925,
		-22.167,22.8898,13.0591,
		-20.7617,21.3895,12.7198,
		-21.1742,20.5857,12.3671
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.4467,21.9255,12.6925);
		glVertex3f(-22.167,22.8898,13.0591);
		glVertex3f(-20.7617,21.3895,12.7198);
		glVertex3f(-21.1742,20.5857,12.3671);
	}glEnd();

	drawTexture(large_marble,
		-22.167,22.8898,13.0591,
		-21.8854,23.8707,13.3634,
		-20.3471,22.2071,13.014,
		-20.7617,21.3895,12.7198
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.167,22.8898,13.0591);
		glVertex3f(-21.8854,23.8707,13.3634);
		glVertex3f(-20.3471,22.2071,13.014);
		glVertex3f(-20.7617,21.3895,12.7198);
	}glEnd();
	//--------------------------------------------------------------------------------column: 3
	drawTexture(large_marble,
		-24.6243,14.3863,6.8021,
		-24.2764,14.9557,7.6107,
		-24.0437,14.7459,7.4923,
		-24.5094,14.2853,6.7421
             );
	glBegin(GL_QUADS);{
		//2->first, 3->last
		glVertex3f(-24.6243,14.3863,6.8021);
		glVertex3f(-24.2764,14.9557,7.6107);
		glVertex3f(-24.0437,14.7459,7.4923);
		glVertex3f(-24.5094,14.2853,6.7421);

	}glEnd();

	drawTexture(large_marble,
		-24.2764,14.9557,7.6107,
		-23.9174,15.5598,8.3767,
		-23.5643,15.2337,8.2017,
		-24.0437,14.7459,7.4923
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.2764,14.9557,7.6107);
		glVertex3f(-23.9174,15.5598,8.3767);
		glVertex3f(-23.5643,15.2337,8.2017);
		glVertex3f(-24.0437,14.7459,7.4923);
	}glEnd();

	drawTexture(large_marble,
		-23.9174,15.5598,8.3767,
		-23.5483,16.1968,9.0979,
		-23.0724,15.7474,8.8683,
		-23.5643,15.2337,8.2017
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.9174,15.5598,8.3767);
		glVertex3f(-23.5483,16.1968,9.0979);
		glVertex3f(-23.0724,15.7474,8.8683);
		glVertex3f(-23.5643,15.2337,8.2017);
	}glEnd();

	drawTexture(large_marble,
		-23.5483,16.1968,9.097,
		-23.1698,16.8645,9.772,
		-22.5693,16.2852,9.49,
		-23.0724,15.7474,8.8683
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.5483,16.1968,9.0979);
		glVertex3f(-23.1698,16.8645,9.772);
		glVertex3f(-22.5693,16.2852,9.49);
		glVertex3f(-23.0724,15.7474,8.8683);
	}glEnd();

	drawTexture(large_marble,
		-23.1698,16.8645,9.772,
		-22.7832,17.5608,10.3968,
		-22.056,16.8454,10.0651,
		-22.5693,16.2852,9.49
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.1698,16.8645,9.772);
		glVertex3f(-22.7832,17.5608,10.3968);
		glVertex3f(-22.056,16.8454,10.0651);
		glVertex3f(-22.5693,16.2852,9.49);
	}glEnd();

	drawTexture(large_marble,
		-22.7832,17.5608,10.3968,
		-22.3892,18.2835,10.9704,
		-21.534,17.4264,10.5918,
		-22.056,16.8454,10.0651
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.7832,17.5608,10.3968);
		glVertex3f(-22.3892,18.2835,10.9704);
		glVertex3f(-21.534,17.4264,10.5918);
		glVertex3f(-22.056,16.8454,10.0651);
	}glEnd();

	drawTexture(large_marble,
		-22.3892,18.2835,10.9704,
		-21.9891,19.0301,11.4911,
		-21.0046,18.0263,11.0685,
		-21.534,17.4264,10.5918
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.3892,18.2835,10.9704);
		glVertex3f(-21.9891,19.0301,11.4911);
		glVertex3f(-21.0046,18.0263,11.0685);
		glVertex3f(-21.534,17.4264,10.5918);
	}glEnd();

	drawTexture(large_marble,
		-21.9891,19.0301,11.4911,
		-21.5837,19.7984,11.9571,
		-20.4689,18.6431,11.4939,
		-21.0046,18.0263,11.0685
             );
	glBegin(GL_QUADS);{
		glVertex3f(-21.9891,19.0301,11.4911);
		glVertex3f(-21.5837,19.7984,11.9571);
		glVertex3f(-20.4689,18.6431,11.4939);
		glVertex3f(-21.0046,18.0263,11.0685);
	}glEnd();

	drawTexture(large_marble,
		-21.5837,19.7984,11.9571,
		-21.1742,20.5857,12.3671,
		-19.9285,19.275,11.8665,
		-20.4689,18.6431,11.4939
             );
	glBegin(GL_QUADS);{
		glVertex3f(-21.5837,19.7984,11.9571);
		glVertex3f(-21.1742,20.5857,12.3671);
		glVertex3f(-19.9285,19.275,11.8665);
		glVertex3f(-20.4689,18.6431,11.4939);
	}glEnd();

	drawTexture(large_marble,
		-21.1742,20.5857,12.3671,
		-20.7617,21.3895,12.7198,
		-19.3846,19.9199,12.1853,
		-19.9285,19.275,11.8665
             );
	glBegin(GL_QUADS);{
		glVertex3f(-21.1742,20.5857,12.3671);
		glVertex3f(-20.7617,21.3895,12.7198);
		glVertex3f(-19.3846,19.9199,12.1853);
		glVertex3f(-19.9285,19.275,11.8665);
	}glEnd();

	drawTexture(large_marble,
		-20.7617,21.3895,12.7198,
		-20.3471,22.2071,13.014,
		-18.8386,20.5758,12.4493,
		-19.3846,19.9199,12.1853
             );
	glBegin(GL_QUADS);{
		glVertex3f(-20.7617,21.3895,12.7198);
		glVertex3f(-20.3471,22.2071,13.014);
		glVertex3f(-18.8386,20.5758,12.4493);
		glVertex3f(-19.3846,19.9199,12.1853);
	}glEnd();

	//--------------------------------------------------------------------------------column: 4
	drawTexture(large_marble,
		-24.5094,14.2853,6.7421,
		-24.0437,14.7459,7.4923,
		-23.8198,14.5454,7.3445,
		-24.3989,14.189,6.6678
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.5094,14.2853,6.7421);
		glVertex3f(-24.0437,14.7459,7.4923);
		glVertex3f(-23.8198,14.5454,7.3445);
		glVertex3f(-24.3989,14.189,6.6678);
	}glEnd();

	drawTexture(large_marble,
		-24.0437,14.7459,7.4923,
		-23.5643,15.2337,8.2017,
		-23.2243,14.9215,7.9815,
		-23.8198,14.5454,7.3445
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.0437,14.7459,7.4923);
		glVertex3f(-23.5643,15.2337,8.2017);
		glVertex3f(-23.2243,14.9215,7.9815);
		glVertex3f(-23.8198,14.5454,7.3445);
	}glEnd();

	drawTexture(large_marble,
		-23.5643,15.2337,8.2017,
		-23.0724,15.7474,8.8683,
		-22.6138,15.3162,8.5771,
		-23.2243,14.9215,7.9815
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.5643,15.2337,8.2017);
		glVertex3f(-23.0724,15.7474,8.8683);
		glVertex3f(-22.6138,15.3162,8.5771);
		glVertex3f(-23.2243,14.9215,7.9815);
	}glEnd();

	drawTexture(large_marble,
		-23.0724,15.7474,8.8683,
		-22.5693,16.2852,9.49,
		-21.9897,15.7284,9.1295,
		-22.6138,15.3162,8.5771
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.0724,15.7474,8.8683);
		glVertex3f(-22.5693,16.2852,9.49);
		glVertex3f(-21.9897,15.7284,9.1295);
		glVertex3f(-22.6138,15.3162,8.5771);
	}glEnd();

	drawTexture(large_marble,
		-22.5693,16.2852,9.49,
		-22.056,16.8454,10.0651,
		-21.3537,16.1567,9.6374,
		-21.9897,15.7284,9.1295
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.5693,16.2852,9.49);
		glVertex3f(-22.056,16.8454,10.0651);
		glVertex3f(-21.3537,16.1567,9.6374);
		glVertex3f(-21.9897,15.7284,9.1295);
	}glEnd();

	drawTexture(large_marble,
		-22.056,16.8454,10.0651,
		-21.534,17.4264,10.5918,
		-20.7073,16.6,10.0991,
		-21.3537,16.1567,9.6374
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.056,16.8454,10.0651);
		glVertex3f(-21.534,17.4264,10.5918);
		glVertex3f(-20.7073,16.6,10.0991);
		glVertex3f(-21.3537,16.1567,9.6374);
	}glEnd();

	drawTexture(large_marble,
		-21.534,17.4264,10.5918,
		-21.0046,18.0263,11.0685,
		-20.0519,17.0568,10.5134,
		-20.7073,16.6,10.0991
             );
	glBegin(GL_QUADS);{
		glVertex3f(-21.534,17.4264,10.5918);
		glVertex3f(-21.0046,18.0263,11.0685);
		glVertex3f(-20.0519,17.0568,10.5134);
		glVertex3f(-20.7073,16.6,10.0991);
	}glEnd();
	drawTexture(large_marble,
		-21.0046,18.0263,11.0685,
		-20.4689,18.6431,11.4939,
		-19.3893,17.5259,10.8791,
		-20.0519,17.0568,10.5134
             );

	glBegin(GL_QUADS);{
		glVertex3f(-21.0046,18.0263,11.0685);
		glVertex3f(-20.4689,18.6431,11.4939);
		glVertex3f(-19.3893,17.5259,10.8791);
		glVertex3f(-20.0519,17.0568,10.5134);
	}glEnd();

	drawTexture(large_marble,
		-20.4689,18.6431,11.4939,
		-19.9285,19.275,11.8665,
		-18.7209,18.0056,11.1951,
		-19.3893,17.5259,10.8791
             );
	glBegin(GL_QUADS);{
		glVertex3f(-20.4689,18.6431,11.4939);
		glVertex3f(-19.9285,19.275,11.8665);
		glVertex3f(-18.7209,18.0056,11.1951);
		glVertex3f(-19.3893,17.5259,10.8791);
	}glEnd();

	drawTexture(large_marble,
		-19.9285,19.275,11.8665,
		-19.3846,19.9199,12.1853,
		-18.0485,18.4947,11.4606,
		-18.7209,18.0056,11.1951
             );
	glBegin(GL_QUADS);{
		glVertex3f(-19.9285,19.275,11.8665);
		glVertex3f(-19.3846,19.9199,12.1853);
		glVertex3f(-18.0485,18.4947,11.4606);
		glVertex3f(-18.7209,18.0056,11.1951);
	}glEnd();

	drawTexture(large_marble,
		-19.3846,19.9199,12.1853,
		-18.8386,20.5758,12.4493,
		-17.3737,18.9916,11.6747,
		-18.0485,18.4947,11.4606
             );
	glBegin(GL_QUADS);{
		glVertex3f(-19.3846,19.9199,12.1853);
		glVertex3f(-18.8386,20.5758,12.4493);
		glVertex3f(-17.3737,18.9916,11.6747);
		glVertex3f(-18.0485,18.4947,11.4606);
	}glEnd();
	//--------------------------------------------------------------------------------column: 5
	drawTexture(large_marble,
		-24.3989,14.189,6.6678,
		-23.8198,14.5454,7.3445,
		-23.6067,14.356,7.1686,
		-24.2939,14.0983,6.5799
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.3989,14.189,6.6678);
		glVertex3f(-23.8198,14.5454,7.3445);
		glVertex3f(-23.6067,14.356,7.1686);
		glVertex3f(-24.2939,14.0983,6.5799);

	}glEnd();

	drawTexture(large_marble,
		-23.8198,14.5454,7.3445,
		-23.2243,14.9215,7.9815,
		-22.9003,14.6259,7.7181,
		-23.6067,14.356,7.1686
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.8198,14.5454,7.3445);
		glVertex3f(-23.2243,14.9215,7.9815);
		glVertex3f(-22.9003,14.6259,7.7181);
		glVertex3f(-23.6067,14.356,7.1686);
	}glEnd();

	drawTexture(large_marble,
		-23.2243,14.9215,7.9815,
		-22.6138,15.3162,8.5771,
		-22.1764,14.9072,8.227,
		-22.9003,14.6259,7.7181
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.2243,14.9215,7.9815);
		glVertex3f(-22.6138,15.3162,8.5771);
		glVertex3f(-22.1764,14.9072,8.227);
		glVertex3f(-22.9003,14.6259,7.7181);
	}glEnd();

	drawTexture(large_marble,
		-22.6138,15.3162,8.5771,
		-21.9897,15.7284,9.1295,
		-21.4366,15.1992,8.6938,
		-22.1764,14.9072,8.227
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.6138,15.3162,8.5771);
		glVertex3f(-21.9897,15.7284,9.1295);
		glVertex3f(-21.4366,15.1992,8.6938);
		glVertex3f(-22.1764,14.9072,8.227);
	}glEnd();

	drawTexture(large_marble,
		-21.9897,15.7284,9.1295,
		-21.3537,16.1567,9.6374,
		-20.6828,15.501,9.1175,
		-21.4366,15.1992,8.6938
             );
	glBegin(GL_QUADS);{
		glVertex3f(-21.9897,15.7284,9.1295);
		glVertex3f(-21.3537,16.1567,9.6374);
		glVertex3f(-20.6828,15.501,9.1175);
		glVertex3f(-21.4366,15.1992,8.6938);
	}glEnd();

	drawTexture(large_marble,
		-21.3537,16.1567,9.6374,
		-20.7073,16.6,10.0991,
		-19.9166,15.8118,9.4986,
		-20.6828,15.501,9.1175
             );
	glBegin(GL_QUADS);{
		glVertex3f(-21.3537,16.1567,9.6374);
		glVertex3f(-20.7073,16.6,10.0991);
		glVertex3f(-19.9166,15.8118,9.4986);
		glVertex3f(-20.6828,15.501,9.1175);
	}glEnd();

	drawTexture(large_marble,
		-20.7073,16.6,10.0991,
		-20.0519,17.0568,10.5134,
		-19.14,16.1307,9.8307,
		-19.9166,15.8118,9.4986
             );
	glBegin(GL_QUADS);{
		glVertex3f(-20.7073,16.6,10.0991);
		glVertex3f(-20.0519,17.0568,10.5134);
		glVertex3f(-19.14,16.1307,9.8307);
		glVertex3f(-19.9166,15.8118,9.4986);
	}glEnd();

	drawTexture(large_marble,
		-20.0519,17.0568,10.5134,
		-19.3893,17.5259,10.8791,
		-18.3547,16.4568,10.1185,
		-19.14,16.1307,9.8307
             );
	glBegin(GL_QUADS);{
		glVertex3f(-20.0519,17.0568,10.5134);
		glVertex3f(-19.3893,17.5259,10.8791);
		glVertex3f(-18.3547,16.4568,10.1185);
		glVertex3f(-19.14,16.1307,9.8307);
	}glEnd();

	drawTexture(large_marble,
		-19.3893,17.5259,10.8791,
		-18.7209,18.0056,11.1951,
		-17.5627,16.7892,10.3592,
		-18.3547,16.4568,10.1185
             );
	glBegin(GL_QUADS);{
		glVertex3f(-19.3893,17.5259,10.8791);
		glVertex3f(-18.7209,18.0056,11.1951);
		glVertex3f(-17.5627,16.7892,10.3592);
		glVertex3f(-18.3547,16.4568,10.1185);
	}glEnd();

	drawTexture(large_marble,
		-18.7209,18.0056,11.1951,
		-18.0485,18.4947,11.4606,
		-16.7657,17.1269,10.5522,
		-17.5627,16.7892,10.3592
             );
	glBegin(GL_QUADS);{
		glVertex3f(-18.7209,18.0056,11.1951);
		glVertex3f(-18.0485,18.4947,11.4606);
		glVertex3f(-16.7657,17.1269,10.5522);
		glVertex3f(-17.5627,16.7892,10.3592);
	}glEnd();

	drawTexture(large_marble,
		-18.0485,18.4947,11.4606,
		-17.3737,18.9916,11.6747,
		-15.9658,17.4691,10.6971,
		-16.7657,17.1269,10.5522
             );
	glBegin(GL_QUADS);{
		glVertex3f(-18.0485,18.4947,11.4606);
		glVertex3f(-17.3737,18.9916,11.6747);
		glVertex3f(-15.9658,17.4691,10.6971);
		glVertex3f(-16.7657,17.1269,10.5522);
	}glEnd();
	//--------------------------------------------------------------------------------column: 6
	drawTexture(large_marble,
	-24.2939,14.0983,6.5799,
		-23.6067,14.356,7.1686,
		-23.4065,14.1795,6.9663,
		-24.1953,14.0139,6.479
             );
	glBegin(GL_QUADS);{
		glVertex3f(-24.2939,14.0983,6.5799);
		glVertex3f(-23.6067,14.356,7.1686);
		glVertex3f(-23.4065,14.1795,6.9663);
		glVertex3f(-24.1953,14.0139,6.479);

	}glEnd();

	drawTexture(large_marble,
		-23.6067,14.356,7.1686,
		-22.9003,14.6259,7.7181,
		-22.5955,14.3498,7.414,
		-23.4065,14.1795,6.9663
             );
	glBegin(GL_QUADS);{
		glVertex3f(-23.6067,14.356,7.1686);
		glVertex3f(-22.9003,14.6259,7.7181);
		glVertex3f(-22.5955,14.3498,7.414);
		glVertex3f(-23.4065,14.1795,6.9663);
	}glEnd();

	drawTexture(large_marble,
		-22.9003,14.6259,7.7181,
		-22.1764,14.9072,8.227,
		-21.7644,14.5242,7.8212,
		-22.5955,14.3498,7.414
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.9003,14.6259,7.7181);
		glVertex3f(-22.1764,14.9072,8.227);
		glVertex3f(-21.7644,14.5242,7.8212);
		glVertex3f(-22.5955,14.3498,7.414);
	}glEnd();

	drawTexture(large_marble,
		-22.1764,14.9072,8.227,
		-21.4366,15.1992,8.6938,
		-20.915,14.7025,8.1869,
		-21.7644,14.5242,7.8212
             );
	glBegin(GL_QUADS);{
		glVertex3f(-22.1764,14.9072,8.227);
		glVertex3f(-21.4366,15.1992,8.6938);
		glVertex3f(-20.915,14.7025,8.1869);
		glVertex3f(-21.7644,14.5242,7.8212);
	}glEnd();

	drawTexture(large_marble,
		-21.4366,15.1992,8.6938,
		-20.6828,15.501,9.1175,
		-20.0492,14.8842,8.5102,
		-20.915,14.7025,8.1869
             );
	glBegin(GL_QUADS);{
		glVertex3f(-21.4366,15.1992,8.6938);
		glVertex3f(-20.6828,15.501,9.1175);
		glVertex3f(-20.0492,14.8842,8.5102);
		glVertex3f(-20.915,14.7025,8.1869);
	}glEnd();

	drawTexture(large_marble,
		-20.6828,15.501,9.1175,
		-19.9166,15.8118,9.4986,
		-19.1692,15.069,8.7904,
		-20.0492,14.8842,8.5102
             );
	glBegin(GL_QUADS);{
		glVertex3f(-20.6828,15.501,9.1175);
		glVertex3f(-19.9166,15.8118,9.4986);
		glVertex3f(-19.1692,15.069,8.7904);
		glVertex3f(-20.0492,14.8842,8.5102);
	}glEnd();

	drawTexture(large_marble,
		-19.9166,15.8118,9.4986,
		-19.14,16.1307,9.8307,
		-18.277,15.2563,9.0268,
		-19.1692,15.069,8.7904
             );
	glBegin(GL_QUADS);{
		glVertex3f(-19.9166,15.8118,9.4986);
		glVertex3f(-19.14,16.1307,9.8307);
		glVertex3f(-18.277,15.2563,9.0268);
		glVertex3f(-19.1692,15.069,8.7904);
	}glEnd();

	drawTexture(large_marble,
		-19.14,16.1307,9.8307,
		-18.3547,16.4568,10.1185,
		-17.3746,15.4457,9.219,
		-18.277,15.2563,9.0268
             );
	glBegin(GL_QUADS);{
		glVertex3f(-19.14,16.1307,9.8307);
		glVertex3f(-18.3547,16.4568,10.1185);
		glVertex3f(-17.3746,15.4457,9.219);
		glVertex3f(-18.277,15.2563,9.0268);
	}glEnd();

	drawTexture(large_marble,
		-18.3547,16.4568,10.1185,
		-17.5627,16.7892,10.3592,
		-16.4643,15.6368,9.3663,
		-17.3746,15.4457,9.219
             );
	glBegin(GL_QUADS);{
		glVertex3f(-18.3547,16.4568,10.1185);
		glVertex3f(-17.5627,16.7892,10.3592);
		glVertex3f(-16.4643,15.6368,9.3663);
		glVertex3f(-17.3746,15.4457,9.219);
	}glEnd();

	drawTexture(large_marble,
		-17.5627,16.7892,10.3592,
		-16.7657,17.1269,10.5522,
		-15.548,15.8291,9.4686,
		-16.4643,15.6368,9.3663
             );
	glBegin(GL_QUADS);{
		glVertex3f(-17.5627,16.7892,10.3592);
		glVertex3f(-16.7657,17.1269,10.5522);
		glVertex3f(-15.548,15.8291,9.4686);
		glVertex3f(-16.4643,15.6368,9.3663);
	}glEnd();

	drawTexture(large_marble,
		-16.7657,17.1269,10.5522,
		-15.9658,17.4691,10.6971,
		-14.6279,16.0222,9.5255,
		-15.548,15.8291,9.4686
             );
	glBegin(GL_QUADS);{
		glVertex3f(-16.7657,17.1269,10.5522);
		glVertex3f(-15.9658,17.4691,10.6971);
		glVertex3f(-14.6279,16.0222,9.5255);
		glVertex3f(-15.548,15.8291,9.4686);
	}glEnd();
	//--------------------------------------------------------------------------------column: 7

}

void drawPetal_3_leftback_side()
{

	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_QUADS);{

		glVertex3f(-24.8968,14.0059,5.8376);
		glVertex3f(-24.7721,14.7273,6.7564);
		glVertex3f(-24.8633,14.5985,6.8768);
		glVertex3f(-24.9601,13.8534,5.9534);

	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.7721,14.7273,6.7564);
		glVertex3f(-24.6719,15.5182,7.6262);
		glVertex3f(-24.7597,15.3953,7.7551);
		glVertex3f(-24.8633,14.5985,6.8768);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.6719,15.5182,7.6262);
		glVertex3f(-24.5655,16.3577,8.4483);
		glVertex3f(-24.6497,16.2414,8.5853);
		glVertex3f(-24.7597,15.3953,7.7551);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.5655,16.3577,8.4483);
		glVertex3f(-24.4533,17.2431,9.2198);
		glVertex3f(-24.5337,17.1337,9.3647);
		glVertex3f(-24.6497,16.2414,8.5853);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.4533,17.2431,9.2198);
		glVertex3f(-24.3359,18.1713,9.9382);
		glVertex3f(-24.412,18.0693,10.0906);
		glVertex3f(-24.5337,17.1337,9.3647);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.3359,18.1713,9.9382);
		glVertex3f(-24.2137,19.1393,10.6011);
		glVertex3f(-24.2852,19.0451,10.7606);
		glVertex3f(-24.412,18.0693,10.0906);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.2137,19.1393,10.6011);
		glVertex3f(-24.0872,20.1438,11.2063);
		glVertex3f(-24.1535,20.0577,11.3724);
		glVertex3f(-24.2852,19.0451,10.7606);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-24.0872,20.1438,11.2063);
		glVertex3f(-23.958,21.1813,11.7512);
		glVertex3f(-24.0175,21.1031,11.9241);
		glVertex3f(-24.1535,20.0577,11.3724);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.958,21.1813,11.7512);
		glVertex3f(-23.8228,22.25,12.2362);
		glVertex3f(-23.8776,22.1799,12.4136);
		glVertex3f(-24.0175,21.1031,11.9241);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.8228,22.25,12.2362);
		glVertex3f(-23.6859,23.3431,12.6568);
		glVertex3f(-23.7343,23.2823,12.8394);
		glVertex3f(-23.8776,22.1799,12.4136);

	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
				glVertex3f(-23.6859,23.3431,12.6568);
		glVertex3f(-23.5464,24.4586,13.013);
		glVertex3f(-23.588,24.4073,13.2001);
		glVertex3f(-23.7343,23.2823,12.8394);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-23.5464,24.4586,13.013);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-23.4393,25.5512,13.4945);
		glVertex3f(-23.588,24.4073,13.2001);
	}glEnd();
}
void drawPetal_3_leftface()
{
	glColor3f(1.0, 1.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.6107,16.0808,9.4607);
		glVertex3f(-15.3754,16.8306,9.9671);
		glVertex3f(-24.8968,14.0059,5.8376);
		glVertex3f(-24.7721,14.7273,6.7564);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.3754,16.8306,9.9671);
		glVertex3f(-15.9954,17.501,10.4806);
		glVertex3f(-24.6719,15.5182,7.6262);
		glVertex3f(-24.8968,14.0059,5.8376);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);

	glBegin(GL_QUADS);{
		glVertex3f(-15.9954,17.501,10.4806);
		glVertex3f(-16.6829,18.2446,10.9933);
		glVertex3f(-24.5655,16.3577,8.4483);
		glVertex3f(-24.6719,15.5182,7.6262);


	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.6829,18.2446,10.9933);
		glVertex3f(-17.3864,19.0053,11.4565);
		glVertex3f(-24.4533,17.2431,9.2198);
		glVertex3f(-24.5655,16.3577,8.4483);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-17.3864,19.0053,11.4565);
		glVertex3f(-18.1507,19.8308,11.8961);
		glVertex3f(-24.3359,18.1713,9.9382);
		glVertex3f(-24.4533,17.2431,9.2198);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.1507,19.8308,11.8961);
		glVertex3f(-18.8992,20.6085,12.2483);
		glVertex3f(-24.2137,19.1393,10.6011);
		glVertex3f(-24.3359,18.1713,9.9382);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.8992,20.6085,12.2483);
		glVertex3f(-19.6242,21.4254,12.5598);
		glVertex3f(-24.0872,20.1438,11.2063);
		glVertex3f(-24.2137,19.1393,10.6011);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.6242,21.4254,12.5598);
		glVertex3f(-20.3867,22.23,12.8092);
		glVertex3f(-23.958,21.1813,11.7512);
		glVertex3f(-24.0872,20.1438,11.2063);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.3867,22.23,12.8092);
		glVertex3f(-21.1359,23.0583,13.0124);
		glVertex3f(-23.8228,22.25,12.2362);
		glVertex3f(-23.958,21.1813,11.7512);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.1359,23.0583,13.0124);
		glVertex3f(-21.8784,23.8631,13.1548);
		glVertex3f(-23.6859,23.3431,12.6568);
		glVertex3f(-23.8228,22.25,12.2362);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.8784,23.8631,13.1548);
		glVertex3f(-22.5968,24.64,13.2407);
		glVertex3f(-23.5464,24.4586,13.013);
		glVertex3f(-23.6859,23.3431,12.6568);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.5968,24.64,13.2407);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-23.5464,24.4586,13.013);
	}glEnd();
}
void drawPetal_3_rightback_side()
{
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-11.6585,26.1535,5.9534);
		glVertex3f(-12.4088,26.1151,6.8768);
		glVertex3f(-11.8155,26.1022,5.8376);
		glVertex3f(-12.5445,26.0343,6.7564);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.4088,26.1151,6.8768);
		glVertex3f(-13.2114,26.0741,7.7551);
		glVertex3f(-13.3408,25.9962,7.6262);
		glVertex3f(-11.8155,26.1022,5.8376);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.2114,26.0741,7.7551);
		glVertex3f(-14.0634,26.0305,8.5853);
		glVertex3f(-14.186,25.9557,8.4483);
		glVertex3f(-13.3408,25.9962,7.6262);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.0634,26.0305,8.5853);
		glVertex3f(-14.9621,25.9846,9.3647);
		glVertex3f(-15.0774,25.913,9.2198);
		glVertex3f(-14.186,25.9557,8.4483);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.9621,25.9846,9.3647);
		glVertex3f(-15.9044,25.9364,10.0906);
		glVertex3f(-16.012,25.8685,9.9382);
		glVertex3f(-15.0774,25.913,9.2198);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.9044,25.9364,10.0906);
		glVertex3f(-16.8871,25.8861,10.7606);
		glVertex3f(-16.9866,25.8223,10.6011);
		glVertex3f(-16.012,25.8685,9.9382);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.8871,25.8861,10.7606);
		glVertex3f(-17.9069,25.834,11.3724);
		glVertex3f(-17.9979,25.7746,11.2063);
		glVertex3f(-16.9866,25.8223,10.6011);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-17.9069,25.834,11.3724);
		glVertex3f(-18.9604,25.7801,11.9241);
		glVertex3f(-19.0423,25.7268,11.7512);
		glVertex3f(-17.9979,25.7746,11.2063);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-18.9604,25.7801,11.9241);
		glVertex3f(-20.0441,25.7247,12.4136);
		glVertex3f(-20.1182,25.6756,12.2362);
		glVertex3f(-19.0423,25.7268,11.7512);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.0441,25.7247,12.4136);
		glVertex3f(-21.1543,25.668,12.8394);
		glVertex3f(-21.2188,25.6245,12.6568);
		glVertex3f(-20.1182,25.6756,12.2362);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.1543,25.668,12.8394);
		glVertex3f(-22.2873,25.61,13.2001);
		glVertex3f(-22.3417,25.5726,13.013);
		glVertex3f(-21.2188,25.6245,12.6568);

	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.2873,25.61,13.2001);
		glVertex3f(-23.4393,25.5512,13.4945);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-22.3417,25.5726,13.013);
		//2->first, 3->last & before 3 ->2

	}glEnd();
}
void drawPetal_3_rightface()
{
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-11.8155,26.1022,5.8376);
		glVertex3f(-12.5445,26.0343,6.7564);
		glVertex3f(-14.6107,16.0808,9.4607);
		glVertex3f(-15.3754,16.8306,9.9671);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-12.5445,26.0343,6.7564);
		glVertex3f(-13.3408,25.9962,7.6262);
		glVertex3f(-15.9954,17.501,10.4806);
		glVertex3f(-14.6107,16.0808,9.4607);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-13.3408,25.9962,7.6262);
		glVertex3f(-14.186,25.9557,8.4483);
		glVertex3f(-16.6829,18.2446,10.9933);
		glVertex3f(-15.9954,17.501,10.4806);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-14.186,25.9557,8.4483);
		glVertex3f(-15.0774,25.913,9.2198);
		glVertex3f(-17.3864,19.0053,11.4565);
		glVertex3f(-16.6829,18.2446,10.9933);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-15.0774,25.913,9.2198);
		glVertex3f(-16.012,25.8685,9.9382);
		glVertex3f(-18.1507,19.8308,11.8961);
		glVertex3f(-17.3864,19.0053,11.4565);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.012,25.8685,9.9382);
		glVertex3f(-16.9866,25.8223,10.6011);
		glVertex3f(-18.8992,20.6085,12.2483);
		glVertex3f(-18.1507,19.8308,11.8961);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-16.9866,25.8223,10.6011);
		glVertex3f(-17.9979,25.7746,11.2063);
		glVertex3f(-19.6242,21.4254,12.5598);
		glVertex3f(-18.8992,20.6085,12.2483);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-17.9979,25.7746,11.2063);
		glVertex3f(-19.0423,25.7268,11.7512);
		glVertex3f(-20.3867,22.23,12.8092);
		glVertex3f(-19.6242,21.4254,12.5598);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-19.0423,25.7268,11.7512);
		glVertex3f(-20.1182,25.6756,12.2362);
		glVertex3f(-21.1359,23.0583,13.0124);
		glVertex3f(-20.3867,22.23,12.8092);
	}glEnd();

	//glColor3f(.50, 0.50, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-20.1182,25.6756,12.2362);
		glVertex3f(-21.2188,25.6245,12.6568);
		glVertex3f(-21.8784,23.8631,13.1548);
		glVertex3f(-21.1359,23.0583,13.0124);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-21.2188,25.6245,12.6568);
		glVertex3f(-22.3417,25.5726,13.013);
		glVertex3f(-22.5968,24.64,13.2407);
		glVertex3f(-21.8784,23.8631,13.1548);
	}glEnd();

	//glColor3f(1.0, 0.0, 0.0);
	glBegin(GL_QUADS);{
		glVertex3f(-22.3417,25.5726,13.013);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-23.4113,25.5208,13.2855);
		glVertex3f(-22.5968,24.64,13.2407);
	}glEnd();
}

void drawPetal_3()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			drawPetal_3_right_transparent();
			drawPetal_3_left_transparent();
			drawPetal_3_rightback();
			drawPetal_3_leftback();
			drawPetal_3_leftback_side();
			drawPetal_3_leftface();
			drawPetal_3_rightback_side();
			drawPetal_3_rightface();

		}glPopMatrix();
	}
}
void drawPetal_2_rightface()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// right part of a petal face

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8174,17.4753,5.9536);
				glVertex3f(12.8459,17.4527,6.8079);
				glVertex3f(13.2772,17.3175,6.8079);
				glVertex3f(13.2846,17.3378,5.9536);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8459,17.4527,6.8079);
				glVertex3f(12.8121,17.4430,7.975);
				glVertex3f(13.267,17.2897,7.975);
				glVertex3f(13.2772,17.3175,6.8079);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8121,17.4430,7.975);
				glVertex3f(12.7393,17.4329,8.7476);
				glVertex3f(13.2603,17.2713,8.7476);
				glVertex3f(13.267,17.2897,7.975);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.7393,17.4329,8.7476);
				glVertex3f(12.7598,17.4101,9.2870);
				glVertex3f(13.2557,17.2585,9.2870);
				glVertex3f(13.2603,17.2713,8.7476);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.7598,17.4101,9.2870);
				glVertex3f(12.6838,17.3988,9.8639);
				glVertex3f(13.2365,17.2403,9.7977);
				glVertex3f(13.2557,17.2585,9.2870);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.6838,17.3988,9.8639);
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(13.2049,17.2199,10.2509);
				glVertex3f(13.2365,17.2403,9.7977);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(13.1569,17.1888,10.9394);
				glVertex3f(13.2049,17.2199,10.2509);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(12.3406,17.3526,11.8870);
				glVertex3f(13.0403,17.1418,11.9161);
				glVertex3f(13.1569,17.1888,10.9394);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.3406,17.3526,11.8870);
				glVertex3f(12.1210,17.3297,12.8261);
				glVertex3f(12.9117,17.0959,12.8444);
				glVertex3f(13.0403,17.1418,11.9161);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.1210,17.3297,12.8261);
				glVertex3f(11.8872,17.3103,13.6246);
				glVertex3f(12.7962,17.0680,13.4018);
				glVertex3f(12.9117,17.0959,12.8444);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.8872,17.3103,13.6246);
				glVertex3f(11.6185,17.2905,14.4584);
				glVertex3f(12.5751,17.0101,14.4584);
				glVertex3f(12.7962,17.0680,13.4018);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.6185,17.2905,14.4584);
				glVertex3f(11.3391,17.2732,15.1985);
				glVertex3f(12.3584,16.9667,15.2306);
				glVertex3f(12.5751,17.0101,14.4584);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.3391,17.2732,15.1985);
				glVertex3f(11.033,17.2544,15.9525);
				glVertex3f(12.13,16.9209,16.0443);
				glVertex3f(12.3584,16.9667,15.2306);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.033,17.2544,15.9525);
				glVertex3f(10.725,17.2445,16.6159);
				glVertex3f(11.9247,16.8907,16.6267);
				glVertex3f(12.13,16.9209,16.0443);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.725,17.2445,16.6159);
				glVertex3f(10.4384,17.2282,17.2260);
				glVertex3f(11.7518,16.8605,17.1108);
				glVertex3f(11.9247,16.8907,16.6267);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.4384,17.2282,17.2260);
				glVertex3f(10.2728,17.2260,17.5224);
				glVertex3f(11.5788,16.8303,17.5949);
				glVertex3f(11.7518,16.8605,17.1108);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.2728,17.2260,17.5224);
				glVertex3f(9.9836,17.2170,18.0356);
				glVertex3f(11.3529,16.7995,18.1157);
				glVertex3f(11.5788,16.8303,17.5949);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.9836,17.2170,18.0356);
				glVertex3f(9.6249,17.2059,18.6719);
				glVertex3f(11.1117,16.7666,18.6719);
				glVertex3f(11.3529,16.7995,18.1157);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.6249,17.2059,18.6719);
				glVertex3f(9.1190,17.2273,19.3169);
				glVertex3f(10.9246,16.7410,19.1031);
				glVertex3f(11.1117,16.7666,18.6719);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.1190,17.2273,19.3169);
				glVertex3f(8.8102,17.1978,19.8957);
				glVertex3f(10.5571,16.6995,19.8147);
				glVertex3f(10.9246,16.7410,19.1031);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.8102,17.1978,19.8957);
				glVertex3f(8.4037,17.1981,20.4395);
				glVertex3f(10.2850,16.6702,20.343);
				glVertex3f(10.5571,16.6995,19.8147);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4037,17.1981,20.4395);
				glVertex3f(8.3079,17.1987,20.5624);
				glVertex3f(10.1709,16.6558,20.5624);
				glVertex3f(10.2850,16.6702,20.343);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.3079,17.1987,20.5624);
				glVertex3f(7.9858,17.2008,20.9756);
				glVertex3f(9.8755,16.6287,21.0507);
				glVertex3f(10.1709,16.6558,20.5624);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9858,17.2008,20.9756);
				glVertex3f(7.688,17.2013,21.3566);
				glVertex3f(9.5838,16.6019,21.533);
				glVertex3f(9.8755,16.6287,21.0507);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.688,17.2013,21.3566);
				glVertex3f(7.1801,17.2254,21.8695);
				glVertex3f(9.3215,16.5778,21.9667);
				glVertex3f(9.5838,16.6019,21.533);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1801,17.2254,21.8695);
				glVertex3f(6.6392,17.2254,22.5136);
				glVertex3f(8.9177,16.5488,22.5437);
				glVertex3f(9.3215,16.5778,21.9667);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.6392,17.2254,22.5136);
				glVertex3f(6.2435,17.2407,22.9001);
				glVertex3f(8.6841,16.5320,22.8774);
				glVertex3f(8.9177,16.5488,22.5437);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.2435,17.2407,22.9001);
				glVertex3f(5.9082,17.2548,23.2197);
				glVertex3f(8.5380,16.5215,23.0861);
				glVertex3f(8.6841,16.5320,22.8774);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.9082,17.2548,23.2197);
				glVertex3f(5.3810,17.2755,23.7211);
				glVertex3f(7.8324,16.4827,23.9940);
				glVertex3f(8.5380,16.5215,23.0861);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3810,17.2755,23.7211);
				glVertex3f(5.3383,17.1581,24.1537);
				glVertex3f(7.5229,16.4659,24.3786);
				glVertex3f(7.8324,16.4827,23.9940);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3383,17.1581,24.1537);
				glVertex3f(5.2503,16.9163,25.0453);
				glVertex3f(6.7827,16.4441,25.1717);
				glVertex3f(7.5229,16.4659,24.3786);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2503,16.9163,25.0453);
				glVertex3f(5.1994,16.7765,25.5605);
				glVertex3f(6.2472,16.4209,25.7944);
				glVertex3f(6.7827,16.4441,25.1717);
			}glEnd();


			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.1994,16.7765,25.5605);
				glVertex3f(5.1348,16.599,26.2148);
				glVertex3f(5.7332,16.4209,26.2434);
				glVertex3f(6.2472,16.4209,25.7944);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.1348,16.599,26.2148);
				glVertex3f(5.1025,16.5103,26.5420);
				glVertex3f(5.381,16.4131,26.6131);
				glVertex3f(5.7332,16.4209,26.2434);
			}glEnd();


			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(5.1025,16.5103,26.5420);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.381,16.4131,26.6131);
			}glEnd();

			//------------------- face of a petal end

		}glPopMatrix();
	}
}
void drawPetal_2_leftface()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left part of a petal face

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9067,21.6765,5.9534);
				glVertex3f(-0.9177,21.6462,7.2216);
				glVertex3f(-0.4616,21.5103,7.2216);
				glVertex3f(-0.4435,21.5295,5.9534);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9177,21.6462,7.2216);
				glVertex3f(-0.9375,21.5919,9.4936);
				glVertex3f(-0.3556,21.4027,9.5259);
				glVertex3f(-0.4616,21.5103,7.2216);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9375,21.5919,9.4936);
				glVertex3f(-0.8943,21.4690,11.1525);
				glVertex3f(-0.159,21.2391,11.0836);
				glVertex3f(-0.3556,21.4027,9.5259);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.8943,21.4690,11.1525);
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.0361,21.0767,12.5058);
				glVertex3f(-0.159,21.2391,11.0836);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(0.3133,20.7896,14.0929);
				glVertex3f(-0.0361,21.0767,12.5058);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(0.76,20.4386,15.6422);
				glVertex3f(0.3133,20.7896,14.0929);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(1.4338,19.9352,17.4426);
				glVertex3f(0.76,20.4386,15.6422);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(2.4082,19.2361,19.4869);
				glVertex3f(1.4338,19.9352,17.4426);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(3.3408,18.6234,20.9393);
				glVertex3f(2.4082,19.2361,19.4869);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(4.3924,17.8982,22.5534);
				glVertex3f(3.3408,18.6234,20.9393);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(5.381,17.2755,23.7211);
				glVertex3f(4.3924,17.8982,22.5534);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(5.3013,17.0566,24.5281);
				glVertex3f(5.381,17.2755,23.7211);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(4.129,17.0735,25.7615);
				glVertex3f(5.1796,16.722,25.7615);
				glVertex3f(5.3013,17.0566,24.5281);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(4.129,17.0735,25.7615);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.1796,16.722,25.7615);
			}glEnd();
			//------------------- face of a petal end

		}glPopMatrix();
	}
}

void drawPetal_2_rightback()
{
	for(int i=0; i<1; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left back part of a petal face

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.7677,27.9475,8.183);
				glVertex3f(8.5208,27.9148,7.8667);
			}glEnd();

			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.5208,27.9148,7.8667);
				glVertex3f(8.3098,27.8663,7.5216);
			}glEnd();

			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.3098,27.8663,7.5216);
				glVertex3f(8.1379,27.8026,7.1532);
			}glEnd();

			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.1379,27.8026,7.1532);
				glVertex3f(8.0078,27.7244,6.7674);
			}glEnd();

			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.0078,27.7244,6.7674);
				glVertex3f(7.9215,27.6327,6.3704);
			}glEnd();


			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(7.9215,27.6327,6.3704);
				glVertex3f(7.8803,27.5289,5.9686);//last
			}glEnd();
			// -------------------------------------------------------------------------------col-1
			glColor3f(1.0, 1.0, 1.0);
			drawTexture(large_marble,
               8.7677,27.9475,8.1830,
               8.6470,27.5694,10.3582,
               8.4380,27.5656,10.0903,
               8.640,27.93310,8.0288
               );

			glBegin(GL_QUADS);{
				glVertex3f(8.7677,27.9475,8.1830);
				glVertex3f(8.6470,27.5694,10.3582);
				glVertex3f(8.4380,27.5656,10.0903);
				glVertex3f(8.640,27.93310,8.0288);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               8.6470,27.5694,10.3582,
               8.4754,27.0348,12.4809,
               8.1879,27.0545,12.0989,
               8.4380,27.5656,10.0903
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.6470,27.5694,10.3582);
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(8.4380,27.5656,10.0903);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               8.4754,27.0348,12.4809,
               8.2546,26.3489,14.5346,
               7.8917,26.4047,14.0401,
               8.1879,27.0545,12.0989
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(7.8917,26.4047,14.0401);
				glVertex3f(8.1879,27.0545,12.0989);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               8.2546,26.3489,14.5346,
               7.9870,25.5183,16.5042,
               7.5515,25.6224,15.9003,
               7.8917,26.4047,14.0401
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(7.9870,25.5183,16.5042);
				glVertex3f(7.5515,25.6224,15.9003);
				glVertex3f(7.8917,26.4047,14.0401);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               7.9870,25.5183,16.5042,
               7.6752,24.5508,18.3755,
               7.1700,24.7146,17.667,
               7.5515,25.6224,15.9003
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.9870,25.5183,16.5042);
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.1700,24.7146,17.667);
				glVertex3f(7.5515,25.6224,15.9003);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               7.6752,24.5508,18.3755,
               7.3223,23.4549,20.1358,
               6.7501,23.6892,19.3286,
               7.1700,24.7146,17.667
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(6.7501,23.6892,19.3286);
				glVertex3f(7.1700,24.7146,17.667);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
            drawTexture(large_marble,
               7.3223,23.4549,20.1358,
               6.9316,22.2404,21.7735,
               6.3055,22.22534,20.9061,
               6.7501,23.6892,19.3286
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(6.3055,22.22534,20.9061);
				glVertex3f(6.7501,23.6892,19.3286);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               6.9316,22.2404,21.7735,
               6.5067,20.9174,23.2788,
               5.7484,21.2735,22.3183,
               6.3055,22.22534,20.9061
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(5.7484,21.2735,22.3183);
				glVertex3f(6.3055,22.22534,20.9061);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               6.5067,20.9174,23.2788,
               6.0514,19.4966,24.6435,
               5.2319,19.9499,23.6042,
               5.7484,21.2735,22.3183
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(5.2319,19.9499,23.6042);
				glVertex3f(5.7484,21.2735,22.3183);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               6.0514,19.4966,24.6435,
               5.5694,17.9891,25.8609,
               4.6909,18.5466,24.7533,
               5.2319,19.9499,23.6042
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(4.6909,18.5466,24.7533);
				glVertex3f(5.2319,19.9499,23.6042);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               5.5694,17.9891,25.8609,
               5.0646,16.4061,26.9262,
               4.1290,17.0735,25.7615,
               4.6909,18.5466,24.7533
               );
			glBegin(GL_QUADS);{
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(4.1290,17.0735,25.7615);
				glVertex3f(4.6909,18.5466,24.7533);
			}glEnd();
			//----------------------------------------------------------------------------------------------col=2
			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               8.640,27.93310,8.0288,
               8.4380,27.5656,10.0903,
               8.2435,27.5553,9.8093,
               8.5208,27.9148,7.8667
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.640,27.93310,8.0288);
				glVertex3f(8.4380,27.5656,10.0903);

				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(8.5208,27.9148,7.8667);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               8.4380,27.5656,10.0903,
               8.1879,27.0545,12.0989,
               7.9210,27.0652,11.6987,
               8.2435,27.5553,9.8093
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.4380,27.5656,10.0903);
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(7.9210,27.0652,11.6987);
				glVertex3f(8.2435,27.5553,9.8093);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               8.1879,27.0545,12.0989,
               7.8917,26.4047,14.0401,
               7.5553,26.4491,13.5223,
               7.9210,27.0652,11.6987
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(7.8917,26.4047,14.0401);

				glVertex3f(7.5553,26.4491,13.5223);
				glVertex3f(7.9210,27.0652,11.6987);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               7.8917,26.4047,14.0401,
               7.5515,25.6224,15.9003,
               7.1486,25.7127,15.2679,
               7.5553,26.4491,13.5223
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.8917,26.4047,14.0401);
				glVertex3f(7.5515,25.6224,15.9003);

				glVertex3f(7.1486,25.7127,15.2679);
				glVertex3f(7.5553,26.4491,13.5223);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               7.5515,25.6224,15.9003,
               7.1700,24.7146,17.667,
               6.7034,24.8623,16.9246,
               7.1486,25.7127,15.2679
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.5515,25.6224,15.9003);
				glVertex3f(7.1700,24.7146,17.667);


				glVertex3f(6.7034,24.8623,16.9246);
				glVertex3f(7.1486,25.7127,15.2679);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               7.1700,24.7146,17.667,
               6.7501,23.6892,19.3286,
               6.2225,23.9053,18.4823,
               6.7034,24.8623,16.9246
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.1700,24.7146,17.667);
				glVertex3f(6.7501,23.6892,19.3286);
				glVertex3f(6.2225,23.9053,18.4823);
				glVertex3f(6.7034,24.8623,16.9246);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               6.7501,23.6892,19.3286,
               6.3055,22.22534,20.9061,
               5.5906,22.7548,19.9831,
               6.2225,23.9053,18.4823
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.7501,23.6892,19.3286);
				glVertex3f(6.3055,22.22534,20.9061);
				glVertex3f(5.5906,22.7548,19.9831);
				glVertex3f(6.2225,23.9053,18.4823);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               6.3055,22.22534,20.9061,
               5.7484,21.2735,22.3183,
               5.0451,21.6080,21.3062,
               5.5906,22.7548,19.9831
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.3055,22.22534,20.9061);
				glVertex3f(5.7484,21.2735,22.3183);


				glVertex3f(5.0451,21.6080,21.3062);
				glVertex3f(5.5906,22.7548,19.9831);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               5.7484,21.2735,22.3183,
               5.2319,19.9499,23.6042,
               4.4737,20.3799,22.5077,
               5.0451,21.6080,21.3062
               );
			glBegin(GL_QUADS);{
				glVertex3f(5.7484,21.2735,22.3183);
				glVertex3f(5.2319,19.9499,23.6042);


				glVertex3f(4.4737,20.3799,22.5077);
				glVertex3f(5.0451,21.6080,21.3062);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               5.2319,19.9499,23.6042,
               4.6909,18.5466,24.7533,
               3.8798,19.0793,23.5829,
               4.4737,20.3799,22.5077
               );
			glBegin(GL_QUADS);{
				glVertex3f(5.2319,19.9499,23.6042);
				glVertex3f(4.6909,18.5466,24.7533);


				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(4.4737,20.3799,22.5077);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               4.6909,18.5466,24.7533,
               4.1290,17.0735,25.7615,
               3.2669,17.715,24.5281,
               3.8798,19.0793,23.5829
               );
			glBegin(GL_QUADS);{
				glVertex3f(4.6909,18.5466,24.7533);
				glVertex3f(4.1290,17.0735,25.7615);


				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(3.8798,19.0793,23.5829);
			}glEnd();

			//col=3
			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               8.5208,27.9148,7.8667,
               8.2435,27.5553,9.8093,
               8.0643,27.5384,9.5163,
               8.4106,27.8925,7.6974
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.5208,27.9148,7.8667);

				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(8.4106,27.8925,7.6974);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               8.2435,27.5553,9.8093,
               7.9210,27.0652,11.6987,
               7.6757,27.0666,11.2820,
               8.0643,27.5384,9.5163
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(7.9210,27.0652,11.6987);
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(8.0643,27.5384,9.5163);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               7.9210,27.0652,11.6987,
               7.5553,26.4491,13.5223,
               7.2468,26.4815,12.9832,
               7.6757,27.0666,11.2820
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.9210,27.0652,11.6987);

				glVertex3f(7.5553,26.4491,13.5223);
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(7.6757,27.0666,11.2820);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               7.5553,26.4491,13.5223,
               7.1486,25.7127,15.2679,
               6.7797,25.7882,14.6096,
               7.2468,26.4815,12.9832
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.5553,26.4491,13.5223);

				glVertex3f(7.1486,25.7127,15.2679);
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(7.2468,26.4815,12.9832);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               7.1486,25.7127,15.2679,
               6.7034,24.8623,16.9246,
               6.277,24.9925,16.1515,
               6.7797,25.7882,14.6096
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.1486,25.7127,15.2679);


				glVertex3f(6.7034,24.8623,16.9246);
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(6.7797,25.7882,14.6096);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               6.7034,24.8623,16.9246,
               6.2225,23.9053,18.4823,
               5.7412,24.1011,17.6003,
               6.277,24.9925,16.1515
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.7034,24.8623,16.9246);


				glVertex3f(6.2225,23.9053,18.4823);
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(6.277,24.9925,16.1515);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               6.2225,23.9053,18.4823,
               5.5906,22.7548,19.9831,
               4.9956,22.9798,19.0224,
               5.7412,24.1011,17.6003
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.2225,23.9053,18.4823);


				glVertex3f(5.5906,22.7548,19.9831);
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(5.7412,24.1011,17.6003);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               5.5906,22.7548,19.9831,
               5.0451,21.6080,21.3062,
               4.3989,21.9176,20.2465,
               4.9956,22.9798,19.0224
               );
			glBegin(GL_QUADS);{
				glVertex3f(5.5906,22.7548,19.9831);


				glVertex3f(5.0451,21.6080,21.3062);
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(4.9956,22.9798,19.0224);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               5.0451,21.6080,21.3062,
               4.4737,20.3799,22.5077,
               3.7788,20.7825,21.3583,
               4.3989,21.9176,20.2465
               );
			glBegin(GL_QUADS);{
				glVertex3f(5.0451,21.6080,21.3062);


				glVertex3f(4.4737,20.3799,22.5077);
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(4.3989,21.9176,20.2465);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               4.4737,20.3799,22.5077,
               3.8798,19.0793,23.5829,
               3.1383,19.5823,22.3538,
               3.7788,20.7825,21.3583
               );
			glBegin(GL_QUADS);{
				glVertex3f(4.4737,20.3799,22.5077);


				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(3.7788,20.7825,21.3583);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               3.8798,19.0793,23.5829,
               3.2669,17.715,24.5281,
               2.4806,18.3247,23.2301,
               3.1383,19.5823,22.3538
               );
			glBegin(GL_QUADS);{
				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(3.1383,19.5823,22.3538);

			}glEnd();

			//col=4
			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               8.4106,27.8925,7.6974,
               8.0643,27.5384,9.5163,
               7.9010,27.5149,9.2126,
               8.3098,27.8663,7.5216
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.4106,27.8925,7.6974);
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(8.3098,27.8663,7.5216);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               8.0643,27.5384,9.5163,
               7.6757,27.0666,11.2820,
               7.4528,27.0586,10.8504,
               7.9010,27.5149,9.2126
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(7.9010,27.5149,9.2126);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               7.6757,27.0666,11.2820,
               7.2468,26.4815,12.9832,
               6.9671,26.5013,12.4251,
               7.4528,27.0586,10.8504
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(7.4528,27.0586,10.8504);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               7.2468,26.4815,12.9832,
               6.7797,25.7882,14.6096,
               6.4461,25.848,13.928,
               6.9671,26.5013,12.4251
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(6.9671,26.5013,12.4251);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               6.7797,25.7882,14.6096,
               6.277,24.9925,16.1515,
               5.8921,25.1039,15.3507,
               6.4461,25.848,13.928
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(6.4461,25.848,13.928);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               6.277,24.9925,16.1515,
               5.7412,24.1011,17.6003,
               5.3077,24.2747,16.6861,
               5.8921,25.1039,15.3507
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(5.8921,25.1039,15.3507);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               5.7412,24.1011,17.6003,
               4.9956,22.9798,19.0224,
               4.4532,23.1801,18.0232,
               5.3077,24.2747,16.6861
               );
			glBegin(GL_QUADS);{
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(5.3077,24.2747,16.6861);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               4.9956,22.9798,19.0224,
               4.3989,21.9176,20.2465,
               3.8117,22.1992,19.1435,
               4.4532,23.1801,18.0232
               );
			glBegin(GL_QUADS);{
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(4.4532,23.1801,18.0232);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               4.3989,21.9176,20.2465,
               3.7788,20.7825,21.3583,
               3.1491,21.1539,20.1604,
               3.8117,22.1992,19.1435
               );
			glBegin(GL_QUADS);{
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(3.8117,22.1992,19.1435);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               3.7788,20.7825,21.3583,
               3.1383,19.5823,22.3538,
               2.4683,20.0510,21.0708,
               3.1491,21.1539,20.1604
               );
			glBegin(GL_QUADS);{
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(3.1491,21.1539,20.1604);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               3.1383,19.5823,22.3538,
               2.4806,18.3247,23.2301,
               1.7722,18.8972,21.8723,
               2.4683,20.0510,21.0708
               );
			glBegin(GL_QUADS);{
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(2.4683,20.0510,21.0708);
			}glEnd();

			//col=5

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               8.3098,27.8663,7.5216,
               7.9010,27.5149,9.2126,
               7.7544,27.4847,8.8995,
8.2188,27.8363,7.3399
               );
			glBegin(GL_QUADS);{
				glVertex3f(8.3098,27.8663,7.5216);
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(8.2188,27.8363,7.3399);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               7.9010,27.5149,9.2126,
               7.4528,27.0586,10.8504,
               7.2532,27.0408,10.4057,
                7.7544,27.4847,8.8995
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(7.7544,27.4847,8.8995);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               7.4528,27.0586,10.8504,
               6.9671,26.5013,12.4251,
               6.7173,26.5082,11.8504,
                7.2532,27.0408,10.4057
               );
			glBegin(GL_QUADS);{
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(7.2532,27.0408,10.4057);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               6.9671,26.5013,12.4251,
               6.4461,25.848,13.928,
               6.1488,25.8913,13.226,
                6.7173,26.5082,11.8504
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(6.7173,26.5082,11.8504);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               6.4461,25.848,13.928,
               5.8921,25.1039,15.3507,
               5.5499,25.1950,14.5257,
                6.1488,25.8913,13.226
               );
			glBegin(GL_QUADS);{
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(6.1488,25.8913,13.226);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               5.8921,25.1039,15.3507,
               5.3077,24.2747,16.6861,
               4.9232,24.4244,15.7434,
                5.5499,25.1950,14.5257
               );
			glBegin(GL_QUADS);{
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(5.5499,25.1950,14.5257);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               5.3077,24.2747,16.6861,
               4.4532,23.1801,18.0232,
               3.9652,23.3534,16.9896,
                4.9232,24.4244,15.7434
               );
			glBegin(GL_QUADS);{
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(4.9232,24.4244,15.7434);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               4.4532,23.1801,18.0232,
               3.8117,22.1992,19.1435,
               3.2853,22.4499,18.0016,
                3.9652,23.3534,16.9896
               );
			glBegin(GL_QUADS);{
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(3.9652,23.3534,16.9896);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               3.8117,22.1992,19.1435,
               3.1491,21.1539,20.1604,
               2.5865,21.4904,18.9188,
               3.2853,22.4499,18.0016
               );
			glBegin(GL_QUADS);{
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(3.2853,22.4499,18.0016);
			}glEnd();

			//glColor3f(.50, 0.50, 0.0);
			drawTexture(large_marble,
               3.1491,21.1539,20.1604,
               2.4683,20.0510,21.0708,
               1.8716,20.4809,19.7386,
               2.5865,21.4904,18.9188
               );
			glBegin(GL_QUADS);{
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(2.5865,21.4904,18.9188);
			}glEnd();

			//glColor3f(1.0, 0.0, 0.0);
			drawTexture(large_marble,
               2.4683,20.0510,21.0708,
               1.7722,18.8972,21.8723,
               1.1432,19.4271,20.4595,
               1.8716,20.4809,19.7386
               );
			glBegin(GL_QUADS);{
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(1.8716,20.4809,19.7386);
			}glEnd();

			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++col=6
			drawTexture(large_marble,

				8.2188,27.8363,7.3399,
				7.7544,27.4847,8.8995,
				7.6248,27.4479,8.5781,
				8.1379,27.8026,7.1532);


			drawTexture(large_marble,
               7.7544,27.4847,8.8995
				,7.2532,27.0408,10.4057
				,7.0775,27.0131,9.9497

				,7.6248,27.4479,8.5781);


			drawTexture(large_marble,
               7.2532,27.0408,10.4057
				,6.7173,26.5082,11.8504
				,6.4982,26.5017,11.2612

                ,7.0775,27.0131,9.9497);


			drawTexture(large_marble,
               6.7173,26.5082,11.8504
				,6.1488,25.8913,13.226
				,5.8888,25.9175,12.5063

				,6.4982,26.5017,11.2612);

			drawTexture(large_marble,
               6.1488,25.8913,13.226,
				5.5499,25.1950,14.5257,
				5.2517,25.265,13.6796,

			5.8888,25.9175,12.5063);


			drawTexture(large_marble,
               5.5499,25.1950,14.5257,
				4.9232,24.4244,15.7434,
				4.5891,24.5486,14.7761,

			5.2517,25.265,13.6796);


			drawTexture(large_marble,
               4.9232,24.4244,15.7434,
				3.9652,23.3534,16.9896,
				3.533,23.4977,15.9259,

				4.5891,24.5486,14.7761);


			drawTexture(large_marble,
               3.9652,23.3534,16.9896,
			3.2853,22.4499,18.0016,
				2.821,22.667,16.8254,

				3.533,23.4977,15.9259);


			drawTexture(large_marble,
               3.2853,22.4499,18.0016,
			2.5865,21.4904,18.9188,
				2.0924,21.7887,17.6382,

				2.821,22.667,16.8254);


			drawTexture(large_marble,
               2.5865,21.4904,18.9188,
			1.8716,20.4809,19.7386,
			1.3108,20.8513,18.3386,

	2.0924,21.7887,17.6382);


			drawTexture(large_marble,
               1.8716,20.4809,19.7386,
			1.1432,19.4271,20.4595,
			0.5953,19.9095,18.9972,

			1.3108,20.8513,18.3386);


			/*
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.2188,27.8363,7.3399);
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(8.1379,27.8026,7.1532);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(7.0775,27.0131,9.9497);

				glVertex3f(7.6248,27.4479,8.5781);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(6.4982,26.5017,11.2612);

				glVertex3f(7.0775,27.0131,9.9497);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(5.8888,25.9175,12.5063);

				glVertex3f(6.4982,26.5017,11.2612);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(5.2517,25.265,13.6796);

				glVertex3f(5.8888,25.9175,12.5063);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(4.5891,24.5486,14.7761);

				glVertex3f(5.2517,25.265,13.6796);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(3.533,23.4977,15.9259);

				glVertex3f(4.5891,24.5486,14.7761);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(2.821,22.667,16.8254);

				glVertex3f(3.533,23.4977,15.9259);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(2.0924,21.7887,17.6382);

				glVertex3f(2.821,22.667,16.8254);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(1.3108,20.8513,18.3386);

				glVertex3f(2.0924,21.7887,17.6382);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(0.5953,19.9095,18.9972);

				glVertex3f(1.3108,20.8513,18.3386);
			}glEnd();

            */
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++col=7
			drawTexture(large_marble,
               8.1379,27.8026,7.1532,
			7.6248,27.4479,8.5781,
			7.5128,27.4045,8.2499,
			8.0675,27.7653,6.9621);


			drawTexture(large_marble,
               7.6248,27.4479,8.5781,
			7.0775,27.0131,9.9497,
			6.9265,26.9755,9.4844,

			7.5128,27.4045,8.2499);


			drawTexture(large_marble,
               7.0775,27.0131,9.9497,
			6.4982,26.5017,11.2612,
			6.3105,26.4815,10.6601,

			6.9265,26.9755,9.4844);



			drawTexture(large_marble,
               6.4982,26.5017,11.2612,
			5.8888,25.9175,12.5063,
			5.6671,25.926,11.7721,
			6.3105,26.4815,10.6601);



			drawTexture(large_marble,
               5.8888,25.9175,12.5063,
			5.2517,25.265,13.6796,
			4.9982,25.3128,12.816,

			5.6671,25.926,11.7721);


			drawTexture(large_marble,
               5.2517,25.265,13.6796,

			4.5891,24.5486,14.7761,
			4.3063,24.646,13.7879,

			4.9982,25.3128,12.816);


			drawTexture(large_marble,
               4.5891,24.5486,14.7761,
				3.533,23.4977,15.9259,
				3.158,23.6113,14.8363,

			4.3063,24.646,13.7879);


			drawTexture(large_marble,
               3.533,23.4977,15.9259,
			2.821,22.667,16.8254,
			2.4203,22.8482,15.6197,

			3.158,23.6113,14.8363);

drawTexture(large_marble,
            2.821,22.667,16.8254,
			2.0924,21.7887,17.6382,
			1.6681,22.0457,16.3239,

			2.4203,22.8482,15.6197);


			drawTexture(large_marble,
               2.0924,21.7887,17.6382,
			1.3108,20.8513,18.3386,
			0.9037,21.2082,16.9479,

			1.6681,22.0457,16.3239);


			drawTexture(large_marble,
               1.3108,20.8513,18.3386,
			0.5953,19.9095,18.9972,
			0.1295,20.3398,17.491,

			0.9037,21.2082,16.9479);

            /*
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.1379,27.8026,7.1532);
				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(8.0675,27.7653,6.9621);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(7.0775,27.0131,9.9497);
				glVertex3f(6.9265,26.9755,9.4844);

				glVertex3f(7.5128,27.4045,8.2499);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.0775,27.0131,9.9497);
				glVertex3f(6.4982,26.5017,11.2612);
				glVertex3f(6.3105,26.4815,10.6601);

				glVertex3f(6.9265,26.9755,9.4844);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.4982,26.5017,11.2612);
				glVertex3f(5.8888,25.9175,12.5063);
				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(6.3105,26.4815,10.6601);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(5.8888,25.9175,12.5063);
				glVertex3f(5.2517,25.265,13.6796);
				glVertex3f(4.9982,25.3128,12.816);

				glVertex3f(5.6671,25.926,11.7721);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2517,25.265,13.6796);

				glVertex3f(4.5891,24.5486,14.7761);
				glVertex3f(4.3063,24.646,13.7879);

				glVertex3f(4.9982,25.3128,12.816);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(4.5891,24.5486,14.7761);
				glVertex3f(3.533,23.4977,15.9259);
				glVertex3f(3.158,23.6113,14.8363);

				glVertex3f(4.3063,24.646,13.7879);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(3.533,23.4977,15.9259);
				glVertex3f(2.821,22.667,16.8254);
				glVertex3f(2.4203,22.8482,15.6197);

				glVertex3f(3.158,23.6113,14.8363);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.821,22.667,16.8254);
				glVertex3f(2.0924,21.7887,17.6382);
				glVertex3f(1.6681,22.0457,16.3239);

				glVertex3f(2.4203,22.8482,15.6197);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(2.0924,21.7887,17.6382);
				glVertex3f(1.3108,20.8513,18.3386);
				glVertex3f(0.9037,21.2082,16.9479);

				glVertex3f(1.6681,22.0457,16.3239);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.3108,20.8513,18.3386);
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(0.1295,20.3398,17.491);

				glVertex3f(0.9037,21.2082,16.9479);
			}glEnd();
            */

			//---------------------------------------------------------------------------col=8
			drawTexture(large_marble,
               8.0675,27.7653,6.9621,
				7.5128,27.4045,8.2499,
				7.4188,27.3546,7.916,
				8.0078,27.7244,6.7674);


			drawTexture(large_marble,
               7.5128,27.4045,8.2499,
				6.9265,26.9755,9.4844,
				6.8006,26.928,9.0114,

				7.4188,27.3546,7.916);


			drawTexture(large_marble,
               6.9265,26.9755,9.4844,
				6.3105,26.4815,10.6601,
				6.155,26.4475,10.0494,

				6.8006,26.928,9.0114);


			drawTexture(large_marble,
               6.3105,26.4815,10.6601,
				5.6671,25.926,11.7721,
				5.4843,25.9163,11.0261,

				6.155,26.4475,10.0494);


			drawTexture(large_marble,
               5.6671,25.926,11.7721,
				4.9982,25.3128,12.816,
				4.7904,25.3377,11.9382,

				5.4843,25.9163,11.0261);


			drawTexture(large_marble,
               4.9982,25.3128,12.816,
				4.3063,24.646,13.7879,
				4.0757,24.7154,12.783,
4.7904,25.3377,11.9382);


			drawTexture(large_marble,
               4.3063,24.646,13.7879,
				3.158,23.6113,14.8363,
				2.8411,23.6927,13.7254,
4.0757,24.7154,12.783);


			drawTexture(large_marble,
               3.158,23.6113,14.8363,
				2.4203,22.8482,15.6197,
				2.0842,22.9914,14.3894,
2.8411,23.6927,13.7254);


			drawTexture(large_marble,
               2.4203,22.8482,15.6197,
				1.6681,22.0457,16.3239,
				1.3147,22.2589,14.9813,
2.0842,22.9914,14.3894);


			drawTexture(large_marble,
               1.6681,22.0457,16.3239,
				0.9037,21.2082,16.9479,
				0.5348,21.4986,15.5005,
1.3147,22.2589,14.9813);


			drawTexture(large_marble,
               0.9037,21.2082,16.9479,
				0.1295,20.3398,17.491,
				-0.2531,20.7141,15.947,
0.5348,21.4986,15.5005);



			//col=9
			drawTexture(large_marble,
               8.0078,27.7244,6.7674,
				7.4188,27.3546,7.916,
				7.3432,27.2984,7.5779,
				7.9591,27.6802,6.5699);


			drawTexture(large_marble,
               7.4188,27.3546,7.916,
				6.8006,26.928,9.0114,
				6.7003,26.8706,8.5328,
7.3432,27.2984,7.5779);


			drawTexture(large_marble,
               6.8006,26.928,9.0114,
				6.155,26.4475,10.0494,
				6.0322,26.3996,9.4316,

				6.7003,26.8706,8.5328);


			drawTexture(large_marble,
               6.155,26.4475,10.0494,
				5.4843,25.9163,11.0261,

				5.3411,25.8882,10.2714,

				6.0322,26.3996,9.4316);


			drawTexture(large_marble,
               5.4843,25.9163,11.0261,
				4.7904,25.3377,11.9382,
				4.629,25.3393,11.05,
5.3411,25.8882,10.2714);


			drawTexture(large_marble,
               4.7904,25.3377,11.9382,
				4.0757,24.7154,12.783,
				3.898,24.7559,11.7653,

				4.629,25.3393,11.05);


			drawTexture(large_marble,
               4.0757,24.7154,12.783,
				2.8411,23.6927,13.7254,
				2.5835,23.7406,12.5976,

				3.898,24.7559,11.7653);



			drawTexture(large_marble,
               2.8411,23.6927,13.7254,
				2.0842,22.9914,14.3894,
				1.8137,23.0951,13.1395,

				2.5835,23.7406,12.5976);


			drawTexture(large_marble,
               2.0842,22.9914,14.3894,

					1.3147,22.2589,14.9813,
1.0331,22.4261,13.6157,
1.8137,23.0951,13.1395);


			drawTexture(large_marble,
               1.3147,22.2589,14.9813,
				0.5348,21.4986,15.5005,
				0.244,21.7364,14.0263,

				1.0331,22.4261,13.6157);



			drawTexture(large_marble,
               0.5348,21.4986,15.5005,
				-0.2531,20.7141,15.947,
			-0.5516,21.0288,14.3715,

			0.244,21.7364,14.0263);



			//col=10
			drawTexture(large_marble,
               7.9591,27.6802,6.5699,
				7.3432,27.2984,7.5779,
				7.2862,27.236,7.2368,
				7.9215,27.6327,6.3704);

drawTexture(large_marble,
            7.3432,27.2984,7.5779,
			6.7003,26.8706,8.5328,
			6.6259,26.8035,8.0505,
7.2862,27.236,7.2368);

			drawTexture(large_marble,
               6.7003,26.8706,8.5328,
				6.0322,26.3996,9.4316,
				5.9425,26.3379,8.8092,
6.6259,26.8035,8.0505);


			drawTexture(large_marble,
               6.0322,26.3996,9.4316,

				5.3411,25.8882,10.2714,
				5.2379,25.8415,9.511,

				5.9425,26.3379,8.8092);


			drawTexture(large_marble,
               5.3411,25.8882,10.2714,
				4.629,25.3393,11.05,
				4.5143,25.3171,10.1547,
5.2379,25.8415,9.511);



			drawTexture(large_marble,
               4.629,25.3393,11.05,
				3.898,24.7559,11.7653,
				3.7737,24.7671,10.7389,
				4.5143,25.3171,10.1547);

			drawTexture(large_marble,
               3.898,24.7559,11.7653,
				2.5835,23.7406,12.5976,
				2.3858,23.7543,11.4575,
3.7737,24.7671,10.7389);


			drawTexture(large_marble,
               2.5835,23.7406,12.5976,
				1.8137,23.0951,13.1395,
				1.6095,23.158,11.8751,
2.3858,23.7543,11.4575);


			drawTexture(large_marble,
               1.8137,23.0951,13.1395,
1.0331,22.4261,13.6157,
0.8241,22.5455,12.2329,
1.6095,23.158,11.8751);


			drawTexture(large_marble,
               1.0331,22.4261,13.6157,
				0.244,21.7364,14.0263,
				0.0318,21.9191,12.5313,
0.8241,22.5455,12.2329);


			drawTexture(large_marble,
               0.244,21.7364,14.0263,
				-0.5516,21.0288,14.3715,
				-0.7655,21.2812,12.7711,

				0.0318,21.9191,12.5313);



			//col=11
drawTexture(large_marble,
            7.9215,27.6327,6.3704,
7.2862,27.236,7.2368,
				7.2481,27.1676,6.8942,
				7.8952,27.5822,6.1697);


			drawTexture(large_marble,
               7.2862,27.236,7.2368,
				6.6259,26.8035,8.0505,
				6.5778,26.7269,7.5662,
7.2481,27.1676,6.8942);


			drawTexture(large_marble,
               6.6259,26.8035,8.0505,
				5.9425,26.3379,8.8092,
				5.8862,26.2624,8.1845,
6.5778,26.7269,7.5662);

			drawTexture(large_marble,
               5.9425,26.3379,8.8092,
				5.2379,25.8415,9.511,
				5.1752,25.7764,8.748,
5.8862,26.2624,8.1845);

			drawTexture(large_marble,
               5.2379,25.8415,9.511,
				4.5143,25.3171,10.1547,
				4.4469,25.2711,9.2559,
5.1752,25.7764,8.748);

			drawTexture(large_marble,
               4.5143,25.3171,10.1547,
				3.7737,24.7671,10.7389,
				3.7032,24.7486,9.708,
				4.4469,25.2711,9.2559);

			drawTexture(large_marble,
               3.7737,24.7671,10.7389,
				2.3858,23.7543,11.4575,
				2.2486,23.7331,10.3098,
3.7032,24.7486,9.708);

			drawTexture(large_marble,
               2.3858,23.7543,11.4575,
				1.6095,23.158,11.8751,
				1.4722,23.1791,10.6014,

				2.2486,23.7331,10.3098);




			drawTexture(large_marble,
               1.6095,23.158,11.8751,
				0.8241,22.5455,12.2329,
				0.6882,22.6159,10.8385,

				1.4722,23.1791,10.6014);



			drawTexture(large_marble,
               0.8241,22.5455,12.2329,
				0.0318,21.9191,12.5313,
				-0.1012,22.0452,11.0218,

				0.6882,22.6159,10.8385);




			drawTexture(large_marble,
               0.0318,21.9191,12.5313,
				-0.7655,21.2812,12.7711,
				-0.8943,21.469,11.1525,

				-0.1012,22.0452,11.0218);




			//col=12
			drawTexture(large_marble,
               7.8952,27.5822,6.1697,
				7.2481,27.1676,6.8942,
				7.229,27.0935,6.5513,
				7.8803,27.5289,5.9686);




			drawTexture(large_marble,
               7.2481,27.1676,6.8942,
				6.5778,26.7269,7.5662,
				6.5561,26.641,7.082,
				7.229,27.0935,6.5513);






			drawTexture(large_marble,
               6.5778,26.7269,7.5662,
				5.8862,26.2624,8.1845,
				5.8635,26.1736,7.56,
				6.5561,26.641,7.082);





			drawTexture(large_marble,
               5.8862,26.2624,8.1845,
				5.1752,25.7764,8.748,
				5.1531,25.693,7.9851,

				5.8635,26.1736,7.56);



			drawTexture(large_marble,
               5.1752,25.7764,8.748,
				4.4469,25.2711,9.2559,
				4.4269,25.2013,8.3573,

				5.1531,25.693,7.9851);





			drawTexture(large_marble,
               4.4469,25.2711,9.2559,
				3.7032,24.7486,9.708,
				3.6867,24.7002,8.6767,

				4.4269,25.2013,8.3573);





			drawTexture(large_marble,
               3.7032,24.7486,9.708,
				2.2486,23.7331,10.3098,
				2.1725,23.6769,9.1591,
				3.6867,24.7002,8.6767);




			drawTexture(large_marble,
               2.2486,23.7331,10.3098,
				1.4722,23.1791,10.6014,
				1.4022,23.1581,9.3236,

				2.1725,23.6769,9.1591);




			drawTexture(large_marble,
               1.4722,23.1791,10.6014,
				0.6882,22.6159,10.8385,
				0.6258,22.6364,9.4382,

				1.4022,23.1581,9.3236);



			drawTexture(large_marble,
               0.6882,22.6159,10.8385,
			-0.1012,22.0452,11.0218,
			-0.1427,22.1202,9.5215,
            0.6258,22.6364,9.4382);




			drawTexture(large_marble,
               -0.1012,22.0452,11.0218,
				-0.8943,21.469,11.1525,
				-0.938,21.5905,9.5226,

				-0.1427,22.1202,9.5215);




			//col=13

			//------------------- face of a petal end

		}glPopMatrix();
	}
}


void drawPetal_2_leftback()//complete
{
	for(int i=0; i<1; i++)
	{
		glPushMatrix();{
			//glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left back part of a petal face



			glColor3f(1.0, 1.0, 1.0);
			drawTexture(large_marble,
				8.836,28.1654,5.972,
				9.2724,27.1032,5.968,
				9.2962,27.2124,6.3711,
				9.2962,27.2124,6.3711

               );
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.2724,27.1032,5.968);
				glVertex3f(9.2962,27.2124,6.3711);
			}glEnd();

			drawTexture(large_marble,
				8.836,28.1654,5.972,
				9.2962,27.2124,6.3711,
				9.2756,27.3368,6.7684,
				9.2756,27.3368,6.7684
               );			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.2962,27.2124,6.3711);
				glVertex3f(9.2756,27.3368,6.7684);
			}glEnd();

			drawTexture(large_marble,
				8.836,28.1654,5.972,
				9.2756,27.3368,6.7684,
				9.211,27.4746,7.1543,
				9.211,27.4746,7.1543

               );			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.2756,27.3368,6.7684);
				glVertex3f(9.211,27.4746,7.1543);
			}glEnd();

			drawTexture(large_marble,
				8.836,28.1654,5.972,
				9.211,27.4746,7.1543,
				9.1035,27.6237,7.5226,
				9.1035,27.6237,7.5226
               );			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.211,27.4746,7.1543);
				glVertex3f(9.1035,27.6237,7.5226);
			}glEnd();

			drawTexture(large_marble,
				8.836,28.1654,5.972,
				9.1035,27.6237,7.5226,
				8.9549,27.782,7.8674,
					8.9549,27.782,7.8674

               );			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(9.1035,27.6237,7.5226);
				glVertex3f(8.9549,27.782,7.8674);
			}glEnd();

			drawTexture(large_marble,
				8.836,28.1654,5.972,
				8.9549,27.782,7.8674,
				8.7677,27.9475,8.183,
               8.7677,27.9475,8.183
               );

               			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.9549,27.782,7.8674);
				glVertex3f(8.7677,27.9475,8.183);
			}glEnd();
			//=========================================================== col-1

			drawTexture(large_marble,
				9.2724,27.1032,5.9688,
				9.5686,26.3782,6.5527,
				9.6,26.5286,7.2393,
				9.2962,27.2124,6.3711
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.2724,27.1032,5.9688);
				glVertex3f(9.5686,26.3782,6.5527);
				glVertex3f(9.6,26.5286,7.2393);
				glVertex3f(9.2962,27.2124,6.3711);
			}glEnd();

			drawTexture(large_marble,
				9.5686,26.3782,6.5527,
				9.8729,25.627,7.0852,
				9.9038,25.8013,8.0559,
				9.6,26.5286,7.2393
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.5686,26.3782,6.5527);
				glVertex3f(9.8729,25.627,7.0852);
				glVertex3f(9.9038,25.8013,8.0559);
				glVertex3f(9.6,26.5286,7.2393);
			}glEnd();

			drawTexture(large_marble,
				9.8729,25.627,7.0852,
				10.1848,24.8524,7.5657,
				10.2075,25.0337,8.8185,
				9.9038,25.8013,8.0559
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.8729,25.627,7.0852);
				glVertex3f(10.1848,24.8524,7.5657);
				glVertex3f(10.2075,25.0337,8.8185);
				glVertex3f(9.9038,25.8013,8.0559);
			}glEnd();

			drawTexture(large_marble,
				10.1848,24.8524,7.5657,
				10.504,24.057,7.997,
				10.511,24.229,9.5253,
				10.2075,25.0337,8.8185
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.1848,24.8524,7.5657);
				glVertex3f(10.504,24.057,7.997);
				glVertex3f(10.511,24.229,9.5253);
				glVertex3f(10.2075,25.0337,8.8185);
			}glEnd();

			drawTexture(large_marble,
				10.504,24.057,7.997,
				10.8299,23.2435,8.3693,
				10.8141,23.3904,10.1747,
				10.511,24.229,9.5253
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.504,24.057,7.997);
				glVertex3f(10.8299,23.2435,8.3693);
				glVertex3f(10.8141,23.3904,10.1747);
				glVertex3f(10.511,24.229,9.5253);
			}glEnd();

			drawTexture(large_marble,
				10.8299,23.2435,8.3693,
				11.1622,22.4144,8.6925,
				11.1169,22.5212,10.7656,
				10.8141,23.3904,10.1747
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.8299,23.2435,8.3693);
				glVertex3f(11.1622,22.4144,8.6925);
				glVertex3f(11.1169,22.5212,10.7656);
				glVertex3f(10.8141,23.3904,10.1747);
			}glEnd();

			drawTexture(large_marble,
				11.1622,22.4144,8.6925,
				11.8432,20.7198,9.1831,
				11.6955,20.9065,11.4967,
				11.1169,22.5212,10.7656
               );
               			glBegin(GL_QUADS);{
				glVertex3f(11.1622,22.4144,8.6925);
				glVertex3f(11.8432,20.7198,9.1831);
				glVertex3f(11.6955,20.9065,11.4967);
				glVertex3f(11.1169,22.5212,10.7656);
			}glEnd();

			drawTexture(large_marble,
				11.8432,20.7198,9.1831,
				12.1907,19.859,9.3519,
				12.002,19.9787,11.9224,
				11.6955,20.9065,11.4967
               );
               			glBegin(GL_QUADS);{
				glVertex3f(11.8432,20.7198,9.1831);
				glVertex3f(12.1907,19.859,9.3519);
				glVertex3f(12.002,19.9787,11.9224);
				glVertex3f(11.6955,20.9065,11.4967);
			}glEnd();

			drawTexture(large_marble,
				12.1907,19.859,9.3519,
				12.542,18.9925,9.4706,
				12.307,19.0323,12.2887,
				12.002,19.9787,11.9224
               );
               			glBegin(GL_QUADS);{
				glVertex3f(12.1907,19.859,9.3519);
				glVertex3f(12.542,18.9925,9.4706);
				glVertex3f(12.307,19.0323,12.2887);
				glVertex3f(12.002,19.9787,11.9224);
			}glEnd();

			drawTexture(large_marble,
				12.542,18.9925,9.4706,
				12.8942,18.1269,9.5471,
				12.6104,18.0703,12.5959,
				12.307,19.0323,12.2887
               );
               			glBegin(GL_QUADS);{
				glVertex3f(12.542,18.9925,9.4706);
				glVertex3f(12.8942,18.1269,9.5471);
				glVertex3f(12.6104,18.0703,12.5959);
				glVertex3f(12.307,19.0323,12.2887);
			}glEnd();

			drawTexture(large_marble,
				12.8942,18.1269,9.5471,
				13.2529,17.2509,9.5625,
				12.9117,17.0959,12.8444,
				12.6104,18.0703,12.5959
               );
               			glBegin(GL_QUADS);{
				glVertex3f(12.8942,18.1269,9.5471);
				glVertex3f(13.2529,17.2509,9.5625);
				glVertex3f(12.9117,17.0959,12.8444);
				glVertex3f(12.6104,18.0703,12.5959);
			}glEnd();
			//=========================================================col=2

			drawTexture(large_marble,
				9.2962,27.2124,6.3711,
				9.6,26.5286,7.2393,
				9.555,26.7015,7.9193,
				9.2756,27.3368,6.7684
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.2962,27.2124,6.3711);
				glVertex3f(9.6,26.5286,7.2393);
				glVertex3f(9.555,26.7015,7.9193);
				glVertex3f(9.2756,27.3368,6.7684);
			}glEnd();

			drawTexture(large_marble,
				9.6,26.5286,7.2393,
				9.9038,25.8013,8.0559,
				9.8254,26.0031,9.0185,
				9.555,26.7015,7.9193
               );
               			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(9.6,26.5286,7.2393);
				glVertex3f(9.9038,25.8013,8.0559);
				glVertex3f(9.8254,26.0031,9.0185);
				glVertex3f(9.555,26.7015,7.9193);
			}glEnd();

			drawTexture(large_marble,
				9.9038,25.8013,8.0559,
				10.2075,25.0337,8.8185,
				10.0869,25.245,10.0615,
				9.8254,26.0031,9.0185
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.9038,25.8013,8.0559);
				glVertex3f(10.2075,25.0337,8.8185);
				glVertex3f(10.0869,25.245,10.0615);
				glVertex3f(9.8254,26.0031,9.0185);
			}glEnd();

			drawTexture(large_marble,
				10.2075,25.0337,8.8185,
				10.511,24.229,9.5253,
				10.3398,24.4311,11.0445,
				10.0869,25.245,10.0615
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.2075,25.0337,8.8185);
				glVertex3f(10.511,24.229,9.5253);
				glVertex3f(10.3398,24.4311,11.0445);
				glVertex3f(10.0869,25.245,10.0615);
			}glEnd();

			drawTexture(large_marble,
				10.511,24.229,9.5253,
				10.8141,23.3904,10.1747,
				10.5845,23.5652,11.9642,
				10.3398,24.4311,11.0445
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.511,24.229,9.5253);
				glVertex3f(10.8141,23.3904,10.1747);
				glVertex3f(10.5845,23.5652,11.9642);
				glVertex3f(10.3398,24.4311,11.0445);
			}glEnd();

			drawTexture(large_marble,
				10.8141,23.3904,10.1747,
				11.1169,22.5212,10.7656,
				10.8213,22.6514,12.8175,
				10.5845,23.5652,11.9642
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.8141,23.3904,10.1747);
				glVertex3f(11.1169,22.5212,10.7656);
				glVertex3f(10.8213,22.6514,12.8175);
				glVertex3f(10.5845,23.5652,11.9642);
			}glEnd();

			drawTexture(large_marble,
				11.1169,22.5212,10.7656,
				11.6955,20.9065,11.4967,
				11.2626,21.1152,13.775,
				10.8213,22.6514,12.8175
               );
               			glBegin(GL_QUADS);{
				glVertex3f(11.1169,22.5212,10.7656);
				glVertex3f(11.6955,20.9065,11.4967);
				glVertex3f(11.2626,21.1152,13.775);
				glVertex3f(10.8213,22.6514,12.8175);
			}glEnd();

			drawTexture(large_marble,
				11.6955,20.9065,11.4967,
				12.002,19.9787,11.9224,
				11.4897,20.1122,14.45,
				11.2626,21.1152,13.775
               );
               			glBegin(GL_QUADS);{
				glVertex3f(11.6955,20.9065,11.4967);
				glVertex3f(12.002,19.9787,11.9224);
				glVertex3f(11.4897,20.1122,14.45);
				glVertex3f(11.2626,21.1152,13.775);
			}glEnd();

			drawTexture(large_marble,
                12.002,19.9787,11.9224,
				12.307,19.0323,12.2887,
				11.7098,19.076,15.0536,
				11.4897,20.1122,14.45
               );
               			glBegin(GL_QUADS);{
				glVertex3f(12.002,19.9787,11.9224);
				glVertex3f(12.307,19.0323,12.2887);
				glVertex3f(11.7098,19.076,15.0536);
				glVertex3f(11.4897,20.1122,14.45);
			}glEnd();

			drawTexture(large_marble,
				12.307,19.0323,12.2887,
				12.6104,18.0703,12.5959,
				11.9232,18.0109,15.5851,
				11.7098,19.076,15.0536
               );
               			glBegin(GL_QUADS);{
				glVertex3f(12.307,19.0323,12.2887);
				glVertex3f(12.6104,18.0703,12.5959);
				glVertex3f(11.9232,18.0109,15.5851);
				glVertex3f(11.7098,19.076,15.0536);
			}glEnd();

			drawTexture(large_marble,
				12.6104,18.0703,12.5959,
				12.9117,17.0959,12.8444,
				12.13,16.9209,16.0443,
				11.9232,18.0109,15.5851
               );
               			glBegin(GL_QUADS);{
				glVertex3f(12.6104,18.0703,12.5959);
				glVertex3f(12.9117,17.0959,12.8444);
				glVertex3f(12.13,16.9209,16.0443);
				glVertex3f(11.9232,18.0109,15.5851);
			}glEnd();
			//==============================================================col=3

			drawTexture(large_marble,
				9.2756,27.3368,6.7684,
				9.555,26.7015,7.9193,
				9.4345,26.8946,8.5818,
				9.211,27.4746,7.1543
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.2756,27.3368,6.7684);
				glVertex3f(9.555,26.7015,7.9193);
				glVertex3f(9.4345,26.8946,8.5818);
				glVertex3f(9.211,27.4746,7.1543);
			}glEnd();

			drawTexture(large_marble,
				9.555,26.7015,7.9193,
				9.8254,26.0031,9.0185,
				9.6391,26.2297,9.9574,
				9.4345,26.8946,8.5818
               );
               			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(9.555,26.7015,7.9193);
				glVertex3f(9.8254,26.0031,9.0185);
				glVertex3f(9.6391,26.2297,9.9574);
				glVertex3f(9.4345,26.8946,8.5818);
			}glEnd();

			drawTexture(large_marble,
				9.8254,26.0031,9.0185,
				10.0869,25.245,10.0615,
				9.8253,25.484,11.2743,
				9.6391,26.2297,9.9574
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.8254,26.0031,9.0185);
				glVertex3f(10.0869,25.245,10.0615);
				glVertex3f(9.8253,25.484,11.2743);
				glVertex3f(9.6391,26.2297,9.9574);
			}glEnd();

			drawTexture(large_marble,
				10.0869,25.245,10.0615,
				10.3398,24.4311,11.0445,
				9.9938,24.6616,12.5263,
				9.8253,25.484,11.2743
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.0869,25.245,10.0615);
				glVertex3f(10.3398,24.4311,11.0445);
				glVertex3f(9.9938,24.6616,12.5263);
				glVertex3f(9.8253,25.484,11.2743);
			}glEnd();

			drawTexture(large_marble,
				10.3398,24.4311,11.0445,
				10.5845,23.5652,11.9642,
				10.1455,23.7673,13.7076,
				9.9938,24.6616,12.5263
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.3398,24.4311,11.0445);
				glVertex3f(10.5845,23.5652,11.9642);
				glVertex3f(10.1455,23.7673,13.7076);
				glVertex3f(9.9938,24.6616,12.5263);
			}glEnd();

			drawTexture(large_marble,
				10.5845,23.5652,11.9642,
				10.8213,22.6514,12.8175,
				10.2813,22.806,14.8135,
				10.1455,23.7673,13.7076
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.5845,23.5652,11.9642);
				glVertex3f(10.8213,22.6514,12.8175);
				glVertex3f(10.2813,22.806,14.8135);
				glVertex3f(10.1455,23.7673,13.7076);
			}glEnd();

			drawTexture(large_marble,
				10.8213,22.6514,12.8175,
				11.2626,21.1152,13.775,
				10.553,21.3477,15.9786,
				10.2813,22.806,14.8135
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.8213,22.6514,12.8175);
				glVertex3f(11.2626,21.1152,13.775);
				glVertex3f(10.553,21.3477,15.9786);
				glVertex3f(10.2813,22.806,14.8135);
			}glEnd();

			drawTexture(large_marble,
				11.2626,21.1152,13.775,
				11.4897,20.1122,14.45,
				10.6644,20.2639,16.8902,
				10.553,21.3477,15.9786
               );
               			glBegin(GL_QUADS);{
				glVertex3f(11.2626,21.1152,13.775);
				glVertex3f(11.4897,20.1122,14.45);
				glVertex3f(10.6644,20.2639,16.8902);
				glVertex3f(10.553,21.3477,15.9786);
			}glEnd();

			drawTexture(large_marble,
				11.4897,20.1122,14.45,
				11.7098,19.076,15.0536,
				10.7628,19.1312,17.716,
				10.6644,20.2639,16.8902
               );
               			glBegin(GL_QUADS);{
				glVertex3f(11.4897,20.1122,14.45);
				glVertex3f(11.7098,19.076,15.0536);
				glVertex3f(10.7628,19.1312,17.716);
				glVertex3f(10.6644,20.2639,16.8902);
			}glEnd();

			drawTexture(large_marble,
				11.7098,19.076,15.0536,
				11.9232,18.0109,15.5851,
				10.8493,17.9551,18.454,
				10.7628,19.1312,17.716
               );
               			glBegin(GL_QUADS);{
				glVertex3f(11.7098,19.076,15.0536);
				glVertex3f(11.9232,18.0109,15.5851);
				glVertex3f(10.8493,17.9551,18.454);
				glVertex3f(10.7628,19.1312,17.716);
			}glEnd();

			drawTexture(large_marble,
				11.9232,18.0109,15.5851,
				12.13,16.9209,16.0443,
				10.9246,16.741,19.1031,
				10.8493,17.9551,18.454
               );
               			glBegin(GL_QUADS);{
				glVertex3f(11.9232,18.0109,15.5851);
				glVertex3f(12.13,16.9209,16.0443);
				glVertex3f(10.9246,16.741,19.1031);
				glVertex3f(10.8493,17.9551,18.454);
			}glEnd();
			//==============================================================col=4

			drawTexture(large_marble,
				9.211,27.4746,7.1543,
				9.4345,26.8946,8.5818,
				9.2406,27.1053,9.2159,
				9.1035,27.6237,7.5226
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.211,27.4746,7.1543);
				glVertex3f(9.4345,26.8946,8.5818);
				glVertex3f(9.2406,27.1053,9.2159);
				glVertex3f(9.1035,27.6237,7.5226);

			}glEnd();

			drawTexture(large_marble,
				9.4345,26.8946,8.5818,
				9.6391,26.2297,9.9574,
				9.3482,26.4788,10.8572,
				9.2406,27.1053,9.2159
               );
               			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(9.4345,26.8946,8.5818);
				glVertex3f(9.6391,26.2297,9.9574);
				glVertex3f(9.3482,26.4788,10.8572);
				glVertex3f(9.2406,27.1053,9.2159);
			}glEnd();

			drawTexture(large_marble,
				9.6391,26.2297,9.9574,
				9.8253,25.484,11.2743,
				9.4275,25.7486,12.4368,
				9.3482,26.4788,10.8572
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.6391,26.2297,9.9574);
				glVertex3f(9.8253,25.484,11.2743);
				glVertex3f(9.4275,25.7486,12.4368);
				glVertex3f(9.3482,26.4788,10.8572);
			}glEnd();

			drawTexture(large_marble,
				9.8253,25.484,11.2743,
				9.9938,24.6616,12.526,
				9.4795,24.9196,13.9457,
				9.4275,25.7486,12.4368
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.8253,25.484,11.2743);
				glVertex3f(9.9938,24.6616,12.5263);
				glVertex3f(9.4795,24.9196,13.9457);
				glVertex3f(9.4275,25.7486,12.4368);
			}glEnd();

			drawTexture(large_marble,
				9.9938,24.6616,12.5263,
				10.1455,23.7673,13.7076,
				9.5056,23.9974,15.3758,
				9.4795,24.9196,13.9457
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.9938,24.6616,12.5263);
				glVertex3f(10.1455,23.7673,13.7076);
				glVertex3f(9.5056,23.9974,15.3758);
				glVertex3f(9.4795,24.9196,13.9457);
			}glEnd();

			drawTexture(large_marble,
				10.1455,23.7673,13.7076,
				10.2813,22.806,14.8135,
				9.5074,22.9882,16.7195,
				9.5056,23.9974,15.3758
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.1455,23.7673,13.7076);
				glVertex3f(10.2813,22.806,14.8135);
				glVertex3f(9.5074,22.9882,16.7195);
				glVertex3f(9.5056,23.9974,15.3758);
			}glEnd();

			drawTexture(large_marble,
				10.2813,22.806,14.8135,
				10.553,21.3477,15.9786,
				9.5811,21.6083,18.0696,
				9.5074,22.9882,16.7195
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.2813,22.806,14.8135);
				glVertex3f(10.553,21.3477,15.9786);
				glVertex3f(9.5811,21.6083,18.0696);
				glVertex3f(9.5074,22.9882,16.7195);
			}glEnd();

			drawTexture(large_marble,
               				10.553,21.3477,15.9786,
				10.6644,20.2639,16.8902,
				9.5429,20.4415,19.2007,
				9.5811,21.6083,18.0696

               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.553,21.3477,15.9786);
				glVertex3f(10.6644,20.2639,16.8902);
				glVertex3f(9.5429,20.4415,19.2007);
				glVertex3f(9.5811,21.6083,18.0696);
			}glEnd();

			drawTexture(large_marble,
				10.6644,20.2639,16.8902,
				10.7628,19.1312,17.716,
				9.4857,19.2094,20.2293,
				9.5429,20.4415,19.2007
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.6644,20.2639,16.8902);
				glVertex3f(10.7628,19.1312,17.716);
				glVertex3f(9.4857,19.2094,20.2293);
				glVertex3f(9.5429,20.4415,19.2007);
			}glEnd();

			drawTexture(large_marble,
			10.7628,19.1312,17.716,
				10.8493,17.9551,18.454,
				9.4113,17.9191,21.1521,
				9.4857,19.2094,20.2293
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.7628,19.1312,17.716);
				glVertex3f(10.8493,17.9551,18.454);
				glVertex3f(9.4113,17.9191,21.1521);
				glVertex3f(9.4857,19.2094,20.2293);
			}glEnd();

			drawTexture(large_marble,
				10.8493,17.9551,18.454,
				10.9246,16.741,19.1031,
				9.3215,16.5778,21.9667,
				9.4113,17.9191,21.1521
               );
               			glBegin(GL_QUADS);{
				glVertex3f(10.8493,17.9551,18.454);
				glVertex3f(10.9246,16.741,19.1031);
				glVertex3f(9.3215,16.5778,21.9667);
				glVertex3f(9.4113,17.9191,21.1521);
			}glEnd();
			//==============================================================col=5

			drawTexture(large_marble,
				9.1035,27.6237,7.5226,
				9.2406,27.1053,9.2159,
				8.9766,27.3311,9.8114,
				8.9549,27.782,7.8674
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.1035,27.6237,7.5226);
				glVertex3f(9.2406,27.1053,9.2159);
				glVertex3f(8.9766,27.3311,9.8114);
				glVertex3f(8.9549,27.782,7.8674);
			}glEnd();

			drawTexture(large_marble,
				9.2406,27.1053,9.2159,
				9.3482,26.4788,10.8572,
				8.958,26.748,11.7031,
				8.9766,27.3311,9.8114
               );
               			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(9.2406,27.1053,9.2159);
				glVertex3f(9.3482,26.4788,10.8572);
				glVertex3f(8.958,26.748,11.7031);
				glVertex3f(8.9766,27.3311,9.8114);
			}glEnd();

			drawTexture(large_marble,
				9.3482,26.4788,10.8572,
				9.4275,25.7486,12.4368,
				8.9007,26.0374,13.5297,
				8.958,26.748,11.7031
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.3482,26.4788,10.8572);
				glVertex3f(9.4275,25.7486,12.4368);
				glVertex3f(8.9007,26.0374,13.5297);
				glVertex3f(8.958,26.748,11.7031);
			}glEnd();

			drawTexture(large_marble,
				9.4275,25.7486,12.4368,
				9.4795,24.9196,13.9457,
				8.8064,25.2051,15.2792,
				8.9007,26.0374,13.5297
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.4275,25.7486,12.4368);
				glVertex3f(9.4795,24.9196,13.9457);
				glVertex3f(8.8064,25.2051,15.2792);
				glVertex3f(8.9007,26.0374,13.5297);
			}glEnd();

			drawTexture(large_marble,
				9.4795,24.9196,13.9457,
				9.5056,23.9974,15.3758,
				8.6769,24.2577,16.9405,
				8.8064,25.2051,15.2792
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.4795,24.9196,13.9457);
				glVertex3f(9.5056,23.9974,15.3758);
				glVertex3f(8.6769,24.2577,16.9405);
				glVertex3f(8.8064,25.2051,15.2792);
			}glEnd();

			drawTexture(large_marble,
				9.5056,23.9974,15.3758,
				9.5074,22.9882,16.7195,
				8.5147,23.2026,18.5035,
				8.6769,24.2577,16.9405
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.5056,23.9974,15.3758);
				glVertex3f(9.5074,22.9882,16.7195);
				glVertex3f(8.5147,23.2026,18.5035);
				glVertex3f(8.6769,24.2577,16.9405);
			}glEnd();

			drawTexture(large_marble,
				9.5074,22.9882,16.7195,
				9.5811,21.6083,18.0696,
				8.3661,21.9034,20.0121,
				8.5147,23.2026,18.5035
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.5074,22.9882,16.7195);
				glVertex3f(9.5811,21.6083,18.0696);
				glVertex3f(8.3661,21.9034,20.0121);
				glVertex3f(8.5147,23.2026,18.5035);
			}glEnd();

			drawTexture(large_marble,
				9.5811,21.6083,18.0696,
				9.5429,20.4415,19.2007,
				8.1478,20.6554,21.342,
				8.3661,21.9034,20.0121
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.5811,21.6083,18.0696);
				glVertex3f(9.5429,20.4415,19.2007);
				glVertex3f(8.1478,20.6554,21.342);
				glVertex3f(8.3661,21.9034,20.0121);
			}glEnd();

			drawTexture(large_marble,
				9.5429,20.4415,19.2007,
				9.4857,19.2094,20.2293,
				7.9046,19.3257,22.551,
				8.1478,20.6554,21.342
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.5429,20.4415,19.2007);
				glVertex3f(9.4857,19.2094,20.2293);
				glVertex3f(7.9046,19.3257,22.551);
				glVertex3f(8.1478,20.6554,21.342);
			}glEnd();

			drawTexture(large_marble,
				9.4857,19.2094,20.2293,
				9.4113,17.9191,21.1521,
				7.6393,17.9231,23.6341,
				7.9046,19.3257,22.551
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.4857,19.2094,20.2293);
				glVertex3f(9.4113,17.9191,21.1521);
				glVertex3f(7.6393,17.9231,23.6341);
				glVertex3f(7.9046,19.3257,22.551);
			}glEnd();

			drawTexture(large_marble,
				9.4113,17.9191,21.1521,
				9.3215,16.5778,21.9667,
				7.3546,16.4568,24.5877,
				7.6393,17.9231,23.6341
               );
               			glBegin(GL_QUADS);{
				glVertex3f(9.4113,17.9191,21.1521);
				glVertex3f(9.3215,16.5778,21.9667);
				glVertex3f(7.3546,16.4568,24.5877);
				glVertex3f(7.6393,17.9231,23.6341);
			}glEnd();
			//==============================================================col=6

			drawTexture(large_marble,
				8.9549,27.782,7.8674,
				8.9766,27.3311,9.8114,
				8.647,27.5694,10.3582,
				8.7677,27.9475,8.183
               );
               			glBegin(GL_QUADS);{
				glVertex3f(8.9549,27.782,7.8674);
				glVertex3f(8.9766,27.3311,9.8114);
				glVertex3f(8.647,27.5694,10.3582);
				glVertex3f(8.7677,27.9475,8.183);
			}glEnd();

			drawTexture(large_marble,
				8.9766,27.3311,9.8114,
				8.958,26.748,11.7031,
				8.4754,27.0348,12.4809,
				8.647,27.5694,10.3582
               );
               			glBegin(GL_QUADS);{
				//2->1 / 3->4 && prev 3->2 i.e. delete 2
				glVertex3f(8.9766,27.3311,9.8114);
				glVertex3f(8.958,26.748,11.7031);
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.647,27.5694,10.3582);
			}glEnd();

			drawTexture(large_marble,
			8.958,26.748,11.7031,
				8.9007,26.0374,13.5297,
				8.2546,26.3489,14.5346,
				8.4754,27.0348,12.4809
               );
               			glBegin(GL_QUADS);{
				glVertex3f(8.958,26.748,11.7031);
				glVertex3f(8.9007,26.0374,13.5297);
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(8.4754,27.0348,12.4809);
			}glEnd();

			drawTexture(large_marble,
				8.9007,26.0374,13.5297,
				8.8064,25.2051,15.2792,
				7.987,25.5183,16.5042,
				8.2546,26.3489,14.5346
               );
               			glBegin(GL_QUADS);{
				glVertex3f(8.9007,26.0374,13.5297);
				glVertex3f(8.8064,25.2051,15.2792);
				glVertex3f(7.987,25.5183,16.5042);
				glVertex3f(8.2546,26.3489,14.5346);
			}glEnd();

			drawTexture(large_marble,
				8.8064,25.2051,15.2792,
				8.6769,24.2577,16.9405,
				7.6752,24.5508,18.3755,
				7.987,25.5183,16.5042
               );
               			glBegin(GL_QUADS);{
				glVertex3f(8.8064,25.2051,15.2792);
				glVertex3f(8.6769,24.2577,16.9405);
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.987,25.5183,16.5042);
			}glEnd();

			drawTexture(large_marble,
				8.6769,24.2577,16.9405,
				8.5147,23.2026,18.5035,
				7.3223,23.4549,20.1358,
				7.6752,24.5508,18.3755
               );
               			glBegin(GL_QUADS);{
				glVertex3f(8.6769,24.2577,16.9405);
				glVertex3f(8.5147,23.2026,18.5035);
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(7.6752,24.5508,18.3755);
			}glEnd();

			drawTexture(large_marble,
				8.5147,23.2026,18.5035,
				8.3661,21.9034,20.0121,
				6.9316,22.2404,21.7735,
				7.3223,23.4549,20.1358
               );
               			glBegin(GL_QUADS);{
				glVertex3f(8.5147,23.2026,18.5035);
				glVertex3f(8.3661,21.9034,20.0121);
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(7.3223,23.4549,20.1358);
			}glEnd();

			drawTexture(large_marble,
				8.3661,21.9034,20.0121,
				8.1478,20.6554,21.342,
				6.5067,20.9174,23.2788,
				6.9316,22.2404,21.7735
               );
               			glBegin(GL_QUADS);{
				glVertex3f(8.3661,21.9034,20.0121);
				glVertex3f(8.1478,20.6554,21.342);
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(6.9316,22.2404,21.7735);
			}glEnd();

			drawTexture(large_marble,
				8.1478,20.6554,21.342,
				7.9046,19.3257,22.551,
				6.0514,19.4966,24.6435,
				6.5067,20.9174,23.2788
               );
               			glBegin(GL_QUADS);{
				glVertex3f(8.1478,20.6554,21.342);
				glVertex3f(7.9046,19.3257,22.551);
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(6.5067,20.9174,23.2788);
			}glEnd();

			drawTexture(large_marble,
				7.9046,19.3257,22.551,
				7.6393,17.9231,23.6341,
				5.5694,17.9891,25.8609,
				6.0514,19.4966,24.6435
               );
               			glBegin(GL_QUADS);{
				glVertex3f(7.9046,19.3257,22.551);
				glVertex3f(7.6393,17.9231,23.6341);
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(6.0514,19.4966,24.6435);
			}glEnd();

			drawTexture(large_marble,
				7.6393,17.9231,23.6341,
				7.3546,16.4568,24.5877,
				5.0646,16.4061,26.9262,
				5.5694,17.9891,25.8609
               );
               			glBegin(GL_QUADS);{
				glVertex3f(7.6393,17.9231,23.6341);
				glVertex3f(7.3546,16.4568,24.5877);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.5694,17.9891,25.8609);
			}glEnd();
			//==============================================================col=7

		}glPopMatrix();
	}
}
void drawPetal_2_leftface_midsquare()
{
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(-0.4616,21.5103,7.2216);
				glVertex3f(-0.4432,21.4529,9.1098);
				glVertex3f(-0.1562,21.3485,8.8746);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.4432,21.4529,9.1098);
				glVertex3f(-0.3796,21.3899,9.8859);
				glVertex3f(0.3113,21.1332,10.0332);
				glVertex3f(-0.1562,21.3485,8.8746);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.3796,21.3899,9.8859);
				glVertex3f(-0.1562,21.1852,11.7266);
				glVertex3f(1.7951,20.5125,11.9807);
				glVertex3f(0.3113,21.1332,10.0332);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.1562,21.1852,11.7266);
				glVertex3f(0.0798,20.9864,13.0263);
				glVertex3f(3.8515,19.7466,13.2329);
				glVertex3f(1.7951,20.5125,11.9807);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.0798,20.9864,13.0263);
				glVertex3f(0.2196,20.8686,13.626);
				glVertex3f(6.0217,19.0358,13.626);
				glVertex3f(3.8515,19.7466,13.2329);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.2196,20.8686,13.626);
				glVertex3f(0.4273,20.6695,14.64);
				glVertex3f(5.9796,18.92,14.6701);
				glVertex3f(6.0217,19.0358,13.626);
			}glEnd();

			//--------------------------------------------------------
}
void drawPetal_2_rightface_midsquare()
{
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(12.8215,17.4492,7.2349);
				glVertex3f(12.4726,17.4869,8.9036);
				glVertex3f(12.7899,17.4212,8.7476);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.4726,17.4869,8.9036);
				glVertex3f(11.9586,17.5711,10.0706);
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(12.7899,17.4212,8.7476);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.9586,17.5711,10.0706);
				glVertex3f(11.2482,17.7051,11.1214);
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(12.6088,17.3965,10.2298);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.2482,17.7051,11.1214);
				glVertex3f(10.678,17.8227,11.74);
				glVertex3f(12.3719,17.3566,11.74);
				glVertex3f(12.5402,17.3729,10.9394);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.678,17.8227,11.74);
				glVertex3f(8.6061,18.3093,13.1093);
				glVertex3f(12.0465,17.3254,13.0833);
				glVertex3f(12.3719,17.3566,11.74);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.6061,18.3093,13.1093);
				glVertex3f(6.0217,19.0358,13.626);
				glVertex3f(11.8872,17.3103,13.626);
				glVertex3f(12.0465,17.3254,13.0833);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.0217,19.0358,13.626);
				glVertex3f(5.9796,18.92,14.6701);
				glVertex3f(11.5222,17.2863,14.7155);
				glVertex3f(11.8872,17.3103,13.626);
			}glEnd();

			//--------------------------------------------------------
}

void drawPetal_2_face_bottom_circle()
{
    glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		glVertex3f(-0.4435,21.5295,5.9534);
		glVertex3f(-0.4616,21.5103,7.2216);
		glVertex3f(-0.1047,21.3941,7.4439);
		glVertex3f(-0.133,21.4346,5.9534);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-0.4616,21.5103,7.2216);
        glVertex3f(-0.1562,21.3485,8.8746);
        glVertex3f(-0.2886,21.1977,9.1408);
		glVertex3f(-0.1047,21.3941,7.4439);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(-0.1562,21.3485,8.8746);
            glVertex3f(0.5101,21.0455,10.3963);
            glVertex3f(1.4766,20.687,11.1623);

		glVertex3f(-0.2886,21.1977,9.1408);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(0.5101,21.0455,10.3963);
                glVertex3f(2.1041,20.3911,12.2433);
            glVertex3f(2.9412,20.1182,12.4056);

		glVertex3f(1.4766,20.687,11.1623);
	}glEnd();

	glBegin(GL_QUADS);{
		   glVertex3f(2.1041,20.3911,12.2433);
                glVertex3f(4.2328,19.6147,13.361);
                glVertex3f(5.0805,19.3751,13.2009);

		glVertex3f(2.9412,20.1182,12.4056);
	}glEnd();

	glBegin(GL_QUADS);{
		      glVertex3f(4.2328,19.6147,13.361);

                glVertex3f(6.0217,19.0358,13.626);
                glVertex3f(6.6125,18.9004,13.2582);

		     glVertex3f(5.0805,19.3751,13.2009);
	}glEnd();

	glBegin(GL_QUADS);{
		        glVertex3f(6.0217,19.0358,13.626);

                glVertex3f(7.6325,18.568,13.4316);
                glVertex3f(8.1253,18.477,12.9287);

		   glVertex3f(6.6125,18.9004,13.2582);
	}glEnd();

	glBegin(GL_QUADS);{
		        glVertex3f(7.6325,18.568,13.4316);

                    glVertex3f(8.7085,18.2835,13.0622);
                    glVertex3f(9.1927,18.2055,12.4373);

		   glVertex3f(8.1253,18.477,12.9287);
	}glEnd();

	glBegin(GL_QUADS);{
		            glVertex3f(8.7085,18.2835,13.0622);

                        glVertex3f(10.0407,17.9628,12.2825);
                        glVertex3f(10.4658,17.9123,11.4754);

		   glVertex3f(9.1927,18.2055,12.4373);
	}glEnd();

	glBegin(GL_QUADS);{
		                glVertex3f(10.0407,17.9628,12.2825);
        glVertex3f(11.1102,17.7331,11.2805);
        glVertex3f(11.2414,17.7521,10.5793);


		              glVertex3f(10.4658,17.9123,11.4754);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(11.1102,17.7331,11.2805);

            glVertex3f(11.7425,17.6102,10.4356);
            glVertex3f(11.8556,17.6383,9.5042);

		glVertex3f(11.2414,17.7521,10.5793);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(11.7425,17.6102,10.4356);

            glVertex3f(12.4726,17.4869,8.9036);
            glVertex3f(12.3715,17.56,8.0086);

		glVertex3f(11.8556,17.6383,9.5042);
	}glEnd();

	glBegin(GL_QUADS);{
	        glVertex3f(12.4726,17.4869,8.9036);

                glVertex3f(12.8215,17.4492,7.2349);
                glVertex3f(12.5006,17.553,7.0164);

		glVertex3f(12.3715,17.56,8.0086);
	}glEnd();

	glBegin(GL_QUADS);{
	           glVertex3f(12.8215,17.4492,7.2349);

                    glVertex3f(12.8184,17.475,5.9536);
                    glVertex3f(12.5078,17.5699,5.9535);

		glVertex3f(12.5006,17.553,7.0164);
	}glEnd();


}


void drawPetal_2_leftface_upper_glass()
{

}

void drawPetal_2_face_transparent()
{
	glEnable( GL_BLEND ); glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA ); glColor4f(1,1,1,TRANSPARENCY_FACTOR); glDepthMask(0);
	//glColor3f(1.0, 0.0, 1.0);
	glBegin(GL_QUADS);{
		glVertex3f(0.4723,20.6695,14.64);
		glVertex3f(1.4338,19.9352,17.4426);
		glVertex3f(1.5051,20.3324,14.64);
		glVertex3f(1.5051,20.3324,14.64);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(1.5051,20.3324,14.64);
		glVertex3f(1.4338,19.9352,17.4426);
		glVertex3f(2.5352,19.5851,17.4505);
		glVertex3f(2.5352,19.5851,14.64);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(2.5352,19.5851,14.64);
		glVertex3f(2.5352,19.5851,17.4505);
		glVertex3f(3.6366,19.2349,17.4585);
		glVertex3f(3.6366,19.2349,14.64);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(3.6366,19.2349,14.64);
		glVertex3f(3.6366,19.2349,17.4585);
		glVertex3f(4.7379,18.8847,17.4665);
		glVertex3f(4.7379,18.8847,14.64);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(4.7379,18.8847,14.64);
		glVertex3f(4.7379,18.8847,17.4665);
		glVertex3f(5.8393,18.5346,17.4744);
		glVertex3f(5.8393,18.5346,14.64);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(5.8393,18.5346,14.64);
		glVertex3f(5.8393,18.5346,17.4744);
		glVertex3f(6.9477,18.2074,17.4864);
		glVertex3f(6.9477,18.2074,14.64);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(6.9477,18.2074,14.64);
		glVertex3f(6.9477,18.2074,17.4864);
		glVertex3f(8.0561,17.8803,17.4984);
		glVertex3f(8.0561,17.8803,14.64);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(8.0561,17.8803,14.64);
		glVertex3f(8.0561,17.8803,17.4984);
		glVertex3f(9.1645,17.5531,17.5104);
		glVertex3f(9.1645,17.5531,14.64);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(9.1645,17.5531,14.64);
		glVertex3f(9.1645,17.5531,17.5104);
		glVertex3f(10.2728,17.226,17.5224);
		glVertex3f(10.2728,17.226,14.64);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(10.2728,17.226,14.64);
		glVertex3f(10.2728,17.226,17.5224);
		glVertex3f(11.5222,17.863,14.7155);
		glVertex3f(11.5222,17.863,14.7155);
	}glEnd();
	//=============================================== row-2
	glBegin(GL_QUADS);{
		glVertex3f(1.4338,19.9352,17.4426);
		glVertex3f(2.4082,19.2361,19.4869);
		glVertex3f(2.4082,19.2361,19.4869);
		glVertex3f(2.5352,19.5851,17.4505);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(2.5352,19.5851,17.4505);
		glVertex3f(2.4082,19.2361,19.4869);
		glVertex3f(3.6366,19.2349,19.4869);
		glVertex3f(3.6366,19.2349,17.4585);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(3.6366,19.2349,17.4585);
		glVertex3f(3.6366,19.2349,19.4869);
		glVertex3f(4.7379,18.8847,19.4869);
		glVertex3f(4.7379,18.8847,17.4665);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(4.7379,18.8847,17.4665);
		glVertex3f(4.7379,18.8847,19.4869);
		glVertex3f(5.8393,18.5346,19.4869);
		glVertex3f(5.8393,18.5346,17.4744);
	}glEnd();
	/*
	glBegin(GL_QUADS);{
		glVertex3f(5.8393,18.5346,17.4744);
		glVertex3f(5.8393,18.5346,19.4869);

		glVertex3f(6.9477,18.2074,14.64);
		glVertex3f(6.9477,18.2074,17.4864);
	}glEnd();
	*/
	glBegin(GL_QUADS);{
		glVertex3f(5.8393,18.5346,17.4744);
		glVertex3f(5.8393,18.5346,19.4869);
		glVertex3f(8.0561,17.8803,19.4869);
		glVertex3f(8.0561,17.8803,17.4984);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(8.0561,17.8803,17.4984);
		glVertex3f(8.0561,17.8803,19.4869);
		glVertex3f(9.1645,17.5531,19.4869);
		glVertex3f(9.1645,17.5531,17.5104);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(9.1645,17.5531,17.5104);
		glVertex3f(9.1645,17.5531,19.4869);
		glVertex3f(9.1645,17.5531,19.4869);
		glVertex3f(10.2728,17.226,17.5224);
	}glEnd();

	//=============================================== row-3
	glBegin(GL_QUADS);{
		glVertex3f(2.4082,19.2361,19.4869);
		glVertex3f(3.3911,18.5604,21.142);
		glVertex3f(3.3911,18.5604,21.142);
		glVertex3f(3.6366,19.2349,19.4869);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(3.6366,19.2349,19.4869);
		glVertex3f(3.3911,18.5604,21.142);
		glVertex3f(4.7379,18.8847,21.142);
		glVertex3f(4.7379,18.8847,19.4869);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(4.7379,18.8847,19.4869);
		glVertex3f(4.7379,18.8847,21.142);
		glVertex3f(5.8393,18.5346,21.142);
		glVertex3f(5.8393,18.5346,19.4869);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(5.8393,18.5346,19.4869);
		glVertex3f(5.8393,18.5346,21.142);
		glVertex3f(8.0561,17.8803,21.142);
		glVertex3f(8.0561,17.8803,19.4869);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(8.0561,17.8803,19.4869);
		glVertex3f(8.0561,17.8803,21.142);
		glVertex3f(8.0561,17.8803,21.142);
		glVertex3f(9.1645,17.5531,19.4869);
	}glEnd();
	//=============================================== row-4
	glBegin(GL_QUADS);{
		glVertex3f(3.3911,18.5604,21.142);
		glVertex3f(4.3924,17.8982,22.5534);
		glVertex3f(4.3924,17.8982,22.5534);
		glVertex3f(4.7379,18.8847,21.142);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(4.7379,18.8847,21.142);
		glVertex3f(4.3924,17.8982,22.5534);
		//glVertex3f(5.8393,17.5512/*18.5346*/,22.5534);
		glVertex3f(5.4814,17.5512/*18.5346*/,22.5648);
		//glVertex3f(5.8393,18.5346,21.142);
		glVertex3f(5.5929,17.8576,21.1706);
	}glEnd();

	glBegin(GL_QUADS);{
		//glVertex3f(5.8393,18.5346,21.142);
		//glVertex3f(5.8393,18.5346,22.5534);
		glVertex3f(5.5929,17.8576,21.1706);
		//glVertex3f(5.4814,17.5512,22.5648);
		glVertex3f(5.4814,17.5512,22.5648);
		//glVertex3f(5.8393,18.5346,22.5534);
		glVertex3f(6.5772,17.2266,22.5819);
		//glVertex3f(8.0561,17.8803,21.142);
		glVertex3f(7.8092,17.2019,21.2023);
	}glEnd();
	glDepthMask(1); glColor4f(0,0,0,1); glDisable (GL_BLEND);
	//=============================================== row-5
	glColor3f(1.0, 0.0, 1.0);
	glBegin(GL_QUADS);{
		glVertex3f(4.3185,17.9055,22.5385);
		glVertex3f(5.381,17.2755,23.7211);
		glVertex3f(5.381,17.2755,23.7211);
		glVertex3f(5.4814,17.5512,22.5648);
	}glEnd();

	glBegin(GL_QUADS);{
		glVertex3f(5.4814,17.5512,22.5648);
		glVertex3f(5.381,17.2755,23.7211);
		glVertex3f(5.381,17.2755,23.7211);
		glVertex3f(6.5772,17.2266,22.5819);
	}glEnd();
	//=============================================== row-6
}

void drawPetal_2_back_lower_triads()
{
	glColor3f(.2,.2,.2);
	glBegin(GL_TRIANGLES);{
		glVertex3f(-0.4393,21.4974,7.4448);
		glVertex3f(0.6258,22.6364,9.4382);
		glVertex3f(-0.1549,22.1135,9.5041);
	}glEnd();

	glBegin(GL_TRIANGLES);{
		glVertex3f(12.8511,17.448,7.3571);
		glVertex3f(13.0744,17.6866,9.5547);
		glVertex3f(12.8942,18.1269,9.5471);
	}glEnd();
}


void drawPetal_2()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			drawPetal_2_leftface();
			drawPetal_2_rightface();
			drawPetal_2_rightback();
			drawPetal_2_leftback();
			drawPetal_2_leftface_midsquare();
			drawPetal_2_leftface_upper_glass();//NOT IMPLEMENTED YET
			drawPetal_2_rightface_midsquare();
			drawPetal_2_face_bottom_circle();//NOT IMPLEMENTED YET
            drawPetal_2_face_transparent();
		}glPopMatrix();
	}
}
void drawPetal_1_rightback()
{
	//--------------------------column: 0
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.8403,-4.7016,13.284);
		glVertex3f(-18.8268,-4.4563,15.4193);
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-19.705,-5.2887,13.2596);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.8268,-4.4563,15.4193);
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-17.75626,-4.8517,17.2742);
		glVertex3f(-18.752,-5.0751,15.2765);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-16.6489,-3.9292,19.6101);
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-17.75626,-4.8517,17.2742);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.6489,-3.9292,19.6101);
		glVertex3f(-15.4835,-3.6575,21.663);
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-16.7053,-4.5867,19.2118);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.4835,-3.6575,21.663);
		glVertex3f(-14.2731,-3.3588,23.6861);
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-15.6159,-4.3117,21.1207);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.2731,-3.3588,23.6861);
		glVertex3f(-13.0141,-3.0524,25.678);
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-14.4784,-4.0245,23.0102);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.0141,-3.0524,25.678);
		glVertex3f(-11.7048,-2.7327,27.6427);
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-13.2929,-3.7261,24.8814);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-11.7048,-2.7327,27.6427);
		glVertex3f(-10.3556,-2.4062,29.5636);
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-12.0666,-3.4195,26.7173);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.3556,-2.4062,29.5636);
		glVertex3f(-8.9574,-2.0678,31.455);
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-10.7881,-3.0938,28.5454);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-8.9574,-2.0678,31.455);
		glVertex3f(-7.5147,-1.7186,33.3106);
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-9.4796,-2.7649,30.3253);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-7.5147,-1.7186,33.3106);
		glVertex3f(-6.0282,-1.3589,35.1293);
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-8.1315,-2.4264,32.0735);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-6.0282,-1.3589,35.1293);
		glVertex3f(-4.4989,-0.9888,36.9101);
		glVertex3f(-5.3194,-1.7214,35.4705);
		glVertex3f(-6.7445,-2.0785,33.7888);
	}glEnd();
	//--------------------------column: 1
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.705,-5.2887,13.2596);
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-18.583,-6.6951,14.5753);
		glVertex3f(-19.3978,-6.6308,12.9811);

	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-17.7613,-6.5654,16.2535);
		glVertex3f(-18.583,-6.6951,14.5753);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-16.926,-6.3416,17.7525);
		glVertex3f(-17.7613,-6.5654,16.2535);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-16.0612,-6.145,19.2105);
		glVertex3f(-16.926,-6.3416,17.7525);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-15.1335,-5.8648,20.7555);
		glVertex3f(-16.0612,-6.145,19.2105);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-14.1947,-5.6165,22.2283);
		glVertex3f(-15.1335,-5.8648,20.7555);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-13.1743,-5.3434,23.7565);
		glVertex3f(-14.1947,-5.6165,22.2283);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-12.1293,-5.065,25.2601);
		glVertex3f(-13.1743,-5.3434,23.7565);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-11.0822,-4.7881,26.706);
		glVertex3f(-12.1293,-5.065,25.2601);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-10.0079,-4.5048,28.1302);
		glVertex3f(-11.0822,-4.7881,26.706);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-8.907,-4.2149,29.5321);
		glVertex3f(-10.0079,-4.5048,28.1302);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-5.3194,-1.7214,35.4705);
		glVertex3f(-7.78,-3.9188,30.9111);
		glVertex3f(-8.907,-4.2149,29.5321);
	}glEnd();
	//--------------------------column: 2
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.3978,-6.6308,12.9811);
		glVertex3f(-18.583,-6.6951,14.5753);
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-19.1521,-7.715,12.5178);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.583,-6.6951,14.5753);
		glVertex3f(-17.7613,-6.5654,16.2535);
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-18.4077,-8.1401,13.6087);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7613,-6.5654,16.2535);
		glVertex3f(-16.926,-6.3416,17.7525);
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-17.7381,-8.053,15.0332);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.926,-6.3416,17.7525);
		glVertex3f(-16.0612,-6.145,19.2105);
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-17.0562,-7.8517,16.2322);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.0612,-6.145,19.2105);
		glVertex3f(-15.1335,-5.8648,20.7555);
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-16.347,-7.6192,17.4575);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.1335,-5.8648,20.7555);
		glVertex3f(-14.1947,-5.6165,22.2283);
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-15.5995,-7.3933,18.6788);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.1947,-5.6165,22.2283);
		glVertex3f(-13.1743,-5.3434,23.7565);
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-14.8388,-7.1544,19.8907);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1743,-5.3434,23.7565);
		glVertex3f(-12.1293,-5.065,25.2601);
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-13.9911,-6.8918,21.18);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.1293,-5.065,25.2601);
		glVertex3f(-11.0822,-4.7881,26.706);
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-13.1265,-6.6248,22.4544);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-11.0822,-4.7881,26.706);
		glVertex3f(-10.0079,-4.5048,28.1302);
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-12.2784,-6.3665,23.6616);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.0079,-4.5048,28.1302);
		glVertex3f(-8.907,-4.2149,29.5321);
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-11.4115,-6.1036,24.8542);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-8.907,-4.2149,29.5321);
		glVertex3f(-7.78,-3.9188,30.9111);
		glVertex3f(-9.6231,-5.5648,27.1936);
		glVertex3f(-10.5263,-5.8363,26.0317);
	}glEnd();
	//--------------------------column: 3
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.1521,-7.715,12.5178);
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-18.8067,-9.2603,11.3508);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-18.3082,-9.4045,12.211);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-17.8175,-9.3855,13.2702);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-17.3158,-9.2325,14.1256);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-16.8041,-9.0877,14.9583);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-16.2556,-8.9184,15.8423);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-15.7049,-8.7466,16.7121);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-15.0636,-8.5511,17.6805);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-14.4293,-8.3589,18.6155);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-13.8152,-8.1749,19.4974);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-13.1905,-7.9883,20.3711);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-9.6231,-5.5648,27.1936);
		glVertex3f(-12.0487,-7.7309,21.7581);
		glVertex3f(-12.5557,-7.7992,21.2364);
	}glEnd();
	//--------------------------column: 4
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.8067,-9.2603,11.3508);
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-18.6771,-9.8512,10.6575);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-18.2568,-9.9142,11.4855);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-17.8288,-9.9066,12.4349);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-17.3864,-9.7639,13.1925);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.935,-9.62,13.9435);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-16.4663,-9.5006,14.6469);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-16.0035,-9.3536,15.3746);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-15.4121,-9.1349,16.3398);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-14.8458,-8.9545,17.19);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-14.308,-8.7849,17.9786);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-13.7621,-8.6134,18.7611);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-12.0487,-7.7309,21.7581);
		glVertex3f(-12.7191,-8.3296,20.1298);
		glVertex3f(-13.2081,-8.44,19.537);
	}glEnd();
	//--------------------------column: 5
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.6771,-9.8512,10.6575);
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-18.5843,-10.2815,9.9958);

	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-18.2294,-10.3563,10.6732);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.8686,-10.3674,11.4891);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-17.4943,-10.2515,12.1212);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-17.1136,-10.1341,12.7497);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-16.706,-10.0082,13.4056);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-16.3133,-9.8879,14.0259);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-15.8234,-9.7356,14.7801);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-15.3416,-9.5866,15.5088);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-14.8888,-9.4478,16.1802);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-14.4301,-9.3077,16.8473);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-12.7191,-8.3296,20.1298);
		glVertex3f(-13.5314,-9.0551,18.0755);
		glVertex3f(-13.9657,-9.1661,17.5097);
	}glEnd();
	//--------------------------column: 6
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.5843,-10.2815,9.9958);
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-18.499,-10.6862,9.1601);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-18.2285,-10.7262,9.7639);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.9422,-10.7662,10.3985);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.6456,-10.6937,10.8742);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-17.3451,-10.6202,11.3479);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-17.0227,-10.5409,11.8462);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-16.7151,-10.4657,12.3146);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-16.3229,-10.368,12.8998);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-15.9397,-10.2727,13.464);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-15.5839,-10.1849,13.9798);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-15.2246,-10.0962,14.4929);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-13.5314,-9.0551,18.0755);
		glVertex3f(-14.4958,-9.9163,15.5107);
		glVertex3f(-14.8619,-10.0066,15.0032);
	}glEnd();
	//--------------------------column: 7
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.499,-10.6862,9.1601);
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.4483,-10.9382,8.3984);

	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-18.2434,-11.0054,8.8216);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-18.0265,-11.0762,9.2643);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.8032,-11.0507,9.5898);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.5778,-11.0248,9.9141);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-17.3354,-10.9963,10.2582);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-17.1066,-10.9695,10.5796);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-16.8084,-10.9331,10.9924);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-16.5188,-10.8976,11.3896);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-16.2534,-10.8652,11.7498);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-15.9862,-10.8324,12.1085);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-14.4958,-9.9163,15.5107);
		glVertex3f(-15.4185,-10.7403,12.9037);
		glVertex3f(-15.7172,-10.7991,12.4659);
	}glEnd();
	//--------------------------column: 8
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.4483,-10.9382,8.3984);
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.4167,-11.1796,7.1859);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.307,-11.3658,7.4421);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-18.1473,-11.3509,7.6847);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-18.0166,-11.3953,7.7537);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-17.8899,-11.4432,7.8332);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-17.7586,-11.4995,7.9146);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.6367,-11.5514,8.0093);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.4818,-11.6289,8.1123);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.3365,-11.7067,8.2133);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-17.2203,-11.7896,8.2613);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-17.1235,-11.8889,8.2483);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-15.4185,-10.7403,12.9037);
		glVertex3f(-16.8775,-12.0432,8.4274);
		glVertex3f(-16.991,-11.9575,8.3693);
	}glEnd();
	//--------------------------column: 9
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.4167,-11.1796,7.1859);
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.3861,-11.3106,5.972);
		glVertex3f(-18.4215,-11.1571,5.9751);

	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.3534,-11.4654,5.972);
		glVertex3f(-18.3861,-11.3106,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-18.2775,-11.5914,5.972);
		glVertex3f(-18.3534,-11.4654,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-18.22,-11.6869,5.972);
		glVertex3f(-18.2775,-11.5914,5.972);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-18.1503,-11.8028,5.972);
		glVertex3f(-18.22,-11.6869,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-18.0861,-11.9093,5.972);
		glVertex3f(-18.1503,-11.8028,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.9975,-12.0564,5.972);
		glVertex3f(-18.0861,-11.9093,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.9127,-12.1973,5.972);
		glVertex3f(-17.9975,-12.0564,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.8373,-12.3225,5.972);
		glVertex3f(-17.9127,-12.1973,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-17.7619,-12.4478,5.972);
		glVertex3f(-17.8373,-12.3225,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-17.6864,-12.573,5.972);
		glVertex3f(-17.7619,-12.4478,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-16.8775,-12.0432,8.4274);
		glVertex3f(-17.611,-12.6983,5.972);
		glVertex3f(-17.6864,-12.573,5.972);
	}glEnd();
	//--------------------END
}
void drawPetal_1_rightback_texture()
{
	//--------------------------column: 0
	{


		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0); glVertex3f(-18.8268,-4.4563,15.4193);
			glTexCoord2f(1,0); glVertex3f(-17.7431,-4.1941,17.5674);
			glTexCoord2f(1,1); glVertex3f(-17.75626,-4.8517,17.2742);
			glTexCoord2f(0,1); glVertex3f(-18.752,-5.0751,15.2765);
		glEnd();
		glDisable(GL_TEXTURE_2D);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-17.7431,-4.1941,17.5674);
			glTexCoord2f(1,0);
			glVertex3f(-16.6489,-3.9292,19.6101);
			glTexCoord2f(1,1);
			glVertex3f(-16.7053,-4.5867,19.2118);
			glTexCoord2f(0,1);
			glVertex3f(-17.75626,-4.8517,17.2742);
		glEnd();
		glDisable(GL_TEXTURE_2D);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-16.6489,-3.9292,19.6101);
			glTexCoord2f(1,0);
			glVertex3f(-15.4835,-3.6575,21.663);
			glTexCoord2f(1,1);
			glVertex3f(-15.6159,-4.3117,21.1207);
			glTexCoord2f(0,1);
			glVertex3f(-16.7053,-4.5867,19.2118);
		glEnd();
		glDisable(GL_TEXTURE_2D);

			glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-15.4835,-3.6575,21.663);
			glTexCoord2f(1,0);
			glVertex3f(-14.2731,-3.3588,23.6861);
			glTexCoord2f(1,1);
			glVertex3f(-14.4784,-4.0245,23.0102);
			glTexCoord2f(0,1);
			glVertex3f(-15.6159,-4.3117,21.1207);
		glEnd();
		glDisable(GL_TEXTURE_2D);

				glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-14.2731,-3.3588,23.6861);
			glTexCoord2f(1,0);
			glVertex3f(-13.0141,-3.0524,25.678);
			glTexCoord2f(1,1);
			glVertex3f(-13.2929,-3.7261,24.8814);
			glTexCoord2f(0,1);
			glVertex3f(-14.4784,-4.0245,23.0102);
		glEnd();
		glDisable(GL_TEXTURE_2D);

					glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-13.0141,-3.0524,25.678);
			glTexCoord2f(1,0);
			glVertex3f(-11.7048,-2.7327,27.6427);
			glTexCoord2f(1,1);
			glVertex3f(-12.0666,-3.4195,26.7173);
			glTexCoord2f(0,1);
			glVertex3f(-13.2929,-3.7261,24.8814);
		glEnd();
		glDisable(GL_TEXTURE_2D);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-11.7048,-2.7327,27.6427);
			glTexCoord2f(1,0);
			glVertex3f(-10.3556,-2.4062,29.5636);
			glTexCoord2f(1,1);
			glVertex3f(-10.7881,-3.0938,28.5454);
			glTexCoord2f(0,1);
			glVertex3f(-12.0666,-3.4195,26.7173);
		glEnd();
		glDisable(GL_TEXTURE_2D);

			glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-10.3556,-2.4062,29.5636);
			glTexCoord2f(1,0);
			glVertex3f(-8.9574,-2.0678,31.455);
			glTexCoord2f(1,1);
			glVertex3f(-9.4796,-2.7649,30.3253);
			glTexCoord2f(0,1);
			glVertex3f(-10.7881,-3.0938,28.5454);
		glEnd();
		glDisable(GL_TEXTURE_2D);

				glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-8.9574,-2.0678,31.455);
			glTexCoord2f(1,0);
			glVertex3f(-7.5147,-1.7186,33.3106);
			glTexCoord2f(1,1);
			glVertex3f(-8.1315,-2.4264,32.0735);
			glTexCoord2f(0,1);
			glVertex3f(-9.4796,-2.7649,30.3253);
		glEnd();
		glDisable(GL_TEXTURE_2D);

					glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-7.5147,-1.7186,33.3106);
			glTexCoord2f(1,0);
			glVertex3f(-6.0282,-1.3589,35.1293);
			glTexCoord2f(1,1);
			glVertex3f(-6.7445,-2.0785,33.7888);
			glTexCoord2f(0,1);
			glVertex3f(-8.1315,-2.4264,32.0735);
		glEnd();
		glDisable(GL_TEXTURE_2D);

		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,texid1);
		glNormal3f(1.0,0.0,0.0);
		glBegin(GL_POLYGON);
			glTexCoord2f(0,0);
			glVertex3f(-6.0282,-1.3589,35.1293);
			glTexCoord2f(1,0);
			glVertex3f(-4.4989,-0.9888,36.9101);
			glTexCoord2f(1,1);
			glVertex3f(-5.3194,-1.7214,35.4705);
			glTexCoord2f(0,1);
			glVertex3f(-6.7445,-2.0785,33.7888);
		glEnd();
		glDisable(GL_TEXTURE_2D);
	}

	//--------------------------column: 1
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.705,-5.2887,13.2596);
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-18.583,-6.6951,14.5753);
		glVertex3f(-19.3978,-6.6308,12.9811);

	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.752,-5.0751,15.2765);
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-17.7613,-6.5654,16.2535);
		glVertex3f(-18.583,-6.6951,14.5753);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7431,-4.1941,17.5674);
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-16.926,-6.3416,17.7525);
		glVertex3f(-17.7613,-6.5654,16.2535);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.7053,-4.5867,19.2118);
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-16.0612,-6.145,19.2105);
		glVertex3f(-16.926,-6.3416,17.7525);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.6159,-4.3117,21.1207);
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-15.1335,-5.8648,20.7555);
		glVertex3f(-16.0612,-6.145,19.2105);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4784,-4.0245,23.0102);
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-14.1947,-5.6165,22.2283);
		glVertex3f(-15.1335,-5.8648,20.7555);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.2929,-3.7261,24.8814);
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-13.1743,-5.3434,23.7565);
		glVertex3f(-14.1947,-5.6165,22.2283);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.0666,-3.4195,26.7173);
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-12.1293,-5.065,25.2601);
		glVertex3f(-13.1743,-5.3434,23.7565);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.7881,-3.0938,28.5454);
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-11.0822,-4.7881,26.706);
		glVertex3f(-12.1293,-5.065,25.2601);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-9.4796,-2.7649,30.3253);
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-10.0079,-4.5048,28.1302);
		glVertex3f(-11.0822,-4.7881,26.706);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-8.1315,-2.4264,32.0735);
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-8.907,-4.2149,29.5321);
		glVertex3f(-10.0079,-4.5048,28.1302);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-6.7445,-2.0785,33.7888);
		glVertex3f(-5.3194,-1.7214,35.4705);
		glVertex3f(-7.78,-3.9188,30.9111);
		glVertex3f(-8.907,-4.2149,29.5321);
	}glEnd();
	//--------------------------column: 2
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-19.3978,-6.6308,12.9821);
		glTexCoord2f(1,0);
		glVertex3f(-18.583,-6.6951,14.5763);
		glTexCoord2f(1,1);
		glVertex3f(-18.4077,-8.1401,13.6097);
		glTexCoord2f(0,1);
		glVertex3f(-19.1521,-7.715,12.5188);
	glEnd();
	glDisable(GL_TEXTURE_2D);


		glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-18.583,-6.6951,14.5763);
		glTexCoord2f(1,0);
		glVertex3f(-17.7613,-6.5654,16.2545);
		glTexCoord2f(1,1);
		glVertex3f(-17.7381,-8.053,15.0342);
		glTexCoord2f(0,1);
		glVertex3f(-18.4077,-8.1401,13.6097);
	glEnd();
	glDisable(GL_TEXTURE_2D);

			glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-17.7613,-6.5654,16.2545);
		glTexCoord2f(1,0);
		glVertex3f(-16.926,-6.3416,17.7535);
		glTexCoord2f(1,1);
		glVertex3f(-17.0562,-7.8517,16.2332);
		glTexCoord2f(0,1);
		glVertex3f(-17.7381,-8.053,15.0342);
	glEnd();
	glDisable(GL_TEXTURE_2D);

				glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-16.926,-6.3416,17.7535);
		glTexCoord2f(1,0);
		glVertex3f(-16.0612,-6.145,19.2115);
		glTexCoord2f(1,1);
		glVertex3f(-16.347,-7.6192,17.4585);
		glTexCoord2f(0,1);
		glVertex3f(-17.0562,-7.8517,16.2332);
	glEnd();
	glDisable(GL_TEXTURE_2D);

				glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-16.0612,-6.145,19.2115);
		glTexCoord2f(1,0);
		glVertex3f(-15.1335,-5.8648,20.7565);
		glTexCoord2f(1,1);
		glVertex3f(-15.5995,-7.3933,18.6798);
		glTexCoord2f(0,1);
		glVertex3f(-16.347,-7.6192,17.4585);
	glEnd();
	glDisable(GL_TEXTURE_2D);

				glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-15.1335,-5.8648,20.7565);
		glTexCoord2f(1,0);
		glVertex3f(-14.1947,-5.6165,22.2293);
		glTexCoord2f(1,1);
		glVertex3f(-14.8388,-7.1544,19.8917);
		glTexCoord2f(0,1);
		glVertex3f(-15.5995,-7.3933,18.6798);
	glEnd();
	glDisable(GL_TEXTURE_2D);

				glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-14.1947,-5.6165,22.2293);
		glTexCoord2f(1,0);
		glVertex3f(-13.1743,-5.3434,23.7575);
		glTexCoord2f(1,1);
		glVertex3f(-13.9911,-6.8918,21.181);
		glTexCoord2f(0,1);
		glVertex3f(-14.8388,-7.1544,19.8917);
	glEnd();
	glDisable(GL_TEXTURE_2D);

					glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-13.1743,-5.3434,23.7575);
		glTexCoord2f(1,0);
		glVertex3f(-12.1293,-5.065,25.2611);
		glTexCoord2f(1,1);
		glVertex3f(-13.1265,-6.6248,22.4554);
		glTexCoord2f(0,1);
		glVertex3f(-13.9911,-6.8918,21.181);
	glEnd();
	glDisable(GL_TEXTURE_2D);

						glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-12.1293,-5.065,25.2611);
		glTexCoord2f(1,0);
		glVertex3f(-11.0822,-4.7881,26.707);
		glTexCoord2f(1,1);
		glVertex3f(-12.2784,-6.3665,23.6626);
		glTexCoord2f(0,1);
		glVertex3f(-13.1265,-6.6248,22.4554);
	glEnd();
	glDisable(GL_TEXTURE_2D);

							glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-11.0822,-4.7881,26.707);
		glTexCoord2f(1,0);
		glVertex3f(-10.0079,-4.5048,28.1312);
		glTexCoord2f(1,1);
		glVertex3f(-11.4115,-6.1036,24.8552);
		glTexCoord2f(0,1);
		glVertex3f(-12.2784,-6.3665,23.6626);
	glEnd();
	glDisable(GL_TEXTURE_2D);

								glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-10.0079,-4.5048,28.1312);
		glTexCoord2f(1,0);
		glVertex3f(-8.907,-4.2149,29.5331);
		glTexCoord2f(1,1);
		glVertex3f(-10.5263,-5.8363,26.0327);
		glTexCoord2f(0,1);
		glVertex3f(-11.4115,-6.1036,24.8552);
	glEnd();
	glDisable(GL_TEXTURE_2D);

									glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,texid1);
	glNormal3f(1.0,0.0,0.0);
	glBegin(GL_POLYGON);
		glTexCoord2f(0,0);
		glVertex3f(-8.907,-4.2149,29.5331);
		glTexCoord2f(1,0);
		glVertex3f(-7.78,-3.9188,30.9121);
		glTexCoord2f(1,1);
		glVertex3f(-9.6231,-5.5648,27.1946);
		glTexCoord2f(0,1);
		glVertex3f(-10.5263,-5.8363,26.0327);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	//--------------------------column: 3
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-19.1521,-7.715,12.5178);
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-18.8067,-9.2603,11.3508);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.4077,-8.1401,13.6087);
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-18.3082,-9.4045,12.211);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.7381,-8.053,15.0332);
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-17.8175,-9.3855,13.2702);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.0562,-7.8517,16.2322);
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-17.3158,-9.2325,14.1256);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.347,-7.6192,17.4575);
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-16.8041,-9.0877,14.9583);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.5995,-7.3933,18.6788);
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-16.2556,-8.9184,15.8423);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8388,-7.1544,19.8907);
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-15.7049,-8.7466,16.7121);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.9911,-6.8918,21.18);
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-15.0636,-8.5511,17.6805);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1265,-6.6248,22.4544);
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-14.4293,-8.3589,18.6155);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.2784,-6.3665,23.6616);
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-13.8152,-8.1749,19.4974);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-11.4115,-6.1036,24.8542);
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-13.1905,-7.9883,20.3711);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-10.5263,-5.8363,26.0317);
		glVertex3f(-9.6231,-5.5648,27.1936);
		glVertex3f(-12.0487,-7.7309,21.7581);
		glVertex3f(-12.5557,-7.7992,21.2364);
	}glEnd();
	//--------------------------column: 4
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.8067,-9.2603,11.3508);
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-18.6771,-9.8512,10.6575);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.3082,-9.4045,12.211);
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-18.2568,-9.9142,11.4855);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8175,-9.3855,13.2702);
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-17.8288,-9.9066,12.4349);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3158,-9.2325,14.1256);
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-17.3864,-9.7639,13.1925);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.8041,-9.0877,14.9583);
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.935,-9.62,13.9435);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.2556,-8.9184,15.8423);
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-16.4663,-9.5006,14.6469);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.7049,-8.7466,16.7121);
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-16.0035,-9.3536,15.3746);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.0636,-8.5511,17.6805);
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-15.4121,-9.1349,16.3398);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4293,-8.3589,18.6155);
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-14.8458,-8.9545,17.19);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.8152,-8.1749,19.4974);
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-14.308,-8.7849,17.9786);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.1905,-7.9883,20.3711);
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-13.7621,-8.6134,18.7611);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-12.5557,-7.7992,21.2364);
		glVertex3f(-12.0487,-7.7309,21.7581);
		glVertex3f(-12.7191,-8.3296,20.1298);
		glVertex3f(-13.2081,-8.44,19.537);
	}glEnd();
	//--------------------------column: 5
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.6771,-9.8512,10.6575);
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-18.5843,-10.2815,9.9958);

	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2568,-9.9142,11.4855);
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-18.2294,-10.3563,10.6732);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8288,-9.9066,12.4349);
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.8686,-10.3674,11.4891);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3864,-9.7639,13.1925);
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-17.4943,-10.2515,12.1212);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.935,-9.62,13.9435);
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-17.1136,-10.1341,12.7497);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.4663,-9.5006,14.6469);
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-16.706,-10.0082,13.4056);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.0035,-9.3536,15.3746);
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-16.3133,-9.8879,14.0259);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.4121,-9.1349,16.3398);
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-15.8234,-9.7356,14.7801);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8458,-8.9545,17.19);
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-15.3416,-9.5866,15.5088);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.308,-8.7849,17.9786);
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-14.8888,-9.4478,16.1802);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.7621,-8.6134,18.7611);
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-14.4301,-9.3077,16.8473);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.2081,-8.44,19.537);
		glVertex3f(-12.7191,-8.3296,20.1298);
		glVertex3f(-13.5314,-9.0551,18.0755);
		glVertex3f(-13.9657,-9.1661,17.5097);
	}glEnd();
	//--------------------------column: 6
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.5843,-10.2815,9.9958);
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-18.499,-10.6862,9.1601);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2294,-10.3563,10.6732);
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-18.2285,-10.7262,9.7639);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.8686,-10.3674,11.4891);
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.9422,-10.7662,10.3985);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.4943,-10.2515,12.1212);
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.6456,-10.6937,10.8742);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1136,-10.1341,12.7497);
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-17.3451,-10.6202,11.3479);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.706,-10.0082,13.4056);
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-17.0227,-10.5409,11.8462);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.3133,-9.8879,14.0259);
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-16.7151,-10.4657,12.3146);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.8234,-9.7356,14.7801);
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-16.3229,-10.368,12.8998);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.3416,-9.5866,15.5088);
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-15.9397,-10.2727,13.464);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8888,-9.4478,16.1802);
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-15.5839,-10.1849,13.9798);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.4301,-9.3077,16.8473);
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-15.2246,-10.0962,14.4929);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-13.9657,-9.1661,17.5097);
		glVertex3f(-13.5314,-9.0551,18.0755);
		glVertex3f(-14.4958,-9.9163,15.5107);
		glVertex3f(-14.8619,-10.0066,15.0032);
	}glEnd();
	//--------------------------column: 7
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.499,-10.6862,9.1601);
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.4483,-10.9382,8.3984);

	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2285,-10.7262,9.7639);
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-18.2434,-11.0054,8.8216);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-17.9422,-10.7662,10.3985);
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-18.0265,-11.0762,9.2643);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.6456,-10.6937,10.8742);
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.8032,-11.0507,9.5898);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3451,-10.6202,11.3479);
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.5778,-11.0248,9.9141);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.0227,-10.5409,11.8462);
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-17.3354,-10.9963,10.2582);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.7151,-10.4657,12.3146);
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-17.1066,-10.9695,10.5796);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.3229,-10.368,12.8998);
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-16.8084,-10.9331,10.9924);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.9397,-10.2727,13.464);
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-16.5188,-10.8976,11.3896);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.5839,-10.1849,13.9798);
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-16.2534,-10.8652,11.7498);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.2246,-10.0962,14.4929);
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-15.9862,-10.8324,12.1085);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-14.8619,-10.0066,15.0032);
		glVertex3f(-14.4958,-9.9163,15.5107);
		glVertex3f(-15.4185,-10.7403,12.9037);
		glVertex3f(-15.7172,-10.7991,12.4659);
	}glEnd();
	//--------------------------column: 8
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.4483,-10.9382,8.3984);
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.4167,-11.1796,7.1859);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.2434,-11.0054,8.8216);
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.307,-11.3658,7.4421);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-18.0265,-11.0762,9.2643);
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-18.1473,-11.3509,7.6847);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.8032,-11.0507,9.5898);
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-18.0166,-11.3953,7.7537);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.5778,-11.0248,9.9141);
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-17.8899,-11.4432,7.8332);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3354,-10.9963,10.2582);
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-17.7586,-11.4995,7.9146);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1066,-10.9695,10.5796);
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.6367,-11.5514,8.0093);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.8084,-10.9331,10.9924);
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.4818,-11.6289,8.1123);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.5188,-10.8976,11.3896);
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.3365,-11.7067,8.2133);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.2534,-10.8652,11.7498);
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-17.2203,-11.7896,8.2613);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.9862,-10.8324,12.1085);
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-17.1235,-11.8889,8.2483);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-15.7172,-10.7991,12.4659);
		glVertex3f(-15.4185,-10.7403,12.9037);
		glVertex3f(-16.8775,-12.0432,8.4274);
		glVertex3f(-16.991,-11.9575,8.3693);
	}glEnd();
	//--------------------------column: 9
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.4167,-11.1796,7.1859);
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.3861,-11.3106,5.972);
		glVertex3f(-18.4215,-11.1571,5.9751);

	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4 && before 3->2
		glVertex3f(-18.307,-11.3658,7.4421);
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.3534,-11.4654,5.972);
		glVertex3f(-18.3861,-11.3106,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4  && before 3->2
		glVertex3f(-18.1473,-11.3509,7.6847);
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-18.2775,-11.5914,5.972);
		glVertex3f(-18.3534,-11.4654,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-18.0166,-11.3953,7.7537);
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-18.22,-11.6869,5.972);
		glVertex3f(-18.2775,-11.5914,5.972);
	}glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.8899,-11.4432,7.8332);
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-18.1503,-11.8028,5.972);
		glVertex3f(-18.22,-11.6869,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.7586,-11.4995,7.9146);
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-18.0861,-11.9093,5.972);
		glVertex3f(-18.1503,-11.8028,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.6367,-11.5514,8.0093);
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.9975,-12.0564,5.972);
		glVertex3f(-18.0861,-11.9093,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.4818,-11.6289,8.1123);
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.9127,-12.1973,5.972);
		glVertex3f(-17.9975,-12.0564,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.3365,-11.7067,8.2133);
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.8373,-12.3225,5.972);
		glVertex3f(-17.9127,-12.1973,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.2203,-11.7896,8.2613);
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-17.7619,-12.4478,5.972);
		glVertex3f(-17.8373,-12.3225,5.972);
	}glEnd();

	glColor3f(1.0, 1.0, 1.0);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-17.1235,-11.8889,8.2483);
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-17.6864,-12.573,5.972);
		glVertex3f(-17.7619,-12.4478,5.972);
	}glEnd();

	glColor3f(0.50, 0.50, 0.50);
	glBegin(GL_QUADS);{
		//2->1 :: 3->4
		glVertex3f(-16.991,-11.9575,8.3693);
		glVertex3f(-16.8775,-12.0432,8.4274);
		glVertex3f(-17.611,-12.6983,5.972);
		glVertex3f(-17.6864,-12.573,5.972);
	}glEnd();
	//--------------------END
}

void drawPetal_1_leftback()
{
    //------------------------------------------------------ cc =  1
    glColor3f(1,1,1);
    drawTexture(large_marble,
                -20.6084,-1.3898,12.9245,
                -19.9127,-1.3206,14.1111,
                -18.8968,-4.1531,15.4193,
                -19.912,-4.3762,13.6322
                );
    drawTexture(large_marble,
                -19.9127,-1.3206,14.1111,
                -19.2077,-1.0866,15.6368,
                -17.8433,-3.921,17.4991,
                -18.8968,-4.1531,15.4193
                );

    drawTexture(large_marble,
                -19.2077,-1.0866,15.6368,

                -18.4402,-0.9484,16.9685,
                -16.7082,-3.6721,19.6101,
                -17.8433,-3.921,17.4991
                );

    drawTexture(large_marble,
                -18.4402,-0.9484,16.9685,
                -17.6424,-0.8028,18.2987,
                -15.5394,-3.4152,21.663,
                -16.7082,-3.6721,19.6101
                );

    drawTexture(large_marble,
                -17.6424,-0.8028,18.2987,
                -16.8243,-0.6232,19.5966,
                -14.3219,-3.1476,23.6861,
                -15.5394,-3.4152,21.663
                );

    drawTexture(large_marble,
                -16.8243,-0.6232,19.5966,
                -15.9836,-0.4984,20.899,
                -13.0563,-2.8695,25.678,
                -14.3219,-3.1476,23.6861
                );

    drawTexture(large_marble,
                -15.9836,-0.4984,20.899,
                -15.0383,-0.3275,22.2901,
                -11.7435,-2.581,27.6375,
                -13.0563,-2.8695,25.678
                );

    drawTexture(large_marble,
                -15.0383,-0.3275,22.2901,
                -14.0462,-0.1465,23.6974,
                -10.3842,-2.2822,29.5636,
                -11.7435,-2.581,27.6375
                );
    drawTexture(large_marble,
                -14.0462,-0.1465,23.6974,
                -13.1044,0.0277,24.9859,
                -8.9792,-1.9734,31.455,
                -10.3842,-2.2822,29.5636
                );
    drawTexture(large_marble,
                -13.1044,0.0277,24.9859,
                -12.1219,0.2103,26.2813,
                -7.5294,-1.6548,33.3106,
                //,,
                //-7.5294,-1.3291,35.1155,
                -8.9792,-1.9734,31.455
                );

    drawTexture(large_marble,
                -12.1219,0.2103,26.2813,


                -11.1174,0.3977,27.5581,
                -6.0476,-1.3291,35.1155,
                -7.5294,-1.6548,33.3106
                               );
    drawTexture(large_marble,
                -11.1174,0.3977,27.5581,
                -10.0909,0.59,28.8162,

                -4.4978,-0.9877,36.9121,
                                -6.0476,-1.3291,35.1155
                );

    //------------------------------------------------------ cc =  2
    drawTexture(large_marble,
                -21.1651,0.9395,10.9907,
                -20.784,1.0212,11.4557,
                -20.6084,-1.3898,12.9245,
                -19.9127,-1.3206,14.1111
                );

    drawTexture(large_marble,
                -20.784,1.0212,11.4557,
                -20.3953,1.1812,12.4256,
                -19.2077,-1.0866,15.6368,
                -20.6084,-1.3898,12.9245
                );

    drawTexture(large_marble,
                -20.3953,1.1812,12.4256,
                -19.9245,1.2425,13.2073,
                -18.4402,-0.9484,16.9685,
                -19.2077,-1.0866,15.6368
                );

    drawTexture(large_marble,
                -19.9245,1.2425,13.2073,
                -19.4557,1.3122,13.9571,
                -17.6424,-0.8028,18.2987,
                -18.4402,-0.9484,16.9685
                );

    drawTexture(large_marble,
                -19.4557,1.3122,13.9571,
                -18.9945,1.4229,14.6243,
                -16.8243,-0.6232,19.5966,
                -17.6424,-0.8028,18.2987
                );

    drawTexture(large_marble,
                -18.9945,1.4229,14.6243,
                -18.5306,1.5066,15.314,
                -15.9836,-0.4984,20.899,
                -16.8243,-0.6232,19.5966
                );

    drawTexture(large_marble,
                -18.5306,1.5066,15.314,
                -17.8787,1.5489,16.3398,
                -15.0383,-0.3275,22.2901,
                -15.9836,-0.4984,20.899
                );

    drawTexture(large_marble,
                -17.8787,1.5489,16.3398,
                -17.2906,1.635,17.19,
                -14.0462,-0.1465,23.6974,
                -15.0383,-0.3275,22.2901
                );
    drawTexture(large_marble,
                -17.2906,1.635,17.19,
                -16.7329,1.7183,17.9786,
                -13.1044,0.0277,24.9859,
                -14.0462,-0.1465,23.6974
                );
    drawTexture(large_marble,
                -16.7329,1.7183,17.9786,
                -16.167,1.8035,18.7611,
                -12.1219,0.2103,26.2813,
                -13.1044,0.0277,24.9859
                );

    drawTexture(large_marble,
                -16.167,1.8035,18.7611,
                -15.5932,1.8905,19.537,
                -11.1174,0.3977,27.5581,
                -12.1219,0.2103,26.2813
                );
    drawTexture(large_marble,
                -15.5932,1.8905,19.537,
                -15.122,2.0104,20.0983,
                -4.4978,-0.9877,36.9121,
                -11.1174,0.3977,27.5581
                );
    //--------------------------------------------        CCC     == 3
    drawTexture(large_marble,
                -21.4046,1.9046,9.2913,
                -21.4046,1.9046,9.2913,
                -21.1651,0.9395,10.9907,
                -20.784,1.0212,11.4557
                );

    drawTexture(large_marble,
                -21.4046,1.9046,9.2913,
                -20.979,2.0376,9.8368,
                -20.3953,1.1812,12.4256,
                -21.1651,0.9395,10.9907
                );

    drawTexture(large_marble,
                -20.979,2.0376,9.8368,
                -20.7248,2.1071,10.237,
                -19.9245,1.2425,13.2073,
                -20.3953,1.1812,12.4256
                );

    drawTexture(large_marble,
                -20.7248,2.1071,10.237,
                -20.4678,2.1771,10.6356,
                -19.4557,1.3122,13.9571,
                -19.9245,1.2425,13.2073
                );

    drawTexture(large_marble,
                -20.4678,2.1771,10.6356,
                -20.1911,2.252,11.0567,
                -18.9945,1.4229,14.6243,
                -19.4557,1.3122,13.9571
                );

    drawTexture(large_marble,
                -20.1911,2.252,11.0567,
                -19.9288,2.3232,11.4513,
                -18.5306,1.5066,15.314,
                -18.9945,1.4229,14.6243
                );

    drawTexture(large_marble,
                -19.9288,2.3232,11.4513,
                -19.5899,2.4134,11.9512,
                -17.8787,1.5489,16.3398,
                -18.5306,1.5066,15.314
                );

    drawTexture(large_marble,
                -19.5899,2.4134,11.9512,
                -19.2598,2.5014,12.4327,
                -17.2906,1.635,17.19,
                -17.8787,1.5489,16.3398
                );
    drawTexture(large_marble,
                -19.2598,2.5014,12.4327,
                -18.9555,2.5829,12.8711,
                -16.7329,1.7183,17.9786,
                -17.2906,1.635,17.19
                );
    drawTexture(large_marble,
                -18.9555,2.5829,12.8711,
                -18.6485,2.6649,13.3075,
                -16.167,1.8035,18.7611,
                -16.7329,1.7183,17.9786
                );

    drawTexture(large_marble,
                -18.6485,2.6649,13.3075,
                -18.339,2.7475,13.7418,
                -15.5932,1.8905,19.537,
                -16.167,1.8035,18.7611
                );
    drawTexture(large_marble,
                -18.339,2.7475,13.7418,
                -18.027,2.8306,14.1741,
                -15.122,2.0104,20.0983,
                -15.5932,1.8905,19.537
                );
    //--------------------------------------------        CCC     == 4
    drawTexture(large_marble,
                -21.525,2.3498,7.4922,
                -21.525,2.3498,7.4922,
                -20.979,2.0376,9.8368,
                -21.4046,1.9046,9.2913
                );

    drawTexture(large_marble,
                -21.525,2.3498,7.4922,
                -21.101,2.385,8.4026,
                -20.7248,2.1071,10.237,
                -20.979,2.0376,9.8368
                );

    drawTexture(large_marble,
                -21.101,2.385,8.4026,
                -20.9573,2.4712,8.6167,
                -20.4678,2.1771,10.6356,
                -20.7248,2.1071,10.237
                );

    drawTexture(large_marble,
                -20.9573,2.4712,8.6167,
                -20.8021,2.5637,8.8454,
                -20.1911,2.252,11.0567,
                -20.4678,2.1771,10.6356
                );

    drawTexture(large_marble,
                -20.8021,2.5637,8.8454,
                -20.6569,2.6501,9.0579,
                -19.9288,2.3232,11.4513,
                -20.1911,2.252,11.0567
                );

    drawTexture(large_marble,
                -20.6569,2.6501,9.0579,
                -20.4636,2.764,9.3376,
                -19.5899,2.4134,11.9512,
                -19.9288,2.3232,11.4513
                );

    drawTexture(large_marble,
                -20.4636,2.764,9.3376,
                -20.2767,2.8738,9.6061,
                -19.2598,2.5014,12.4327,
                -19.5899,2.4134,11.9512
                );
    drawTexture(large_marble,
                -20.2767,2.8738,9.6061,
                -20.1072,2.9734,9.8479,
                -18.9555,2.5829,12.8711,
                -19.2598,2.5014,12.4327
                );
    drawTexture(large_marble,
                -20.1072,2.9734,9.8479,
                -19.9369,3.0731,10.0891,
                -18.6485,2.6649,13.3075,
                -18.9555,2.5829,12.8711
                );

    drawTexture(large_marble,
                -19.9369,3.0731,10.0891,
                -19.7658,3.1729,10.3296,
                -18.339,2.7475,13.7418,
                -18.6485,2.6649,13.3075
                );
    drawTexture(large_marble,
                -19.7658,3.1729,10.3296,
                -19.5938,3.2729,10.5694,
                -18.027,2.8306,14.1741,
                -18.339,2.7475,13.7418
                );
    //--------------------------------------------        CCC     == 5
    /*drawTexture(large_marble,
                -21.525,2.3498,7.4922,
                -21.525,2.3498,7.4922,
                -20.979,2.0376,9.8368,
                -21.4046,1.9046,9.2913
                )*/

    drawTexture(large_marble,
                -21.5431,2.3635,5.972,
                -21.5321,2.4867,5.972,
                -21.525,2.3498,7.4922,
                -21.101,2.385,8.4026
                );

    drawTexture(large_marble,
                -21.5321,2.4867,5.972,
                -21.5211,2.6117,5.972,
                -20.9573,2.4712,8.6167,
                -21.525,2.3498,7.4922
                );

    drawTexture(large_marble,
                -21.5211,2.6117,5.972,
                -21.5092,2.7464,5.972,
                -20.8021,2.5637,8.8454,
                -20.9573,2.4712,8.6167
                );

    drawTexture(large_marble,
                -21.5092,2.7464,5.972,
                -21.4983,2.8702,5.972,
                -20.6569,2.6501,9.0579,
                -20.8021,2.5637,8.8454
                );

    drawTexture(large_marble,
                -21.4983,2.8702,5.972,
                -21.4832,3.0413,5.972,
                -20.4636,2.764,9.3376,
                -20.6569,2.6501,9.0579
                );

    drawTexture(large_marble,
                -21.4832,3.0413,5.972,
                -21.4687,3.2051,5.972,
                -20.2767,2.8738,9.6061,
                -20.4636,2.764,9.3376
                );
    drawTexture(large_marble,
                -21.4687,3.2051,5.972,
                -21.4558,3.3507,5.972,
                -20.1072,2.9734,9.8479,
                -20.2767,2.8738,9.6061
                );
    drawTexture(large_marble,
                -21.4558,3.3507,5.972,
                -21.4429,3.4964,5.972,
                -19.9369,3.0731,10.0891,
                -20.1072,2.9734,9.8479
                );

    drawTexture(large_marble,
                -21.4429,3.4964,5.972,
                -21.43,3.642,5.972,
                -19.7658,3.1729,10.3296,
                -19.9369,3.0731,10.0891
                );
    drawTexture(large_marble,
                -21.43,3.642,5.972,
                -21.4171,3.7877,5.972,
                -19.5938,3.2729,10.5694,
                -19.7658,3.1729,10.3296
                );
    //--------------------------------------------        CCC     == 6

}


void drawPetal_1()
{
	//texid1 =LoadBitmap("textures/lotus_temple-1-petal-2/translucent_glass.bmp");
	//texid1 =LoadBitmap("textures/red_sandstone.bmp");
	//texid1 =LoadBitmap("E:\\CODES\\Glut\\Offline_0905060\\red_sandstone.bmp");
	//texid1 =LoadBitmap("translucent_glass.bmp");
/*
	drawTexture(texid1,
				0,0,0,
				0,0,30,
				30,0,30,
				30,0,0
				);*/

	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			drawPetal_1_rightback();
/*
				drawTexture(texid1,
				0,0,0,
				0,0,30,
				30,0,30,
				30,0,0
				);*/
				drawPetal_1_leftback();
			//drawPetal_1_rightback_texture();
		}glPopMatrix();

	}
}
void drawUpperPetals()
{
	drawInnerCylinder();
drawPetal_top();
	drawPetal_0();
	drawPetal_3();
	drawPetal_2();
	drawPetal_1();
	drawBase_1();
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



void keyboardListener(unsigned char key, int x,int y){

    switch(key)
        {
            case '1':
            cam.yaw(-delta);
            break; /// lookleft
        case '2':
            cam.yaw(delta);
            break; /// lookright
        case '3':
            cam.pitch(-delta);
            break; /// lookup
        case '4':
            cam.pitch(delta);
            break; /// lookdown
        case '5':
            cam.roll(delta);
            break; /// twistleft
        case '6':
            cam.roll(-delta);
            break; /// twistright
        case '9':
			dr = 1,dg=1,db=1;//set diffuse light to white
			break;
		case '0':
			dr = 0,dg=0,db=1;//set diffuse light to blue
			break;
        case 'a':
		    cam.slide(0,delta,0);
		    break; /// up
		case 's':
		    cam.slide(0,-delta,0);
		    break; /// down
		case '7':
			diff_ang+=diff_delta;
			dfx = diff_rad*cos(PI*diff_ang/180.);
			dfy = diff_rad*sin(PI*diff_ang/180.);
			break;
		case '8':
			diff_ang-=diff_delta;
			dfx = diff_rad*cos(PI*diff_ang/180.);
			dfy = diff_rad*sin(PI*diff_ang/180.);
			break;
					case 'i':
                sz+=10;
			break;
				case 'j':
                sz-=10;
			break;
            case 'k':
                ax=0.3;ay=0.3;az=0.3;
			break;
				case 'l':
                ax=1.0;ay=1.0;az=1.0;
			break;


			}
}

void specialKeyListener(int key, int x,int y){
	switch(key)
	{
		case GLUT_KEY_UP:
		    cam.slide(0,0,-delta);
		    //cameraAngle+=0.5;
		    break; /// forward
		case GLUT_KEY_DOWN:
		    cam.slide(0,0,delta);
		    //cameraAngle-=0.5;
		    break; /// backward
		case GLUT_KEY_LEFT:
		    cam.slide(-delta,0,0);
		    break; /// left
		case GLUT_KEY_RIGHT:
		    cam.slide(delta,0,0);
		    break; /// right
		case GLUT_KEY_PAGE_UP:
		    cam.slide(0,delta,0);
		    break; /// up
		case GLUT_KEY_PAGE_DOWN:
		    cam.slide(0,-delta,0);
		    break; /// down
	}
	glutPostRedisplay();
}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				drawaxes=1-drawaxes;
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}

void initCamera()
{
    glViewport(0,0,1200,700);
    Vector3 up(0,0,1);
    Point3 eye(100.0f,10.0f,150.0f);
    Point3 look(0.0f,0.0f,0.0f);
    cam.set(eye,look,up);
    cam.setShape(30.0f,64.0f/48.0f,0.5f,500.0f);
}

void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	//glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	//glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?

	//gluLookAt(100,100,100,	0,0,0,	0,0,1);
	//gluLookAt(100*cos(cameraAngle), 100*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	//gluLookAt(0,-1,150,	0,0,0,	0,0,1);


	//again select MODEL-VIEW
	//glMatrixMode(GL_MODELVIEW);


	/****************************
	/ Add your objects from here
	****************************/
	//add objects

	drawAxes();
	drawGrid();
	glColor3f(1,0,0);





set_ambient_color(ax,ay,az);
set_diffuse_source(dfx,dfy,dfz,dr,dg,db);

//	set_spot_light(100,100,50,0,-1,-1);

	set_spot_light(sx,sy,sz,0,0,-1);



    drawUpperPetals();
    //ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)

    glFlush();
	glutSwapBuffers();
}

void animate(){
	angle+=0.05;
	//codes for any changes in Models, Camera
	glutPostRedisplay();
}

void init(){

	//codes for initialization
	drawgrid=0;
	drawaxes=1;
	cameraHeight=100.0;
	cameraAngle=1.0;
	angle=0;

	//clear the screen
	glClearColor(0,0,0,0);

	///texture
		///sidewall1 = LoadTexture("E:\\CODES\\Glut\\Offline_0905060\\translucent_glass.bmp");
func();

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(80,	1,	1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){

calculate_points();
	glutInit(&argc,argv);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");


	init();
	initRendering();//init lights
    initCamera();//init camera

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
