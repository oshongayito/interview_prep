#include<iostream>
#include<vector>
#include<utility>
#include<map>
#define ll long long
using namespace std;
vector<vector<ll> > tr;
vector<ll> v;
ll n,k,m;
void visit(ll u)
{
    v[u]=1;
    for(ll i=0;i<tr[u].size();i++)
    {
        if(v[tr[u][i]]==0)
        {
            visit(tr[u][i]);
        }
    }
}
ll dfs()
{
    for(ll i=1;i<=n;i++)
    {
        v[i]=0;
    }
    ll cnt=0;
    for(ll i=1;i<=n;i++)
    {
        if(v[i]==0)
        {
            cnt++;
            visit(i);
        }
    }
    return cnt;
}
int main()
{
    ll i,j,a,b;
    pair<ll,ll> arr;
    //map<pair<ll,ll>,ll > mp;
    while(cin>>n>>k>>m)
    {
        tr.clear();
        tr.resize(n+1);
        v.clear();
        v.resize(n+1);
        for(i=1;i<=k;i++)
        {
            cin>>a>>b;
            tr[a].push_back(b);
            tr[b].push_back(a);
        }
        ll x=dfs();
        for(i=1;i<=m;i++) cin>>a>>b;
        cout<<x-1<<endl;
    }
    return 0;
}
