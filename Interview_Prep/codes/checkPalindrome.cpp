#include<iostream>

using namespace std;
string reverse_string(string str)
{
    string r;
    for(int i=str.size()-1;i>=0;i--)
    {

        r+=str[i];
    }
    return r;
}


bool is_palindrome(string str)
{
    string r;
    r=reverse_string(str);

    if(str==r)return true;
    else return false;
}


bool is_palindrome2(string str)
{
    string r;
    int n = str.size();
    for(int i=0;i<=(n/2);i++){
        if(str[i] != str[n-i-1])return false;
    }
    return true;
}

int main()
{
    string x;

    cin>>x;

    if(is_palindrome(x))cout<<"the string is palindrome"<<endl;
    else cout<<"not palindrome"<<endl;
    return 0;
}
