    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>
    #include<sstream>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))


bool is_possible(vc<int>x,vc<int>y)
{
   // return true;
    if(x.size()>2)return true;
    if(x.size()<2)return false;
    //cout<<y[0]<<" "<<y[1]<<endl;
    if(x[0]==x[1])return false;
    if(y[0]==y[1])return false;
    return true;
}

vc<int> getid(vc<int>x,vc<int>y)
{
    vc<int>ret;

    for(int i=0;i<x.size();i++)
    {
        for(int j=i+1;j<x.size();j++)
        {
            if(x[i]!=x[j] && y[i]!=y[j]){ret.pb(i);ret.pb(j);return ret;}
        }
    }
}
ll calcArea(vc<int>x, vc<int>y)
{
    ll res=0;
    if(x.size()==2)
    {
        res= abs(x[0]-x[1])*abs(y[0]-y[1]);
    }
    else
    {
        vc<int>id=getid(x,y);
        res = abs(x[id[0]]-x[id[1]])*abs(y[id[0]]-y[id[1]]);
    }
    return res;
}
int main()
{
    int n;
    ll area=0;
    vc<int>x,y;

    cin>>n;
    x.resize(n);
    y.resize(n);

    for(int i=0;i<n;i++)
    {
        cin>>x[i];
        cin>>y[i];
    }



    if(!is_possible(x,y)){cout<<"-1"<<endl;return 0;}

    area=calcArea(x,y);

    cout<<area<<endl;


    return 0;
}
