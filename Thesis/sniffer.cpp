#include <iostream>

using namespace Tins;

#include <tins/tins.h>

bool callback(const PDU &pdu) {
    const IP &ip = pdu.rfind_pdu<IP>();
    const TCP &tcp = pdu.rfind_pdu<TCP>();
    std::cout << ip.src_addr() << ':' << tcp.sport() << " -> "
              << ip.dst_addr() << ':' << tcp.dport() << std::endl;
    return true;
}

int main() {
    // Sniff on interface eth0
    // Maximum packet size, 2000 bytes
    Sniffer sniffer("eth0", 2000);
    sniffer.sniff_loop(callback);
}
