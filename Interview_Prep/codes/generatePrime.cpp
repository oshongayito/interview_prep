#include<iostream>
#include<vector>
#include<cmath>

using namespace std;
#define MAX INT_MAX
vector<int>primes;
bool flag[MAX];


void sieve()
{
    for(int i=2;i<sqrt(MAX)+1;)
    {

        for(int j=2;j<(MAX/i)+1;j++)
        {
            flag[i*j]=true;
        }
        if(i>2)i+=2;
        else i++;
    }
    for(int i=2;i<MAX;i++)
    {
        if(!flag[i])primes.push_back(i);
    }
}


int main()
{
    sieve();
    cout<<primes[3]<<endl;
    return 0;
}
