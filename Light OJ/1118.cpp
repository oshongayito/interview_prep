#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#define pi (2*acos(0.0))

using namespace std;

int  main()
{
	int t,i;
	double e,a,b,d,B,C,h,area1,area2,res,x1,y1,r1,x2,y2,r2;
	cin>>t;
	for(i=1;i<=t;i++)
	{
		cin>>x1>>y1>>r1>>x2>>y2>>r2;
		
		d = sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
		
		if(r1<r2){e = r1; r1 = r2; r2 = e;}
		
		if(d>=(r1+r2))cout<<"Case "<<i<<": 0.0"<<endl;
		else if(d<=r1-r2)printf("Case %d: %.10lf\n",i,pi*r2*r2);
		else
		{
		
		B = acos((r1*r1 + d*d - r2*r2)/(2*r1*d))*(180.0/pi);
		C = acos((r2*r2 + d*d - r1*r1)/(2*r2*d))*(180.0/pi);
		
		h = r1* sin((B*pi)/180);
		a = sqrt((r1*r1)-(h*h));
		b = sqrt((r2*r2)-(h*h));
		//area = (0.5)*(a+c)*h;
		area1 = ((B/180)*pi*r1*r1) - (h*a);
		area2 = ((C/180)*pi*r2*r2)- (h*b);
		res = area1 + area2+ 0.0000000001;
		if(d>a)
		printf("Case %d: %.10lf\n",i,res);
		else if(d==a)
		{
			res = area1 + (pi*r2*r2)/2;
		printf("Case %d: %.10lf\n",i,res);
		}
	    else 
	    {
			//s = (r2+r2+h+h)/2;
			//as = sqrt(s*(s-r2)*(s-r2)*(s-d-d));
			area2 = ((C/180)*pi*r2*r2) +  h*b;
			res = area1 +  area2 ;
			printf("Case %d: %.10lf\n",i,res);
		}
	}
	}
	return 0;
}
