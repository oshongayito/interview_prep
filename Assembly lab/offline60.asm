.MODEL SMALL
.STACK 500H
.DATA          
MSG DW ?
NUMB DW ? 
     
REV_NUMB DW ?
REM DW ?  
CHAR DW 25   
FACT DW 2    
I_CNT DW 0
;FACTS DW 10

TEMP DW ? 
TEMP2 DW ?
TEMP3 DW ?  
TEMP4 DW ?

CNT DW 0  
NLINE DB 0AH,0DH,'$'   

MSG1 DW '1> $'
MSG2 DW '2> $'
MSG3 DW 0AH,0DH,'FACTORS FREQUENCY',0AH,0DH,'$'  
MSG4 DW '     $'




.CODE

MAIN PROC  
    
    MOV AX,@DATA
    MOV DS,AX
     
    ;MOV AH,2   
    CALL IN_DEC
    MOV NUMB,CX
    
    LEA DX,MSG1
    MOV AH,9
    INT 21H
    
    
    ; MOV DX,NUMB   
    
    ; INT 21H 
    
       
     
     MOV AX,NUMB 
     
     PUSH NUMB 
    ; PUSH AX
     
     
     
  CALL REVERSE     ;CALLING THE REVERSE PROCEDURE  
  
  LEA DX,MSG
  MOV AH,9
  INT 21H 
      
 LEA DX,NLINE   ;NEWLINE CODE
 MOV AH,9
 INT 21H
  
           
  
  LEA DX,MSG2
  MOV AH,9
INT 21H
  LEA DX,MSG3
  INT 21H
   POP AX 
  CALL FACTORS
  
  
  MOV AH,4CH
  INT 21H
  
  
  
  
  
     

MAIN ENDP
            
            



IN_DEC PROC 
    
    MOV CX,0
    
  I_DEC:
    
    MOV AH,1
    INT 21H
    
    CMP AL,0DH
    JE I_EXIT
    SUB AX,48
    
    MOV BX,10
    ;MOV AX,DX
    
    MOV AH,0
    PUSH AX
    
    MOV AX,CX
    IMUL BX      ;10*CNT 
    ;IMUL I_CNT
    POP BX       ;BX HOLDS AL
    ADD AX,BX    ; AX = 10*I_CNT + AL
    INC I_CNT 
    MOV CX,AX
    JMP I_DEC
    
   I_EXIT:
    RET 
        
    
IN_DEC ENDP

            
            
            
            
            
            
            
            
            
            
                               
                               
REVERSE PROC                   ; PROCEDURE TO REVERSE A NUMBER       
    
    
    ;INPUT AX
    ;OUTPUT DX
    
      MOV CX,0
       ;MOV REV_NUMB,CX
       
       LEA SI,MSG     
       
       
    WHILE_:
        
        CMP AX,0
        JLE END_WHILE
        
        
        CWD
        MOV BX,10
        IDIV BX   
        
        MOV CX,AX
      
       ADD DX,48
        
        MOV MSG+SI,DX      ; CREATING A STRING OF THE REVERSE NUMBER
        ADD SI,1
        
        
       
        
        
        
        JMP WHILE_
        
     END_WHILE:
     
      
        MOV MSG+SI,'$' 
        
         
        
        RET   ;RETURNING TO MAIN PROCEDURE
        
        
    
REVERSE ENDP
                            
                            
                            
                            
                            
                            
                            
                            
                            
                     


   FACTORS PROC   
    
        MOV CX,AX
        
       ; MOV AH,2
       ; MOV DL,' '
       ; INT 21H
        
        MOV AX,CX
           
           ;INPUT AX
           
         WHILE:
         
        
         
         CMP AX,0     ;IF AX<=0 THEN END OF FACTORIZATION
         JLE ENDWHILE
         
         CWD
         MOV BX,FACT  
          MOV CX,AX
         IDIV BX       ;DIVIDING BY FACTORS
         
         
         
         CMP DX,0       ;IF FACT IS A PRIME FACTOR OF THE NUMBER THEN DX=0 
         MOV BX,AX
         JNE INCR_FACT
         
         ;PUSH FACT  
          
         INC CNT 
          
         
            
         
         
         JMP WHILE
        ; JLE SHOW
         
        WHIL: 
          ;INC CNT 
         
         JMP WHILE       ;LOOPING
         
         
      SHOWS:
        
        
        
        CMP FACT,9
        JG E_SHOW
        CMP CNT,0
        JG SHOW   
        JLE I_FACT   
        
        
       
        
            
         
       SHOW: 
         MOV AH,2
        ; MOV DX,0D
         MOV DX,FACT
         ADD DX,48       
         INT 21H         ;PRINTING FACTOR
         MOV DL,' '
         INT 21H 
         INT 21H
         INT 21H
         INT 21H
         INT 21H
         INT 21H
         INT 21H 
         INT 21H   
         
         CMP CNT,9
         JG  E_CNT_SHOW
         MOV DX,CNT  
         ADD DX,48
         INT 21H         ;PRINTING FREQUENCY
         
         
         
         ;MOV DL,' '
         ;INT 21H
         
         
         
         
         
         
      _NLINE:     
         
      LEA DX,NLINE
      MOV AH,9
      INT 21H
      MOV AX,BX
      MOV CNT,0
            
              
 ; MOV AH,2      ;NEWLINE CODE STARTS
 ; MOV DL,0AH
 ; INT 21H
  
  ;MOV DL,0DH
  ;INT 21H       ;NEWLINE CODE ENDS
         
      ; MOV AX,CX  
         
     JMP I_FACT  ;END SHOW
          
          
          
          
          
          
          
     E_CNT_SHOW:
       
     PUSH FACT
     MOV DX,CNT
     MOV FACT,DX
     CALL SHOW_EX
     POP FACT
     JMP _NLINE
     
     
       
      
     
     E_SHOW:
        CMP CNT,0
        JG E_SHOWS  
        JLE I_FACT
        
        E_SHOWS:
         CALL SHOW_EX
         MOV DX,' '
         INT 21H   
         INT 21H 
         INT 21H
         INT 21H
         INT 21H
         INT 21H
         INT 21H
         
         MOV DX,CNT
         ADD DX,48
         INT 21H
         MOV AX,BX 
         MOV CNT,0
         JMP _NLINE
      
        
     EX_SHOW:
        ;CALL SHOW_EX 
        JMP E_SHOW
         
         
              
 ; MOV AH,2      ;NEWLINE CODE STARTS
 ;MOV DL,0AH
 ; INT 21H
  
 ; MOV DL,0DH
 ; INT 21H       ;NEWLINE CODE ENDS    
 
 
         
         
      INCR_FACT:   
      
                              ;INCR_FACT
         JMP SHOWS                         
         
         
        I_FACT: 
        
        CMP AX,0     
         JLE ENDWHILE     ;IF AX<=0 THE END OF FACTORIZATION
         
         MOV AX,CX  
         INC FACT  
         
         
         
         JMP WHILE        ;LOOPING
         
         
      ENDWHILE:
                 
         MOV DX,CNT
         
         RET 
           
    
   FACTORS ENDP      
                     
                     
                     

SHOW_EX PROC
       
      ;INPUT FACT WHERE FACT>10    
      
      MOV TEMP2,BX
      MOV TEMP3,CX
      
      MOV TEMP4,DX
      
      
      
      MOV AX,FACT    
      
      XOR CX,CX
      
     TOP:
     CMP AX,0
     JLE EXIT
     
     MOV BX,10
     CWD
     IDIV BX
     PUSH DX
     INC CX     
     JMP TOP     
          
     EXIT:
      
      POP DX 
      ADD DX,48
      MOV AH,2
      INT 21H
     LOOP EXIT   ;LOOPING
      
      
      MOV BX,TEMP2
      MOV CX,TEMP3
      MOV DX,TEMP4
      
      
      
      RET
      
      
      
      
      
       
      
           
            
    
SHOW_EX ENDP                 



END MAIN