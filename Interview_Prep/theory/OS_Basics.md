### References ###
1. Multiprocessing, Multithreading: https://www.geeksforgeeks.org/operating-system-difference-multitasking-multithreading-multiprocessing/
2. Paging: https://www.geeksforgeeks.org/operating-system-paging/


Data **concurrency** means that many users can access data at the same time.

Data **consistency** means that each user sees a consistent view of the data, including visible changes made by the user's own transactions and transactions of other users.

### Deadlock and Starvation ###
------------------------------
**Deadlock** is a condition where no process proceeds for execution, and each waits for resources that have been acquired by the other processes. 

On the other hands, in **Starvation**, process with high priorities continuously uses the resources preventing low priority process to acquire the resources.


### Locks, Critical Section, Mutex, and Semaphores:
------------------------------
 **Lock** allows only one thread to enter the part that's locked and the lock is not shared with any other processes.

**Critical section** objects provide synchronization similar to that provided by mutex objects, except that critical section objects can be used only by the threads of a single process



A **mutex** is the same as a lock but it can be system wide (shared by multiple processes. A mutex can be owned by only one thread at a time, enabling threads to coordinate mutually exclusive access to a shared resource. Mutex has binary state (0 or 1)



A **semaphore** does the same as a mutex but allows x number of threads to enter, this can be used for example to limit the number of cpu, io or ram intensive tasks running at the same time. A semaphore maintains a count between zero and some maximum value, limiting the number of threads that are simultaneously accessing a shared resource. Semaphore can have multiple states (0, 1, 2 etc.).


You also have **read/write locks** that allows either unlimited number of readers or 1 writer at any given time.

### Thread Pool ###

In computer programming, a thread pool is a software design pattern for achieving concurrency of execution in a computer program. A thread pool maintains multiple threads waiting for tasks to be allocated for concurrent execution by the supervising program. 

One benefit of a thread pool over creating a new thread for each task is that thread creation and destruction overhead is restricted to the initial creation of the pool, which may result in better performance and better system stability. 


