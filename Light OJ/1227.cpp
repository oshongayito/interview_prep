    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define sii(a) scanf("%d",&a)
    #define sii2(a,b) scanf("%d%d",&a,&b)
    #define sii3(a,b,c) scanf("%d%d%d",&a,&b,&c)

    int main()
    {
        int t,n,p,q,m,i,j,k,l,r,s,sum;
        vc <int> wt;
        sii(t);
        //cout<<t<<endl;
                for(k=1;k<=t;k++)
        {
            sii3(n,p,q);
            for(i=0;i<n;i++){sii(m);wt.pb(m);}
            sort(wt.begin(),wt.end());
            if(n<p)l=n;
            else l=p;

            for(i=0,j=l-1,s=0; j>=0 ; j--)
            {
                for(r=i,sum=0 ;r<=j; r++)
                {
                    sum+=wt[r];
                }
                if(sum<=q){s=j+1;break;}
            }
            printf("Case %d: %d\n",k,s);
            wt.clear();

        }
    }
