#include<iostream>
using namespace std;


struct BstNode{
	int data;
	BstNode* left;
	BstNode* right;
};

BstNode* getNewNode(int val){
	BstNode* newNode = new BstNode();
	newNode->data = val;
	newNode->left = NULL;
	newNode->right = NULL;
	return newNode;
}

BstNode* insert(BstNode* root, int val){
	if(root == NULL){
		root = getNewNode(val);
	}
	else if(val <= root->data){
		root->left = insert(root->left,val);
	}
	else{
		root->right = insert(root->right,val);
	}
	return root;
}

bool search(BstNode* root, int val){
	if(root == NULL)return false;
	if(val == root->data)return true;
	if(val < root->data) return search(root->left,val);
	else return search(root->right, val);
}

int findMin(BstNode* root){
	if(root == NULL)return -1; //this case will never happen as findMin will always be called from a valid node
	if(root->left == NULL)return root->data;
	else findMin(root->left);
}

BstNode* deleteNode(BstNode* root, int val){
	if(root == NULL)return NULL;
	if(val < root->data){
		root->left = deleteNode(root->left,val);
	}
	else if(val > root->data){
		root->right = deleteNode(root->right, val);
	}
	else{
		if(root->left == NULL && root->right == NULL){ //case 1: leaf node
			delete root;
			root = NULL;
		}
		else if(root->left == NULL){ // case 2: has one child
			BstNode* temp = root;
			root = root->right;
			delete temp;
		}
		else if(root->right == NULL){ //case 2: has one child
			BstNode* temp = root;
			root = root->left;
			delete temp;
		}
		else{ //case 3: has both childs
			root->data = findMin(root->right);
			root->right = deleteNode(root->right,root->data);
		}
	}
	return root;
}


//Inorder traversal will give a sorted list of integers. COOOOOOOOL!!!
void inorder_traverse(BstNode* root){
	if(root == NULL)return;
	inorder_traverse(root->left);
	cout<<root->data<<endl;
	inorder_traverse(root->right);
}


void preorder_traverse(BstNode* root){
	if(root == NULL)return;
	cout<<root->data<<endl;
	preorder_traverse(root->left);
	preorder_traverse(root->right);
}

int main(){
	BstNode* root = NULL;
	cout<<"inserting 15 in BST"<<endl;
	root = insert(root,15);

	cout<<"inserting 10 in BST"<<endl;
	root = insert(root,10);



	cout<<"searching for 15 in BST"<<endl;
	bool found = search(root, 15);
	if(found)cout<<"found"<<endl;
	else cout<<"not found"<<endl;

	cout<<"searching for 20 in BST"<<endl;
	found = search(root, 20);
	if(found)cout<<"found"<<endl;
	else cout<<"not found"<<endl;
	
	cout<<"inserting 20 in BST"<<endl;
	root = insert(root,20);

	root = insert(root,22);
	root = insert(root,18);
	root = insert(root,17);
	root = insert(root,12);
	root = insert(root,8);

	cout<<"printing inorder_traverse"<<endl;
	inorder_traverse(root);

	cout<<"printing preorder_traverse"<<endl;
	preorder_traverse(root);
	
	cout<<"searching for 20 in BST"<<endl;
	found = search(root, 20);
	if(found)cout<<"found"<<endl;
	else cout<<"not found"<<endl;

	cout<<"deleting 20 in BST"<<endl;
	root = deleteNode(root,20);

	cout<<"printing inorder_traverse"<<endl;
	inorder_traverse(root);

	cout<<"searching for 15 in BST"<<endl;
	found = search(root, 15);
	if(found)cout<<"found"<<endl;
	else cout<<"not found"<<endl;

	cout<<"searching for 20 in BST"<<endl;
	found = search(root, 20);
	if(found)cout<<"found"<<endl;
	else cout<<"not found"<<endl;

	cout<<"printing inorder_traverse"<<endl;
	inorder_traverse(root);

	
	return 0;
}