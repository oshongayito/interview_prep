#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#define pi (2*acos(0.0))

using namespace std;

int  main()
{
	int t,i;
	double a,b,c,B,h,area,res,rat;
	cin>>t;
	for(i=1;i<=t;i++)
	{
		cin>>c>>b>>a>>rat;

		//A = acos((b*b + c*c - a*a)/(2*b*c))*(180.0/pi);
		B = acos((a*a + c*c - b*b)/(2*a*c))*(180.0/pi);
		//C = acos((a*a + b*b - c*c)/(2*a*b))*(180.0/pi);

		cout<<B<<endl;
		h = c* sin((B*pi)/180);
		area = rat*(((0.5)*a*h)/(rat+1));

		res = sqrt((2*area*c)/(a*sin((B*pi)/180))) + 0.0000000001;


		printf("Case %d: %.10lf\n",i,res);
	}
	return 0;
}
