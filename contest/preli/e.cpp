
#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>

#define ll long long
#define pi (2*acos(0.0))
#define pb push_back

using namespace std;


int main()
{
    ll t,x1,y1,x2,y2,i,j,k,l,a,b,c,d;
    cin>>t;
    for(i=1;i<=t;i++)
    {
        cin>>x1>>y1>>x2>>y2;
        if(x1!=x2 && y1!=y2)
        {
            cout<<"Case "<<i<<": "<<x1<<" "<<y1+1<<" "<<x2<<" "<<y2-1<<endl;
        }
        else if(x1==x2)
        {
            cout<<"Case "<<i<<": "<<x1-1<<" "<<y1<<" "<<x2+1<<" "<<y2<<endl;
        }
        else
        {
            cout<<"Case "<<i<<": "<<x1<<" "<<y1+1<<" "<<x2<<" "<<y2-1<<endl;
        }

    }
    return 0;
}
