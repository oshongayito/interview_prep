1. Bit operation questions

2. Linked list

3. Binary tree

4. algorithm that can find the missing number in the sorted array

5. compile with g++/gcc
6. Is Java pass-by-value or pass-by-reference?  
7. Find next largest node in BST
8. Basically talks about network protocols
9. memory management
10. How literal strings are stored in the memory
11. Basic questions on how and where variables and resources are stored and managed for a process.
12. Construct linked list from a python list. In-place linked list modification.  
13. find the missing number in the sorted list with explanation how I did it  
14. find the minimum cycle of a directed graph 
15. Basic questions about C pointers as they relate to Strings.  
16. Evaluate performance of 2 simple implementations of popcount().  
17. Memory layout question (stack,heap,static,code) given a code mention where each line goes in the memory.
18. How to implement stack (what data structure good for that) such as LinkedList or Array pors and cons of each one of them in term of memory.  
19. How to use C to implement C++ OOP features 
20. allocating the 2d array with just one malloc

21. Use C to implement C++ class inheritance and virtual function. 
22. The other one is to print linked list nodes whose indices are following Fibonacci numbers.  
23. Implement a trie data structure with insert, delete and lookup function.
24. Given a BST node, find its inorder successor 
25. print reverse strings, padding and pack 

26. Difference between array and pointer.
27. some basic functions of a Binary Search Tree
28. difference between char* str and char str[]  

29. How many bytes do a double, int, char ..... occupied in memory?

30. Find inorder successor in binary tree
31. basic questions about bit manipulation structure padding and strings

32. Difference between const int * and int *

33. memory management questions especially in void pointers
34. C structs  
35. Implement a generic stack API in C
36. Function parameters evaluation order, function call stack, OS, virtual memory  
37. Knowing the difference between a virtual function and normal function in a class in C++
38. Knowledge related to x86 stack frame, function arguments
39. How would you allocate space for a variably sized doubly indexed array using -std=c99
40. Write a program in C that can compare two strings that represent version numbers (1.1.02 compared to 1.1.3)  
41. pure virtual functions  
42. implement polymorphism using C
43. Analyze the size of a c struct. How large is a double, int, void * pointer.  
44. A question about what the main function in C does and why it is important to know the difference between char, char pointer, char array, and a pointer to char pointer

45. Design malloc.
46. What is the difference between vector and linked list?
47. The role of fflush to flush out the strings.
48. It is basically about representation of 0 in floating number using hex.  
49. Implement a stack library for a program, where each data type has a diff stack. 
50. OS based questions: Threads, Multitasking [My code (in step 1) behaviour in multiprocessor environment], Paging, virtual memory, context switching, dispatcher etc.  

51. Questions on Process memory structure. Stack, heap etc.
52. OS questions: system call? static/dynamic linking? Virtual memory? Process Vs Threads? Deadlock case? How to handle shared data between threads?  
53. Design own library to handle dynamic allocation/deallocation of memory. What data structure to maintain these information? 

54. Design and implement a circular queue in C.  
55. Convert a 3D linked list into a 2D one.  
56. printf("%d%d%d", i++, i++,i++)  
57. Design a memory allocator module

58. dichotomy problem 
59. How printf works, regarding stack pointers (assembly)  

60. What is a zombie process?
61. what does the "new" keyword in C++ do exactly
62. Size of 3 given structures?  
63. Size of class objects with virtual method?
64. Eventually, I was asked questions regarding C/C++, the memory layout of a program (stack, heap, etc), what is a mutex, what is and how to prevent deadlock from happening, etc.
65. How to calculate memory space of struct with and without sizeof. 
66. How much memory void pointer will take? How much memory int/double/char will take on 64bit architecture
67. How would you implement malloc give 1GB space in memory.
68. strcmp
69. C++-like templates in C with void * and memcpy  
70.  Print string in reverse without allocating memory
71. Difference between C and C++  

72. I was asked about malloc versus new, allocating memory for two dimensional arrays, and the use of static and const.

73. How to find the size of an integer without using sizeof() keyword?

74. To check whether the binary format of a number is palindrome or not in most elegant manner? 

75. Implement strcpy; implement this using command line arguments.  
76. What will be the sizeof the following structure on a 32-bit machine: struct s
{
int i;
char c;
long d;
void *p;
int a[0];
};  
77. Multi-threading, locking, mutex problem solve (leetcode)
78. What's pushed on the stack when a function with variable number of args is called? 
79. 

54. (second interview)
given the code:

void increment( ) {
    static int i = 0;
    return ++i;
}

int main(void) {
    printf("%d %d", increment( ), increment( ) );
}

What would the output be? How would you change printf to output them in the right order?  
55. 


Networking:

1. IP routing protocols, such as RIP, OSPF, BGP, IS-IS, or PIM
2. understanding of networking including capabilities like L2, L3, and fundamentals of commercial switching HW.
3. Extensible Operating System (EOS)
4. Layer 2 features such as 802.1d bridging, the 802.1d Spanning Tree Protocol, the 802.1ax Link Aggregation Control Protocol, the 802.1AB Link Layer Discovery Protocol, or RFC 1812 IP routing.
5. 


Behavioral:
1. What is one thing you will change about Arista?
2. How will you handle stress
3. Why do you want to work at Arista?  


Other Questions:

https://www.geeksforgeeks.org/arista-networks-interview-set-3/

https://www.careercup.com/question?id=5660692209205248



void print(char *str, ...)
{
	va_list vl;
	va_start(vl, str);

	va_arg(vl, int);

	va_end(vl);

}


1. Do arista use any db or log to keep track of the state of the processes in the data centres? For the crash recovery
2. What type of tests are done usually? 
3. Agile?
4. 



