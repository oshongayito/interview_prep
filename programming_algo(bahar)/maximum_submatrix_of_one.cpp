int max_area(int a[],int len)
{
	int b[len];
	stack<int> s;
	for(int i=0;i<len;i++)
	{
		while(!s.empty())
		{
			if(a[i]<=a[(int)s.top()]) s.pop();
			else break;
		}
		int t=0;
		if(s.empty()) t=-1;
		else t=(int) s.top();
		
		b[i]=i-t-1;
		s.push(i);
	}
	
	while(!s.empty()) s.pop();
	
	for(int i=len-1;i>=0;i--)
	{
		while(!s.empty())
		{
			if(a[i]<=a[(int)s.top()]) s.pop();
			else break;
		}
		int t=0;
		if(s.empty()) t=len;
		else t=(int) s.top();
		
		b[i]+=t-i-1;
		s.push(i);
	}
	
	int max=0;
	
	for(int i=0;i<len;i++)
	{
		b[i]=a[i]*(b[i]+1);
		if(max<b[i]) max=b[i];
	}
	
	return max;
}

int max_submatrix(string s[],int n)
{
	int max=0,max_i;
	int len=(int)s[0].size();
	int a[n][len];
	
	for(int i=0;i<len;i++)
	{
		a[0][i]=s[0][i]-'0';
	}
	for(int i=1;i<n;i++)
	{
		for(int j=0;j<len;j++)
		{
			if(s[i][j]=='1') 
			{
				a[i][j]=a[i-1][j]+1;
			}
			else a[i][j]=0;
		}
	}
	
	
	for(int i=0;i<n;i++)
	{
		max_i=max_area(a[i],len);
		if(max<max_i) max=max_i;
	}
	
	return max;
}


