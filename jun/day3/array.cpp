/*
this is an example for understanding array
*/
#include<stdio.h>

int main()
{
    int a[6];
    a[0]=15;
    a[1]=16;
    a[2]=15;
    a[3]=16;
    a[4]=16;
    a[5]=16;

    printf("%d ",a[0]);
    printf("%d ",a[1]);
    printf("%d ",a[2]);
    printf("%d ",a[3]);
    printf("%d ",a[4]);
    printf("%d",a[5]);


    return 0;
}
