#include <bits/stdc++.h>
#include<GL/glut.h>
using namespace std;

typedef long long ll;
typedef vector<int> vi;
typedef vector<vi> vvi;
typedef vector<string> vs;

#define inf 1061109567
#define pb push_back
#define mp make_pair
#define all(a) a.begin(),a.end()
#define mem(x,a) memset(x,a,sizeof(x))
#define rep(i,n) for(int i(0),_n(n);i<_n;++i)
#define repi(i,a,b) for(int i(a),_b(b);i<=_b;++i)
#define repr(i,a,b) for(int i(a),_b(b);i>=_b;--i)
#define repe(it,c) for(__typeof((c).begin()) it=(c).begin();it!=(c).end();++it)
#define len(x) ((int)(x.size()))
#define DEBUG 1 
#if DEBUG && !ONLINE_JUDGE 
	#define debug(args...) (Debugger()) , args
	class Debugger { public: Debugger(const std::string& _separator = ", ") : first(true), separator(_separator){} template<typename ObjectType> Debugger& operator , (const ObjectType& v) { if(!first) std::cerr << separator; std::cerr << v; first = false; return *this; } ~Debugger() { std::cerr << endl;} private: bool first; std::string separator; }; template <typename T1, typename T2> inline std::ostream& operator << (std::ostream& os, const std::pair<T1, T2>& p) { return os << "(" << p.first << ", " << p.second << ")"; } template<typename T> inline std::ostream &operator << (std::ostream & os,const std::vector<T>& v) { bool first = true; os << "["; for(unsigned int i = 0; i < v.size(); i++) { if(!first) os << ", "; os << v[i]; first = false; } return os << "]"; } template<typename T> inline std::ostream &operator << (std::ostream & os,const std::set<T>& v) { bool first = true; os << "["; for (typename std::set<T>::const_iterator ii = v.begin(); ii != v.end(); ++ii) { if(!first) os << ", "; os << *ii; first = false; } return os << "]"; } template<typename T1, typename T2> inline std::ostream &operator << (std::ostream & os,const std::map<T1, T2>& v) { bool first = true; os << "["; for (typename std::map<T1, T2>::const_iterator ii = v.begin(); ii != v.end(); ++ii) { if(!first) os << ", "; os << *ii ; first = false; } return os << "]"; } 
#else 
		#define debug(args...) 
#endif

#include "camera.h"
#include "mega2.h"
#include "color.h"
Camera cam;
double delta=5.0;

bool drawaxes = 1;
void drawAxes()
{
	//~ if(drawaxes==1)
	//~ {
		glColor3f(1.0, 0, 0);
		glBegin(GL_LINES);{
			glVertex3f( 100,0,0);
			glVertex3f(-100,0,0);
			glVertex3f(0,-100,0);
			glVertex3f(0, 100,0);
			glVertex3f(0,0, 100);
			glVertex3f(0,0,-100);
		}glEnd();
	//~ }
}

bool drawgrid = 1;

void drawGrid()
{
	int i;
	//~ if(drawgrid==1)
	//~ {
		glColor3f(0, 0, 1);	//grey
		glBegin(GL_LINES);{
			for(i=-8;i<=8;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -90, 0);
				glVertex3f(i*10,  90, 0);

				//lines parallel to X-axis
				glVertex3f(-90, i*10, 0);
				glVertex3f( 90, i*10, 0);
			}
		}glEnd();
	//~ }
}



float dfx,dfy,dfz,dr,dg,db,diff_rad=50,diff_delta=5,diff_ang=0;

void myKeyboard(unsigned char key, int x, int y)
{
    switch(key)
    {
        case '1':
            cam.yaw(-delta);
            break; /// lookleft
        case '2':
            cam.yaw(delta);
            break; /// lookright
        case '3':
            cam.pitch(-delta);
            break; /// lookup
        case '4':
            cam.pitch(delta);
            break; /// lookdown
        case '5':
            cam.roll(delta);
            break; /// twistleft
        case '6':
            cam.roll(-delta);
            break; /// twistright
        case '9':
			dr = 1,dg=1,db=1;
			break;
		case '0':
			dr = 0,dg=0,db=1;
			break;
		case '7':
			diff_ang+=diff_delta;
			dfx = diff_rad*cos(PI*diff_ang/180.);
			dfy = diff_rad*sin(PI*diff_ang/180.);
			break;
		case '8':
			diff_ang-=diff_delta;
			dfx = diff_rad*cos(PI*diff_ang/180.);
			dfy = diff_rad*sin(PI*diff_ang/180.);
			break;
		
    }
    glutPostRedisplay();
}



void myDisplay()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glClearColor(0,0,0,1);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	set_ambient_color(0.5,0.5,0.5);
	set_diffuse_source(dfx,dfy,dfz,dr,dg,db);
	set_spot_light(0,130,130,0,-1,-1);
    drawAxes();
    drawGrid();
    
    
    
    draw();
    

    
    
    //~ tower.drawTower();
    //tower.drawRoundedRod(20.0, 120.0, 15);
    //glColor3f(0.5,0.5,0.5);
    //glutWireSphere(10,20,20);
    //~ glutWireTeapot(10.0); 
    
    //~ glPushMatrix();			// save the current camera transform
    //~ glRotated(90, 1, 0, 0);	// rotate by rotAngle about y-axis
    //glEnable(GL_COLOR_MATERIAL);	// specify object color
    //~ glColor3f(0.5,0.5,0.5);		// redish
    //~ glutWireTeapot(10);			// draw the teapot
    //~ glPopMatrix();
    
    glFlush();
    glutSwapBuffers();
}

void specialKeyListener(int key, int x,int y)
{
    //head rotations about axes
    //use INSERT key instead of PAGE_DOWN, mine not working
	switch(key)
	{
		case GLUT_KEY_UP:
		    cam.slide(0,0,-delta);
		    break; /// forward
		case GLUT_KEY_DOWN:
		    cam.slide(0,0,delta);
		    break; /// backward
		case GLUT_KEY_LEFT:
		    cam.slide(-delta,0,0);
		    break; /// left
		case GLUT_KEY_RIGHT:
		    cam.slide(delta,0,0);
		    break; /// right
		case GLUT_KEY_PAGE_UP:
		    cam.slide(0,delta,0);
		    break; /// up
		case GLUT_KEY_PAGE_DOWN:
		    cam.slide(0,-delta,0);
		    break; /// down
	}
	glutPostRedisplay();
}

void initCamera()
{
    glViewport(0,0,1200,700);
    Vector3 up(0,0,1);
    Point3 eye(150.0f,150.0f,150.0f);
    Point3 look(0.0f,0.0f,0.0f);
    cam.set(eye,look,up);
    cam.setShape(30.0f,64.0f/48.0f,0.5f,500.0f);
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    //glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);
    glutInitWindowSize(1200,700);
    glutInitWindowPosition(50,50);
    glutCreateWindow("fly a camera around a teapot");
    glEnable(GL_DEPTH_TEST);

    glMatrixMode(GL_PROJECTION);
    glutKeyboardFunc(myKeyboard);
    glutSpecialFunc(specialKeyListener);
    glutDisplayFunc(myDisplay);
    //~ glutDisplayFunc(myDisplay);
    glClearColor(0,0,0,1);
    //glColor3f(0,0,0);
	
	
	dfx = 50;
	dfy = 50;
	dfz = 50;
	dr = 1;
	dg = 1;
	db = 1;
	
	init();
	initRendering();
	
	
    initCamera();
    
    
    glutMainLoop();
    return 0;
}






