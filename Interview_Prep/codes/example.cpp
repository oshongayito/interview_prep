#include<stdio.h>

int gcd(int u, int v)
{
    if(v != 0)
    {
        return gcd(v, u%v);
    }
    else
    {
        return u;
    }

}

int lcm(int u,int v)
{
    return (u*v)/gcd(u,v);
}

int main()
{

    printf("gcd is %d",lcm(8,12));

    return 0;
}
