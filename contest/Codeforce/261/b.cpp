    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))


int main()
{
    map<ll,ll> count;
    vc<ll> beauty;
    ll n,x,y,i,j;

    cin>>n;
    beauty.resize(n);

    for(i=0;i<n;i++)
    {
        cin>>beauty[i];
        count[beauty[i]]++;
    }

    sort(beauty.begin(),beauty.end());
    if(beauty[0]!=beauty[n-1])
    cout<<beauty[n-1]-beauty[0]<<" "<<count[beauty[0]]*count[beauty[n-1]]<<endl;
    else
    cout<<beauty[n-1]-beauty[0]<<" "<<(count[beauty[0]]*(count[beauty[n-1]]-1))/2<<endl;

    return 0;
}
