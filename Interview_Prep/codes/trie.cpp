#include<iostream>
#include<vector>
using namespace std;
#define alphabet_size 26

class Trie{
public:
    vector<Trie*> childs;
    bool endOfWord;
    Trie() {
        this->childs.resize(alphabet_size, NULL);
        endOfWord = false;
    }
};

void insert(Trie* root, string str){
    for(int i = 0; i < str.size(); i++) {
        if(!root->childs[str[i] - 'a']) {
            root->childs[str[i] - 'a'] = new Trie();
        }
        root = root->childs[str[i] - 'a'];
    }
    if(root) {
        root->endOfWord = true;
    }
}


bool search(Trie* root, string str){
    for(int i=0;i<str.size();i++){
        if(!root->childs[str[i]-'a'])return false;
        root = root->childs[str[i]-'a'];
    }
    if(root && root->endOfWord)return true;
    return false;
}


int main(){
    Trie* root = new Trie();

    insert(root,"winter");
    insert(root,"winnipeg");
    insert(root,"ant");
    insert(root,"antarctica");
    if(search(root, "winter"))cout<<"winter found"<<endl;
    else cout<<"winter not found"<<endl;

    if(search(root, "win"))cout<<"win found"<<endl;
    else cout<<"win not found"<<endl;

    if(search(root, "winnipeg"))cout<<"winnipeg found"<<endl;
    else cout<<"winnipeg not found"<<endl;

    if(search(root, "antarctica"))cout<<"antarctica found"<<endl;
    else cout<<"antarctica not found"<<endl;

    if(search(root, "an"))cout<<"an found"<<endl;
    else cout<<"an not found"<<endl;

    if(search(root, "zoo"))cout<<"zoo found"<<endl;
    else cout<<"zoo not found"<<endl;
    return 0;
}