#include <set>
#include <map>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>
#include <vector>
#include <string>
#include <cctype>
#include <cstdio>
#include <iomanip>
#include <sstream>
#include <cstdlib>
#include <climits>
#include <complex>
#include <numeric>
#include <valarray>
#include <iostream>
#include <string.h>
#include <algorithm>
using namespace std;



    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector



class FoxAndHandleEasy {
	public:
	string isPossible(string S, string T) {
		return "hel";
	}
};






// BEGIN CUT HERE
namespace moj_harness {
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				cerr << "Illegal input! Test case " << casenum << " does not exist." << endl;
			}
			return;
		}

		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}

		if (total == 0) {
			cerr << "No test cases run." << endl;
		} else if (correct < total) {
			cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << endl;
		} else {
			cerr << "All " << total << " tests passed!" << endl;
		}
	}

	int verify_case(int casenum, const string &expected, const string &received, clock_t elapsed) {
		cerr << "Example " << casenum << "... ";

		string verdict;
		vector<string> info;
		char buf[100];

		if (elapsed > CLOCKS_PER_SEC / 200) {
			sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}

		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}

		cerr << verdict;
		if (!info.empty()) {
			cerr << " (";
			for (int i=0; i<(int)info.size(); ++i) {
				if (i > 0) cerr << ", ";
				cerr << info[i];
			}
			cerr << ")";
		}
		cerr << endl;

		if (verdict == "FAILED") {
			cerr << "    Expected: \"" << expected << "\"" << endl;
			cerr << "    Received: \"" << received << "\"" << endl;
		}

		return verdict == "PASSED";
	}

	int run_test_case(int casenum) {
		switch (casenum) {
		case 0: {
			string S                  = "Ciel";
			string T                  = "CieCiell";
			string expected__         = "Yes";

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 1: {
			string S                  = "Ciel";
			string T                  = "FoxCiel";
			string expected__         = "No";

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 2: {
			string S                  = "FoxCiel";
			string T                  = "FoxFoxCielCiel";
			string expected__         = "Yes";

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 3: {
			string S                  = "FoxCiel";
			string T                  = "FoxCielCielFox";
			string expected__         = "No";

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 4: {
			string S                  = "Ha";
			string T                  = "HaHaHaHa";
			string expected__         = "No";

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 5: {
			string S                  = "TheHandleCanBeVeryLong";
			string T                  = "TheHandleCanBeVeryLong";
			string expected__         = "No";

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 6: {
			string S                  = "Long";
			string T                  = "LongLong";
			string expected__         = "Yes";

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 7: {
			string S                  = ;
			string T                  = ;
			string expected__         = ;

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 8: {
			string S                  = ;
			string T                  = ;
			string expected__         = ;

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 9: {
			string S                  = ;
			string T                  = ;
			string expected__         = ;

			clock_t start__           = clock();
			string received__         = FoxAndHandleEasy().isPossible(S, T);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}

int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(atoi(argv[i]));
	}
}
// END CUT HERE
