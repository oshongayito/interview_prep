
## Steps ##

Step 1: Requirements clarifications
		Calrify the high-level features of the system.

Step 2: Back-of-the-envelope estimation
		Talk about the numbers (overall expected traffic, storage, bandwidth).

Step 3: System interface definition
		API specification.

Step 4: Defining data model
		talk about the DB tables (entities) needed with their attributes.

Step 5: High-level design
		Block diagram (client, load balancer, app server, load balancer, DB)

Step 6: Detailed design
		Pick the interesting block and provide detailed design.

Step 7: Identifying and resolving bottlenecks
		Single point of failure, health check, slow server


### Requirement clarification: ###


Non-Functional Requirements:

1. highly available or consistent?

2. Latency?

3. Read heavy or write heavy?


### Back-of-the-envelope estimation ###

1. Total Traffic?

2. Queries (Read/Write) per second (QPS)?

3. Storage estimation

4. Bandwidth estimation

5. Memory (cache) estimation (e.g., 80/20 rule) (per day)

### APIs ###

1. write down apis?

### Database schema design ###

1. Tables

2. SQL/NOSQL (NoSQL -> e.g. -> wide column store -> Cassandra -> No single point of failure -> combination of both key-value store and tabular system)

3. Storage system (file based storage or object storage (Amazon S3, Hadoom Distributed File System (HDFS)))


### System design ###

1. Block Diagram

2. Main algorithm (if any)


### Data Partitioning and Replication ###

1. Range based

2. Hash based

3. Directory based

4. Single point of failures

### Caching ###

1. Size

2. Eviction policy

3. Cache replicas

### Load Balancers ###

1. We can add a Load balancing layer at three places in our system:

	a. Between Clients and Application servers
	b. Between Application Servers and database servers
	c. Between Application Servers and Cache servers

2. Load Balancing Algorithm
	a. Round robin
	b. Weighted distribution (always ask the server with less load)


### Security and Permissions ###













