////////////////                         binary search             ////////////////////////////////////////


#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>

#define ll long long
#define pi (2*acos(0.0))
#define pb push_back

using namespace std;


int  main()
{

    int i;
    double u,ln,l,w,r,ax,ay,bx,by,cx,cy,dx,dy,a,b,c,x,y,z,j,k,t,area,l1,l2,h1,h2,angle;
    char cr;

    cin>>t;
    for(i=1;i<=t;i++){

    cin>>ln>>cr>>w;
    if(ln<w){ax=ln;ln=w;w=ax;}
    r = sqrt((ln*ln)+(w*w));
    angle = acos((2*r*r - w*w)/(2*r*r))*(180.0/pi);
    z=0.2;
    //b=z;
    u=400;
    l=0;
   while(abs(z)>0.000000001)
    {
        x=(u+l)/2;

        h1 = ln*x;
        h2 = w*x;
        r = sqrt((h1*h1)+(h2*h2))/2;
        angle = acos(((2*r*r) - (h2*h2))/(2*r*r))*(180.0/pi);
        z= (2*(angle*r*(pi/180))+(2*h1))-400;
        if(z>0){u=x;}
        else if(z<0){l=x;}
    }
    x+=pow(10,-9);
    printf("Case %d: %.10lf %.10lf\n",i,x*ln,x*w);
    }

	return 0;
}
