    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))


int main()
{
    ll n,m,k,g=0;

    map< int, pair<int,int> >maps;

    vc<int>narr;
    vc<int>marr;

    cin>>n>>m>>k;
    narr.resize(n);
    marr.resize(m);

    for(int i=0;i<n;i++)
    {
        cin>>narr[i];
        maps[narr[i]]=mp(i/k,i%k);
    }

    for(int i=0;i<m;i++)cin>>marr[i];

    for(int i=0;i<m;i++)
    {
        int p= (maps[marr[i]].first*k) + maps[marr[i]].second;


        g+=(maps[marr[i]].first+1);

        if(maps[marr[i]].second==0)
        {

            if(p>0)
            {

                maps[marr[i]].first--;
                maps[narr[p-1]].first++;
                maps[marr[i]].second=k-1;
                maps[narr[p-1]].second=0;

               int temp = narr[p];
                narr[p]=narr[p-1];
                narr[p-1]=temp;

            }
        }
        else
        {
            maps[marr[i]].second--;
            maps[narr[p-1]].second++;

            int temp = narr[p];
            narr[p]=narr[p-1];
            narr[p-1]=temp;

        }
    }

    cout<<g<<endl;




    return 0;
}
