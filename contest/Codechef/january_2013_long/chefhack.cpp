    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)

    #define MAX 10000001
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))
//#define is_prime(n)(if(prime)


    int n;
    bool vis[355][355];
    pair<int,int>pr;
    queue< pair<int,int> > que;
    int arr[355][355];
int flag[MAX/64];
vector<int>prime;
int iprime[MAX+1];
int p,q;

void sieve(){
        unsigned i,j,k;
        flag[0]|=0;
        for(i=3;i<sqrt(MAX)+1;i+=2){
                if(!is_it(i)){
                        for(j=i*i,k=i<<1;j<MAX;j+=k)set_it(j);
                }
        }
        if(MAX>1){prime.pb(2);iprime[2]=1;}
        for(i=3;i<MAX;i+=2){
                if(!is_it(i)){
                        prime.pb(i);
                        iprime[i]=prime.size();
                }
        }
}


void bfs(int x, int y)//for adjacecy matrix representation of graph
{
    //if(vis[x][y])return;
    //vis[x][y]=1;
    que.push(mp(x,y));
    while(!que.empty())
    {
        pr=que.front();
        que.pop();
        p=pr.first;
        q=pr.second;
        vis[p][q]=1;
        //cout<<arr[p][q]<<endl;

        if(q+1<n && !iprime[arr[p][q+1]] && !vis[p][q+1] && ((arr[x][y]%2==0 && arr[p][q+1]%2==0)||(arr[x][y]%2==1&&arr[p][q+1]%2==1)))
        {
            que.push(mp(p,q+1));
            vis[p][q+1]=1;
//            arr[p][q+1]=0;

        }
        if(p+1<n && !iprime[arr[p+1][q]] && !vis[p+1][q] && ((arr[x][y]%2==0 && arr[p+1][q]%2==0)||(arr[x][y]%2==1&&arr[p+1][q]%2==1)))
        {
            que.push(mp(p+1,q));
            vis[p+1][q]=1;
            //arr[p+1][q]=0;
        }

        if(p-1>=0 && !iprime[arr[p-1][q]] && !vis[p-1][q] && ((arr[x][y]%2==0 && arr[p-1][q]%2==0)||(arr[x][y]%2==1&&arr[p-1][q]%2==1)))
        {
            que.push(mp(p-1,q));
            vis[p-1][q]=1;
            //arr[p-1][q]=0;
        }
        if(q-1>=0 && !iprime[arr[p][q-1]] && !vis[p][q-1] && ((arr[x][y]%2==0 && arr[p][q-1]%2==0)||(arr[x][y]%2==1&&arr[p][q-1]%2==1)))
        {
            que.push(mp(p,q-1));
            vis[p][q-1]=1;
            //arr[p][q-1]=0;
        }


    }
    return;

}
int main()
{
    ll x,y,z,k,ans=0;
    int i,j,t;
    vector< pair<int,int> >cord;
    pair<int,int> crd;
    sieve();
    queue< pair<int,int> > emptyque;
    //freopen("ip.txt","r",stdin);

    scanf("%d",&t);
    while(t--){


    //for(i=0;i<10;i++)cout<<prime[i]<<" ";
    scanf("%d",&n);
    for(i=0;i<n;i++)
        for(j=0;j<n;j++)
        {
            scanf("%d",&arr[i][j]);
        }
        for(i=0;i<n;i++)
                for(j=0;j<n;j++)
                {
                    if(!vis[i][j] )
                    {
                        if(!iprime[arr[i][j]])
                            {bfs(i,j);que=emptyque;}
                        cord.pb(mp(i,j));
                    }
                }

        for(j=0,ans=0;j<cord.size();j++)
        {
            x=cord[j].first;
            y=cord[j].second;
            //cout<<"$"<<arr[x][y]<<endl;
            //if(!vis[x][y]){
            if(iprime[arr[x][y]])
            {
                ans+=(iprime[arr[x][y]]-1);
            }
            else if(!iprime[arr[x][y]])
            {
                if(arr[x][y]%2==0)
                {
                    ans+=(arr[x][y]/2);
                }
                else
                {
                    ans+=(arr[x][y]+3)/2;
                    //cout<<vis[i][j]<<endl;
                }
            }
            else ans+=0;
            //cout<<arr[i][j]<<" "<<iprime[arr[i][j]]<<" "<<ans<<endl;

            //}
        }
        printf("%lld\n",ans);

        ans=0;
        for(i=0;i<351;i++)for(j=0;j<351;j++){vis[i][j]=0;arr[i][j]=0;}
        que = emptyque;
        cord.clear();


    }
return 0;
}
