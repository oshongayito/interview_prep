#include<iostream>
#include<vector>
#include<sstream>
#include<string>
using namespace std;

string str_replace_all(string str, string prev, string new_str){
	int start_pos = 0;
	while((start_pos = str.find(prev, start_pos)) != string::npos){
		str.replace(start_pos, prev.size(), new_str);
		start_pos+=new_str.size();
	}
	return str;
}

int main(){

	string s = "hey buddy, howw are you??? howw is everything else???";

	cout<<str_replace_all(s, "howw", "how")<<endl;

	return 0;
}