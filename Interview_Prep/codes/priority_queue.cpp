#include<iostream>
using namespace std;

#include<queue>

/* 
heaps can be implemented using priority queue too
*/
int main(){
	priority_queue<int>maxHeap;
	priority_queue<int, vector<int>, greater<int> >minHeap;

	cout<<"pushing in max heap 10, 5, 7"<<endl;
	maxHeap.push(10);
	maxHeap.push(5);
	maxHeap.push(7);

	cout<<"popping from max heap"<<endl;
	cout<<maxHeap.top()<<endl;
	maxHeap.pop();
	cout<<maxHeap.top()<<endl;
	maxHeap.pop();
	cout<<maxHeap.top()<<endl;
	maxHeap.pop();

	cout<<endl;
	cout<<"pushing in min heap 10, 5, 7"<<endl;
	minHeap.push(10);
	minHeap.push(5);
	minHeap.push(7);

	cout<<"popping from min heap"<<endl;
	cout<<minHeap.top()<<endl;
	minHeap.pop();
	cout<<minHeap.top()<<endl;
	minHeap.pop();
	cout<<minHeap.top()<<endl;
	minHeap.pop();


	return 0;
}



