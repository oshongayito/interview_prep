
#include "Ray.h"
#include "Object.h"
#define PI acos(-1.0)

class sphere : public Object{
public:
    Vector3 center;
    double centerX,centerY,centerZ;
    double radius;
    double colorR,colorG,colorB;
    double ambCoeff;
    double difCoeff;
    double specCoeff;
    double shinness;
    Color color;


    void setCenter(double x,double y,double z)
    {
        centerX=x;
        centerY=y;
        centerZ=z;
        center.set(x,y,z);
    }
    void setRadius(double x)
    {
        radius=x;
    }
    void setColor(double r,double g,double b)
    {
        colorR=r;
        colorG=g;
        colorB=b;

        color.set(r,g,b,0.5);

    }
    void setLightCoeff(double amb,double diff,double spec)
    {
        ambCoeff =amb;
        difCoeff=diff;
        specCoeff=spec;
    }
    void setShinness(double shine)
    {
        shinness=shine;
    }
   	virtual double findIntersection(Ray ray) {
		Vector3 ray_origin = ray.getRayOrigin();
		double ray_origin_x = ray_origin.x;
		double ray_origin_y = ray_origin.y;
		double ray_origin_z = ray_origin.z;

		Vector3 ray_direction = ray.getRayDirection();
		double ray_direction_x = ray_direction.x;
		double ray_direction_y = ray_direction.y;
		double ray_direction_z = ray_direction.z;

		Vector3 sphere_center = center;
		double sphere_center_x = sphere_center.x;
		double sphere_center_y = sphere_center.y;
		double sphere_center_z = sphere_center.z;

		double a = 1; // normalized
		double b = (2*(ray_origin_x - sphere_center_x)*ray_direction_x) + (2*(ray_origin_y - sphere_center_y)*ray_direction_y) + (2*(ray_origin_z - sphere_center_z)*ray_direction_z);
		double c = pow(ray_origin_x - sphere_center_x, 2) + pow(ray_origin_y - sphere_center_y, 2) + pow(ray_origin_z - sphere_center_z, 2) - (radius*radius);

		double discriminant = b*b - 4*c;

		if (discriminant > 0) {
			/// the ray intersects the sphere

			// the first root
			double root_1 = ((-1*b - sqrt(discriminant))/2) - 0.000001;

			if (root_1 > 0) {
				// the first root is the smallest positive root
				return root_1;
			}
			else {
				// the second root is the smallest positive root
				double root_2 = ((sqrt(discriminant) - b)/2) - 0.000001;
				return root_2;
			}
		}
		else {
			// the ray missed the sphere
			return -1;
		}
	}


virtual Vector3 getNormalAt(Vector3 point) {
		// normal always points away from the center of a sphere
		Vector3 normal_Vect = point.vectAdd(center.neg());
		normal_Vect.normalize();
		return normal_Vect;
	}
    Color getColor()
    {
        return color;
    }


};

