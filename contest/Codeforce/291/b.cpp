    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 1000000000
    #define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
    #define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))

int gcd(int a,int b)
{
    if(b==0)return MAX;
    if(a==0)return 0;
    if(a==b)return MAX+1;


    return __gcd(a,b);
}




int main()
{
    ll n,x,y,count=0;
    vector< pair<int,int> >arr;
    vector<bool>flag;

     cin>>n;
    cin>>x>>y;


    flag.resize(n);


    for(int i=0;i<n;i++)
    {
        int p,q;
        cin>>p>>q;
        arr.pb(mp(p,q));
    }



    for(int i=0;i<n;i++)
    {
        if(flag[i])continue;
        flag[i]=true;

        int u,l;
        int z;
        z=gcd(arr[i].second-y,arr[i].first-x);

        if(z!=MAX && z!=0 && z!=MAX+1)
        {u=(arr[i].second-y)/z;
        l=(arr[i].first-x)/z;
        }


        for(int j=i+1;j<n;j++)
        {
            int o;

            o=gcd(arr[j].second-y,arr[j].first-x);
            if(o!=MAX && o!=MAX+1 && o!=0)
            {
                if((arr[j].second-y)/o ==u && (arr[j].first-x)/o ==l)
                {
                    flag[j]=true;
                }
            }
            else if(o==z && (o==MAX||o==MAX+1||o==0))
                {
                    flag[j]=true;
                }


        }
        count++;
    }
    cout<<count<<endl;
    return 0;
}
