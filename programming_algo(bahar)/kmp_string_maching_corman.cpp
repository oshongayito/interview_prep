#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
using namespace std;
vector<long long int> Compute_Prifix_Function(string P)
{
	long long int m=(long long int)P.size();
	vector<long long int> x(m+5);
	x[0]=-1;
	long long int k=-1;
	
	for(long long int q=1;q<m;q++)
	{
		while(k>=0 && P[k+1]!=P[q]) k=x[k];
		if(P[k+1]==P[q]) k++;
		x[q]=k;
	} 
	
	return x;
}

bool KMP_Matcher(string T,string P)
{
	long long int n=(long long int)T.size();
	long long int m=(long long int)P.size();
	vector<long long int> x=Compute_Prifix_Function(P);
	bool flag=0;
	long long int q=-1;
	
	for(long long int i=0;i<n;i++)
	{
		while(q>=0 && P[q+1]!=T[i]) q=x[q];
		if(P[q+1]==T[i]) q++;
		if(q==(m-1))
		{
			printf("Pattern occurs with shift %lld\n",i-m+1);
			flag=1;
			q=x[q];
		} 
	} 
	
	return flag;
}


int main()
{
	return 0;
}

