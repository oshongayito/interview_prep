    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))

vector<bool>vis;

int countbit(int n)
{
    int k;
    for(k=0;n;k++)n/=2;
    return k;
}
int main()
{
    int n,k,i,j,l;
    ll res;
    int mask=0;
    vector<int>arr;

    cin>>n;
    arr.resize(n+1);
    vis.resize(n+1);
    for(i=0;i<n+1;i++)vis[i]=0;
    res=0;
    for(i=n;i>0;i--)
    {
        if(!vis[i]){
        l=countbit(i);
        mask=pow(2,l)-1;
        k=(i^mask);
        arr[k]=i;
        arr[i]=k;
        res+=(k^i);
        //cout<<k<<" "<<i<<endl;
        vis[i]=1;
        vis[k]=1;
        }
    }
    if(!vis[0])arr[0]=0;
    cout<<res*2<<endl;
    for(i=0;i<n+1;i++)
    {
        if(i>0)cout<<" ";
        cout<<arr[i];
    }
    cout<<endl;

    return 0;
}
