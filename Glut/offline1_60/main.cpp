#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include<fstream>
#ifdef _WIN32
#include<windows.h>
#include<GL/glut.h>
#elif _WIN64
#include<windows.h>
#include<GL/glut.h>
#elif __APPLE__
#include<GLUT/glut.h>
#else
#include<GL/glut.h>
#endif
#define MAX_HEIGHT 70

using namespace std;
double const pi=acos(-1);

double cameraHeight;
double cameraAngle;
int drawgrid;
int drawaxes;
double angle;
double radius;
double pos,acce,dir;




struct point
{
	double x,y,z;
	point(){}
	point(double a,double b,double c):x(a),y(b),z(c){}
};


void drawAxes()
{
	if(drawaxes==1)
	{
		glColor3f(0, 0, 1);
		glBegin(GL_LINES);{
			glVertex3f( 100,0,0);
			glVertex3f(-100,0,0);
			glVertex3f(0,-100,0);
			glVertex3f(0, 100,0);
			glVertex3f(0,0, 100);
			glVertex3f(0,0,-100);
		}glEnd();
	}
}


void drawGrid()
{
	int i;
	if(drawgrid==1)
	{
		glColor3f(0.2, 0.3, 0.4);	//grey
		glBegin(GL_LINES);{
			for(i=-8;i<=8;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -90, 0);
				glVertex3f(i*10,  90, 0);

				//lines parallel to X-axis
				glVertex3f(-90, i*10, 0);
				glVertex3f( 90, i*10, 0);
			}
		}glEnd();
	}
}




void keyboardListener(unsigned char key, int x,int y){
	switch(key){

		case '1':
			drawgrid=1-drawgrid;
			break;

		default:
			break;
	}
}


void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			cameraHeight -= 3.0;
			break;
		case GLUT_KEY_UP:		// up arrow key
			cameraHeight += 3.0;
			break;

		case GLUT_KEY_RIGHT:
			cameraAngle += 0.03;
			break;
		case GLUT_KEY_LEFT:
			cameraAngle -= 0.03;
			break;

		case GLUT_KEY_PAGE_UP:
			break;
		case GLUT_KEY_PAGE_DOWN:
			break;

		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
			break;
		case GLUT_KEY_END:
            exit(0);
			break;

		default:
			break;
	}
}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				drawaxes=1-drawaxes;
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}






void drawsphere2(float radius,int slices,int stacks)
{
	struct point points[100][100];
		int i,j;
	double h,r;
	for(i=0;i<=stacks;i++)
	{
		h=radius*sin(((double)i/(double)stacks)*(pi/2))*.5;
		r= radius*cos(((double)i/(double)stacks)*(pi/2))*.55;
		//r=exp(-((h*h)/45))*sqrt(radius*radius-h*h);
        //if(i > (stacks/2))r=(r/h);
		for(j=0;j<=slices;j++)
		{
			points[i][j].x=r*cos(((double)j/(double)slices)*2*pi);
			points[i][j].y=r*sin(((double)j/(double)slices)*2*pi);
			points[i][j].z=h;
		}

	}
	for(i=0;i<stacks;i++)
	{
		for(j=0;j<slices;j++)
		{
			//glColor3f((double)i/(double)stacks,(double)i/(double)stacks,(double)i/(double)stacks);
			//glColor3f(0.6,0.6,0.6);
			 glColor3f(.5,.75,0.5);


			glBegin(GL_QUADS);{

				glVertex3f(points[i][j].x,points[i][j].y,points[i][j].z);
				glVertex3f(points[i][j+1].x,points[i][j+1].y,points[i][j+1].z);
				glVertex3f(points[i+1][j+1].x,points[i+1][j+1].y,points[i+1][j+1].z);
				glVertex3f(points[i+1][j].x,points[i+1][j].y,points[i+1][j].z);

			}glEnd();
		}

	}
}


void draw(){

	glColor3f(0,0,1);
	glTranslatef(0,0,pos);
	//drawSphere();

	drawsphere2(20,100,80);
		glRotatef(180,1,0,0);
		drawsphere2(20,100,80);
}


void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
     / set-up camera here
     ********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?

	//gluLookAt(100,100,100,	0,0,0,	0,0,1);
	gluLookAt(100*cos(cameraAngle), 100*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	//gluLookAt(0,-1,150,	0,0,0,	0,0,1);


	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);

	drawAxes();
	drawGrid();
	//~ glColor3f(1,0,0);
	/****************************
     / Add your objects from here
     ****************************/

	draw();

	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}

void animate(){
	angle+=0.05;
	//~
	//~ pos += dir*0.5*acce*(t*t);
	//~ t-=dir/10;
	//~ if(pos<radius){
		//~ pos = radius;
		//~ dir*=-1;
	//~ }else if(pos>MAX_HEIGHT){
		//~ pos = MAX_HEIGHT;
		//~ dir*=-1;
	//~ }
	//~
	//codes for any changes in Models, Camera
	glutPostRedisplay();
}

void init(){
	//codes for initialization
	drawgrid=0;
	drawaxes=1;
	cameraHeight=100.0;
	cameraAngle=1.0;
	angle=0;

	radius = 5;
	pos = MAX_HEIGHT;
	//~ t=0;
	acce = 9.8;
	dir = -1;
	//clear the screen
	glClearColor(0,0,0,0);

	/************************
     / set-up projection here
     ************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(80,	9./7.,	1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}



void reshapeHandler(int w,int h){
	if(!h)h=1;
	double rat = 1.0*w/h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0,0,w,h);
	gluPerspective(80,rat,1,10000);
	glMatrixMode(GL_MODELVIEW);
}


void bounce(int t){
	//~ double tt=t/100.;
	pos += 0.00001*dir*0.5*acce*(t*t); //0.00001->scaling factor
	t-=dir;
	if(pos<radius*2){
		pos = radius*2;
		dir*=-1;
	}else if(pos>MAX_HEIGHT){
		pos = MAX_HEIGHT;
		dir*=-1;
		t=0;
	}
	glutPostRedisplay();
	//cout<<t<<endl;
	glutTimerFunc(10,bounce,t);
}

int main(int argc, char **argv){
	//~ freopen("in.txt","r",stdin);
	//freopen("out.txt","w",stdout);

	glutInit(&argc,argv);
	glutInitWindowSize(900, 700);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("Bouncing Ball");

	init();

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

    glutReshapeFunc(reshapeHandler);
    glutTimerFunc(20,bounce,0);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
