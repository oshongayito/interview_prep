/*
 * GLUT Shapes Demo
 *
 * Written by Nigel Stewart November 2003
 *
 * This program is test harness for the sphere, cone
 * and torus shapes in GLUT.
 *
 * Spinning wireframe and smooth shaded shapes are
 * displayed until the ESC or q key is pressed.  The
 * number of geometry stacks and slices can be adjusted
 * using the + and - keys.
 */
#include<windows.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>


#define pi 2*acos(0)


#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>

static int slices = 16;
static int stacks = 16;

double cameraHeight;
double cameraAngle;
int drawgrid;
int drawaxes;
double angle;

/* GLUT callback Handlers */

void drawAxes()
{
	if(drawaxes==1)
	{
		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);{
			glVertex3f( 100,0,0);
			glVertex3f(-100,0,0);
			glVertex3f(0,-100,0);
			glVertex3f(0, 100,0);
			glVertex3f(0,0, 100);
			glVertex3f(0,0,-100);
		}glEnd();
	}
}


void drawGrid()
{
	int i;
	if(drawgrid==1)
	{
		glColor3f(0.6, 0.6, 0.6);	//grey
		glBegin(GL_LINES);{
			for(i=-8;i<=8;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -90, 0);
				glVertex3f(i*10,  90, 0);

				//lines parallel to X-axis
				glVertex3f(-90, i*10, 0);
				glVertex3f( 90, i*10, 0);
			}
		}glEnd();
	}
}



static void resize(int width, int height)
{
    const float ar = (float) width / (float) height;

    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glFrustum(-ar, ar, -1.0, 1.0, 2.0, 100.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity() ;
}


//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
void drawInnerCylinder()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.20, 0.60, 0.80);
			glBegin(GL_QUADS);{
				glVertex3f(0.1925,-4.6020,5.9534);
				glVertex3f(0.1925,-4.6020,34.2124);
				glVertex3f(1.6491,-4.0018,34.2124);
				glVertex3f(1.6491,-4.0018,5.9534);
			}glEnd();
			glColor3f(0.80, 0.60, 0.20);
			glBegin(GL_QUADS);{
				glVertex3f(1.6491,-4.0018,5.9534);
				glVertex3f(1.6491,-4.0018,34.2124);
				glVertex3f(3.1056,-3.4016,34.2124);
				glVertex3f(3.1056,-3.4016,5.9534);
			}glEnd();
		}glPopMatrix();

	}
}
void drawPetal_top()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.40, 0.50, 0.60);
			glBegin(GL_POLYGON);{
				glVertex3f(0.1925,-4.6020,34.2124);
				glVertex3f(0.0,0.0,36.3171);
				glVertex3f(3.1056,-3.4016,34.2124);
			}glEnd();
		}glPopMatrix();

	}
}
void drawPetal_0()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.10, 0.20, 0.30);
			glBegin(GL_QUADS);{
				glVertex3f(0.9033,-21.5959,5.9534);
				glVertex3f(0.1925,-4.602,34.2124);
				glVertex3f(3.1056,-3.4016,34.2124);
				glVertex3f(14.5736,-15.9628,5.9534);
			}glEnd();
		}glPopMatrix();

	}
}
void drawPetal_1()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.0, 1.0, 0.0);
			glBegin(GL_POLYGON);{
				glVertex3f(-23.4393,25.5512,13.4945);
				glVertex3f(-14.6279,16.0222,9.5255);
				glVertex3f(-24.9601,13.8534,5.9534);
				//glVertex3f(7.8803,27.5289,5.9686);
			}glEnd();

			glColor3f(.50, 0.00, 0.0);
			glBegin(GL_POLYGON);{
				glVertex3f(-23.4393,25.5512,13.4945);
				glVertex3f(-14.6279,16.0222,9.5255);
				glVertex3f(-11.6585,26.1535,5.9534);
				//glVertex3f(9.2724,27.1032,5.9688);
			}glEnd();
		}glPopMatrix();

	}
}
void drawPetal_2_rightface()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// right part of a petal face


			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8174,17.4753,5.9536);
				glVertex3f(12.8459,17.4527,6.8079);
				glVertex3f(13.2772,17.3175,6.8079);
				glVertex3f(13.2846,17.3378,5.9536);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8459,17.4527,6.8079);
				glVertex3f(12.8121,17.4430,7.975);
				glVertex3f(13.267,17.2897,7.975);
				glVertex3f(13.2772,17.3175,6.8079);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8121,17.4430,7.975);
				glVertex3f(12.7393,17.4329,8.7476);
				glVertex3f(13.2603,17.2713,8.7476);
				glVertex3f(13.267,17.2897,7.975);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.7393,17.4329,8.7476);
				glVertex3f(12.7598,17.4101,9.2870);
				glVertex3f(13.2557,17.2585,9.2870);
				glVertex3f(13.2603,17.2713,8.7476);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.7598,17.4101,9.2870);
				glVertex3f(12.6838,17.3988,9.8639);
				glVertex3f(13.2365,17.2403,9.7977);
				glVertex3f(13.2557,17.2585,9.2870);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.6838,17.3988,9.8639);
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(13.2049,17.2199,10.2509);
				glVertex3f(13.2365,17.2403,9.7977);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(13.1569,17.1888,10.9394);
				glVertex3f(13.2049,17.2199,10.2509);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(12.3406,17.3526,11.8870);
				glVertex3f(13.0403,17.1418,11.9161);
				glVertex3f(13.1569,17.1888,10.9394);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.3406,17.3526,11.8870);
				glVertex3f(12.1210,17.3297,12.8261);
				glVertex3f(12.9117,17.0959,12.8444);
				glVertex3f(13.0403,17.1418,11.9161);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.1210,17.3297,12.8261);
				glVertex3f(11.8872,17.3103,13.6246);
				glVertex3f(12.7962,17.0680,13.4018);
				glVertex3f(12.9117,17.0959,12.8444);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.8872,17.3103,13.6246);
				glVertex3f(11.6185,17.2905,14.4584);
				glVertex3f(12.5751,17.0101,14.4584);
				glVertex3f(12.7962,17.0680,13.4018);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.6185,17.2905,14.4584);
				glVertex3f(11.3391,17.2732,15.1985);
				glVertex3f(12.3584,16.9667,15.2306);
				glVertex3f(12.5751,17.0101,14.4584);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.3391,17.2732,15.1985);
				glVertex3f(11.033,17.2544,15.9525);
				glVertex3f(12.13,16.9209,16.0443);
				glVertex3f(12.3584,16.9667,15.2306);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.033,17.2544,15.9525);
				glVertex3f(10.725,17.2445,16.6159);
				glVertex3f(11.9247,16.8907,16.6267);
				glVertex3f(12.13,16.9209,16.0443);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.725,17.2445,16.6159);
				glVertex3f(10.4384,17.2282,17.2260);
				glVertex3f(11.7518,16.8605,17.1108);
				glVertex3f(11.9247,16.8907,16.6267);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.4384,17.2282,17.2260);
				glVertex3f(10.2728,17.2260,17.5224);
				glVertex3f(11.5788,16.8303,17.5949);
				glVertex3f(11.7518,16.8605,17.1108);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.2728,17.2260,17.5224);
				glVertex3f(9.9836,17.2170,18.0356);
				glVertex3f(11.3529,16.7995,18.1157);
				glVertex3f(11.5788,16.8303,17.5949);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.9836,17.2170,18.0356);
				glVertex3f(9.6249,17.2059,18.6719);
				glVertex3f(11.1117,16.7666,18.6719);
				glVertex3f(11.3529,16.7995,18.1157);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.6249,17.2059,18.6719);
				glVertex3f(9.1190,17.2273,19.3169);
				glVertex3f(10.9246,16.7410,19.1031);
				glVertex3f(11.1117,16.7666,18.6719);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.1190,17.2273,19.3169);
				glVertex3f(8.8102,17.1978,19.8957);
				glVertex3f(10.5571,16.6995,19.8147);
				glVertex3f(10.9246,16.7410,19.1031);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.8102,17.1978,19.8957);
				glVertex3f(8.4037,17.1981,20.4395);
				glVertex3f(10.2850,16.6702,20.343);
				glVertex3f(10.5571,16.6995,19.8147);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4037,17.1981,20.4395);
				glVertex3f(8.3079,17.1987,20.5624);
				glVertex3f(10.1709,16.6558,20.5624);
				glVertex3f(10.2850,16.6702,20.343);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.3079,17.1987,20.5624);
				glVertex3f(7.9858,17.2008,20.9756);
				glVertex3f(9.8755,16.6287,21.0507);
				glVertex3f(10.1709,16.6558,20.5624);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9858,17.2008,20.9756);
				glVertex3f(7.688,17.2013,21.3566);
				glVertex3f(9.5838,16.6019,21.533);
				glVertex3f(9.8755,16.6287,21.0507);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.688,17.2013,21.3566);
				glVertex3f(7.1801,17.2254,21.8695);
				glVertex3f(9.3215,16.5778,21.9667);
				glVertex3f(9.5838,16.6019,21.533);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1801,17.2254,21.8695);
				glVertex3f(6.6392,17.2254,22.5136);
				glVertex3f(8.9177,16.5488,22.5437);
				glVertex3f(9.3215,16.5778,21.9667);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.6392,17.2254,22.5136);
				glVertex3f(6.2435,17.2407,22.9001);
				glVertex3f(8.6841,16.5320,22.8774);
				glVertex3f(8.9177,16.5488,22.5437);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.2435,17.2407,22.9001);
				glVertex3f(5.9082,17.2548,23.2197);
				glVertex3f(8.5380,16.5215,23.0861);
				glVertex3f(8.6841,16.5320,22.8774);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.9082,17.2548,23.2197);
				glVertex3f(5.3810,17.2755,23.7211);
				glVertex3f(7.8324,16.4827,23.9940);
				glVertex3f(8.5380,16.5215,23.0861);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3810,17.2755,23.7211);
				glVertex3f(5.3383,17.1581,24.1537);
				glVertex3f(7.5229,16.4659,24.3786);
				glVertex3f(7.8324,16.4827,23.9940);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3383,17.1581,24.1537);
				glVertex3f(5.2503,16.9163,25.0453);
				glVertex3f(6.7827,16.4441,25.1717);
				glVertex3f(7.5229,16.4659,24.3786);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2503,16.9163,25.0453);
				glVertex3f(5.1994,16.7765,25.5605);
				glVertex3f(6.2472,16.4209,25.7944);
				glVertex3f(6.7827,16.4441,25.1717);
			}glEnd();


			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.1994,16.7765,25.5605);
				glVertex3f(5.1348,16.599,26.2148);
				glVertex3f(5.7332,16.4209,26.2434);
				glVertex3f(6.2472,16.4209,25.7944);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.1348,16.599,26.2148);
				glVertex3f(5.1025,16.5103,26.5420);
				glVertex3f(5.381,16.4131,26.6131);
				glVertex3f(5.7332,16.4209,26.2434);
			}glEnd();


			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(5.1025,16.5103,26.5420);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.381,16.4131,26.6131);
			}glEnd();

			//------------------- face of a petal end

		}glPopMatrix();
	}
}
void drawPetal_2_leftface()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left part of a petal face

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9067,21.6765,5.9534);
				glVertex3f(-0.9177,21.6462,7.2216);
				glVertex3f(-0.4616,21.5103,7.2216);
				glVertex3f(-0.4435,21.5295,5.9534);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9177,21.6462,7.2216);
				glVertex3f(-0.9375,21.5919,9.4936);
				glVertex3f(-0.3556,21.4027,9.5259);
				glVertex3f(-0.4616,21.5103,7.2216);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9375,21.5919,9.4936);
				glVertex3f(-0.8943,21.4690,11.1525);
				glVertex3f(-0.159,21.2391,11.0836);
				glVertex3f(-0.3556,21.4027,9.5259);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.8943,21.4690,11.1525);
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.0361,21.0767,12.5058);
				glVertex3f(-0.159,21.2391,11.0836);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(0.3133,20.7896,14.0929);
				glVertex3f(-0.0361,21.0767,12.5058);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(0.76,20.4386,15.6422);
				glVertex3f(0.3133,20.7896,14.0929);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(1.4338,19.9352,17.4426);
				glVertex3f(0.76,20.4386,15.6422);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(2.4082,19.2361,19.4869);
				glVertex3f(1.4338,19.9352,17.4426);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(3.3408,18.6234,20.9393);
				glVertex3f(2.4082,19.2361,19.4869);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(4.3924,17.8982,22.5534);
				glVertex3f(3.3408,18.6234,20.9393);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(5.381,17.2755,23.7211);
				glVertex3f(4.3924,17.8982,22.5534);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(5.3013,17.0566,24.5281);
				glVertex3f(5.381,17.2755,23.7211);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(4.129,17.0735,25.7615);
				glVertex3f(5.1796,16.722,25.7615);
				glVertex3f(5.3013,17.0566,24.5281);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(4.129,17.0735,25.7615);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.1796,16.722,25.7615);
			}glEnd();
			//------------------- face of a petal end

		}glPopMatrix();
	}
}
void drawPetal_2_rightback()
{
	for(int i=0; i<1; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left back part of a petal face

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.7677,27.9475,8.183);
				glVertex3f(8.0675,27.7653,6.9621);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.0675,27.7653,6.9621);
				glVertex3f(7.8803,27.5289,5.9686);
			}glEnd();
			// col-1
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.7677,27.9475,8.1830);
				glVertex3f(8.6470,27.5694,10.3582);
				glVertex3f(8.4380,27.5656,10.0903);
				glVertex3f(8.640,27.93310,8.0288);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.6470,27.5694,10.3582);
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(8.4380,27.5656,10.0903);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(7.8917,26.4047,14.0401);
				glVertex3f(8.1879,27.0545,12.0989);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(7.9870,25.5183,16.5042);
				glVertex3f(7.5515,25.6224,15.9003);
				glVertex3f(7.8917,26.4047,14.0401);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9870,25.5183,16.5042);
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.1700,24.7146,17.667);
				glVertex3f(7.5515,25.6224,15.9003);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(6.7501,23.6892,19.3286);
				glVertex3f(7.1700,24.7146,17.667);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(6.3055,22.22534,20.9061);
				glVertex3f(6.7501,23.6892,19.3286);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(5.7484,21.2735,22.3183);
				glVertex3f(6.3055,22.22534,20.9061);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(5.2319,19.9499,23.6042);
				glVertex3f(5.7484,21.2735,22.3183);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(4.6909,18.5466,24.7533);
				glVertex3f(5.2319,19.9499,23.6042);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(4.1290,17.0735,25.7615);
				glVertex3f(4.6909,18.5466,24.7533);
			}glEnd();
			//col=2
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.640,27.93310,8.0288);
				glVertex3f(8.4380,27.5656,10.0903);

				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(8.5208,27.9148,7.8667);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4380,27.5656,10.0903);
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(7.9210,27.0652,11.6987);
				glVertex3f(8.2435,27.5553,9.8093);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(7.8917,26.4047,14.0401);

				glVertex3f(7.5553,26.4491,13.5223);
				glVertex3f(7.9210,27.0652,11.6987);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.8917,26.4047,14.0401);
				glVertex3f(7.5515,25.6224,15.9003);

				glVertex3f(7.1486,25.7127,15.2679);
				glVertex3f(7.5553,26.4491,13.5223);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.5515,25.6224,15.9003);
				glVertex3f(7.1700,24.7146,17.667);


				glVertex3f(6.7034,24.8623,16.9246);
				glVertex3f(7.1486,25.7127,15.2679);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1700,24.7146,17.667);
				glVertex3f(6.7501,23.6892,19.3286);


				glVertex3f(6.2225,23.9053,18.4823);
				glVertex3f(6.7034,24.8623,16.9246);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7501,23.6892,19.3286);
				glVertex3f(6.3055,22.22534,20.9061);


				glVertex3f(5.5906,22.7548,19.9831);
				glVertex3f(6.2225,23.9053,18.4823);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.3055,22.22534,20.9061);
				glVertex3f(5.7484,21.2735,22.3183);


				glVertex3f(5.0451,21.6080,21.3062);
				glVertex3f(5.5906,22.7548,19.9831);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.7484,21.2735,22.3183);
				glVertex3f(5.2319,19.9499,23.6042);


				glVertex3f(4.4737,20.3799,22.5077);
				glVertex3f(5.0451,21.6080,21.3062);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2319,19.9499,23.6042);
				glVertex3f(4.6909,18.5466,24.7533);


				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(4.4737,20.3799,22.5077);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.6909,18.5466,24.7533);
				glVertex3f(4.1290,17.0735,25.7615);


				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(3.8798,19.0793,23.5829);
			}glEnd();

			//col=3
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.5208,27.9148,7.8667);

				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(8.4106,27.8925,7.6974);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(7.9210,27.0652,11.6987);
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(8.0643,27.5384,9.5163);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9210,27.0652,11.6987);

				glVertex3f(7.5553,26.4491,13.5223);
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(7.6757,27.0666,11.2820);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.5553,26.4491,13.5223);

				glVertex3f(7.1486,25.7127,15.2679);
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(7.2468,26.4815,12.9832);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1486,25.7127,15.2679);


				glVertex3f(6.7034,24.8623,16.9246);
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(6.7797,25.7882,14.6096);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7034,24.8623,16.9246);


				glVertex3f(6.2225,23.9053,18.4823);
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(6.277,24.9925,16.1515);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.2225,23.9053,18.4823);


				glVertex3f(5.5906,22.7548,19.9831);
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(5.7412,24.1011,17.6003);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5906,22.7548,19.9831);


				glVertex3f(5.0451,21.6080,21.3062);
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(4.9956,22.9798,19.0224);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.0451,21.6080,21.3062);


				glVertex3f(4.4737,20.3799,22.5077);
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(4.3989,21.9176,20.2465);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.4737,20.3799,22.5077);


				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(3.7788,20.7825,21.3583);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(3.1383,19.5823,22.3538);

			}glEnd();

			//col=4
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4106,27.8925,7.6974);
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(8.3098,27.8663,7.5216);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(7.9010,27.5149,9.2126);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(7.4528,27.0586,10.8504);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(6.9671,26.5013,12.4251);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(6.4461,25.848,13.928);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(5.8921,25.1039,15.3507);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(5.3077,24.2747,16.6861);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(4.4532,23.1801,18.0232);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(3.8117,22.1992,19.1435);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(3.1491,21.1539,20.1604);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(2.4683,20.0510,21.0708);
			}glEnd();

			//col=5

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.3098,27.8663,7.5216);
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(8.2188,27.8363,7.3399);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(7.7544,27.4847,8.8995);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(7.2532,27.0408,10.4057);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(6.7173,26.5082,11.8504);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(6.1488,25.8913,13.226);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(5.5499,25.1950,14.5257);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(4.9232,24.4244,15.7434);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(3.9652,23.3534,16.9896);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(3.2853,22.4499,18.0016);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(2.5865,21.4904,18.9188);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(1.8716,20.4809,19.7386);
			}glEnd();

			//col=6
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.2188,27.8363,7.3399);
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(8.1379,27.8026,7.1532);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(7.0775,27.0131,9.9497);

				glVertex3f(7.6248,27.4479,8.5781);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(6.4982,26.5017,11.2612);

				glVertex3f(7.0775,27.0131,9.9497);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(5.8888,25.9175,12.5063);

				glVertex3f(6.4982,26.5017,11.2612);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(5.2517,25.265,13.6796);

				glVertex3f(5.8888,25.9175,12.5063);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(4.5891,24.5486,14.7761);

				glVertex3f(5.2517,25.265,13.6796);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(3.533,23.4977,15.9259);

				glVertex3f(4.5891,24.5486,14.7761);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(2.821,22.667,16.8254);

				glVertex3f(3.533,23.4977,15.9259);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(2.0924,21.7887,17.6382);

				glVertex3f(2.821,22.667,16.8254);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(1.3108,20.8513,18.3386);

				glVertex3f(2.0924,21.7887,17.6382);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(0.5953,19.9095,18.9972);

				glVertex3f(1.3108,20.8513,18.3386);
			}glEnd();


			//col=7
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.1379,27.8026,7.1532);
				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(8.0675,27.7653,6.9621);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(7.0775,27.0131,9.9497);
				glVertex3f(6.9265,26.9755,9.4844);

				glVertex3f(7.5128,27.4045,8.2499);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.0775,27.0131,9.9497);
				glVertex3f(6.4982,26.5017,11.2612);
				glVertex3f(6.3105,26.4815,10.6601);

				glVertex3f(6.9265,26.9755,9.4844);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.4982,26.5017,11.2612);
				glVertex3f(5.8888,25.9175,12.5063);
				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(6.3105,26.4815,10.6601);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(5.8888,25.9175,12.5063);
				glVertex3f(5.2517,25.265,13.6796);
				glVertex3f(4.9982,25.3128,12.816);

				glVertex3f(5.6671,25.926,11.7721);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2517,25.265,13.6796);

				glVertex3f(4.5891,24.5486,14.7761);
				glVertex3f(4.3063,24.646,13.7879);

				glVertex3f(4.9982,25.3128,12.816);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(4.5891,24.5486,14.7761);
				glVertex3f(3.533,23.4977,15.9259);
				glVertex3f(3.158,23.6113,14.8363);

				glVertex3f(4.3063,24.646,13.7879);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(3.533,23.4977,15.9259);
				glVertex3f(2.821,22.667,16.8254);
				glVertex3f(2.4203,22.8482,15.6197);

				glVertex3f(3.158,23.6113,14.8363);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.821,22.667,16.8254);
				glVertex3f(2.0924,21.7887,17.6382);
				glVertex3f(1.6681,22.0457,16.3239);

				glVertex3f(2.4203,22.8482,15.6197);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(2.0924,21.7887,17.6382);
				glVertex3f(1.3108,20.8513,18.3386);
				glVertex3f(0.9037,21.2082,16.9479);

				glVertex3f(1.6681,22.0457,16.3239);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.3108,20.8513,18.3386);
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(0.1295,20.3398,17.491);

				glVertex3f(0.9037,21.2082,16.9479);
			}glEnd();


			//col=8
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.0675,27.7653,6.9621);
				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(7.4188,27.3546,7.916);
				glVertex3f(8.0078,27.7244,6.7674);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(6.9265,26.9755,9.4844);
				glVertex3f(6.8006,26.928,9.0114);

				glVertex3f(7.4188,27.3546,7.916);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.9265,26.9755,9.4844);
				glVertex3f(6.3105,26.4815,10.6601);
				glVertex3f(6.155,26.4475,10.0494);

				glVertex3f(6.8006,26.928,9.0114);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.3105,26.4815,10.6601);
				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(5.4843,25.9163,11.0261);

				glVertex3f(6.155,26.4475,10.0494);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(4.9982,25.3128,12.816);
				glVertex3f(4.7904,25.3377,11.9382);

				glVertex3f(5.4843,25.9163,11.0261);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9982,25.3128,12.816);
				glVertex3f(4.3063,24.646,13.7879);
				glVertex3f(4.0757,24.7154,12.783);

				glVertex3f(4.7904,25.3377,11.9382);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(4.3063,24.646,13.7879);
				glVertex3f(3.158,23.6113,14.8363);
				glVertex3f(2.8411,23.6927,13.7254);

				glVertex3f(4.0757,24.7154,12.783);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(3.158,23.6113,14.8363);
				glVertex3f(2.4203,22.8482,15.6197);
				glVertex3f(2.0842,22.9914,14.3894);

				glVertex3f(2.8411,23.6927,13.7254);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4203,22.8482,15.6197);
				glVertex3f(1.6681,22.0457,16.3239);
				glVertex3f(1.3147,22.2589,14.9813);

				glVertex3f(2.0842,22.9914,14.3894);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.6681,22.0457,16.3239);
				glVertex3f(0.9037,21.2082,16.9479);
				glVertex3f(0.5348,21.4986,15.5005);

				glVertex3f(1.3147,22.2589,14.9813);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.9037,21.2082,16.9479);
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(-0.2531,20.7141,15.947);

				glVertex3f(0.5348,21.4986,15.5005);

			}glEnd();


			//col=9
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.0078,27.7244,6.7674);
				glVertex3f(7.4188,27.3546,7.916);
				glVertex3f(7.3432,27.2984,7.5779);
				glVertex3f(7.9591,27.6802,6.5699);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.4188,27.3546,7.916);
				glVertex3f(6.8006,26.928,9.0114);
				glVertex3f(6.7003,26.8706,8.5328);

				glVertex3f(7.3432,27.2984,7.5779);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.8006,26.928,9.0114);
				glVertex3f(6.155,26.4475,10.0494);
				glVertex3f(6.0322,26.3996,9.4316);

				glVertex3f(6.7003,26.8706,8.5328);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(6.155,26.4475,10.0494);
				glVertex3f(5.4843,25.9163,11.0261);

				glVertex3f(5.3411,25.8882,10.2714);

				glVertex3f(6.0322,26.3996,9.4316);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.4843,25.9163,11.0261);
				glVertex3f(4.7904,25.3377,11.9382);
				glVertex3f(4.629,25.3393,11.05);

				glVertex3f(5.3411,25.8882,10.2714);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.7904,25.3377,11.9382);
				glVertex3f(4.0757,24.7154,12.783);
				glVertex3f(3.898,24.7559,11.7653);

				glVertex3f(4.629,25.3393,11.05);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(4.0757,24.7154,12.783);
				glVertex3f(2.8411,23.6927,13.7254);
				glVertex3f(2.5835,23.7406,12.5976);

				glVertex3f(3.898,24.7559,11.7653);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(2.8411,23.6927,13.7254);
				glVertex3f(2.0842,22.9914,14.3894);
				glVertex3f(1.8137,23.0951,13.1395);

				glVertex3f(2.5835,23.7406,12.5976);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

					glVertex3f(2.0842,22.9914,14.3894);

					glVertex3f(1.3147,22.2589,14.9813);


				glVertex3f(1.0331,22.4261,13.6157);

				glVertex3f(1.8137,23.0951,13.1395);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.3147,22.2589,14.9813);
				glVertex3f(0.5348,21.4986,15.5005);
				glVertex3f(0.244,21.7364,14.0263);

				glVertex3f(1.0331,22.4261,13.6157);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.5348,21.4986,15.5005);
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(-0.5516,21.0288,14.3715);

				glVertex3f(0.244,21.7364,14.0263);

			}glEnd();

			//col=10
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9591,27.6802,6.5699);
				glVertex3f(7.3432,27.2984,7.5779);
				glVertex3f(7.2862,27.236,7.2368);
				glVertex3f(7.9215,27.6327,6.3704);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.3432,27.2984,7.5779);
				glVertex3f(6.7003,26.8706,8.5328);
				glVertex3f(6.6259,26.8035,8.0505);

				glVertex3f(7.2862,27.236,7.2368);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7003,26.8706,8.5328);
				glVertex3f(6.0322,26.3996,9.4316);
				glVertex3f(5.9425,26.3379,8.8092);

				glVertex3f(6.6259,26.8035,8.0505);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

					glVertex3f(6.0322,26.3996,9.4316);

				glVertex3f(5.3411,25.8882,10.2714);
				glVertex3f(5.2379,25.8415,9.511);

				glVertex3f(5.9425,26.3379,8.8092);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.3411,25.8882,10.2714);
				glVertex3f(4.629,25.3393,11.05);
				glVertex3f(4.5143,25.3171,10.1547);

				glVertex3f(5.2379,25.8415,9.511);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.629,25.3393,11.05);
				glVertex3f(3.898,24.7559,11.7653);
				glVertex3f(3.7737,24.7671,10.7389);
				glVertex3f(4.5143,25.3171,10.1547);



			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(3.898,24.7559,11.7653);
				glVertex3f(2.5835,23.7406,12.5976);
				glVertex3f(2.3858,23.7543,11.4575);

				glVertex3f(3.7737,24.7671,10.7389);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{



				glVertex3f(2.5835,23.7406,12.5976);
				glVertex3f(1.8137,23.0951,13.1395);
				glVertex3f(1.6095,23.158,11.8751);

				glVertex3f(2.3858,23.7543,11.4575);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.8137,23.0951,13.1395);


				glVertex3f(1.0331,22.4261,13.6157);
				glVertex3f(0.8241,22.5455,12.2329);

				glVertex3f(1.6095,23.158,11.8751);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.0331,22.4261,13.6157);
				glVertex3f(0.244,21.7364,14.0263);
				glVertex3f(0.0318,21.9191,12.5313);

				glVertex3f(0.8241,22.5455,12.2329);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(0.244,21.7364,14.0263);
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(-0.7655,21.2812,12.7711);

				glVertex3f(0.0318,21.9191,12.5313);

			}glEnd();

			//col=11
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9215,27.6327,6.3704);
				glVertex3f(7.2862,27.236,7.2368);
				glVertex3f(7.2481,27.1676,6.8942);
				glVertex3f(7.8952,27.5822,6.1697);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.2862,27.236,7.2368);
				glVertex3f(6.6259,26.8035,8.0505);
				glVertex3f(6.5778,26.7269,7.5662);

				glVertex3f(7.2481,27.1676,6.8942);




			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.6259,26.8035,8.0505);
				glVertex3f(5.9425,26.3379,8.8092);
				glVertex3f(5.8862,26.2624,8.1845);

				glVertex3f(6.5778,26.7269,7.5662);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(5.9425,26.3379,8.8092);
				glVertex3f(5.2379,25.8415,9.511);
				glVertex3f(5.1752,25.7764,8.748);

				glVertex3f(5.8862,26.2624,8.1845);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.2379,25.8415,9.511);
				glVertex3f(4.5143,25.3171,10.1547);
				glVertex3f(4.4469,25.2711,9.2559);

				glVertex3f(5.1752,25.7764,8.748);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.5143,25.3171,10.1547);
				glVertex3f(3.7737,24.7671,10.7389);
				glVertex3f(3.7032,24.7486,9.708);

				glVertex3f(4.4469,25.2711,9.2559);



			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(3.7737,24.7671,10.7389);
				glVertex3f(2.3858,23.7543,11.4575);
				glVertex3f(2.2486,23.7331,10.3098);

				glVertex3f(3.7032,24.7486,9.708);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{



				glVertex3f(2.3858,23.7543,11.4575);
				glVertex3f(1.6095,23.158,11.8751);
				glVertex3f(1.4722,23.1791,10.6014);

				glVertex3f(2.2486,23.7331,10.3098);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.6095,23.158,11.8751);
				glVertex3f(0.8241,22.5455,12.2329);
				glVertex3f(0.6882,22.6159,10.8385);

				glVertex3f(1.4722,23.1791,10.6014);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.8241,22.5455,12.2329);
				glVertex3f(0.0318,21.9191,12.5313);
				glVertex3f(-0.1012,22.0452,11.0218);

				glVertex3f(0.6882,22.6159,10.8385);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(0.0318,21.9191,12.5313);
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.8943,21.469,11.1525);

				glVertex3f(-0.1012,22.0452,11.0218);


			}glEnd();

			//col=12
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.8952,27.5822,6.1697);
				glVertex3f(7.2481,27.1676,6.8942);
				glVertex3f(7.229,27.0935,6.5513);
				glVertex3f(7.8803,27.5289,5.9686);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.2481,27.1676,6.8942);
				glVertex3f(6.5778,26.7269,7.5662);
				glVertex3f(6.5561,26.641,7.082);

				glVertex3f(7.229,27.0935,6.5513);




			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.5778,26.7269,7.5662);
				glVertex3f(5.8862,26.2624,8.1845);
				glVertex3f(5.8635,26.1736,7.56);

				glVertex3f(6.5561,26.641,7.082);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(5.8862,26.2624,8.1845);
				glVertex3f(5.1752,25.7764,8.748);
				glVertex3f(5.1531,25.693,7.9851);

				glVertex3f(5.8635,26.1736,7.56);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.1752,25.7764,8.748);
				glVertex3f(4.4469,25.2711,9.2559);
				glVertex3f(4.4269,25.2013,8.3573);

				glVertex3f(5.1531,25.693,7.9851);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.4469,25.2711,9.2559);
				glVertex3f(3.7032,24.7486,9.708);
				glVertex3f(3.6867,24.7002,8.6767);

				glVertex3f(4.4269,25.2013,8.3573);



			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(3.7032,24.7486,9.708);
				glVertex3f(2.2486,23.7331,10.3098);
				glVertex3f(2.1725,23.6769,9.1591);

				glVertex3f(3.6867,24.7002,8.6767);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{



				glVertex3f(2.2486,23.7331,10.3098);
				glVertex3f(1.4722,23.1791,10.6014);
				glVertex3f(1.4022,23.1581,9.3236);

				glVertex3f(2.1725,23.6769,9.1591);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.4722,23.1791,10.6014);
				glVertex3f(0.6882,22.6159,10.8385);
				glVertex3f(0.6258,22.6364,9.4382);

				glVertex3f(1.4022,23.1581,9.3236);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.6882,22.6159,10.8385);
				glVertex3f(-0.1012,22.0452,11.0218);
				glVertex3f(-0.1427,22.1202,9.5215);
				glVertex3f(0.6258,22.6364,9.4382);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(-0.1012,22.0452,11.0218);
				glVertex3f(-0.8943,21.469,11.1525);
				glVertex3f(-0.938,21.5905,9.5226);

				glVertex3f(-0.1427,22.1202,9.5215);


			}glEnd();

			//col=13

			//------------------- face of a petal end

		}glPopMatrix();
	}
}
void drawPetal_2()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			drawPetal_2_leftface();
			drawPetal_2_rightface();
			drawPetal_2_rightback();
			/*
			glPushMatrix();{
				glRotatef(10,1,0,0);
				glRotatef(180,0,0,1);
				glTranslatef(-5.0646*2,-16.4061*2,0.0);
				drawPetal_2_face();
			}glPopMatrix();
			drawPetal_2_face();
			*/
			//------------------- face of a petal end

			//------------------- back part of a petal
			/*
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{// right part of a single petal
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(-0.938,21.5905,9.5226);
				glVertex3f(7.8803,27.5289,5.9686);
			}glEnd();
			*/
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{// left part of a single petal
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(13.2529,17.2509,9.5625);
				glVertex3f(9.2724,27.1032,5.9688);
			}glEnd();
			//------------------- back part of a petal

		}glPopMatrix();
	}
}
void drawPetal_3()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
				glVertex3f(20.2079,-2.698,13.69322);
				glVertex3f(4.5658,-0.6096,36.9101);
				glVertex3f(20.892,5.9092,5.972);
				glVertex3f(21.1266,4.1837,5.9716);
			}glEnd();

			glColor3f(0.50, 0.50, 0.50);
			glBegin(GL_QUADS);{
				glVertex3f(20.2079,-2.698,13.69322);
				glVertex3f(4.5658,-0.6096,36.9101);
				glVertex3f(18.8301,-10.8843,5.972);
				glVertex3f(19.4355,-9.5892,5.972);
			}glEnd();
		}glPopMatrix();

	}
}
void drawUpperPetals()
{
	drawInnerCylinder();
	drawPetal_top();
	drawPetal_0();
	drawPetal_3();
	drawPetal_2();
	drawPetal_1();
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^





static void display(void)
{
 //clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?

	//gluLookAt(100,100,100,	0,0,0,	0,0,1);
	gluLookAt(100*cos(cameraAngle), 100*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	//gluLookAt(0,-1,150,	0,0,0,	0,0,1);


	//again select MODEL-VIEW
	glMatrixMode(GL_MODELVIEW);


	/****************************
	/ Add your objects from here
	****************************/
	//add objects

	drawAxes();
	drawGrid();
	glColor3f(1,0,0);


	//drawsphere(20,20,20);
	//glRotatef(180,1,0,0);
	//drawsphere(20,20,20);


	//drawss();


	//clipping example
     /*
	double coef[4];

	coef[0] = 0;	//-1.x
	coef[1] = 0;	//0.y
	coef[2] = -1;	//0.z
	coef[3] = 57;	//10
	//standard format:: ax + by + cz + d >= 0
	/// the cutting plane equation: x = 10
	/// we will keep the points with
	//		x <= 10
	//OR	-1.x + 0.y + 1.z + 10 >= 0


	glClipPlane(GL_CLIP_PLANE0,coef);


	//now we enable the clip plane

	glEnable(GL_CLIP_PLANE0);{
		drawsphere(20,100,80);
		glRotatef(180,1,0,0);
		drawsphere(20,100,80);
	}glDisable(GL_CLIP_PLANE0);
*/

    drawUpperPetals();
    //ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glutSwapBuffers();
}


static void key(unsigned char key, int x, int y)
{
    switch (key)
    {
        case 27 :
        case 'q':
            exit(0);
            break;

        case '+':
            slices++;
            stacks++;
            break;

        case '-':
            if (slices>3 && stacks>3)
            {
                slices--;
                stacks--;
            }
            break;
    }

    glutPostRedisplay();
}


void keyboardListener(unsigned char key, int x,int y){
	switch(key){

		case '1':
			drawgrid=1-drawgrid;
			break;

		default:
			break;
	}
}


void specialKeyListener(int key, int x,int y){
	switch(key){
		case GLUT_KEY_DOWN:		//down arrow key
			cameraHeight -= 3.0;
			break;
		case GLUT_KEY_UP:		// up arrow key
			cameraHeight += 3.0;
			break;

		case GLUT_KEY_RIGHT:
			cameraAngle += 0.03;
			break;
		case GLUT_KEY_LEFT:
			cameraAngle -= 0.03;
			break;

		case GLUT_KEY_PAGE_UP:
			break;
		case GLUT_KEY_PAGE_DOWN:
			break;

		case GLUT_KEY_INSERT:
			break;

		case GLUT_KEY_HOME:
			break;
		case GLUT_KEY_END:
			break;

		default:
			break;
	}
}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				drawaxes=1-drawaxes;
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}



static void idle(void)
{
    glutPostRedisplay();
}

const GLfloat light_ambient[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
const GLfloat light_diffuse[]  = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat light_position[] = { 2.0f, 5.0f, 5.0f, 0.0f };

const GLfloat mat_ambient[]    = { 0.7f, 0.7f, 0.7f, 1.0f };
const GLfloat mat_diffuse[]    = { 0.8f, 0.8f, 0.8f, 1.0f };
const GLfloat mat_specular[]   = { 1.0f, 1.0f, 1.0f, 1.0f };
const GLfloat high_shininess[] = { 100.0f };

/* Program entry point */

int main(int argc, char *argv[])
{
    glutInit(&argc, argv);
    glutInitWindowSize(640,480);
    glutInitWindowPosition(10,10);
    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);

    glutCreateWindow("GLUT Shapes");

    glutReshapeFunc(resize);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutIdleFunc(idle);

    glClearColor(1,1,1,1);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE);
    glEnable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);

    glLightfv(GL_LIGHT0, GL_AMBIENT,  light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE,  light_diffuse);
    glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);

    glMaterialfv(GL_FRONT, GL_AMBIENT,   mat_ambient);
    glMaterialfv(GL_FRONT, GL_DIFFUSE,   mat_diffuse);
    glMaterialfv(GL_FRONT, GL_SPECULAR,  mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, high_shininess);

    glutMainLoop();

    return EXIT_SUCCESS;
}
