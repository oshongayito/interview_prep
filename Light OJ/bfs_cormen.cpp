#include<iostream>
#include<vector>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<queue>

#define black 1
#define white 0

using namespace std;


bool graph[20001][20001];
bool vis[20001];
int dis[20001];
int edges;

void bfs(int s)
{
    queue <int > Q;
    int i,j,k,l,a,b,u,v;

    Q.push(s);

    while(!Q.empty())
    {
        u = Q.front();
        Q.pop();

        for(i=0;i<20000;i++)
        {
            if(graph[u][i]==1 && vis[i]==white)
            {
                Q.push(i);
                dis[i]=dis[u]+1;
            }
        }
        vis[u]=black;
    }

}



int main()
{
    int i,x,y;
    cin>>edges;
    for(i=1;i<=edges;i++)
    {
        cin>>x>>y;
        graph[x][y]=1;
    }
    bfs(1);


    return 0;
}
