#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#define pi (2*acos(0.0))

using namespace std;

int  main()
{
	int t,i;
	double n,a,b,c,B,A,h,area,res,rat,R,r,bd,bg;
	cin>>t;
	for(i=1;i<=t;i++)
	{
		cin>>R>>n;
		
		B = 360/n;
		A = (180-B)/2 ;
		b = (R*sin((B*pi)/180))/(sin((A*pi)/180));
		c = R;
		a = R;
		h = c * sin(((180-90-A)*pi)/180);
		res = R/((R/h)+1)+0.0000000001;
		
		printf("Case %d: %.10lf\n",i,res);
	}
	return 0;
}

