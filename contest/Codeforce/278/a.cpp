    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))

bool check_luck(ll x)
{

    while(x)
    {

        if(x>0 && x%10==8)return true;
        else if(x<0 && x%10 == -8)return true;
        x/=10;

    }
    return false;
}

int main()
{
    ll n,i;

    cin>>n;

    for(i=1;!check_luck(n+1);i++)
    {
        n++;

    }


    cout<<i<<endl;

    return 0;
}
