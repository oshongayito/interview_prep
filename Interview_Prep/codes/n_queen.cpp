#include<iostream>
#include<vector>
#include<string>
using namespace std;

void visit(int x,int y,vector<vector<bool> >&visited, int n){
    for(int i=x+1, j=1, k=1;i<n;i++,j++,k++){
        visited[i][y] = true;
        if( y-j >=0)visited[i][y-j] = true;
        if(y+k <n)visited[i][y+k] = true;
    }
}


void isValid(int x, int y, vector<vector<bool> >visited, int n, vector<string>curRes, vector<vector<string> >&res){
    if(visited[x][y]){
//        if(x == 3 && y==2)cout<<"wronggg"<<endl;
        return;
    }
    curRes[x][y]='Q';
//    cout<<"x is "<<x<<" pos is: "<<curRes[x]<<endl;

    //found a solution
    if(x == n-1){
        res.push_back(curRes);
        return;
    }
    visit(x, y, visited, n);
    for(int i=0;i<n;i++){
        isValid(x+1, i, visited, n, curRes, res);
    }
    return;
}


vector<vector<string> > solveNQueens(int A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details
    vector<vector<bool> >visited;
    string dummy = "";
    for(int i=0;i<A;i++)dummy+=".";
    vector<string>dummyVec;
    dummyVec.resize(A,dummy);
    visited.resize(A);
    for(int i=0;i<A;i++){
        visited[i].resize(A,false);
    }
    vector<vector<string> > res;

    for(int i=0;i<A;i++){
        isValid(0,i, visited, A, dummyVec, res);
    }
    return res;
}


int main(){
    vector<vector<string> >res = solveNQueens(4);
    for(int i=0;i<res.size();i++){
        for(int j=0;j<res[i].size();j++){
            cout<<res[i][j]<<" ";
        }
        cout<<endl;
    }
    return 0;
}
/*
[".Q..",  // Solution 1
"...Q",
"Q...",
"..Q."],
 */