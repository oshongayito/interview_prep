    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000LL

#define false 0
#define true 1
#define ROW 201
#define COL 201
#define infi 50000000  //infi for infinityc



/////////////////////////////////////////////////////////////////////



void read_input(void);
void initialize(void);
void floyd_warshall(void);
void print_path (long i, long j);

FILE *fp;

long n,e,d[50][50],w[50][50],p[50][50];
int g[ROW][COL];

void read_input(void)
{
long i,j,u,v,wg;
//clrscr();
fp=fopen("fldwarsh.txt","r");
fscanf(fp,"%ld%ld",&n,&e);
for(i=0;i<n;++i)
       for(j=0;j<n;++j)
		{
		w[i][j]=32767;
		p[i][j]=-1;
		}

for(i=1;i<=e;++i)
	{
	fscanf(fp,"%ld%ld%ld",&u,&v,&wg);
	w[u][v]=wg;
	w[v][u]=wg;
	}
}

void initialize(void)
{

 int i,j;
for(i=0;i<n;++i)
       for(j=0;j<n;++j)
		{
		w[i][j]=g[i][j];
		p[i][j]=-1;
		}

for (i=0; i<n; i++)
	{
	for (j=0; j<n; j++)
		{
		d[i][j] = w[i][j];
		p[i][j] = i;
		}
	}
for (i=0; i<n; i++)
  d[i][i] = 0;

}


int min(int x, int y)
{
if(x<y)
	return x;
else
	return y;
}

void floyd_warshall(void)
{
long i,j,k,m,s;
for (k=0;k<n;k++)
	{
	for (i=0;i<n;i++)
		for (j=0;j<n;j++)
			{
			if(d[i][k]+d[k][j]<d[i][j])
				{
				d[i][j] = d[i][k]+d[k][j] ;
				p[i][j] = p[k][j];
				}

			}


	}
}


void print_path (long i, long j)
{
  if (i!=j)
    print_path(i,p[i][j]);
  printf("%ld ",j);

}


///////////////////////////////////////////////////////////////////////




class prims
{
   int graph[ROW][COL];
   public:
   prims();
   int nodes;
   void createGraph();
   double primsAlgo();
};

prims :: prims(){
     for(int i=0;i<ROW;i++)
       for(int j=0;j<COL;j++)
     graph[i][j]=0;
}

void prims :: createGraph(){
    int i,j,edg,x,p,q;
    //cout<<"Enter Total Nodes : ";
    cin>>nodes>>edg;
    //cout<<"\n\nEnter Adjacency Matrix : \n";

    for(i=0;i<nodes;i++)
        for(j=0;j<nodes;j++)graph[i][j]=0;

        for(i=0;i<edg;i++){cin>>p>>q>>x;graph[p-1][q-1]=x;graph[q-1][p-1]=x;}

    //Assign infinity to all graph[i][j] where weight is 0.
    for(i=0;i<nodes;i++){
        for(j=0;j<nodes;j++){
           if(graph[i][j]==0)
          graph[i][j]=infi;
          g[i][j]=graph[i][j];
        }
    }
}

double prims :: primsAlgo(){
    int selected[ROW],i,j,ne; //ne for no. of edges
    int min,x,y;

    for(i=0;i<nodes;i++)
       selected[i]=false;

    selected[0]=true;
    ne=0;
    double ans=0;

    while(ne < nodes-1){
       min=infi;

       for(i=0;i<nodes;i++)
       {
          if(selected[i]==true){
         for(j=0;j<nodes;j++){
            if(selected[j]==false){
               if(min > graph[i][j])
               {
               min=graph[i][j];
               x=i;
               y=j;
               }
            }
         }
          }
       }
       selected[y]=true;
       ans+=(double)(graph[x][y]);
       //cout<<"\n"<<x+1<<" --> "<<y+1;
       ne=ne+1;
    }
    ans/=2;
    return ans;
    //printf("%.6lf\n",ans);
}
/*
int main(){
    double res1,res2;
    prims MST;
    //clrscr();
    //cout<<"\nPrims Algorithm to find Minimum Spanning Tree\n";
    MST.createGraph();
    res1 = MST.primsAlgo();
    res2 = floyd_warshall()
    //getch();
}
*/
int main()
{
int i,j;
double res1,res2;
    prims MST;
    //clrscr();
    //cout<<"\nPrims Algorithm to find Minimum Spanning Tree\n";
    MST.createGraph();
    res1 = MST.primsAlgo();
    //res2 = floyd_warshall();

//read_input();
cout<<res1<<endl;
initialize();
floyd_warshall();
int min,max;
n=MST.nodes;

for(i=0,max=999999;i<n;++i){
	for(j=0,min=-1;j<n;++j)
		{
		    if(d[i][j]>min)min=d[i][j];
		//print_path (i,j);
		//printf("(cost = %ld)\n",d[i][j]);
		}
		if(min<max)max=min;
}
cout<<max<<endl;
    return 0;
}
