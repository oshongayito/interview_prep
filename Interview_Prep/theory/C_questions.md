### What are main characteristics of C language?###

C is a procedural language. The main features of C language include low-level access to memory, simple set of keywords, and clean style. These features make it suitable for system programming like operating system or compiler development.



### What is memory leak? Why it should be avoided? ###

Memory leak occurs when programmers create a memory in heap and forget to delete it. Memory leaks are particularly serious issues for programs like daemons and servers which by definition never terminate.

### What are local static variables? What is their use?###

A local static variable is a variable whose lifetime doesn’t end with a function call where it is declared. It extends for the lifetime of complete program. All calls to the function share the same copy of local static variables. Static variables can be used to count the number of times a function is called


### What are static functions? What is their use?###

In C, functions are global by default. The “static” keyword before a function name makes it static. Unlike global functions in C, access to static functions is restricted to the file where they are declared. Therefore, when we want to restrict access to functions, we make them static. Another reason for making functions static can be reuse of the same function name in other files. 

### What is difference between i++ and ++i?###

The expression ‘i++’ returns the old value and then increments i. The expression ++i increments the value and returns new value.

### Structure Padding ###

https://fresh2refresh.com/c-programming/c-structure-padding/

### Design memory pool ###

https://stackoverflow.com/questions/11749386/implement-own-memory-pool

### Variable argument ###
http://www.equestionanswers.com/c/c-printf-scanf-working-principle.php#:~:text=printf%20or%20print%20function%20in,arguments%20function%20or%20vararg%20function

### Memory Segments ###
Program memory is divided into different segments: a text segment for program instructions, a data segment for variables and arrays defined at compile time, a stack segment for temporary (or automatic) variables defined in subroutines and functions, and a heap segment for variables allocated during runtime by functions, such as malloc (in C) and allocate (in Fortran). For more, see About program segments.


### References: ###
1. https://www.geeksforgeeks.org/commonly-asked-c-programming-interview-questions-set-2/
2. New Reference