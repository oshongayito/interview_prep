## TSP: ##
The optimization version: find the shortest (with minimum cost) Hamiltonian cycle in a weighted graph.

This is a NP-Hard Problem. There is no known polynomial time algorithm to solve this problem. Verifying whether there is one such cycle is not possible. Hence, it isn't NP-Complete as well. However, if we convert it into decision version, where we want to know wheter there is a cycle of cost less than K, then it is verifiable and becomes NP-Complete.

### Hamiltonian Cycle: ###
A Hamiltonian path visit all the vertices in a graph exactly once. If a Hamiltonian path is a cycle, it is a Hamiltonian cycle.


### DP Approach: ###
If we know that there exists a Hamiltonian Cycle in the graph, we can use DP to solve the TSP problem.
If size of S is 2, then S must be {1, i},
 C(S, i) = dist(1, i) 
Else if size of S is greater than 2.
 C(S, i) = min { C(S-{i}, j) + dis(j, i)} where j belongs to S, j != i and j != 1.

 It takes O(N^2 * 2^N) which is exponential.

 ## KnapSack ##
 Given a set of items, each with a weight and a value, determine the number of each item to include in a collection so that the total weight is less than or equal to a given limit and the total value is as large as possible.

 ### 0/1 Knapsack, Unbounded Knapsack: ###
 Time Complexity O(NW), where N is total items, W is the total weight.
 This is polynomial in terms of size of N, however not in terms of W. It is based on the value of W.
 If we want to calculate the complexity in terms of the size of W as well, it will take the following form.
 Size(W) = Log2(Val(W))
 Therefore, Val(W) = 2 ^ (Size(W))
 So, O(NW) becomes O(N* 2^(Size(W))) which is exponential in the size of W input.

 ## References: ##
 1. [https://en.wikipedia.org/wiki/Knapsack_problem](Wiki)
 2. [https://cs.stackexchange.com/questions/909/knapsack-problem-np-complete-despite-dynamic-programming-solution](stack exchange)

