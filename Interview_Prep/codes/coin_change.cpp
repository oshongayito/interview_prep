
/*
 * Related leetcode problems: https://leetcode.com/problems/coin-change/
 * https://leetcode.com/problems/coin-change-2/
 */


#include<iostream>
using namespace std;

#include<vector>


vector< vector<long long>>DP;
vector<int> coins;
int total_coins;


/*
 * You are given coins of different denominations and a total amount of money. 
 * Write a function to compute the number of combinations that make up that amount.
 * You may assume that you have infinite number of each kind of coin.
 */

// time complexity:  O(amount * n_coins)
long long all_ways_to_change_memoization(long long amount, int n_coins){
	if(n_coins == 0){
		return 0;
	}
	if(amount<0){
	    return 0;
	}
	if(amount == 0){
		return 1; 
	}

	// This ensures that uniqueness of the combinations, and also avoids recomputations
	if(DP[amount][n_coins] != -1){
		return DP[amount][n_coins];
	}

	DP[amount][n_coins] = all_ways_to_change_memoization(amount - coins[n_coins-1], n_coins) + //taking the nth coin
						  all_ways_to_change_memoization(amount, n_coins-1); //not taking the nth coin
	return DP[amount][n_coins];
}


// Here is the iterative solution with O(n) space
long long all_ways_to_change_dp(long long amount, int n_coins){
 	vector<int> dp(amount+1, 0);
    
    dp[0] = 1;
    
    for(int i=0; i<n_coins; i++)
    {
        for(int j = 1; j <= amount; j++)
        {
            dp[j] = dp[j] + 
            		((j - coins[i]) >= 0 ? dp[j - coins[i]] : 0);
        }
    }
    return dp[amount];
}

/*
This function returns the minimum number of coins for changing the amount.
If not posible return -1
*/
int min_coins_to_change(vector<int>& coins, int amount) {
       vector<long long>dp(amount+1, amount+1);
        dp[0]=0;
        for(int i=1;i<=amount;i++){
            for(int j=0;j<coins.size();j++){
                if(coins[j] <= i)dp[i] = min(dp[i], 1+dp[i-coins[j]]);
            }
        }
        return dp[amount]==(amount+1)?-1:dp[amount];
    }

int main(){
	int amt,x;
	cin>>amt;
	cin>>total_coins;

	for(int i=0;i<total_coins;i++){
		cin>>x;
		coins.push_back(x);
	}


	DP.resize(amount+1);
	for(int i=0;i<amount+1;i++){
		DP[i].resize(total_coins+1, -1);
	}

	cout<<all_ways_to_change(amt, total_coins)<<endl;


	return 0;
}


