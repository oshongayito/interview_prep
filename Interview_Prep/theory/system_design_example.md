
This editor is synced in real time with your peer.

Use it to share thoughts and resources, such as:
- API design
- Pseudo code for specific components
- Data model/schema
- Back-of-the-envelope calculations
- Reference links
- Link to whiteboard or diagram such as https://sketchboard.me/new

Good luck!

- Features scope
=================
: Store Text Only
: Time expiration
: Get Text with the url
: Generate a URL for the content pasted
: Publically available content


UI
--
------------------------
|     Text Box

hjerlrhjlerjle
ljewl;jerljr
;jk;erkkre;ewr
|
------------------------

Expiration: 14 Days


Rough Estimates for Reads and Writes
------------------------------------

10 M DAU ; Once A day
Writers: 1M
Readers: 10M
Size: 1MB Text

Data Size: 10^6 * 10^6 => 1TB Writes
Reads: 10TB/Day

Write: 1TB/ 24*3600 =  11MBS
Reads: 110MBS

Total: 500TB storage ~3 years you might get to 1PB

API:
---
Single Creation of pastebin at a time:
Endpoint: /post/$userId/$title/$ttl body: {content}
Response: HttpStatusCode, shortUrl*



Data Model:
-----------

1. Read Heavy and Write once, with expiration of the data
2. Reads would decay at a fast rate and there could be a tail distribution would be insignificant

Row:
----
User
id, name...

PastedBins
id, userId, title, text, expirationTime, generatedUrl

Expiration:
id, pastedBinId, expirationTime + currentTime



Generate Url
-------------
1. Generate md5
2. Take 30 bits from the 128 generated
3. Convert them into 6 alpha numeric
4. Use that to generate url.

md5(content + userId + ttl + time) => 128 bits back: 16 bytes in database    
128 bits=> 64 bits => 1 Long 

[a-z][0-9]

5*6 => 30 bits out of 128

(36^6) ~21B
===> http://pastebin.com/vc-ma-pa


/post  ==> store the text and generate the url.

Storage and Expiration
----------------------

/get/$url

1. Expiration: When we get a read for -check if expired; If expired delete the row and return 404 status code
2. Database maintanenace job to delete stale entries


Bottlenecks:
------------
When we generate the pastebin; store that in redis with ttl of 2/4 days depending upon the distribution.









