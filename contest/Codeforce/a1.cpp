/*
 * 10466 how far
 * submission 0
 * coded at 120 pm on march 21, 2006
 *
 */

#include <stdio.h>
#include <math.h>
#define PI (2.*(acos(0.)))

int main() {
    double dist;
    int n,i,j;
    double t;
    struct {
        double radius;
        double time;
    }body[52];

    while(scanf("%d %lf",&n, &t)==2) {
        for(i=0;i<n;i++)
            scanf("%lf %lf",&body[i].radius, &body[i].time);
        dist=body[0].radius;
        printf("%.4lf",dist);
        for(i=1;i<n;i++) {
            double ti;
            double angle;
            ti=fmod(t,body[i].time);
            angle=(ti/body[i].time)*2.*PI;
            if(angle>PI) angle=angle-PI;
            else angle=PI-angle;
            if(angle<90)
            dist=sqrt(pow(dist,2.)+pow(body[i].radius,2.) - (2*dist*body[i].radius*cos(angle))); //cosine rule
            else if(angle>90)
            dist=sqrt(pow(dist,2.)+pow(body[i].radius,2.) + (2*dist*body[i].radius*cos(angle))); //cosine rule
            else if(angle==90)
            dist=sqrt(pow(dist,2.)+pow(body[i].radius,2.)); //cosine rule

            printf(" %.4lf",dist);
        }
        printf("\n");
    }
return 0;
}
