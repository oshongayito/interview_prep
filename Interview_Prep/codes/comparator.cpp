
/*
 * https://leetcode.com/problems/rank-teams-by-votes/
 */


class Solution {
public:
    // The comparator defines whether the first element will go before the second element
    // in the sorted sequence.
    // Remember for prirority queue the top element resides at the end.
    static bool comp(vector<int>&a, vector<int>&b)
    {
        for(int i=0;i<a.size()-1;i++)
        {
            if(a[i] != b[i])
            {
                return a[i] > b[i];
            }
        }
        return a.back() < b.back();
    }
        
    string rankTeams(vector<string>& votes) {
        vector<vector<int> > counts(26, vector<int>(27, 0));
        
        // for(int i=0;i<26;i++)
        // {
        //     counts[i].back() = i;
        // }
        
        for(auto &v:votes)
        {
            for(int i=0;i<v.size();i++)
            {
                counts[v[i]-'A'][i]++;
                counts[v[i]-'A'].back() = v[i]- 'A' + 1;
            }
        }
        sort(counts.begin(), counts.end(), comp);
        
        string ans = "";
        
        for(int i=0;i<counts.size();i++)
        {
            if(counts[i].back() != 0)ans+=(counts[i].back()+'A'-1);
        }
        return ans;
        
    }
};

//https://leetcode.com/problems/rank-teams-by-votes/discuss/524964/C%2B%2BTry-to-write-a-loop-within-comparator-function-and-it-works!-Time-for-sharing!


