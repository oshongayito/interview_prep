### Links ###
1. Intro to Behavioural Interviews: https://www.youtube.com/watch?v=PJKYqLP6MRE
2. Non Technical: https://github.com/yangshun/tech-interview-handbook/tree/master/non-technical
3. Behavioural: https://github.com/yangshun/tech-interview-handbook/blob/master/non-technical/behavioral.md
4. Amazon Principles: https://www.amazon.jobs/en/principles
5. Interviewing at Amazon: https://medium.com/@scarletinked/are-you-the-leader-were-looking-for-interviewing-at-amazon-8301d787815d
6. Want to Work at Amazon? : https://www.inc.com/bill-murphy-jr/want-to-work-at-amazon-heres-how-to-answer-most-important-interview-questions.html
7. What questions to expect in an Amazon Interview? : https://kraftshala.com/what-questions-to-expect-in-amazon-interview/
8. Two examples for each of Amazon’s 14 Leadership Principles: https://www.youtube.com/watch?v=RzlUQCy84rQ

 ### Tips ###
1. Keep your focus on the question asked
2. Make sure your answer is tangible (clear and definite)
3. Research Amazon and have a compelling reason for "Why Amazon?"
4. Review the Amazon leadership principles
5. Ask clarifying questions to fully understand the question being presented
6. Provide examples using metrics/data
7. Prepare questions for your interviewers at the end of each interview. 
8. Try to think beyond typical questions.
9. Interviewers appreciate questions that can promote discussions and interaction


### The STAR Method ###
1. Situation: 
[]Describe the situation that you were in or the task that you needed to accomplish
[]Must describe a specific event or situation
[]Not a generalised Description of what you have done int the past
[]Be sure to give enough detail for the interviewer to understand
[]Be both thorough and concise at the same time.
2. Task:
[]What goal were you working toward?
3. Action:
[]Describe the actions you took to address the situation with an appropriate amount of detail
[]Being sure to keep the focus on you
[]What specific steps you took 
[]What was your particular contribution
[]Be careful that you don’t describe what the team or group did when talking about a project
[]Say what you actually did
[]Use the word “I" not “We” when describing actions
4. Result:
[]Describe the outcome of your actions
[]Don’t be Shu about taking credit for your behaviour
[]What happened
[]How did the event end
[]What did you accomplish
[]What did you learn
[]Make sure your answer contains multiple positive results


### Prepare for ###
1. [] Why Amazon? (Amazon)
2. []Go through each project or components of your resume and ensure that you can talk about them in details:
    * []Challenges
    * []Mistakes/Failures
    * []Enjoyed
    * []Leadership
    * []Conflicts
    * []What you’d do differently
3. []What are your weaknesses?
    1. []Deliver In Time : because of thinking for perfect solution and delaying the delivery
4. []What questions should you ask for interviewer?
5. []Tell me about yourself.
6. []Tell me about a time you had a disagreement with your manager.
7. []Tell me about a time when you had a conflict with a co-worker or teammate. (Amazon)
8. []What project are you currently working on?
9. []How do you tackle challenges? Name a difficult challenge you faced while working on a project, how you overcame it, and what you learned.
10. []Talk about a project you are most passionate about, or one where you did your best work.
11. []How do you deal with a failed deadline? (Amazon)
12. []In my professional experience have you worked on something without getting approval from your manager? (Amazon) 
13. []Tell me a situation where you would have done something differently from what you actually did. (Amazon) 
14. []What is the most exceedingly bad misstep you've made at any point? (Amazon)
15. []Describe what Human Resources means to you. (Amazon)
16.  []How would you improve Amazon's website? (Amazon)


### Questions to Ask ###
1. []How easy it is to approach to a senior employee at Amazon?
2. []What are you most proud about in your career so far? 
3. []What is the most important/valuable thing you have learnt from working here?
4. []What does a typical day look like for you?
5. []What was your best moment so far at the company?
6. []What is unique about working at this company that you have not experienced elsewhere?
7. []What are the engineering challenges that the company/team is facing?
8. []What is the most fulfilling/exciting/technically complex project that you've worked on here so far?
9. []How would you describe your engineering culture?
10. []What does the company do to nurture and train its employees?


### Amazon Leadership principles ###
1. []Customer Obsession
    * []Tell me about a time when you were not able to meet a time commitment. What prevented you from meeting it? What was the outcome and what did you learn from it?
2. []Ownership
    * []Give me an example of a time you faced a conflict while working on a team. How did you handle that?
    * []Do you collaborate well?
3. []Invent and Simplify
    * []Tell me about a recent innovation
    * []Describe a situation in which you found a creative way to overcome an obstacle.
4. []Are Right, A Lot
    * []Tell me about a time you stepped up into a leadership role (CS)
    * []Tell me about a time you made a mistake ()
    * []Tell me about a time you disagreed with a colleague or a boss. What is the process you used to work it out?
    * []Give me an example of making an important decision in the absence of good data. What was the situation and how did you arrive at your decision? Did the decision turn out to be the correct one?
    * []Describe a situation where you thought you were right, but your peers or supervisor didn’t agree with you. How did you convince them you were right? How did you react? What was the outcome?
5. []Learn and Be Curious
    * [] Give me an example of a time you faced a conflict while working on a team. How did you handle that?
    * []Tell me about a time when you missed an obvious solution to a problem
6. []Hire and Develop the Best
    * []What did you do when you need to motivate a group of individuals?
7. []Insist on the Highest Standards
    * []Do you collaborate well?
    * []Tell me about a time you wish you’d handled a situation differently with a colleague.
8. []Think Big
    * []Tell me about a time when you faced a problem that had multiple possible solutions
    * []Tell me about a time when you had to choose between technologies for a project
9. []Bias for Action
    * []Describe a time when you sacrificed short term goals for long term success
10. []Frugality
    * []  Tell me about a time when you had to deal with ambiguity
11. []Earn Trust
    * []Tell me about a time when you received negative feedback from your manager. How did you respond?
12. []Dive Deep
    * []What did you do when you need to motivate a group of individuals?
    * []How have you leveraged data to develop a strategy?
13. []Have Backbone; Disagree and Commit
    * []How do you deal with someone who doesn’t like you?
14. []Deliver Results
    * []Give me an example of a time when you set a difficult goal and were able to meet or achieve it.




### Amazon Leadership principles ###
1. []Customer Obsession
    1. []CS QI project work: with business team to understand the requirements as they are the customer of our system.
    2. []iPay : transaction process: reduce time to process real transaction with bank
2. []Ownership
    1. []iPay: transactions miss match: BG, CP: account opening amount send problem from 
    2. []CiC: Ownership, changing GIT from SVN. And planning to improve batch processing
3. []Invent and Simplify
    1. []Creative Circle Leadership
4. []Are Right, A Lot
    1. [] Interface Team: Generic Methods creation from a number of similarly used methods in different products.
5. []Learn and Be Curious
    1. []Alithia data saving regarding Country and DTA Country
    2. []GraphQL
6. []Hire and Develop the Best
    1. [] Collaboration with Business for QI requirments
    2. []Help team members to learn GIT, JAVA, BuildProcess: Maven
7. []Insist on the Highest Standards
    1. []CiC: changing SVN to GIT. For code review and maintaining project quality. Planning to update batch processing.
    2. []Fixing root cause of a problem: Memory Leak: Java VisualVM, jProfiler
    3. []Finding root cause of a problem: Cache Clear Bug: Cache + Kafka + Consumer problem
8. []Think Big
    1. []Improvement of a feature: 
9. []Bias for Action
    1. []Speed maters and Calculated Risk Taking: Queue Implementation for Cassandra Backup: CommitLog, Incremental Backup, Snapshot
10. []Frugality
    1. []iPay: Project for Viewing trusted transactions: Unreachable deadline, committed
11. []Earn Trust
    1. []Made serious mistake at Work
12. []Dive Deep
    1. []Finding root cause of a problem: Cache Clear Bug: Cache + Kafka + Consumer problem
    2. []Creative Circle: Math Olympiad
13. []Have Backbone; Disagree and Commit
    1. []Spark Process: Introduction of a sequencial process which was a bottleneck of whole spark process
    2. []Finding data duplication within importing data and data in the database : disagree with manager
    3. []iPay: Project for Viewing trusted transactions: Unreachable deadline, committed
14. []Deliver Results
    1. []iPay: Project for Viewing trusted transactions: Unreachable deadline, committed