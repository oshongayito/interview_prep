## Batch Transfer of Objects to DataLake in S3 ##

### Overview ###
Sending batch of records to the S3 for future training, monitoring purpose.
Files will be saved in json format. Will also have some metadata.

### Solution ###
We can have a scheduled job run daily. We need a new table (transfer log) to keep track of the records which were sent or will be sent soon.

### Challenges ###
1. **How to send thousands of records?** if we send them at once, lot of memory maybe needed, also network maybe overwhelmed. Instead we can send them in chunks or batches. So if we have 10000 records to send, we can chunk them in small pages (50 per page), and then send one page at a time.

2. **How to detect duplicates** Set the status of the object to PENDING in the transfer log. While fetching for a new transfer make sure you are not fetching records which already exist in the transfer log.

3. ** What if microservice responsible for sending data crashes after sending data successfully to the S3 but before returning response to the caller?** In that case, the statuses of the transfer won't be updated properly, which may result in duplicate sending or missing records in the S3. We may have a new endpoint in the microservice to check for a file status in S3. However, to do so we need to store the file names in the transfer log.

4. ** Using the same transfer log / sync log from Signal team!!! ** Signal team 

5. Two design meetings to go over the challenges and discuss with all.

6. Sending Documents along with Cases. There can be multiple documents associated with a single Case. That means the total number of Documents could be a multiple of total number of Cases. In case of AstraZeneca, the total number of Cases were around 500k and the total number of documents were around 1M. 

7. Send the documents separately, have separate transfer log entries for the documents. The chunk of documents to transfer at a time should be smaller compared to the chunk of Cases, as the documents size may be much higher than the case size (in bytes).

8. Also need to save the S3 file path in the transfer log with the file extension. When we save the document in the S3, we double encrypt the documents. So when somebody wants to download the document, he needs to know the extension to properly decrypt it.


9. The whole case, document transfer process should be done asynchronously, and in a multi-threaded manner. Also need to make sure the overall memory, and network it consumes doesn't affect the other services. Because, this whole process is for monitoring, or future training purpose, we do not want the regular user facing tasks to get hampered by this transfer process.

