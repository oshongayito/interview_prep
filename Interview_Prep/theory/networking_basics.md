
### TCP vs UDP ###
---------------
TCP (Transmission Control Protocol) is connection oriented, whereas UDP (User Datagram Protocol) is connection-less. This means that TCP tracks all data sent, requiring acknowledgment for each octet (generally). UDP does not use acknowledgments at all, and is usually used for protocols where a few lost datagrams do not matter. UDP is faster than TCP.

Because of acknowledgments, TCP is considered a reliable data transfer protocol. It ensures that no data is sent to the upper layer application that is out of order, duplicated, or has missing pieces. It can even manage transmissions to attempt to reduce congestion.

UDP is a very lightweight protocol defined in RFC 768. The primary uses for UDP include service advertisements, such as routing protocol updates and server availability, one-to-many multicast applications, and streaming applications, such as voice and video, where a lost datagram is far less important than an out-of-order datagram.

Reference: https://enterprise.netscout.com/edge/tech-tips/difference-between-tcp-and-udp


### Hubs, Switches, and Routers ###
-------------------------------

Hubs: Creates a network where every packet from source is sent to all the destinations

Switches: Like hubs, but sends packets to only the mentioned destination

Routers: Connects two different networks. Links between home network to some other severs (e.g., google)

### Subnet Mask ###
-----------------------
https://support.microsoft.com/en-us/help/164015/understanding-tcp-ip-addressing-and-subnetting-basics


### Layer 3 (network layer) routing protocols ###
---------------------------------------------------
https://www.comparitech.com/net-admin/routing-protocol-types-guide/


### BGP and Autonomous System ###
-----------------------------------
https://blog.cdemi.io/beginners-guide-to-understanding-bgp/#:~:text=BGP%20is%20short%20for%20Border,route%20traffic%20across%20the%20internet.&text=BGP%20is%20a%20Layer%204,BGP%20to%20exchange%20routing%20information.

### L2, L3 switching ###
-------------------------
https://www.netmanias.com/en/post/blog/6348/arp-ip-routing-network-protocol-switching/switching-and-routing-part-3-l2-ethernet-switching-by-l3-switch#:~:text=This%20and%20next%20posts%20are,IP%20forwarding)%20by%20L3%20switch.&text=It%20has%20only%20one%20type,Ethernet%20switch%20and%20IP%20router


### Routing, switching ###
---------------------------
https://www.cisco.com/c/en_my/solutions/small-business/products/routers-switches/routing-switching-primer.html

### Bridging, spanning tree protocol ###
------------------------------
https://en.wikipedia.org/wiki/Bridging_(networking)
https://en.wikipedia.org/wiki/Spanning_Tree_Protocol

### Link Aggregation and LACP ###
-----------------------------------
https://en.wikipedia.org/wiki/Link_aggregation

Reference: https://youtu.be/Ofjsh_E4HFY