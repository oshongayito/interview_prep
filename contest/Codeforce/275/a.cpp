    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))

ll gcd_iter(ll u, ll v) {
  ll t;
  while (v) {
    t = u;
    u = v;
    v = t % v;
  }
  return u < 0 ? -u : u; /* abs(u) */
}




int main()
{
    //string l,r;
    ll l,r,a,b,c;
    bool flag;
    cin>>l>>r;

    flag=false;

    for(ll i=l;i<=r&& !flag;i++)
    {
        for(ll j=i+1;j<=r&& !flag;j++)
        {
            for(ll k=j+1;k<=r && !flag;k++)
            {
                if(gcd_iter(i,j)==1&& gcd_iter(j,k)==1 && gcd_iter(i,k)>1)
                {
                    a=i;
                    b=j;
                    c=k;
                    flag=true;
                    break;
                }
            }
        }
    }

    if(flag)
    {
        cout<<a<<" "<<b<<" "<<c<<endl;
    }
    else
    {
        cout<<"-1"<<endl;
    }
    return 0;
}
