#include<cstdio>
#include<cstring>
#include<cstdlib>
#include<cmath>
#include<cctype>
#include<climits>
#include<ctime>
#include<iostream>
#include<sstream>
#include<string>
#include<vector>
#include<map>
#include<stack>
#include<queue>
#include<algorithm>
#include<set>
#include<bitset>
#include<complex>
#include<numeric>
#include<valarray>

using namespace std;

#define ll long long
#define ull unsigned long long
#define vi vector<int>
#define vll vector<ll>
#define vvi vector<vector<int> >
#define pii pair<int,int>
#define vpii vector<pair<int,int> >
#define vs vector<string>
#define pb push_back
#define mp make_pair
#define PI acos(-1)
#define all(a) a.begin(),a.end()
#define mem(a,v) memset(a,v,sizeof(a))
#define eps 1e-9
#define repi(i,a,n) for(int i=(a);i<(n);i++)
#define rep(i,n) for(int i=0;i<n;i++)
#define len(a) ((int)a.size())
#define repr(i,a,n) for(int i=(n);i>=(a);i--)
template<class T> inline T gcd(T a,T b) {if(a<0)return gcd(-a,b);if(b<0)return gcd(a,-b);return (b==0)?a:gcd(b,a%b);}
#define inf 999999999

ll ary[(1<<16)+10];
bool vt[(1<<16)+10];

int flip(int x,int num){
	num=15-num;
	int a=num/4,b=num%4;
	x=x^(1<<num);
	if(b>0)x=x^(1<<(num-1));
	if(b<3)x=x^(1<<(num+1));
	if(a>0)x=x^(1<<(num-4));
	if(a<3)x=x^(1<<(num+4));
	return x;
}


ll df(int x){
	if(x==0)return 0;
	if(vt[x])return ary[x];
	ll re=inf;
	vt[x]=true;
	rep(i,16){
		ll p=1+df(flip(x,i));
		if(p<re){
			re=p;
		}
		//~ re=min(re,));
	}
	ary[x]=re;
	return re;
}


int main(void){
	string s[4],ss="$";
	rep(i,4){cin>>s[i];ss=ss+s[i];}
	int x=0;
	ss=ss.substr(1);
	rep(i,16){
		if(ss[i]=='b')x=x|(1<<(16-i-1));
	}

	rep(i,(1<<16)+10)ary[i]=inf;
	mem(vt,0);
	ll re=inf;
//	rep(i,16){
		re=min(re,df(x));
//	}
	//~ prn(x);
	//~ rep(i,16)cout<<i<<" "<<flip(p,i)<<endl;
	if(re>=inf)cout<<"Impossible"<<endl;
	else cout<<re<<endl;
	return 0;
}

