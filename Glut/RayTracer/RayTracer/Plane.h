#ifndef _Plane_H
#define _Plane_H

#include "math.h"



#include "Object.h"


class Plane : public Object{
	Vector3 normal;
	double distance;
	Color color;

	public:

	Plane ();

	Plane (Vector3, double, Color);

	// method functions
	Vector3 getPlaneNormal () { return normal; }
	double getPlaneDistance () { return distance; }
	virtual Color getColor () { return color; }

	virtual Vector3 getNormalAt(Vector3 point) {
		return normal;
	}

	virtual double findIntersection(Ray ray) {
		Vector3 ray_direction = ray.getRayDirection();

//        cout<<"aisi...";
//
//        normal.print();
//        ray_direction.print();
		double a = ray_direction.dot(normal);

		if (a == 0) {
			// ray is parallel to the plane
			return -1;
		}
		else {
			double b = normal.dot(ray.getRayOrigin().vectAdd(normal.Mult(distance).neg()));
//			cout<<" --"<<b<<" "<<a<<endl;
			return -1*b/a;
		}
	}

};

Plane::Plane () {
	normal = Vector3(1,0,0);
	distance = 0;
	Color q(0.5,0.5,0.5, 0);
	color = q;
}

Plane::Plane (Vector3 normalValue, double distanceValue, Color colorValue) {
	normal = normalValue;
	distance = distanceValue;
	color = colorValue;
}

#endif
