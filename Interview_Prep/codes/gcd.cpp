
/*
 * An efficient solution is to use Euclidean algorithm which is the main algorithm
 * used for this purpose. The idea is, GCD of two numbers doesn’t change if 
 * smaller number is subtracted from a bigger number.
 */


#include<iostream>

using namespace std;



int gcd(int a,int b)
{
    while(a!=b)
    {
        if(a>b)a=a-b;
        else b=b-a;
    }
    return a;
}

//faster
int gcd2(int a, int b){
    if(a==0){
        return b;
    }
    return gcd2(b%a, a);
}


int main()
{

    int x,y;

    cin>>x>>y;

    cout<<gcd(x,y)<<endl;
    return 0;
}


