#include <set>
#include <map>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>
#include <vector>
#include <string>
#include <cctype>
#include <cstdio>
#include <iomanip>
#include <sstream>
#include <cstdlib>
#include <climits>
#include <complex>
#include <numeric>
#include <valarray>
#include <iostream>
#include <string.h>
#include <algorithm>
using namespace std;



class PenguinPals {
	public:
	int findMaximumMatching(string colors) {
		int i,j,k,r,b,x,y,l,cnt=0;
		l=colors.size();
		char max;
		bool vis[100];
		int arr[100];
        r=0;
        b=0;
		for(i=0;i<100;i++){vis[i]=0;arr[i]=0;}
    for(i=0;i<l;i++)
    {
        if(colors[i]=='R')r++;
        else b++;
    }
    if(r>b)max='R';
    else max ='B';
		    for(i=0,j=l-1,cnt=0; i<l && j>=0 && (!vis[i] || !vis[j]) ;)
		    {
		        if(!vis[i+1] && colors[i]==colors[i+1])
		        {
		            cnt++;
		            i+=2;
		            vis[i]=1;
		            vis[i+1]=1;
		            //cout<<i;
		        }
		        if(!vis[j-1]&& colors[j]==colors[j-1])
		        {
		            cnt++;
		            j-=2;
		            vis[j]=1;
		            vis[j-1]=1;
		            //cout<<" "<<j<<endl;
		        }

		        else
		        {
		            if(colors[j]==colors[i]){
		            cnt++;
		            vis[j]=1;
		            vis[i]=1;}
		            else
		            {
		                if(colors[j]==max){i++;vis[i-1]=1;}
		        else {j--;vis[j+1]=1;}

		            }
		        }

		        cout<<cnt<<" "<<i<<" "<<j<<endl;
		        //else i--;
		    }
		    return cnt;

	}
};






// BEGIN CUT HERE
namespace moj_harness {
	int run_test_case(int);
	void run_test(int casenum = -1, bool quiet = false) {
		if (casenum != -1) {
			if (run_test_case(casenum) == -1 && !quiet) {
				cerr << "Illegal input! Test case " << casenum << " does not exist." << endl;
			}
			return;
		}

		int correct = 0, total = 0;
		for (int i=0;; ++i) {
			int x = run_test_case(i);
			if (x == -1) {
				if (i >= 100) break;
				continue;
			}
			correct += x;
			++total;
		}

		if (total == 0) {
			cerr << "No test cases run." << endl;
		} else if (correct < total) {
			cerr << "Some cases FAILED (passed " << correct << " of " << total << ")." << endl;
		} else {
			cerr << "All " << total << " tests passed!" << endl;
		}
	}

	int verify_case(int casenum, const int &expected, const int &received, clock_t elapsed) {
		cerr << "Example " << casenum << "... ";

		string verdict;
		vector<string> info;
		char buf[100];

		if (elapsed > CLOCKS_PER_SEC / 200) {
			sprintf(buf, "time %.2fs", elapsed * (1.0/CLOCKS_PER_SEC));
			info.push_back(buf);
		}

		if (expected == received) {
			verdict = "PASSED";
		} else {
			verdict = "FAILED";
		}

		cerr << verdict;
		if (!info.empty()) {
			cerr << " (";
			for (int i=0; i<(int)info.size(); ++i) {
				if (i > 0) cerr << ", ";
				cerr << info[i];
			}
			cerr << ")";
		}
		cerr << endl;

		if (verdict == "FAILED") {
			cerr << "    Expected: " << expected << endl;
			cerr << "    Received: " << received << endl;
		}

		return verdict == "PASSED";
	}

	int run_test_case(int casenum) {
		switch (casenum) {
		case 0: {
			string colors             = "RRBRBRBB";
			int expected__            = 3;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 1: {
			string colors             = "RRRR";
			int expected__            = 2;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 2: {
			string colors             = "BBBBB";
			int expected__            = 2;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 3: {
			string colors             = "RBRBRBRBR";
			int expected__            = 4;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 4: {
			string colors             = "RRRBRBRBRBRB";
			int expected__            = 5;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 5: {
			string colors             = "R";
			int expected__            = 0;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 6: {
			string colors             = "RBRRBBRB";
			int expected__            = 3;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}
		case 7: {
			string colors             = "RBRBBRBRB";
			int expected__            = 4;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}

		// custom cases

/*      case 8: {
			string colors             = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 9: {
			string colors             = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
/*      case 10: {
			string colors             = ;
			int expected__            = ;

			clock_t start__           = clock();
			int received__            = PenguinPals().findMaximumMatching(colors);
			return verify_case(casenum, expected__, received__, clock()-start__);
		}*/
		default:
			return -1;
		}
	}
}

int main(int argc, char *argv[]) {
	if (argc == 1) {
		moj_harness::run_test();
	} else {
		for (int i=1; i<argc; ++i)
			moj_harness::run_test(atoi(argv[i]));
	}
}
// END CUT HERE
