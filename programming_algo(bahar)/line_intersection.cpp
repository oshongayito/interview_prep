/*
 
 for details see the link: http://paulbourke.net/geometry/lineline2d/
 
 */

#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<algorithm>
#include<cmath>
using namespace std;

#define eps 1e-9

class point{
	public:
	double x;
	double y;
	point(double xx,double yy)
	{
		x=xx;
		y=yy;
	}
	point(){};
};


bool is_in_box(point p,point a,point b)
{
	return min(a.x,b.x)<=p.x && p.x<=max(a.x,b.x) && min(a.y,b.y)<=p.y && p.y<=max(a.y,b.y);
}

bool line_intersection(point a,point b,point c,point d,point &e)
{
	double mua,mub;
	double demon,neuma,neumb;
	
	demon=(d.y-c.y)*(b.x-a.x)-(d.x-c.x)*(b.y-a.y);
	neuma=(d.x-c.x)*(a.y-c.y)-(d.y-c.y)*(a.x-c.x);
	neumb=(b.x-a.x)*(a.y-c.y)-(b.y-a.y)*(a.x-c.x);
	
	/* Are the line coincident? */
	
	if(fabs(demon)<eps && fabs(neuma)<eps && fabs(neumb)<eps)
	{
        e.x=(a.x+b.x)/2;
		e.y=(a.y+b.y)/2;
		return true;
	} 
	
	/* Are the line parallel */
	
	if(fabs(demon)<eps)
	{
		e.x=0.0;
		e.y=0.0;
		return false;
	}
	
	/* Is the intersection along the the segments */

	mua=neuma/demon;
	mub=neumb/demon;
	
	if(mua<0 || mua>1 || mub<0 || mub>1)
	{
		e.x=0;
		e.y=0;
		return false;
	}
	
	e.x=a.x+mua*(b.x-a.x);
	e.y=a.y+mua*(b.y-a.y);
	
	return true;
}
