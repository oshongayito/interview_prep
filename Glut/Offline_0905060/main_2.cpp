#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<windows.h>
#include<GL/glut.h>

using namespace std;

#include<vector>
#include<iostream>
#include<fstream>
#include<string>


#define pi 2*acos(0)
#define PI acos(-1)

#include "color.h"
#include "camera.h"


double cameraHeight;
double cameraAngle;
int drawgrid;
int drawaxes;
double angle;
float dfx=100,dfy=100,dfz=50,dr=1.0,dg=1.0,db=1.0,diff_rad=50,diff_delta=5,diff_ang=0;

Camera cam;
double delta=3.0;

GLuint texid1,texid2,texid3;
int num_texture = -1;


vector< vector< vector<double>>>points;


struct point
{
	double x,y,z;
};

bool flag[100][100];


void drawAxes()
{
	if(drawaxes==1)
	{
		glColor3f(1.0, 1.0, 1.0);
		glBegin(GL_LINES);{
			glVertex3f( 100,0,0);
			glVertex3f(-100,0,0);
			glVertex3f(0,-100,0);
			glVertex3f(0, 100,0);
			glVertex3f(0,0, 100);
			glVertex3f(0,0,-100);
		}glEnd();
	}
}


void drawGrid()
{
	int i;
	if(drawgrid==1)
	{
		glColor3f(0.6, 0.6, 0.6);	//grey
		glBegin(GL_LINES);{
			for(i=-8;i<=8;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -90, 0);
				glVertex3f(i*10,  90, 0);

				//lines parallel to X-axis
				glVertex3f(-90, i*10, 0);
				glVertex3f( 90, i*10, 0);
			}
		}glEnd();
	}
}


void drawSquare(float a)
{
	glBegin(GL_QUADS);{
		glVertex3f( a, a,2);
		glVertex3f( a,-a,2);
		glVertex3f(-a,-a,2);
		glVertex3f(-a, a,2);
	}glEnd();
}


void drawss()
{
	glPushMatrix();{
		glRotatef(angle,0,0,1);
		glTranslatef(75,0,0);
		glRotatef(2*angle,0,0,1);


		glPushMatrix();{
			glRotatef(angle,0,0,1);
			glTranslatef(25,0,0);
			glRotatef(3*angle,0,0,1);
			glColor3f(0.0, 0.0, 1.0);
			drawSquare(5);

		}glPopMatrix();



		glColor3f(1.0, 0.0, 0.0);
		drawSquare(10.0);
	}glPopMatrix();

}

//draws half sphere
void drawsphere(float radius,int slices,int stacks)
{
	struct point points[100][100];
		int i,j;
	double h,r;
	for(i=0;i<=stacks;i++)
	{
		h=radius*sin(((double)i/(double)stacks)*(pi/2));
		//r=cos(((double)i/(double)stacks)*(pi))*sqrt(radius*radius-h*h);
		r=exp(-((h*h)/45))*sqrt(radius*radius-h*h);
        //if(i > (stacks/2))r=(r/h);
		for(j=0;j<=slices;j++)
		{
			points[i][j].x=r*cos(((double)j/(double)slices)*2*pi);
			points[i][j].y=r*sin(((double)j/(double)slices)*2*pi);
			points[i][j].z=h*4;
		}

	}
	for(i=0;i<stacks;i++)
	{
		for(j=0;j<slices;j++)
		{
			//glColor3f((double)i/(double)stacks,(double)i/(double)stacks,(double)i/(double)stacks);
			//glColor3f(0.6,0.6,0.6);
			if((j%(((i+1)))>=0 )&&(j%(((i+1)))<=5))glColor3f(0.5,0.5,0.5);
            else glColor3f(1,1,1);


			glBegin(GL_QUADS);{

				glVertex3f(points[i][j].x,points[i][j].y,points[i][j].z);
				glVertex3f(points[i][j+1].x,points[i][j+1].y,points[i][j+1].z);
				glVertex3f(points[i+1][j+1].x,points[i+1][j+1].y,points[i+1][j+1].z);
				glVertex3f(points[i+1][j].x,points[i+1][j].y,points[i+1][j].z);

			}glEnd();
		}

	}
}

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

void drawUpperFloor()
{
    glPushMatrix();{



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(0.0,0.0,5.9534);
                glVertex3f(25.4094,32.9892,5.9534);
                glVertex3f(24.0915,34.0066,5.9534);




			}glEnd();





            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(24.0915,34.0066,5.9534);
                glVertex3f(22.2059,32.0151,5.9534);




			}glEnd();




            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(22.2059,32.0151,5.9534);
                glVertex3f(17.4366,29.3635,5.9534);




			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(17.4366,29.3635,5.9534);
                glVertex3f(14.7498,28.8129,5.9534);




			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(14.7498,28.8129,5.9534);
                glVertex3f(12.0071,28.8168,5.9534);




			}glEnd();






            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(12.0071,28.8168,5.9534);
                glVertex3f(9.3219,29.3749,5.9534);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(9.3219,29.3749,5.9534);
                glVertex3f(6.8049,30.4643,5.9534);




			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(6.8049,30.4643,5.9534);
                glVertex3f(4.5600,32.0399,5.9534);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(4.5600,32.0399,5.9534);
                glVertex3f(2.6799,34.0368,5.9534);




			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(2.6799,34.0368,5.9534);
                glVertex3f(1.2423,36.3724,5.9534);




			}glEnd();





            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(1.2423,36.3724,5.9534);
                glVertex3f(0.3064,38.9505,5.9534);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(0.3064,38.9505,5.9534);
                glVertex3f(-0.0892,41.6656,5.9534);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(0.0,0.0,5.9534);
                glVertex3f(-0.0892,41.6656,5.9534);
               glVertex3f(-1.7403,41.6040,5.9534);




			}glEnd();



	}glPopMatrix();


}


void calculate_points()
{
    ifstream  data("E:\\CODES\\Glut\\Offline_0905060\\relling.txt");
string line;
double temp;

     vector<double>x;
      // x.resize(3);
    vector< vector<double>>y;
    //y.resize(4);


     for(int i=0,j=0;std::getline(data,line);i++)
    {
       //cout<<line<<endl;
         if(line==""){i--;continue;}

       temp = atof(line.c_str());
       x.push_back((double)temp);

   // cout<<line<<" "<<temp<<" "<<i<<endl;

       if(i%3==2)
       {
           y.push_back(x);
           x.clear();
        //   x.resize(3);
           j++;
           if(j%4==0)
           {
               points.push_back(y);
               y.clear();
          //     y.resize(4);
           }

       }

    }

    for(int i=0;i<points.size() && false;i++)
    {
        for(int j=0;j<points[i].size();j++)
        {
            for(int k=0;k<3;k++)
            {
                cout<<points[i][j][k]<<" ";
            }
            cout<<endl;
        }
        cout<<endl;
    }

}


void drawRelling()
{



for(int i=0;i<(int)points.size()-1;i++)
{
    for(int j=0;j<(int)points[i].size();j++)
    {
        glPushMatrix();{




            glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


                glVertex3f(points[i][j][0],points[i][j][1],points[i][j][2]);

                glVertex3f(points[i+1][j][0],points[i+1][j][1],points[i+1][j][2]);
                glVertex3f(points[i+1][(j+1)%4][0],points[i+1][(j+1)%4][1],points[i+1][(j+1)%4][2]);

                glVertex3f(points[i][(j+1)%4][0],points[i][(j+1)%4][1],points[i][(j+1)%4][2]);


			}glEnd();

				}glPopMatrix();


    }

}




}

void drawStairSideInside()
{
    glPushMatrix();{




            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(29.4997,41.0298,5.2489);
                glVertex3f(29.766,41.3755,4.6635);
                glVertex3f(29.6449,41.2183,4.6635);

                glVertex3f(29.4997,41.0298,5.0505);



			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(24.6,34.667,5.2489);
                glVertex3f(29.4997,41.0298,5.2489);
                glVertex3f(29.4997,41.0298,5.0505);

                glVertex3f(24.6,34.667,5.0505);


			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(24.0915,34.0066,6.1519);
                glVertex3f(24.6,34.667,5.2489);

                glVertex3f(24.6,34.667,5.0505);
                glVertex3f(24.0915,34.0066,5.9534);


			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(24.0915,34.0066,6.1519);
                glVertex3f(22.2059,32.0151,6.1519);
                glVertex3f(22.2059,32.0151,5.9534);
                glVertex3f(24.0915,34.0066,5.9534);


			}glEnd();




            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(22.2059,32.0151,6.1519);

                glVertex3f(17.4366,29.3635,6.1519);
                glVertex3f(17.4366,29.3635,5.9534);
                glVertex3f(22.2059,32.0151,5.9534);





			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(17.4366,29.3635,6.1519);

			    glVertex3f(14.7498,28.8129,6.1519);
			    glVertex3f(14.7498,28.8129,5.9534);
			    glVertex3f(17.4366,29.3635,5.9534);





			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(14.7498,28.8129,6.1519);

			    glVertex3f(12.0071,28.8168,6.1519);
			    glVertex3f(12.0071,28.8168,5.9534);
                glVertex3f(14.7498,28.8129,5.9534);




			}glEnd();






            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(12.0071,28.8168,6.1519);

                glVertex3f(9.3219,29.3749,6.1519);

                glVertex3f(9.3219,29.3749,5.9534);

                glVertex3f(12.0071,28.8168,5.9534);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(9.3219,29.3749,6.1519);

                glVertex3f(6.8049,30.4643,6.1519);

                glVertex3f(6.8049,30.4643,5.9534);

                glVertex3f(9.3219,29.3749,5.9534);



			}glEnd();

            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(6.8049,30.4643,6.1519);

                glVertex3f(4.5600,32.0399,6.1519);
                glVertex3f(4.5600,32.0399,5.9534);
glVertex3f(6.8049,30.4643,5.9534);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(4.5600,32.0399,6.1519);

                glVertex3f(2.6799,34.0368,6.1519);

                glVertex3f(2.6799,34.0368,5.9534);

glVertex3f(4.5600,32.0399,5.9534);



			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(2.6799,34.0368,6.1519);

                glVertex3f(1.2423,36.3724,6.1519);

                glVertex3f(1.2423,36.3724,5.9534);

glVertex3f(2.6799,34.0368,5.9534);



			}glEnd();





            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(1.2423,36.3724,6.1519);

                glVertex3f(0.3064,38.9505,6.1519);
                glVertex3f(0.3064,38.9505,5.9534);

glVertex3f(1.2423,36.3724,5.9534);



			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
			    glVertex3f(0.3064,38.9505,6.1519);


                glVertex3f(-0.0892,41.6656,6.1519);
                glVertex3f(-0.0892,41.6656,5.9534);

                glVertex3f(0.3064,38.9505,5.9534);



			}glEnd();



            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(-0.0892,41.6656,6.1519);
                glVertex3f(-0.1220,42.4984,5.1795);
                glVertex3f(-0.1220,42.4984,5.0505);
                glVertex3f(-0.0892,41.6656,5.9534);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(-0.1220,42.4984,5.1795);
                glVertex3f(-0.4492,50.53,5.1795);
                glVertex3f(-0.4492,50.53,5.0505);
                glVertex3f(-0.1220,42.4984,5.0505);




			}glEnd();


			            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{


                glVertex3f(-0.4492,50.53,5.1795);
                glVertex3f(-0.467,50.9633,4.6635);

                glVertex3f(-0.4589,50.7651,4.6635);
                glVertex3f(-0.4492,50.53,5.0505);




			}glEnd();




	}glPopMatrix();


}



void drawInnerCircle2()
{
    glPushMatrix();{

///center flower

            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(0.0,0.0,4.6635);
                glVertex3f(1.3931,4.3903,4.6635);
                glVertex3f(3.8892,2.4677,4.6635);


			}glEnd();


            glColor3f(1.0, 0.0, 1.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(0.0,0.0,4.6635);
                glVertex3f(3.8892,2.4677,4.6635);
                glVertex3f(4.5656,-0.6096,4.6635);


			}glEnd();


///circle 1

//1
            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(6.4595,11.1881,4.6635);

			   glVertex3f(9.1351,9.1351,4.6635);
			   glVertex3f(9.1351,9.1351,0.5259);

                glVertex3f(6.4595,11.1881,0.5259);


			}glEnd();



            glColor3f(0.0, 0.5, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(9.1351,9.1351,4.6635);

			    glVertex3f(11.1881,6.4595,4.6635);
			    glVertex3f(11.1881,6.4595,0.5259);

			    glVertex3f(9.1351,9.1351,0.5259);




			}glEnd();


			            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(11.1881,6.4595,4.6635);
			    glVertex3f(12.4878,3.3437,4.6635);
			    glVertex3f(12.4878,3.3437,0.5259);
			    glVertex3f(11.1881,6.4595,0.5259);


			}glEnd();





///circle 2
            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

			    glVertex3f(9.6200,16.6624,4.6635);
			    glVertex3f(13.6048,13.6048,4.6635);
			    glVertex3f(13.6048,13.6048,0.5259);
			    glVertex3f(9.6200,16.6624,0.5259);

			}glEnd();

			//1
            glColor3f(0.0, 0.5, 1.0);
			glBegin(GL_QUADS);{

			    glVertex3f(13.6048,13.6048,4.6635);
			    glVertex3f(16.6624,9.6200,4.6635);
			    glVertex3f(16.6624,9.6200,0.5259);
			    glVertex3f(13.6048,13.6048,0.5259);

			}glEnd();

			//1
            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

			    glVertex3f(16.6624,9.6200,4.6635);
			    glVertex3f(18.5845,4.9797,4.6635);
			    glVertex3f(18.5845,4.9797,0.5259);
			    glVertex3f(16.6624,9.6200,0.5259);

			}glEnd();



	}glPopMatrix();


}


void drawStairSide()
{
     glPushMatrix();{

//1
            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

                glVertex3f(29.7660,41.3755,4.6645);
                glVertex3f(29.4997,41.0298,5.2489);
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(29.6089,41.4968,4.6635);



			}glEnd();

			            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(29.6089,41.4968,4.6635);
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(29.3425,41.1509,5.0505);
                glVertex3f(29.4878,41.3396,4.7925);



			}glEnd();

            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(29.6089,41.4968,4.6635);
                glVertex3f(29.4878,41.3396,4.7925);
                glVertex3f(29.4878,41.3396,4.6635);



			}glEnd();

glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{
			    glVertex3f(29.4152,41.2452,4.7925);
                glVertex3f(29.4878,41.3396,4.7925);
                glVertex3f(29.4878,41.3396,4.6635);



			}glEnd();

		            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(29.4878,41.3396,4.7925);
                glVertex3f(29.4152,41.2452,4.9215);
                glVertex3f(29.4152,41.2452,4.7925);



			}glEnd();


			     glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(29.3425,41.1509,4.9215);
                glVertex3f(29.4152,41.2452,4.9215);
                glVertex3f(29.4152,41.2452,4.7925);



			}glEnd();



		            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(29.4152,41.2452,4.9215);
                glVertex3f(29.3425,41.1509,5.0505);
                glVertex3f(29.3425,41.1509,4.9215);



			}glEnd();


//2
			            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(29.4997,41.0298,5.2489);
                glVertex3f(24.6,34.667,5.2489);
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(29.3425,41.1509,5.2489);




			}glEnd();


            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(23.9336,34.1284,5.9534);
                glVertex3f(24.4429,34.7882,5.0505);

			}glEnd();

									            glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(23.9336,34.1284,5.9534);
                glVertex3f(23.9336,34.1284,4.6635);
                glVertex3f(24.2691,34.5636,4.0682);


			}glEnd();





						            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(29.3425,41.1509,5.0505);


			}glEnd();







					            glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_LINE);{
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(24.4429,34.7882,5.2489);


			}glEnd();


		            glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_LINE);{
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(24.4429,34.7882,5.0505);


			}glEnd();

					            glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_LINE);{
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(29.3425,41.1509,5.0505);


			}glEnd();


					            glColor3f(0.0, 0.0, 0.0);
			glBegin(GL_LINE);{
                glVertex3f(29.3425,41.1509,5.2489);
                glVertex3f(29.3425,41.1509,5.0505);


			}glEnd();




glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
			    glVertex3f(29.4152,41.2452,4.7925);
                glVertex3f(26.2293,37.1090,4.0682);
                glVertex3f(26.2293,37.1090,1.9547);
                glVertex3f(29.4878,41.3396,4.6635);



			}glEnd();


			glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(29.3425,41.1509,5.0505);
                glVertex3f(26.2293,37.1090,4.0682);
                glVertex3f(29.3425,41.1509,4.9215);




			}glEnd();


			glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(29.3425,41.1509,5.0505);
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(26.2293,37.1090,4.0682);




			}glEnd();

			glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(29.3425,41.1509,4.9215);
                                glVertex3f(26.2293,37.1090,4.0682);
                                glVertex3f(29.4152,41.2452,4.7925);





			}glEnd();


			glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{

                glVertex3f(26.2293,37.1090,4.0682);
                glVertex3f(24.4429,34.7882,5.0505);
                glVertex3f(24.2691,34.5636,4.0682);

			}glEnd();






//3
			            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(24.6,34.667,5.2489);
                glVertex3f(24.0915,34.0066,6.1519);
                glVertex3f(23.9336,34.1248,6.1519);
                glVertex3f(24.4429,34.7882,5.2489);




			}glEnd();

					            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(24.4429,34.7882,5.2489);
                glVertex3f(23.9336,34.1248,6.1519);
            glVertex3f(23.9336,34.1248,4.6635);
            glVertex3f(24.4429,34.7882,4.0682);




			}glEnd();



//4

					   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(24.0915,34.0066,6.1519);
                glVertex3f(22.2059,32.0151,6.1519);
                glVertex3f(22.0758,32.1663,6.1519);
                glVertex3f(23.9336,34.1248,6.1519);




			}glEnd();

							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(23.9336,34.1248,6.1519);
                glVertex3f(22.0758,32.1663,6.1519);
                glVertex3f(22.0758,32.1663,4.6635);
                glVertex3f(23.9336,34.1248,4.6635);




			}glEnd();

//5


					   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(22.2059,32.0151,6.1519);
                glVertex3f(19.9566,30.4458,6.1519);
                glVertex3f(19.8597,30.6202,6.1519);
                glVertex3f(22.0758,32.1663,6.1519);




			}glEnd();

					   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(22.0758,32.1663,6.1519);
                glVertex3f(19.8597,30.6202,6.1519);
                glVertex3f(19.8597,30.6202,4.6635);
                glVertex3f(22.0758,32.1663,4.6635);

			}glEnd();



//6

				   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(19.9566,30.4458,6.1519);
                glVertex3f(17.4366,29.3635,6.1519);
                glVertex3f(17.3769,29.5539,6.1519);

                glVertex3f(19.8597,30.6202,6.1519);




			}glEnd();

						   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(19.8597,30.6202,6.1519);
                glVertex3f(17.3769,29.5539,6.1519);
                glVertex3f(17.3769,29.5539,4.6635);
                glVertex3f(19.8597,30.6202,4.6635);





			}glEnd();


//7


							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(17.4366,29.3635,6.1519);
                glVertex3f(14.7498,28.8129,6.1519);
                glVertex3f(14.7298,29.0114,6.1519);
                glVertex3f(17.3769,29.5539,6.1519);




			}glEnd();

									   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(17.3769,29.5539,6.1519);
                glVertex3f(14.7298,29.0114,6.1519);
                glVertex3f(14.7298,29.0114,4.6635);
                glVertex3f(17.3769,29.5539,4.6635);




			}glEnd();

//8

							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(14.7498,28.8129,6.1519);
                glVertex3f(12.0071,28.8168,6.1519);
                glVertex3f(12.0277,29.0152,6.1519);
                glVertex3f(14.7298,29.0114,6.1519);




			}glEnd();


						   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(14.7298,29.0114,6.1519);
                glVertex3f(12.0277,29.0152,6.1519);
                glVertex3f(12.0277,29.0152,4.6635);
                glVertex3f(14.7298,29.0114,4.6635);




			}glEnd();



//9


							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(12.0071,28.8168,6.1519);
                glVertex3f(9.3219,29.3749,6.1519);
                glVertex3f(9.3821,29.5651,6.1519);
                glVertex3f(12.0277,29.0152,6.1519);




			}glEnd();

									   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

                glVertex3f(12.0277,29.0152,6.1519);
                glVertex3f(9.3821,29.5651,6.1519);

               glVertex3f(9.3821,29.5651,4.6635);

                glVertex3f(12.0277,29.0152,4.6635);


			}glEnd();



//10
							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(9.3219,29.3749,6.1519);
                glVertex3f(6.8049,30.4643,6.1519);
                glVertex3f(6.9023,30.6384,6.1519);
                glVertex3f(9.3821,29.5651,6.1519);




			}glEnd();


								   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
			                    glVertex3f(9.3821,29.5651,6.1519);
                glVertex3f(6.9023,30.6384,6.1519);

                glVertex3f(6.9023,30.6384,4.6635);

			                    glVertex3f(9.3821,29.5651,4.6635);


			}glEnd();


//11
							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(6.8049,30.4643,6.1519);
                glVertex3f(4.5600,32.0399,6.1519);
                glVertex3f(4.6906,32.1907,6.1519);
                glVertex3f(6.9023,30.6384,6.1519);




			}glEnd();

									   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
			                    glVertex3f(6.9023,30.6384,6.1519);
                glVertex3f(4.6906,32.1907,6.1519);

                glVertex3f(4.6906,32.1907,4.6635);

			                    glVertex3f(6.9023,30.6384,4.6635);


			}glEnd();


//12
							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(4.5600,32.0399,6.1519);
                glVertex3f(2.6799,34.0368,6.1519);
                glVertex3f(2.8383,34.1581,6.1519);
                glVertex3f(4.6906,32.1907,6.1519);




			}glEnd();


									   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(4.6906,32.1907,6.1519);
                glVertex3f(2.8383,34.1581,6.1519);

                glVertex3f(2.8383,34.1581,4.6635);
                glVertex3f(4.6906,32.1907,4.6635);



			}glEnd();




//13

							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(2.6799,34.0368,6.1519);
                glVertex3f(1.2423,36.3724,6.1519);
                glVertex3f(1.4219,36.4592,6.1519);
                glVertex3f(2.8383,34.1581,6.1519);




			}glEnd();




							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
			                    glVertex3f(2.8383,34.1581,6.1519);
                glVertex3f(1.4219,36.4592,6.1519);

                glVertex3f(1.4219,36.4592,4.6635);

			                    glVertex3f(2.8383,34.1581,4.6635);


			}glEnd();




//14




							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

                glVertex3f(1.2423,36.3724,6.1519);
                glVertex3f(0.3064,38.9505,6.1519);
                glVertex3f(0.4999,38.9991,6.1519);
                glVertex3f(1.4219,36.4592,6.1519);




			}glEnd();


							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

                glVertex3f(1.4219,36.4592,6.1519);
                glVertex3f(0.4999,38.9991,6.1519);

                glVertex3f(0.4999,38.9991,4.6635);

                glVertex3f(1.4219,36.4592,4.6635);


			}glEnd();


//15
							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

                glVertex3f(0.3064,38.9505,6.1519);
                glVertex3f(-0.0892,41.6656,6.1519);
               glVertex3f(0.1102,41.673,6.1519);
                glVertex3f(0.4999,38.9991,6.1519);




			}glEnd();

									   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

                glVertex3f(0.4999,38.9991,6.1519);
               glVertex3f(0.1102,41.673,6.1519);

               glVertex3f(0.1102,41.673,4.6635);

                glVertex3f(0.4999,38.9991,4.6635);


			}glEnd();


//16

							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

               glVertex3f(-0.0892,41.6656,6.1519);
                glVertex3f(-0.122,42.4984,5.1795);
               glVertex3f(0.0763,42.5058,5.1795);
               glVertex3f(0.1102,41.673,6.1519);




			}glEnd();

							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

               glVertex3f(0.1102,41.673,6.1519);
           glVertex3f(0.0763,42.5058,5.1795);

           glVertex3f(0.0763,42.5058,4.6635);

               glVertex3f(0.1102,41.673,4.0730);


			}glEnd();


//17

							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

                glVertex3f(-0.122,42.4984,5.1795);
               glVertex3f(-0.4492,50.5270,5.1795);
               glVertex3f(-0.251,50.5351,5.1795);
               glVertex3f(0.0763,42.5058,5.1795);




			}glEnd();

										   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

               glVertex3f(0.0763,42.5058,5.1795);
               glVertex3f(-0.251,50.5351,5.1795);
               glVertex3f(-0.2607,50.7730,4.6635);
               glVertex3f(0.0763,42.5058,4.0730);





			}glEnd();


										   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{

               glVertex3f(-0.2607,50.7730,4.6635);
               glVertex3f(-0.0431,45.4353,4.0730);
               glVertex3f(0.0763,42.5058,4.0730);





			}glEnd();

													   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{

               glVertex3f(-0.2607,50.7730,4.6635);
               glVertex3f(-0.0431,45.4353,1.9547);
               glVertex3f(-0.0431,45.4353,4.0730);





			}glEnd();



//18

							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{

               glVertex3f(-0.4492,50.5270,5.1795);
               glVertex3f(-0.467,50.9633,4.6635);
               glVertex3f(-0.2687,50.9713,4.6635);
               glVertex3f(-0.251,50.5351,5.1795);




			}glEnd();


							   glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_TRIANGLES);{
                glVertex3f(-0.251,50.5351,5.1795);
                glVertex3f(-0.2687,50.9713,4.6635);
               glVertex3f(-0.2607,50.773,4.6635);



			}glEnd();





	}glPopMatrix();


}

void drawStair()
{
     glPushMatrix();{


            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-50.0735,8.3633,4.6635);
                glVertex3f(-50.0762,8.3637,4.7925);
                glVertex3f(-50.5188,5.0641,4.7925);
                glVertex3f(-50.5185,5.0641,4.6635);


			}glEnd();


			            glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-50.0762,8.3637,4.7925);
                glVertex3f(-49.9585,8.3473,4.7921);
                glVertex3f(-50.4003,5.0483,4.7921);
                glVertex3f(-50.5188,5.0641,4.7925);


			}glEnd();

			            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-49.9585,8.3473,4.7921);
                glVertex3f(-49.9585,8.3478,4.9215);
                glVertex3f(-50.4007,5.0484,4.9215);
                glVertex3f(-50.4003,5.0483,4.7921);


			}glEnd();


			            glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-49.9585,8.3478,4.9215);
                glVertex3f(-49.8404,8.3319,4.9215);
                glVertex3f(-50.2819,5.0325,4.9215);

                glVertex3f(-50.4007,5.0484,4.9215);


			}glEnd();



			            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-49.8404,8.3319,4.9215);
                glVertex3f(-49.8404,8.3319,5.0505);
                glVertex3f(-50.2827,5.0326,5.0505);
                glVertex3f(-50.2819,5.0325,4.9215);


			}glEnd();


			            glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-49.8404,8.3319,5.0505);
                glVertex3f(-41.874,7.2596,5.0505);
                glVertex3f(-42.3224,3.9707,5.0505);
                glVertex3f(-50.2827,5.0326,5.0505);


			}glEnd();

			            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.874,7.2596,5.0505);
                glVertex3f(-41.874,7.2596,5.1795);
                glVertex3f(-42.3224,3.9707,5.1795);
                glVertex3f(-42.3224,3.9707,5.0505);


			}glEnd();
			            glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.874,7.2596,5.1795);
                glVertex3f(-41.756,7.2436,5.1795);
                glVertex3f(-42.2044,3.955,5.1795);
                glVertex3f(-42.3224,3.9707,5.1795);


			}glEnd();


			            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.756,7.2436,5.1795);
                glVertex3f(-41.756,7.2436,5.3085);
                glVertex3f(-42.2044,3.955,5.3085);
                glVertex3f(-42.2044,3.955,5.1795);


			}glEnd();


			            glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.756,7.2436,5.3085);
                glVertex3f(-41.638,7.2275,5.3085);
                glVertex3f(-42.0864,3.9392,5.3085);
                glVertex3f(-42.2044,3.955,5.3085);


			}glEnd();

			            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.638,7.2275,5.3085);
                glVertex3f(-41.638,7.2275,5.4375);
                glVertex3f(-42.0864,3.9392,5.4375);
                glVertex3f(-42.0864,3.9392,5.3085);


			}glEnd();

			            glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.638,7.2275,5.4375);
                glVertex3f(-41.52,7.2115,5.4375);
                glVertex3f(-41.9684,3.9235,5.4375);
                glVertex3f(-42.0864,3.9392,5.4375);


			}glEnd();


						            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.52,7.2115,5.4375);
                glVertex3f(-41.52,7.2115,5.5665);
                glVertex3f(-41.9684,3.9235,5.5665);
                glVertex3f(-41.9684,3.9235,5.4375);


			}glEnd();

						            glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.52,7.2115,5.5665);
                glVertex3f(-41.4020,7.1954,5.5665);
                glVertex3f(-41.8503,3.9078,5.5665);
                glVertex3f(-41.9684,3.9235,5.5665);


			}glEnd();

						            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.4020,7.1954,5.5665);
                glVertex3f(-41.4020,7.1954,5.6955);
                glVertex3f(-41.8503,3.9078,5.6955);
                glVertex3f(-41.8503,3.9078,5.5665);


			}glEnd();


						            glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.4020,7.1954,5.6955);
                glVertex3f(-41.284,7.1794,5.6955);
                glVertex3f(-41.7323,3.892,5.6955);
                glVertex3f(-41.8503,3.9078,5.6955);


			}glEnd();

						            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.284,7.1794,5.6955);
                glVertex3f(-41.284,7.1794,5.8244);
                glVertex3f(-41.7323,3.892,5.8244);
                glVertex3f(-41.7323,3.892,5.6955);


			}glEnd();



						            glColor3f(0.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.284,7.1794,5.8244);
                glVertex3f(-41.1661,7.1633,5.8244);
                glVertex3f(-41.6143,3.8763,5.8244);
                glVertex3f(-41.7323,3.892,5.8244);


			}glEnd();

						            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.1661,7.1633,5.8244);
                glVertex3f(-41.1661,7.1633,5.9534);
                glVertex3f(-41.6143,3.8763,5.9534);
                glVertex3f(-41.6143,3.8763,5.8244);


			}glEnd();

						            glColor3f(0.0, 0.0, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.1661,7.1633,5.9534);
                glVertex3f(-41.0481,7.1473,5.9534);
                glVertex3f(-41.4963,3.8605,5.9534);
                glVertex3f(-41.6143,3.8763,5.9534);


			}glEnd();

					            glColor3f(0.5, 0.5, 1.0);
			glBegin(GL_QUADS);{
                glVertex3f(-41.5229,3.6629,4.6635);
                glVertex3f(-41.0481,3.7535,5.9534);
                glVertex3f(-40.7183,7.1374,5.9534);
                glVertex3f(-41.0208,7.345,4.6635);

			}glEnd();








	}glPopMatrix();

     }
void drawBase_innerPillar()
{

     glPushMatrix();{


//pillar thick
            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(28.3303,-5.0232,1.9547);
                glVertex3f(28.3303,-5.0232,4.6635);
                glVertex3f(28.7455,-2.6683,4.6635);
                glVertex3f(28.7455,-2.6683,1.9547);



			}glEnd();


			            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(27.3042,-4.8423,1.9547);
                glVertex3f(27.3042,-4.8423,4.6635);
                glVertex3f(28.3303,-5.0232,4.6635);
                glVertex3f(28.3303,-5.0232,1.9547);



			}glEnd();


			            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(28.7455,-2.6683,1.9547);
                glVertex3f(28.7455,-2.6683,4.6635);
                glVertex3f(27.7195,-2.4874,4.6635);
                glVertex3f(27.7195,-2.4874,1.9547);

			}glEnd();



			            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(27.7195,-2.4874,1.9547);
                glVertex3f(27.7195,-2.4874,4.6635);
                glVertex3f(27.3042,-4.8423,4.6635);
                glVertex3f(27.3042,-4.8423,1.9547);



			}glEnd();



//pillar thin and tall


/*
glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(-1.9391,42.3806,1.9547);
                glVertex3f(-1.6417,42.3924,1.9547);
                glVertex3f(-1.6101,41.6044,1.9547);
                glVertex3f(-1.9090,41.5924,1.9547);



			}glEnd();

			glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(-1.9391,42.3806,4.6635);
                glVertex3f(-1.6417,42.3924,4.6635);
                glVertex3f(-1.6102,41.6044,4.6635);
                glVertex3f(-1.9092,41.5971,4.6635);


			}glEnd();
*/


			//start
						glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(-1.9391,42.3806,1.9547);
                glVertex3f(-1.9391,42.3806,4.6635);
                glVertex3f(-1.9092,41.5971,4.6635);
                glVertex3f(-1.9090,41.5924,1.9547);



			}glEnd();

            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(-1.6417,42.3924,1.9547);
                glVertex3f(-1.6417,42.3924,4.6635);
                glVertex3f(-1.9391,42.3806,4.6635);
                glVertex3f(-1.9391,42.3806,1.9547);



			}glEnd();

			            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(-1.6101,41.6044,1.9547);
                glVertex3f(-1.6102,41.6044,4.6635);

                glVertex3f(-1.6417,42.3924,4.6635);
                glVertex3f(-1.6417,42.3924,1.9547);



			}glEnd();


	            glColor3f(0.0, 0.5, 0.0);
			glBegin(GL_QUADS);{
                glVertex3f(-1.9090,41.5924,1.9547);
                glVertex3f(-1.9092,41.5971,4.6635);
                glVertex3f(-1.6102,41.6044,4.6635);

                glVertex3f(-1.6101,41.6044,1.9547);


			}glEnd();




		}glPopMatrix();
}

void drawBase_innerCircle()
{
         glPushMatrix();{


    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(40.2348,-21.0336,1.9547);
                glVertex3f(41.1874,-19.2996,1.9547);
            }glEnd();



    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(40.1503,-18.8904,1.9547);
				glVertex3f(35.4776,-16.0489,1.9547);
			}glEnd();


    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(35.4776,-16.0489,1.9547);
				glVertex3f(33.5702,-14.1350,1.9547);
			}glEnd();


    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(33.5702,-14.1350,1.9547);
				glVertex3f(32.0889,-11.8751,1.9547);
			}glEnd();



    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(32.0889,-11.8751,1.9547);
				glVertex3f(31.095,-9.3624,1.9547);
			}glEnd();


    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(31.095,-9.3624,1.9547);
				glVertex3f(30.6296,-6.7007,1.9547);
			}glEnd();


    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(30.6296,-6.7007,1.9547);
				glVertex3f(30.7117,-3.9998,1.9547);
			}glEnd();


    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(30.7117,-3.9998,1.9547);
				glVertex3f(31.3380,-1.3713,1.9547);
			}glEnd();



    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(31.3380,-1.3713,1.9547);
				glVertex3f(32.4828,1.0764,1.9547);
			}glEnd();


    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(32.4828,1.0764,1.9547);
				glVertex3f(34.0986,3.2422,1.9547);
			}glEnd();



    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(34.0986,3.2422,1.9547);
				glVertex3f(38.46,6.3857,1.9547);
			}glEnd();


    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(38.46,6.3857,1.9547);
				glVertex3f(41.0256,7.2337,1.9547);
			}glEnd();



    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(41.0256,7.2337,1.9547);
				glVertex3f(43.6904,7.7032,1.9547);
			}glEnd();

    		  glColor3f(0.0, 0.5, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(0.0,0.0,1.9547);
				glVertex3f(44.8116,7.8034,1.9547);
				glVertex3f(44.3418,9.7497,1.9547);
			}glEnd();







		}glPopMatrix();

}

void drawBase1_leftface()
{
		glPushMatrix();{
	//		glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left part of a petal face

//1st layer
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(53.4968,-28.8032,0.3473);
				glVertex3f(48.8869,-27.2255,4.6635);
				glVertex3f(50.2543,-24.6173,4.6635);
				glVertex3f(54.1767,-27.5063,0.3473);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(54.1767,-27.5063,0.3473);
				glVertex3f(50.2543,-24.6173,4.6635);

                glVertex3f(66.0919,-8.8239,4.6635);
				glVertex3f(72.1067,-9.6264,0.3473);


			}glEnd();


            glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(72.1067,-9.6264,0.3473);
				glVertex3f(66.0919,-8.8239,4.6635);
				glVertex3f(54.9498,10.5679,4.6635);
				glVertex3f(59.4953,12.3225,0.3473);

			}glEnd();

//2nd layer
            glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(48.8869,-27.2255,4.6635);
				glVertex3f(46.2973,-27.2255,4.6635);
				glVertex3f(48.1777,-23.0878,4.6635);
				glVertex3f(50.2543,-24.6173,4.6635);

			}glEnd();

            glColor3f(0.5, 0.5, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(50.2543,-24.6173,4.6635);

				glVertex3f(48.1777,-23.0878,4.6635);
				glVertex3f(62.9031,-8.4035,4.6635);
				glVertex3f(66.0919,-8.8239,4.6635);

			}glEnd();

            glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(66.0919,-8.8239,4.6635);
                glVertex3f(62.9031,-8.4035,4.6635);
				glVertex3f(52.5433,9.6390,4.6635);
				glVertex3f(54.9498,10.5679,4.6635);

			}glEnd();

//3rd layer
            glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(46.2973,-27.2255,4.6635);
				glVertex3f(44.1035,-25.162,4.6635);
				glVertex3f(45.8320,-21.8613,4.6635);
				glVertex3f(48.1777,-23.0878,4.6635);

			}glEnd();

            glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(48.1777,-23.0878,4.6635);
				glVertex3f(45.8320,-21.8613,4.6635);
				glVertex3f(59.7539,-7.9782,4.6635);
				glVertex3f(62.9031,-8.4035,4.6635);

			}glEnd();

            glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

                glVertex3f(62.9031,-8.4035,4.6635);

				glVertex3f(59.7539,-7.9782,4.6635);
				glVertex3f(50.1503,9.1157,4.6635);
				glVertex3f(52.5433,9.6390,4.6635);

			}glEnd();

//4th layer (grass)
glColor3f(.5, 0.5, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(45.8320,-21.8613,4.6635);
				glVertex3f(41.0999,-19.3869,1.9547);
				glVertex3f(53.3916,-7.1294,1.9547);
				glVertex3f(59.7539,-7.9782,4.6635);

			}glEnd();

						glColor3f(0.5, 0.5, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(59.7539,-7.9782,4.6635);

				glVertex3f(53.3916,-7.1294,1.9547);
				glVertex3f(44.7376,7.9322,1.9547);
				glVertex3f(50.1503,9.1157,4.6635);

			}glEnd();

//5th layer (near water)
glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(41.0999,-19.3869,1.9547);
				glVertex3f(40.1503,-18.8904,1.9051);
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(53.3916,-7.1294,1.9547);

			}glEnd();

						glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(53.3916,-7.1294,1.9547);

				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(43.6904,7.7032,1.9051);
				glVertex3f(44.7376,7.9322,1.9547);

			}glEnd();




		}glPopMatrix();

}

void drawBase_water()
{
        glPushMatrix();{

    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(40.1503,-18.8904,1.9051);
				glVertex3f(35.4776,-16.0489,1.9051);
			}glEnd();


    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(35.4776,-16.0489,1.9051);
				glVertex3f(33.5702,-14.1350,1.9547);
			}glEnd();


    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(33.5702,-14.1350,1.9547);
				glVertex3f(32.0889,-11.8751,1.9051);
			}glEnd();



    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(32.0889,-11.8751,1.9051);
				glVertex3f(31.095,-9.3624,1.9051);
			}glEnd();


    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(31.095,-9.3624,1.9051);
				glVertex3f(30.6296,-6.7007,1.9051);
			}glEnd();


    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(30.6296,-6.7007,1.9051);
				glVertex3f(30.7117,-3.9998,1.9547);
			}glEnd();


    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(30.7117,-3.9998,1.9547);
				glVertex3f(31.3380,-1.3713,1.9051);
			}glEnd();



    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(31.3380,-1.3713,1.9051);
				glVertex3f(32.4828,1.0764,1.9051);
			}glEnd();


    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(32.4828,1.0764,1.9051);
				glVertex3f(34.0986,3.2422,1.9051);
			}glEnd();



    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(34.0986,3.2422,1.9051);
				glVertex3f(38.46,6.3857,1.9051);
			}glEnd();


    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(38.46,6.3857,1.9051);
				glVertex3f(41.0256,7.2337,1.9547);
			}glEnd();



    		  glColor3f(0.0, 0.0, 0.5);
			glBegin(GL_TRIANGLES);{
				glVertex3f(52.115,-6.959,1.9051);
				glVertex3f(41.0256,7.2337,1.9547);
				glVertex3f(43.6904,7.7032,1.9547);
			}glEnd();






		}glPopMatrix();

}

void drawBase_1()
{


	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			///drawBase1_leftface();
			///drawBase_water();
			//drawBase_innerfloor();
			///drawBase_innerCircle();
			///drawBase_innerPillar();
			drawStairSide();
			///drawStair();
			///drawUpperFloor();
			///drawStairSideInside();
			///drawRelling();
			drawInnerCircle2();
			///drawInnerCircle1();

			//drawPetal_2_rightface();
			//drawPetal_2_rightback();


			//------------------- back part of a petal

		}glPopMatrix();
	}


}
void drawInnerCylinder()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.20, 0.60, 0.80);
			glBegin(GL_QUADS);{
				glVertex3f(0.1925,-4.6020,5.9534);
				glVertex3f(0.1925,-4.6020,34.2124);
				glVertex3f(1.6491,-4.0018,34.2124);
				glVertex3f(1.6491,-4.0018,5.9534);
			}glEnd();
			glColor3f(0.80, 0.60, 0.20);
			glBegin(GL_QUADS);{
				glVertex3f(1.6491,-4.0018,5.9534);
				glVertex3f(1.6491,-4.0018,34.2124);
				glVertex3f(3.1056,-3.4016,34.2124);
				glVertex3f(3.1056,-3.4016,5.9534);
			}glEnd();
		}glPopMatrix();

	}
}
void drawPetal_top()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.40, 0.50, 0.60);
			glBegin(GL_POLYGON);{
				glVertex3f(0.1925,-4.6020,34.2124);
				glVertex3f(0.0,0.0,36.3171);
				glVertex3f(3.1056,-3.4016,34.2124);
			}glEnd();
		}glPopMatrix();

	}
}
void drawPetal_0()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.10, 0.20, 0.30);
			glBegin(GL_QUADS);{
				glVertex3f(0.9033,-21.5959,5.9534);
				glVertex3f(0.1925,-4.602,34.2124);
				glVertex3f(3.1056,-3.4016,34.2124);
				glVertex3f(14.5736,-15.9628,5.9534);
			}glEnd();
		}glPopMatrix();

	}
}
void drawPetal_1()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(0.0, 1.0, 0.0);
			glBegin(GL_POLYGON);{
				glVertex3f(-23.4393,25.5512,13.4945);
				glVertex3f(-14.6279,16.0222,9.5255);
				glVertex3f(-24.9601,13.8534,5.9534);
				//glVertex3f(7.8803,27.5289,5.9686);
			}glEnd();

			glColor3f(.50, 0.00, 0.0);
			glBegin(GL_POLYGON);{
				glVertex3f(-23.4393,25.5512,13.4945);
				glVertex3f(-14.6279,16.0222,9.5255);
				glVertex3f(-11.6585,26.1535,5.9534);
				//glVertex3f(9.2724,27.1032,5.9688);
			}glEnd();
		}glPopMatrix();

	}
}
void drawPetal_2_rightface()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// right part of a petal face


			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8174,17.4753,5.9536);
				glVertex3f(12.8459,17.4527,6.8079);
				glVertex3f(13.2772,17.3175,6.8079);
				glVertex3f(13.2846,17.3378,5.9536);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8459,17.4527,6.8079);
				glVertex3f(12.8121,17.4430,7.975);
				glVertex3f(13.267,17.2897,7.975);
				glVertex3f(13.2772,17.3175,6.8079);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.8121,17.4430,7.975);
				glVertex3f(12.7393,17.4329,8.7476);
				glVertex3f(13.2603,17.2713,8.7476);
				glVertex3f(13.267,17.2897,7.975);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.7393,17.4329,8.7476);
				glVertex3f(12.7598,17.4101,9.2870);
				glVertex3f(13.2557,17.2585,9.2870);
				glVertex3f(13.2603,17.2713,8.7476);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.7598,17.4101,9.2870);
				glVertex3f(12.6838,17.3988,9.8639);
				glVertex3f(13.2365,17.2403,9.7977);
				glVertex3f(13.2557,17.2585,9.2870);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.6838,17.3988,9.8639);
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(13.2049,17.2199,10.2509);
				glVertex3f(13.2365,17.2403,9.7977);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.6088,17.3965,10.2298);
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(13.1569,17.1888,10.9394);
				glVertex3f(13.2049,17.2199,10.2509);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.5402,17.3729,10.9394);
				glVertex3f(12.3406,17.3526,11.8870);
				glVertex3f(13.0403,17.1418,11.9161);
				glVertex3f(13.1569,17.1888,10.9394);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.3406,17.3526,11.8870);
				glVertex3f(12.1210,17.3297,12.8261);
				glVertex3f(12.9117,17.0959,12.8444);
				glVertex3f(13.0403,17.1418,11.9161);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(12.1210,17.3297,12.8261);
				glVertex3f(11.8872,17.3103,13.6246);
				glVertex3f(12.7962,17.0680,13.4018);
				glVertex3f(12.9117,17.0959,12.8444);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.8872,17.3103,13.6246);
				glVertex3f(11.6185,17.2905,14.4584);
				glVertex3f(12.5751,17.0101,14.4584);
				glVertex3f(12.7962,17.0680,13.4018);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.6185,17.2905,14.4584);
				glVertex3f(11.3391,17.2732,15.1985);
				glVertex3f(12.3584,16.9667,15.2306);
				glVertex3f(12.5751,17.0101,14.4584);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.3391,17.2732,15.1985);
				glVertex3f(11.033,17.2544,15.9525);
				glVertex3f(12.13,16.9209,16.0443);
				glVertex3f(12.3584,16.9667,15.2306);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(11.033,17.2544,15.9525);
				glVertex3f(10.725,17.2445,16.6159);
				glVertex3f(11.9247,16.8907,16.6267);
				glVertex3f(12.13,16.9209,16.0443);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.725,17.2445,16.6159);
				glVertex3f(10.4384,17.2282,17.2260);
				glVertex3f(11.7518,16.8605,17.1108);
				glVertex3f(11.9247,16.8907,16.6267);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.4384,17.2282,17.2260);
				glVertex3f(10.2728,17.2260,17.5224);
				glVertex3f(11.5788,16.8303,17.5949);
				glVertex3f(11.7518,16.8605,17.1108);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(10.2728,17.2260,17.5224);
				glVertex3f(9.9836,17.2170,18.0356);
				glVertex3f(11.3529,16.7995,18.1157);
				glVertex3f(11.5788,16.8303,17.5949);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.9836,17.2170,18.0356);
				glVertex3f(9.6249,17.2059,18.6719);
				glVertex3f(11.1117,16.7666,18.6719);
				glVertex3f(11.3529,16.7995,18.1157);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.6249,17.2059,18.6719);
				glVertex3f(9.1190,17.2273,19.3169);
				glVertex3f(10.9246,16.7410,19.1031);
				glVertex3f(11.1117,16.7666,18.6719);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(9.1190,17.2273,19.3169);
				glVertex3f(8.8102,17.1978,19.8957);
				glVertex3f(10.5571,16.6995,19.8147);
				glVertex3f(10.9246,16.7410,19.1031);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.8102,17.1978,19.8957);
				glVertex3f(8.4037,17.1981,20.4395);
				glVertex3f(10.2850,16.6702,20.343);
				glVertex3f(10.5571,16.6995,19.8147);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4037,17.1981,20.4395);
				glVertex3f(8.3079,17.1987,20.5624);
				glVertex3f(10.1709,16.6558,20.5624);
				glVertex3f(10.2850,16.6702,20.343);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.3079,17.1987,20.5624);
				glVertex3f(7.9858,17.2008,20.9756);
				glVertex3f(9.8755,16.6287,21.0507);
				glVertex3f(10.1709,16.6558,20.5624);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9858,17.2008,20.9756);
				glVertex3f(7.688,17.2013,21.3566);
				glVertex3f(9.5838,16.6019,21.533);
				glVertex3f(9.8755,16.6287,21.0507);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.688,17.2013,21.3566);
				glVertex3f(7.1801,17.2254,21.8695);
				glVertex3f(9.3215,16.5778,21.9667);
				glVertex3f(9.5838,16.6019,21.533);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1801,17.2254,21.8695);
				glVertex3f(6.6392,17.2254,22.5136);
				glVertex3f(8.9177,16.5488,22.5437);
				glVertex3f(9.3215,16.5778,21.9667);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.6392,17.2254,22.5136);
				glVertex3f(6.2435,17.2407,22.9001);
				glVertex3f(8.6841,16.5320,22.8774);
				glVertex3f(8.9177,16.5488,22.5437);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.2435,17.2407,22.9001);
				glVertex3f(5.9082,17.2548,23.2197);
				glVertex3f(8.5380,16.5215,23.0861);
				glVertex3f(8.6841,16.5320,22.8774);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.9082,17.2548,23.2197);
				glVertex3f(5.3810,17.2755,23.7211);
				glVertex3f(7.8324,16.4827,23.9940);
				glVertex3f(8.5380,16.5215,23.0861);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3810,17.2755,23.7211);
				glVertex3f(5.3383,17.1581,24.1537);
				glVertex3f(7.5229,16.4659,24.3786);
				glVertex3f(7.8324,16.4827,23.9940);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3383,17.1581,24.1537);
				glVertex3f(5.2503,16.9163,25.0453);
				glVertex3f(6.7827,16.4441,25.1717);
				glVertex3f(7.5229,16.4659,24.3786);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2503,16.9163,25.0453);
				glVertex3f(5.1994,16.7765,25.5605);
				glVertex3f(6.2472,16.4209,25.7944);
				glVertex3f(6.7827,16.4441,25.1717);
			}glEnd();


			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.1994,16.7765,25.5605);
				glVertex3f(5.1348,16.599,26.2148);
				glVertex3f(5.7332,16.4209,26.2434);
				glVertex3f(6.2472,16.4209,25.7944);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.1348,16.599,26.2148);
				glVertex3f(5.1025,16.5103,26.5420);
				glVertex3f(5.381,16.4131,26.6131);
				glVertex3f(5.7332,16.4209,26.2434);
			}glEnd();


			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(5.1025,16.5103,26.5420);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.381,16.4131,26.6131);
			}glEnd();

			//------------------- face of a petal end

		}glPopMatrix();
	}
}
void drawPetal_2_leftface()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left part of a petal face

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9067,21.6765,5.9534);
				glVertex3f(-0.9177,21.6462,7.2216);
				glVertex3f(-0.4616,21.5103,7.2216);
				glVertex3f(-0.4435,21.5295,5.9534);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9177,21.6462,7.2216);
				glVertex3f(-0.9375,21.5919,9.4936);
				glVertex3f(-0.3556,21.4027,9.5259);
				glVertex3f(-0.4616,21.5103,7.2216);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.9375,21.5919,9.4936);
				glVertex3f(-0.8943,21.4690,11.1525);
				glVertex3f(-0.159,21.2391,11.0836);
				glVertex3f(-0.3556,21.4027,9.5259);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.8943,21.4690,11.1525);
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.0361,21.0767,12.5058);
				glVertex3f(-0.159,21.2391,11.0836);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(0.3133,20.7896,14.0929);
				glVertex3f(-0.0361,21.0767,12.5058);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(0.76,20.4386,15.6422);
				glVertex3f(0.3133,20.7896,14.0929);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(1.4338,19.9352,17.4426);
				glVertex3f(0.76,20.4386,15.6422);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(2.4082,19.2361,19.4869);
				glVertex3f(1.4338,19.9352,17.4426);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(3.3408,18.6234,20.9393);
				glVertex3f(2.4082,19.2361,19.4869);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(4.3924,17.8982,22.5534);
				glVertex3f(3.3408,18.6234,20.9393);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(5.381,17.2755,23.7211);
				glVertex3f(4.3924,17.8982,22.5534);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(5.3013,17.0566,24.5281);
				glVertex3f(5.381,17.2755,23.7211);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(4.129,17.0735,25.7615);
				glVertex3f(5.1796,16.722,25.7615);
				glVertex3f(5.3013,17.0566,24.5281);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(4.129,17.0735,25.7615);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(5.1796,16.722,25.7615);
			}glEnd();
			//------------------- face of a petal end

		}glPopMatrix();
	}
}
void drawPetal_2_rightback()
{
	for(int i=0; i<1; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			// left back part of a petal face

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.7677,27.9475,8.183);
				glVertex3f(8.0675,27.7653,6.9621);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_TRIANGLES);{
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(8.0675,27.7653,6.9621);
				glVertex3f(7.8803,27.5289,5.9686);
			}glEnd();
			// col-1
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.7677,27.9475,8.1830);
				glVertex3f(8.6470,27.5694,10.3582);
				glVertex3f(8.4380,27.5656,10.0903);
				glVertex3f(8.640,27.93310,8.0288);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.6470,27.5694,10.3582);
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(8.4380,27.5656,10.0903);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4754,27.0348,12.4809);
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(7.8917,26.4047,14.0401);
				glVertex3f(8.1879,27.0545,12.0989);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.2546,26.3489,14.5346);
				glVertex3f(7.9870,25.5183,16.5042);
				glVertex3f(7.5515,25.6224,15.9003);
				glVertex3f(7.8917,26.4047,14.0401);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9870,25.5183,16.5042);
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.1700,24.7146,17.667);
				glVertex3f(7.5515,25.6224,15.9003);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.6752,24.5508,18.3755);
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(6.7501,23.6892,19.3286);
				glVertex3f(7.1700,24.7146,17.667);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.3223,23.4549,20.1358);
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(6.3055,22.22534,20.9061);
				glVertex3f(6.7501,23.6892,19.3286);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.9316,22.2404,21.7735);
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(5.7484,21.2735,22.3183);
				glVertex3f(6.3055,22.22534,20.9061);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.5067,20.9174,23.2788);
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(5.2319,19.9499,23.6042);
				glVertex3f(5.7484,21.2735,22.3183);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.0514,19.4966,24.6435);
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(4.6909,18.5466,24.7533);
				glVertex3f(5.2319,19.9499,23.6042);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5694,17.9891,25.8609);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(4.1290,17.0735,25.7615);
				glVertex3f(4.6909,18.5466,24.7533);
			}glEnd();
			//col=2
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.640,27.93310,8.0288);
				glVertex3f(8.4380,27.5656,10.0903);

				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(8.5208,27.9148,7.8667);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4380,27.5656,10.0903);
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(7.9210,27.0652,11.6987);
				glVertex3f(8.2435,27.5553,9.8093);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.1879,27.0545,12.0989);
				glVertex3f(7.8917,26.4047,14.0401);

				glVertex3f(7.5553,26.4491,13.5223);
				glVertex3f(7.9210,27.0652,11.6987);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.8917,26.4047,14.0401);
				glVertex3f(7.5515,25.6224,15.9003);

				glVertex3f(7.1486,25.7127,15.2679);
				glVertex3f(7.5553,26.4491,13.5223);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.5515,25.6224,15.9003);
				glVertex3f(7.1700,24.7146,17.667);


				glVertex3f(6.7034,24.8623,16.9246);
				glVertex3f(7.1486,25.7127,15.2679);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1700,24.7146,17.667);
				glVertex3f(6.7501,23.6892,19.3286);


				glVertex3f(6.2225,23.9053,18.4823);
				glVertex3f(6.7034,24.8623,16.9246);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7501,23.6892,19.3286);
				glVertex3f(6.3055,22.22534,20.9061);


				glVertex3f(5.5906,22.7548,19.9831);
				glVertex3f(6.2225,23.9053,18.4823);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.3055,22.22534,20.9061);
				glVertex3f(5.7484,21.2735,22.3183);


				glVertex3f(5.0451,21.6080,21.3062);
				glVertex3f(5.5906,22.7548,19.9831);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.7484,21.2735,22.3183);
				glVertex3f(5.2319,19.9499,23.6042);


				glVertex3f(4.4737,20.3799,22.5077);
				glVertex3f(5.0451,21.6080,21.3062);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2319,19.9499,23.6042);
				glVertex3f(4.6909,18.5466,24.7533);


				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(4.4737,20.3799,22.5077);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.6909,18.5466,24.7533);
				glVertex3f(4.1290,17.0735,25.7615);


				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(3.8798,19.0793,23.5829);
			}glEnd();

			//col=3
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.5208,27.9148,7.8667);

				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(8.4106,27.8925,7.6974);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.2435,27.5553,9.8093);
				glVertex3f(7.9210,27.0652,11.6987);
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(8.0643,27.5384,9.5163);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9210,27.0652,11.6987);

				glVertex3f(7.5553,26.4491,13.5223);
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(7.6757,27.0666,11.2820);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.5553,26.4491,13.5223);

				glVertex3f(7.1486,25.7127,15.2679);
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(7.2468,26.4815,12.9832);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.1486,25.7127,15.2679);


				glVertex3f(6.7034,24.8623,16.9246);
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(6.7797,25.7882,14.6096);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7034,24.8623,16.9246);


				glVertex3f(6.2225,23.9053,18.4823);
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(6.277,24.9925,16.1515);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.2225,23.9053,18.4823);


				glVertex3f(5.5906,22.7548,19.9831);
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(5.7412,24.1011,17.6003);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5906,22.7548,19.9831);


				glVertex3f(5.0451,21.6080,21.3062);
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(4.9956,22.9798,19.0224);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.0451,21.6080,21.3062);


				glVertex3f(4.4737,20.3799,22.5077);
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(4.3989,21.9176,20.2465);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.4737,20.3799,22.5077);


				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(3.7788,20.7825,21.3583);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.8798,19.0793,23.5829);
				glVertex3f(3.2669,17.715,24.5281);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(3.1383,19.5823,22.3538);

			}glEnd();

			//col=4
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.4106,27.8925,7.6974);
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(8.3098,27.8663,7.5216);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.0643,27.5384,9.5163);
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(7.9010,27.5149,9.2126);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.6757,27.0666,11.2820);
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(7.4528,27.0586,10.8504);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.2468,26.4815,12.9832);
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(6.9671,26.5013,12.4251);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7797,25.7882,14.6096);
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(6.4461,25.848,13.928);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.277,24.9925,16.1515);
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(5.8921,25.1039,15.3507);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.7412,24.1011,17.6003);
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(5.3077,24.2747,16.6861);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9956,22.9798,19.0224);
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(4.4532,23.1801,18.0232);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.3989,21.9176,20.2465);
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(3.8117,22.1992,19.1435);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.7788,20.7825,21.3583);
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(3.1491,21.1539,20.1604);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.1383,19.5823,22.3538);
				glVertex3f(2.4806,18.3247,23.2301);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(2.4683,20.0510,21.0708);
			}glEnd();

			//col=5

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.3098,27.8663,7.5216);
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(8.2188,27.8363,7.3399);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9010,27.5149,9.2126);
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(7.7544,27.4847,8.8995);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.4528,27.0586,10.8504);
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(7.2532,27.0408,10.4057);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.9671,26.5013,12.4251);
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(6.7173,26.5082,11.8504);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.4461,25.848,13.928);
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(6.1488,25.8913,13.226);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.8921,25.1039,15.3507);
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(5.5499,25.1950,14.5257);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.3077,24.2747,16.6861);
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(4.9232,24.4244,15.7434);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.4532,23.1801,18.0232);
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(3.9652,23.3534,16.9896);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.8117,22.1992,19.1435);
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(3.2853,22.4499,18.0016);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.1491,21.1539,20.1604);
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(2.5865,21.4904,18.9188);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4683,20.0510,21.0708);
				glVertex3f(1.7722,18.8972,21.8723);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(1.8716,20.4809,19.7386);
			}glEnd();

			//col=6
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.2188,27.8363,7.3399);
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(8.1379,27.8026,7.1532);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.7544,27.4847,8.8995);
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(7.0775,27.0131,9.9497);

				glVertex3f(7.6248,27.4479,8.5781);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.2532,27.0408,10.4057);
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(6.4982,26.5017,11.2612);

				glVertex3f(7.0775,27.0131,9.9497);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7173,26.5082,11.8504);
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(5.8888,25.9175,12.5063);

				glVertex3f(6.4982,26.5017,11.2612);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.1488,25.8913,13.226);
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(5.2517,25.265,13.6796);

				glVertex3f(5.8888,25.9175,12.5063);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.5499,25.1950,14.5257);
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(4.5891,24.5486,14.7761);

				glVertex3f(5.2517,25.265,13.6796);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9232,24.4244,15.7434);
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(3.533,23.4977,15.9259);

				glVertex3f(4.5891,24.5486,14.7761);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.9652,23.3534,16.9896);
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(2.821,22.667,16.8254);

				glVertex3f(3.533,23.4977,15.9259);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(3.2853,22.4499,18.0016);
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(2.0924,21.7887,17.6382);

				glVertex3f(2.821,22.667,16.8254);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.5865,21.4904,18.9188);
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(1.3108,20.8513,18.3386);

				glVertex3f(2.0924,21.7887,17.6382);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(1.8716,20.4809,19.7386);
				glVertex3f(1.1432,19.4271,20.4595);
				glVertex3f(0.5953,19.9095,18.9972);

				glVertex3f(1.3108,20.8513,18.3386);
			}glEnd();


			//col=7
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.1379,27.8026,7.1532);
				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(8.0675,27.7653,6.9621);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.6248,27.4479,8.5781);
				glVertex3f(7.0775,27.0131,9.9497);
				glVertex3f(6.9265,26.9755,9.4844);

				glVertex3f(7.5128,27.4045,8.2499);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.0775,27.0131,9.9497);
				glVertex3f(6.4982,26.5017,11.2612);
				glVertex3f(6.3105,26.4815,10.6601);

				glVertex3f(6.9265,26.9755,9.4844);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.4982,26.5017,11.2612);
				glVertex3f(5.8888,25.9175,12.5063);
				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(6.3105,26.4815,10.6601);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(5.8888,25.9175,12.5063);
				glVertex3f(5.2517,25.265,13.6796);
				glVertex3f(4.9982,25.3128,12.816);

				glVertex3f(5.6671,25.926,11.7721);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(5.2517,25.265,13.6796);

				glVertex3f(4.5891,24.5486,14.7761);
				glVertex3f(4.3063,24.646,13.7879);

				glVertex3f(4.9982,25.3128,12.816);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(4.5891,24.5486,14.7761);
				glVertex3f(3.533,23.4977,15.9259);
				glVertex3f(3.158,23.6113,14.8363);

				glVertex3f(4.3063,24.646,13.7879);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(3.533,23.4977,15.9259);
				glVertex3f(2.821,22.667,16.8254);
				glVertex3f(2.4203,22.8482,15.6197);

				glVertex3f(3.158,23.6113,14.8363);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.821,22.667,16.8254);
				glVertex3f(2.0924,21.7887,17.6382);
				glVertex3f(1.6681,22.0457,16.3239);

				glVertex3f(2.4203,22.8482,15.6197);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(2.0924,21.7887,17.6382);
				glVertex3f(1.3108,20.8513,18.3386);
				glVertex3f(0.9037,21.2082,16.9479);

				glVertex3f(1.6681,22.0457,16.3239);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.3108,20.8513,18.3386);
				glVertex3f(0.5953,19.9095,18.9972);
				glVertex3f(0.1295,20.3398,17.491);

				glVertex3f(0.9037,21.2082,16.9479);
			}glEnd();


			//col=8
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.0675,27.7653,6.9621);
				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(7.4188,27.3546,7.916);
				glVertex3f(8.0078,27.7244,6.7674);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.5128,27.4045,8.2499);
				glVertex3f(6.9265,26.9755,9.4844);
				glVertex3f(6.8006,26.928,9.0114);

				glVertex3f(7.4188,27.3546,7.916);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.9265,26.9755,9.4844);
				glVertex3f(6.3105,26.4815,10.6601);
				glVertex3f(6.155,26.4475,10.0494);

				glVertex3f(6.8006,26.928,9.0114);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.3105,26.4815,10.6601);
				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(5.4843,25.9163,11.0261);

				glVertex3f(6.155,26.4475,10.0494);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.6671,25.926,11.7721);
				glVertex3f(4.9982,25.3128,12.816);
				glVertex3f(4.7904,25.3377,11.9382);

				glVertex3f(5.4843,25.9163,11.0261);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.9982,25.3128,12.816);
				glVertex3f(4.3063,24.646,13.7879);
				glVertex3f(4.0757,24.7154,12.783);

				glVertex3f(4.7904,25.3377,11.9382);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(4.3063,24.646,13.7879);
				glVertex3f(3.158,23.6113,14.8363);
				glVertex3f(2.8411,23.6927,13.7254);

				glVertex3f(4.0757,24.7154,12.783);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(3.158,23.6113,14.8363);
				glVertex3f(2.4203,22.8482,15.6197);
				glVertex3f(2.0842,22.9914,14.3894);

				glVertex3f(2.8411,23.6927,13.7254);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(2.4203,22.8482,15.6197);
				glVertex3f(1.6681,22.0457,16.3239);
				glVertex3f(1.3147,22.2589,14.9813);

				glVertex3f(2.0842,22.9914,14.3894);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.6681,22.0457,16.3239);
				glVertex3f(0.9037,21.2082,16.9479);
				glVertex3f(0.5348,21.4986,15.5005);

				glVertex3f(1.3147,22.2589,14.9813);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.9037,21.2082,16.9479);
				glVertex3f(0.1295,20.3398,17.491);
				glVertex3f(-0.2531,20.7141,15.947);

				glVertex3f(0.5348,21.4986,15.5005);

			}glEnd();


			//col=9
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(8.0078,27.7244,6.7674);
				glVertex3f(7.4188,27.3546,7.916);
				glVertex3f(7.3432,27.2984,7.5779);
				glVertex3f(7.9591,27.6802,6.5699);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.4188,27.3546,7.916);
				glVertex3f(6.8006,26.928,9.0114);
				glVertex3f(6.7003,26.8706,8.5328);

				glVertex3f(7.3432,27.2984,7.5779);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.8006,26.928,9.0114);
				glVertex3f(6.155,26.4475,10.0494);
				glVertex3f(6.0322,26.3996,9.4316);

				glVertex3f(6.7003,26.8706,8.5328);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(6.155,26.4475,10.0494);
				glVertex3f(5.4843,25.9163,11.0261);

				glVertex3f(5.3411,25.8882,10.2714);

				glVertex3f(6.0322,26.3996,9.4316);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.4843,25.9163,11.0261);
				glVertex3f(4.7904,25.3377,11.9382);
				glVertex3f(4.629,25.3393,11.05);

				glVertex3f(5.3411,25.8882,10.2714);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.7904,25.3377,11.9382);
				glVertex3f(4.0757,24.7154,12.783);
				glVertex3f(3.898,24.7559,11.7653);

				glVertex3f(4.629,25.3393,11.05);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(4.0757,24.7154,12.783);
				glVertex3f(2.8411,23.6927,13.7254);
				glVertex3f(2.5835,23.7406,12.5976);

				glVertex3f(3.898,24.7559,11.7653);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(2.8411,23.6927,13.7254);
				glVertex3f(2.0842,22.9914,14.3894);
				glVertex3f(1.8137,23.0951,13.1395);

				glVertex3f(2.5835,23.7406,12.5976);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

					glVertex3f(2.0842,22.9914,14.3894);

					glVertex3f(1.3147,22.2589,14.9813);


				glVertex3f(1.0331,22.4261,13.6157);

				glVertex3f(1.8137,23.0951,13.1395);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.3147,22.2589,14.9813);
				glVertex3f(0.5348,21.4986,15.5005);
				glVertex3f(0.244,21.7364,14.0263);

				glVertex3f(1.0331,22.4261,13.6157);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.5348,21.4986,15.5005);
				glVertex3f(-0.2531,20.7141,15.947);
				glVertex3f(-0.5516,21.0288,14.3715);

				glVertex3f(0.244,21.7364,14.0263);

			}glEnd();

			//col=10
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9591,27.6802,6.5699);
				glVertex3f(7.3432,27.2984,7.5779);
				glVertex3f(7.2862,27.236,7.2368);
				glVertex3f(7.9215,27.6327,6.3704);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.3432,27.2984,7.5779);
				glVertex3f(6.7003,26.8706,8.5328);
				glVertex3f(6.6259,26.8035,8.0505);

				glVertex3f(7.2862,27.236,7.2368);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.7003,26.8706,8.5328);
				glVertex3f(6.0322,26.3996,9.4316);
				glVertex3f(5.9425,26.3379,8.8092);

				glVertex3f(6.6259,26.8035,8.0505);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

					glVertex3f(6.0322,26.3996,9.4316);

				glVertex3f(5.3411,25.8882,10.2714);
				glVertex3f(5.2379,25.8415,9.511);

				glVertex3f(5.9425,26.3379,8.8092);
			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.3411,25.8882,10.2714);
				glVertex3f(4.629,25.3393,11.05);
				glVertex3f(4.5143,25.3171,10.1547);

				glVertex3f(5.2379,25.8415,9.511);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.629,25.3393,11.05);
				glVertex3f(3.898,24.7559,11.7653);
				glVertex3f(3.7737,24.7671,10.7389);
				glVertex3f(4.5143,25.3171,10.1547);



			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(3.898,24.7559,11.7653);
				glVertex3f(2.5835,23.7406,12.5976);
				glVertex3f(2.3858,23.7543,11.4575);

				glVertex3f(3.7737,24.7671,10.7389);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{



				glVertex3f(2.5835,23.7406,12.5976);
				glVertex3f(1.8137,23.0951,13.1395);
				glVertex3f(1.6095,23.158,11.8751);

				glVertex3f(2.3858,23.7543,11.4575);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.8137,23.0951,13.1395);


				glVertex3f(1.0331,22.4261,13.6157);
				glVertex3f(0.8241,22.5455,12.2329);

				glVertex3f(1.6095,23.158,11.8751);
			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.0331,22.4261,13.6157);
				glVertex3f(0.244,21.7364,14.0263);
				glVertex3f(0.0318,21.9191,12.5313);

				glVertex3f(0.8241,22.5455,12.2329);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(0.244,21.7364,14.0263);
				glVertex3f(-0.5516,21.0288,14.3715);
				glVertex3f(-0.7655,21.2812,12.7711);

				glVertex3f(0.0318,21.9191,12.5313);

			}glEnd();

			//col=11
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.9215,27.6327,6.3704);
				glVertex3f(7.2862,27.236,7.2368);
				glVertex3f(7.2481,27.1676,6.8942);
				glVertex3f(7.8952,27.5822,6.1697);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.2862,27.236,7.2368);
				glVertex3f(6.6259,26.8035,8.0505);
				glVertex3f(6.5778,26.7269,7.5662);

				glVertex3f(7.2481,27.1676,6.8942);




			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.6259,26.8035,8.0505);
				glVertex3f(5.9425,26.3379,8.8092);
				glVertex3f(5.8862,26.2624,8.1845);

				glVertex3f(6.5778,26.7269,7.5662);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(5.9425,26.3379,8.8092);
				glVertex3f(5.2379,25.8415,9.511);
				glVertex3f(5.1752,25.7764,8.748);

				glVertex3f(5.8862,26.2624,8.1845);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.2379,25.8415,9.511);
				glVertex3f(4.5143,25.3171,10.1547);
				glVertex3f(4.4469,25.2711,9.2559);

				glVertex3f(5.1752,25.7764,8.748);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.5143,25.3171,10.1547);
				glVertex3f(3.7737,24.7671,10.7389);
				glVertex3f(3.7032,24.7486,9.708);

				glVertex3f(4.4469,25.2711,9.2559);



			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(3.7737,24.7671,10.7389);
				glVertex3f(2.3858,23.7543,11.4575);
				glVertex3f(2.2486,23.7331,10.3098);

				glVertex3f(3.7032,24.7486,9.708);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{



				glVertex3f(2.3858,23.7543,11.4575);
				glVertex3f(1.6095,23.158,11.8751);
				glVertex3f(1.4722,23.1791,10.6014);

				glVertex3f(2.2486,23.7331,10.3098);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.6095,23.158,11.8751);
				glVertex3f(0.8241,22.5455,12.2329);
				glVertex3f(0.6882,22.6159,10.8385);

				glVertex3f(1.4722,23.1791,10.6014);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.8241,22.5455,12.2329);
				glVertex3f(0.0318,21.9191,12.5313);
				glVertex3f(-0.1012,22.0452,11.0218);

				glVertex3f(0.6882,22.6159,10.8385);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(0.0318,21.9191,12.5313);
				glVertex3f(-0.7655,21.2812,12.7711);
				glVertex3f(-0.8943,21.469,11.1525);

				glVertex3f(-0.1012,22.0452,11.0218);


			}glEnd();

			//col=12
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(7.8952,27.5822,6.1697);
				glVertex3f(7.2481,27.1676,6.8942);
				glVertex3f(7.229,27.0935,6.5513);
				glVertex3f(7.8803,27.5289,5.9686);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(7.2481,27.1676,6.8942);
				glVertex3f(6.5778,26.7269,7.5662);
				glVertex3f(6.5561,26.641,7.082);

				glVertex3f(7.229,27.0935,6.5513);




			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(6.5778,26.7269,7.5662);
				glVertex3f(5.8862,26.2624,8.1845);
				glVertex3f(5.8635,26.1736,7.56);

				glVertex3f(6.5561,26.641,7.082);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(5.8862,26.2624,8.1845);
				glVertex3f(5.1752,25.7764,8.748);
				glVertex3f(5.1531,25.693,7.9851);

				glVertex3f(5.8635,26.1736,7.56);

			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(5.1752,25.7764,8.748);
				glVertex3f(4.4469,25.2711,9.2559);
				glVertex3f(4.4269,25.2013,8.3573);

				glVertex3f(5.1531,25.693,7.9851);



			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{
				glVertex3f(4.4469,25.2711,9.2559);
				glVertex3f(3.7032,24.7486,9.708);
				glVertex3f(3.6867,24.7002,8.6767);

				glVertex3f(4.4269,25.2013,8.3573);



			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(3.7032,24.7486,9.708);
				glVertex3f(2.2486,23.7331,10.3098);
				glVertex3f(2.1725,23.6769,9.1591);

				glVertex3f(3.6867,24.7002,8.6767);


			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{



				glVertex3f(2.2486,23.7331,10.3098);
				glVertex3f(1.4722,23.1791,10.6014);
				glVertex3f(1.4022,23.1581,9.3236);

				glVertex3f(2.1725,23.6769,9.1591);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(1.4722,23.1791,10.6014);
				glVertex3f(0.6882,22.6159,10.8385);
				glVertex3f(0.6258,22.6364,9.4382);

				glVertex3f(1.4022,23.1581,9.3236);

			}glEnd();

			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{

				glVertex3f(0.6882,22.6159,10.8385);
				glVertex3f(-0.1012,22.0452,11.0218);
				glVertex3f(-0.1427,22.1202,9.5215);
				glVertex3f(0.6258,22.6364,9.4382);


			}glEnd();

			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{


				glVertex3f(-0.1012,22.0452,11.0218);
				glVertex3f(-0.8943,21.469,11.1525);
				glVertex3f(-0.938,21.5905,9.5226);

				glVertex3f(-0.1427,22.1202,9.5215);


			}glEnd();

			//col=13

			//------------------- face of a petal end

		}glPopMatrix();
	}
}
void drawPetal_2()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);
			//------------------- face of a petal start
			drawPetal_2_leftface();
			drawPetal_2_rightface();
			drawPetal_2_rightback();
			/*
			glPushMatrix();{
				glRotatef(10,1,0,0);
				glRotatef(180,0,0,1);
				glTranslatef(-5.0646*2,-16.4061*2,0.0);
				drawPetal_2_face();
			}glPopMatrix();
			drawPetal_2_face();
			*/
			//------------------- face of a petal end

			//------------------- back part of a petal
			/*
			glColor3f(1.0, 0.0, 0.0);
			glBegin(GL_QUADS);{// right part of a single petal
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(-0.938,21.5905,9.5226);
				glVertex3f(7.8803,27.5289,5.9686);
			}glEnd();
			*/
			glColor3f(.50, 0.50, 0.0);
			glBegin(GL_QUADS);{// left part of a single petal
				glVertex3f(8.836,28.1654,5.972);
				glVertex3f(5.0646,16.4061,26.9262);
				glVertex3f(13.2529,17.2509,9.5625);
				glVertex3f(9.2724,27.1032,5.9688);
			}glEnd();
			//------------------- back part of a petal

		}glPopMatrix();
	}
}
void drawPetal_3()
{
	for(int i=0; i<9; i++)
	{
		glPushMatrix();{
			glRotatef(i*40,0,0,1);

			glColor3f(1.0, 1.0, 1.0);
			glBegin(GL_QUADS);{
				glVertex3f(20.2079,-2.698,13.69322);
				glVertex3f(4.5658,-0.6096,36.9101);
				glVertex3f(20.892,5.9092,5.972);
				glVertex3f(21.1266,4.1837,5.9716);
			}glEnd();

			glColor3f(0.50, 0.50, 0.50);
			glBegin(GL_QUADS);{
				glVertex3f(20.2079,-2.698,13.69322);
				glVertex3f(4.5658,-0.6096,36.9101);
				glVertex3f(18.8301,-10.8843,5.972);
				glVertex3f(19.4355,-9.5892,5.972);
			}glEnd();
		}glPopMatrix();

	}
}
void drawUpperPetals()
{
	///drawInnerCylinder();
	///drawPetal_top();
	///drawPetal_0();
	///drawPetal_3();
	///drawPetal_2();
	///drawPetal_1();
	drawBase_1();
}
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^



void keyboardListener(unsigned char key, int x,int y){

    switch(key)
        {
            case '1':
            cam.yaw(-delta);
            break; /// lookleft
        case '2':
            cam.yaw(delta);
            break; /// lookright
        case '3':
            cam.pitch(-delta);
            break; /// lookup
        case '4':
            cam.pitch(delta);
            break; /// lookdown
        case '5':
            cam.roll(delta);
            break; /// twistleft
        case '6':
            cam.roll(-delta);
            break; /// twistright
        case '9':
			dr = 1,dg=1,db=1;//set diffuse light to white
			break;
		case '0':
			dr = 0,dg=0,db=1;//set diffuse light to blue
			break;
        case 'a':
		    cam.slide(0,delta,0);
		    break; /// up
		case 's':
		    cam.slide(0,-delta,0);
		    break; /// down
		case '7':
			diff_ang+=diff_delta;
			dfx = diff_rad*cos(PI*diff_ang/180.);
			dfy = diff_rad*sin(PI*diff_ang/180.);
			break;
		case '8':
			diff_ang-=diff_delta;
			dfx = diff_rad*cos(PI*diff_ang/180.);
			dfy = diff_rad*sin(PI*diff_ang/180.);
			break;
			}
}

void specialKeyListener(int key, int x,int y){
	switch(key)
	{
		case GLUT_KEY_UP:
		    cam.slide(0,0,-delta);
		    break; /// forward
		case GLUT_KEY_DOWN:
		    cam.slide(0,0,delta);
		    break; /// backward
		case GLUT_KEY_LEFT:
		    cam.slide(-delta,0,0);
		    break; /// left
		case GLUT_KEY_RIGHT:
		    cam.slide(delta,0,0);
		    break; /// right
		case GLUT_KEY_PAGE_UP:
		    cam.slide(0,delta,0);
		    break; /// up
		case GLUT_KEY_PAGE_DOWN:
		    cam.slide(0,-delta,0);
		    break; /// down
	}
	glutPostRedisplay();
}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				drawaxes=1-drawaxes;
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}

void initCamera()
{
    glViewport(0,0,1200,700);
    Vector3 up(0,0,1);
    Point3 eye(100.0f,10.0f,150.0f);
    Point3 look(0.0f,0.0f,0.0f);
    cam.set(eye,look,up);
    cam.setShape(30.0f,64.0f/48.0f,0.5f,500.0f);
}

void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
	/ set-up camera here
	********************/
	//load the correct matrix -- MODEL-VIEW matrix
	///glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	///glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?

	//gluLookAt(100,100,100,	0,0,0,	0,0,1);
	///gluLookAt(100*cos(cameraAngle), 100*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	//gluLookAt(0,-1,150,	0,0,0,	0,0,1);


	//again select MODEL-VIEW
	//glMatrixMode(GL_MODELVIEW);


	/****************************
	/ Add your objects from here
	****************************/
	//add objects

	drawAxes();
	drawGrid();
	glColor3f(1,0,0);





    //set_ambient_color(1.0,1.0,1.0);
	//set_diffuse_source(dfx,dfy,dfz,dr,dg,db);
	set_spot_light(100,100,50,0,-1,-1);



    drawUpperPetals();
    //ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)

    glFlush();
	glutSwapBuffers();
}

void animate(){
	angle+=0.05;
	//codes for any changes in Models, Camera
	glutPostRedisplay();
}

void init(){

	//codes for initialization
	drawgrid=0;
	drawaxes=1;
	cameraHeight=100.0;
	cameraAngle=1.0;
	angle=0;

	//clear the screen
	glClearColor(0,0,0,0);

	/************************
	/ set-up projection here
	************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(80,	1,	1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

int main(int argc, char **argv){

calculate_points();
	glutInit(&argc,argv);
	glutInitWindowSize(500, 500);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

	glutCreateWindow("My OpenGL Program");


	init();
	initRendering();//init lights
    initCamera();//init camera

	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

	glutMainLoop();		//The main loop of OpenGL

	return 0;
}
