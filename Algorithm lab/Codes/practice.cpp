#include<iostream>
#include<cstdio>
using namespace std;


long long int hex_nacci[10001];
int a, b, c, d, e, f;
 long long int fn( int n ) {
    int i;
    if( n == 0 ) return a;
    if( n == 1 ) return b;
    if( n == 2 ) return c;
    if( n == 3 ) return d;
    if( n == 4 ) return e;
    if( n == 5 ) return f;

    for(i=6;i<=n;i++)
		{
			hex_nacci[i] = ((hex_nacci[i-1]%10000007)+(hex_nacci[i-2]%10000007)+(hex_nacci[i-3]%10000007)+(hex_nacci[i-4]%10000007)+(hex_nacci[i-5]%10000007)+(hex_nacci[i-6]%10000007))%10000007;
		}
		//cout<<hex_nacci[n];
    return hex_nacci[n];
}
int main() {
    int n, caseno = 0, cases;
    scanf("%d", &cases);
    while( cases-- ) {
        scanf("%d %d %d %d %d %d %d", &a, &b, &c, &d, &e, &f, &n);
        hex_nacci[0]=a;
        hex_nacci[1]=b;
        hex_nacci[2]=c;
        hex_nacci[3]=d;
        hex_nacci[4]=e;
        hex_nacci[5]=f;
        //printf("Case %d: %llu\n", ++caseno, (fn(n) % 10000007));
        cout<<"Case "<<++caseno<<": "<<(fn(n)%10000007)<<endl;
    }
    return 0;
}
