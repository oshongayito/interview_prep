    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)

int main()
{
    string s,res;
    int k,i,j,l;
    char c;
    map<char,int> mymap;

    cin>>k>>s;
    l=s.size();
    for(i=0;i<l;i++)
    {
        mymap[s[i]]++;
    }
    bool flag=0;
    for(i=0,c='a';i<26;i++,c++)
    {
        if(mymap[c]%k==0)
            mymap[c]/=k;
        else {flag=1;break;}
    }
    if(!flag)
    {
        res="";
        for(i=0,c='a';i<26;i++,c++)
        {
            for(j=0;j<mymap[c];j++)
                res.pb(c);

        }
        for(i=0;i<k;i++)cout<<res;
        cout<<endl;
    }
    else cout<<-1<<endl;

    return 0;

}
