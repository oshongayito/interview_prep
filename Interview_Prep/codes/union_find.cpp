#include<iostream>
#include<vector>
using namespace std;

/*

Problems: 1. find the number of islands, 2. leetcode: 684

When to use Union-find over DFS

** if the graph is dynamically changing, union find is better suited than dfs. 
** union find for online setting
otherwise, with static graph, dfs is more straightforward and asymptotically faster

Time Complexity:

Using both path compression, splitting, or halving and union by rank or size ensures
 that the amortized time per operation is only O(alpha(n))
  which is optimal,[6] where O(alpha(n)), alpha(n) is the inverse Ackermann function. 
  This function has a value alpha (n)<5 for any value 
  of n that can be written in this physical universe, so the DISJOINT SET OPERATIONS take place
   in essentially CONSTANT Time

*/

class UnionFind{

public:
	vector<int>parents;
	vector<int>ranks;
	int setCount;

	UnionFind(int n){
		parents.resize(n, -1);
		ranks.resize(n, 0);
		setCount = 0;
	}

	void makeSet(int i){
		parents[i] = i;
		setCount++;
	}

	//avg complexity O(1)
	int find(int i){
		if(parents[i]!= i)
		{
			parents[i] = find(parents[i]); //path compression
		}
		return parents[i];
	}

	//avg complexity O(1)
	void makeUnion(int a, int b){
		int p_a = find(a);
		int p_b = find(b);

		if(p_a == p_b)return;

		// union by rank
		if(ranks[p_a] == ranks[p_b]){
			parents[p_b] = p_a;
			ranks[p_a]++;
		}
		else if(ranks[p_a] > ranks[p_b]){
			parents[p_b] = p_a;
		}
		else{
			parents[p_a] = p_b;
		}
		setCount--;
	}
};


/**/

int main(){
	UnionFind uf(10);

	cout<<"initial set count: "<<uf.setCount<<endl;
	cout<<"make set with 1, 2, 3 separately"<<endl;
	uf.makeSet(1);
	uf.makeSet(2);
	uf.makeSet(3);
	cout<<"set count now: "<<uf.setCount<<endl;
	cout<<"union 1 and 2"<<endl;
	uf.makeUnion(1,2);

	cout<<"set count now: "<<uf.setCount<<endl;

	return 0;
}



