### My Project Experiences ###
1. Digital prescription project

	Description:
	A personal book keeping system for one's own prescriptions. 

	Challenges:
	How can we log in the user. We used JWT token as it is easy to implement and serves our purpose initially. 
	But, we faced problem while logging out the users, as the token remains valid until it's expiry time.
	We thought about keeping all the tokens in a database table after users log in. However, it will store all the tokens in this process. Besides, the backend needs to do a database lookup to check whether a token exists or not for every requests.

	But, if instead we store the logged out tokens only, only a subset of all the tokens need to be stored. So, we inserted the tokens in a table to store which tokens has already been deactivated or logged out.

	Better: I could do even better. I also thought about creating a database job, which will run in a prespecified time to clean up the tokens which have timed out. Or I could use a java spring tokenstore, which provides basic token operations (access, revoke etc.).




2. iPay admin panel
	
	Description: We had a page for managing user-role permissions of different activities of iPay admins. 

	Challenges: However, there were lags in loading the page as the number of role activity pairs were not small. 

	Solution: We were thinking about how to reduce the response time. And then we came up with an idea of grouping the activities based on their types (login, payment, profile) etc. Then we loaded the group names only in the beginning. And then clicking on the group names will bring the whole permission list for that specific group.
	It drastically reduced the loading time.


