### Turing Machine: ###
A machine capable of reading from a infinite stripe of 0s and 1s and making changes to the stripe at current state.
And when the machine reach to a halting state, the state of the stripe is the solution.

### Halting Problem: ###
No one can say whether an arbitrary computer program (with a given input) will ultimately halt or continue running forever.

### Turing Complete: ###
For a computer program, if it can do what a Turing machine can do, we call the program Turing Complete.

### Decision Problem: ###
Problems where the answer is either YES or NO.

Example: Input: A natural number 𝑥,
		 Question: Is 𝑥 an even number?



### P (Polynomial): ###

𝖯 is the class of decision problems that can be solved efficiently,
i.e. decision problems which have polynomial-time algorithms.

More formally, we say a decision problem 𝑄 is in 𝖯 iff

there is an efficient algorithm 𝐴 such that
for all inputs 𝑥,

if 𝑄(𝑥)=𝑌𝐸𝑆 then 𝐴(𝑥)=𝑌𝐸𝑆,
if 𝑄(𝑥)=𝑁𝑂 then 𝐴(𝑥)=𝑁𝑂

Examples: Sorting, searching etc.

### NP (Non deterministically Polynomial): ###
𝖭𝖯  is the class of decision problems which have efficient verifiers, i.e.
there is a polynomial time algorithm that can verify if a given solution is correct.
However, the solvers are non-polynomial or exponential.

More formally, we say a decision problem 𝑄 is in 𝖭𝖯 iff

there is an efficient algorithm 𝑉 called verifier such that
for all inputs 𝑥,

if 𝑄(𝑥)=𝑌𝐸𝑆 then there is a proof 𝑦 such that 𝑉(𝑥,𝑦)=𝑌𝐸𝑆,
if 𝑄(𝑥)=𝑁𝑂 then for all proofs 𝑦, 𝑉(𝑥,𝑦)=𝑁𝑂.


Therefore, all P's are also NP, but not the other way. Because, for all P problems we can also verify them in polynomial time.

Example: Sudkoku solvers, TSP (the decision version: whether thare is a cycle in the graph having cost shorter than a given number K)


### Reduction: ###
If problem A can be reduced to problem B in polynomial time, then we can say that A is reducible to B. Here, if B is solvable in polynomial time, then we can say that A is also solvable in polynomial time.


### NP Hard: ###
NP-Hard problems are at least as hard as NP problems. That is, every problem in NP can be reduced to NP-Hard problems. This problems may or may not be decision problems. And also the solutions of this problems may or may not be verifiable in polynomial time.

Example: Halting Problem (not NP-Complete), subset sum problem (NP-complete as well), TSP (the optimization version: find the shortest Hamiltonian cycle in a weighted graph)




### NP Complete: ###
NP-Complete problems are both NP and NP-Hard. That means all problems in NP can be reduced to it, and the solutions of the problems are verifiable in polynomial time. If we can solve NP-complete problems in polynomial time (we hope one day we will), we will be able to solve all the NP problems in polynomial time. These are the hardest of NP problems.

Example: Subset Sum, 3 SAT, TSP (the decision version: whether thare is a cycle in the graph having cost shorter than a given number K)



____________________________________________________________
| Problem Type | Verifiable in P time | Solvable in P time | Increasing Difficulty
___________________________________________________________|           |
| P            |        Yes           |        Yes         |           |
| NP           |        Yes           |     Yes or No *    |           |
| NP-Complete  |        Yes           |      Unknown       |           |
| NP-Hard      |     Yes or No **     |      Unknown ***   |           |
____________________________________________________________           V


Notes on Yes or No entries:

* An NP problem that is also P is solvable in P time.
** An NP-Hard problem that is also NP-Complete is verifiable in P time.
*** NP-Complete problems (all of which form a subset of NP-hard) might be. The rest of NP hard is not.




NP
|___ P
|___ NP-Complete


NP-Hard
|___ NP-Complete


So, NP-Complete are both in NP-Hard and NP.



#### References ####
1. [https://cs.stackexchange.com/questions/9556/what-is-the-definition-of-p-np-np-complete-and-np-hard] (Stack Exchange)
2. https://www.youtube.com/watch?v=DumOqL85Ryc
3. https://www.youtube.com/watch?v=dNRDvLACg5Q