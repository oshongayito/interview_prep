    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)

    #define MAX 1000000007
    //int memo[10000][10000];

    map< pair<int,int>,int>maps;
    int n;
    ll rs;
    //int arr[17][17];

int dp(int m,int n)
{

    if((m==1)&&(n==1))return 2;
    if(m==1)return n+1;
    if(n==1)return m+1;

    if(maps[mp(m,n)])return maps[mp(m,n)];

    rs= (1+ max(dp(m-1,n),max(dp(m-1,n-1),dp(m,n-1))))%MAX;
    maps[mp(m,n)]=rs;
    return rs;


}



int main()
{
    int t,i,j,k,n,m;
    //int maxb[17],sorted[17][17];
    bool vis[17];
    int mask;
    ll res=0;
    //cout<<(1<<0);

    cin>>t;
    for(k=1;k<=t;k++){
//    memset(maxb,0,17);
  //  memset(vis,0,17);

    cin>>n>>m;
    res = dp(n,m);

    cout<<"Case "<<k<<": "<<res%MAX<<endl;
    //clear(memo);

    //clear(arr);
    //clear(maps);
    maps.clear();
    }


    return 0;
}
