#include<iostream>
#include<cstdio>
#include<cmath>
#include<vector>

using namespace std;

int main()
{
    long long int tests,a,b,c,i,j,k,l,sum,arr[20],c_sum[20];
    vector <long long int>stack;
    //cin>>a;

    for(i=1,b=1,c_sum[0]=1;b<=1000000000000000000;i++)
    {
        arr[i-1]=b;
        if(i>1)c_sum[i-1]=c_sum[i-2]+arr[i-1];
        b*=i;
    }

    cin>>tests;
    for(i=1;i<=tests;i++)
    {

    cin>>a;
    for(j=0;j<20;j++)
    {
        if(a<arr[j])break;
    }
    //if(a==arr[j])cout<<"Case "<<i<<": "<<j<<"!"<<endl;
    if(a>c_sum[j-1])cout<<"Case "<<i<<": impossible "<<endl;
    else if(a==c_sum[j-1])
    {
        printf("Case %lld: ",i);
        for(k=0;k<j;k++){if(k>0)cout<<"+";cout<<k<<"!";}
        cout<<endl;
    }
    else
    {
        //for(k=j-1;k>=0;k--)
        stack.push_back(j-1);
            for(l=j-2,sum=arr[j-1];l>=0;l--)
            {
                if(sum==a)break;
                if(sum+arr[l]<=a){sum=sum+arr[l];stack.push_back(l);}
            }
            if(sum!=a)cout<<"Case "<<i<<": impossible"<<endl;
            else
            {
                printf("Case %lld: ",i);
                for(k=(int)stack.size();k>0;k--)
                {
                    if(k<(int)stack.size())cout<<"+";
                    cout<<stack[k-1]<<"!";
                }
                cout<<endl;
            }
        stack.clear();
    }
    }
    //cout<<i<<" "<<b<<" "<<arr[19]<<endl;
    return 0;
}
