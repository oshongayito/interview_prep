#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<cmath>

//#define sum(n,a,d) (n)*(((2*a)+((n-1)*d))/2)
#define ll long long
using namespace std;

ll sum(ll n, ll a, ll d)
{
    return  (n)*(((2*a)+((n-1)*d))/2);
}

int main()
{
    ll n,m,i,j,s1,s2,a1,a2,d1,d2,t;
    cin>>t;
    for(i=1;i<=t;i++){
    cin>>n>>m;

    a1 = sum(m,1,1);
    a2 = sum(m,1+m,1);
    d1 = sum(m,1+m+m,1)-a1;
    d2 = sum(m,1+m+m+m,1)-a2;

    s1 = sum(n/(2*m),a1,d1);
    s2 = sum(n/(2*m),a2,d2);

    //cout<<a1<<" "<<d1<<" "<<a2<<" "<<d2<<endl;

    cout<<"Case "<<i<<": "<<s2-s1<<endl;
    }
    return 0;

}
