#include<iostream>
#include<vector>
#include<string>


using namespace std;

string reverse_string(string str)
{
    string r;
    for(int i=str.size()-1;i>=0;i--)
    {

        r+=str[i];
    }
    return r;
}


bool is_palindrome(string str)
{
    string r;
    r=reverse_string(str);

    if(str==r)return true;
    else return false;
}




int ways(string str)
{
    int x=0;
    vector<int>count;
    count.resize(str.size());

    for(int i=1;i<str.size();i++)
    {
        vector<string>x;
        count[i]=count[i-1];
        x=all_subsets(str.substr(0,i));
        for(int j=0;j<x.size();j++)
        {
            if(is_palindrome(x[j]+str[i]))count[i]++;
        }

    }
    return count[str.size()-1]+1;
}

int main()
{

    vector<string>res;

    string str;
    cin>>str;

    cout<<ways(str)<<endl;

    return 0;
}
