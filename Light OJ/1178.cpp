    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)

using namespace std;

int  main()
{
	int t,i;
	double e,a,b,c,d,B,h,area,res;
	cout.precision(10);
	cin>>t;
	for(i=1;i<=t;i++)
	{
		cin>>a>>b>>c>>d;
		if(a>c)e=a-c;
		else e = c-a;
		B = acos((d*d + e*e - b*b)/(2*d*e))*(180.0/pi);
		h = d* sin((B*pi)/180);
		area = (0.5)*(a+c)*h;
		res = area + 0.0000000001;

		cout<<"Case "<<i<<": ";


		cout<<fixed<<res<<endl;
		//printf("Case %d: %.10lf\n",i,res);
	}
	return 0;
}
