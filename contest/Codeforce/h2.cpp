#include<iostream>
#include<complex>
using namespace std;

typedef complex<double> point;

#define vec(a,b) (b)-(a)
#define dot(a,b) (conj(a)*(b)).real()
#define cross(a,b) (conj(a)*(b)).imag()
#define lensqr(a) dot(a,a)
#define EPS 1e-9

double N, xstr, ystr, xend, yend, xtopLeft, ytopLeft, xbottomRight, ybottomRight;

bool Lines_intersect(const point &a, const point &b, const point &p, const point &q, point &ret) {
   double d1 = cross(vec(a,p),vec(a,b)),
         d2 = cross(vec(a,q),vec(a,b));
   ret = (d1 * q - d2 * p) / (d1 - d2);
   return fabs(d1 - d2) > EPS;
}

bool pointOnRay(const point &a, const point &b, const point &p) {
   return cross(vec(a,b),vec(a,p)) < EPS
         && cross(vec(a,b),vec(a,p)) > -EPS
         && dot(vec(a,b),vec(a,p)) > -EPS;
}

bool pointOnSegment(const point &a, const point &b, const point &p) {
   if (lensqr(vec(a,b)) < EPS)
      return lensqr(vec(a,p)) < EPS;
   return pointOnRay(a, b, p) && pointOnRay(b, a, p);
}

int main() {
   cin >> N;
   point ret;
   while (N--) {
      cin >> xstr >> ystr >> xend >> yend >> xtopLeft >> ytopLeft >> xbottomRight >> ybottomRight;

      if (Lines_intersect(point(xstr, ystr), point(xend, yend), point(xtopLeft, ytopLeft), point(xtopLeft, ybottomRight), ret)
            && pointOnSegment(point(xstr, ystr), point(xend, yend), ret)
            && pointOnSegment(point(xtopLeft, ytopLeft), point(xtopLeft, ybottomRight), ret)) {
         cout << "T" << endl;
         continue;
      }

      if (Lines_intersect(point(xstr, ystr), point(xend, yend), point(xtopLeft, ytopLeft), point(xbottomRight, ytopLeft), ret)
            && pointOnSegment(point(xstr, ystr), point(xend, yend), ret)
            && pointOnSegment(point(xtopLeft, ytopLeft), point(xbottomRight, ytopLeft), ret)) {
         cout << "T" << endl;
         continue;
      }

      if (Lines_intersect(point(xstr, ystr), point(xend, yend), point(xbottomRight, ytopLeft), point(xbottomRight, ybottomRight), ret)
            && pointOnSegment(point(xstr, ystr), point(xend, yend), ret)
            && pointOnSegment(point(xbottomRight, ytopLeft), point(xbottomRight, ybottomRight), ret)) {
         cout << "T" << endl;
         continue;
      }

      if (Lines_intersect(point(xstr, ystr), point(xend, yend), point(xtopLeft, ybottomRight), point(xbottomRight, ybottomRight), ret)
            && pointOnSegment(point(xstr, ystr), point(xend, yend), ret)
            && pointOnSegment(point(xtopLeft, ybottomRight), point(xbottomRight, ybottomRight), ret)) {
         cout << "T" << endl;
         continue;
      }

      if (xstr <= (xtopLeft > xbottomRight ? xtopLeft : xbottomRight)
            && xstr >= (xtopLeft < xbottomRight ? xtopLeft : xbottomRight)
            && ystr <= (ytopLeft > ybottomRight ? ytopLeft : ybottomRight)
            && ystr >= (ytopLeft < ybottomRight ? ytopLeft : ybottomRight)) {
         cout << "T" << endl;
         continue;
      }
      cout << "F" << endl;
   }
   return 0;
}
