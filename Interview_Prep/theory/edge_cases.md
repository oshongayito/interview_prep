### For integer arrays: ###
1. can it be empty?
2. does it have duplicates?
3. what is the maximum size?
4. can it have negative values?
5. for multi-dimensional arrays, are the dimensions same for all?
6. is it sorted?
7. does the order of the element matters?



### For strings: ###
1. 1 to 3 are same
2. what type of characters will be there? (alphabetes, alpha numeric or what)
3. Case sensitive or not



### Linked Lists: ###
1. Single node
2. Two nodes
3. Linked list has cycle. Clarify with the interviewer whether there can be a cycle in the list. Usually the answer is no.


### Maths: ###
1. Division by 0
2. Integer overflow and underflow



### Matrix: ###
1. Empty matrix. Check that none of the arrays are 0 length.
2. 1 x 1 matrix.
3. Matrix with only one row or column.

## Common Tricks: ##

1. Sliding window can be helpful for substring or subarray problems. (be careful of negative numbers though)

2. For questions where summation or multiplication of a subarray is involved, pre-computation using hashing or a prefix, suffix sum, or product might be useful

3. If you are given a sequence and the interviewer asks for O(1) space, it might be possible to use the array itself as a hash table. For example, if the array has values only from 1 to N, where N is the length of the array, negate the value at that index (minus one) to indicate the presence of that number.

4. To optimize space, sometimes you do not have to store the entire DP table in memory. The last two values or the last two rows of the matrix will suffice.

5. Some inputs look like they are trees, but they are actually graphs. Clarify this with your interviewer. In that case, you will have to handle cycles and keep a set of visited nodes when traversing.

6. Clarify with the interviewer whether [1, 2] and [2, 3] are considered overlapping intervals, because it affects how you will write your equality checks.

7. A common routine for interval questions is to sort the array of intervals by the start value of each interval.

8. When a question involves “a multiple of a number”, modulo might be useful.

9. Check for and handle overflow and underflow if you are using a typed language like Java and C++. At the very least, mention that overflow or underflow is possible and ask whether you need to handle it.

10. Consider negative numbers and floating point numbers. This may sound obvious, but when you are under pressure in an interview, many obvious points go unnoticed.

11.  Questions involving matrices are usually related to dynamic programming or graph traversal.

12. If you see a top or lowest k mentioned in the question, it is usually a sign that a heap can be used to solve the problem

13. For subarray sum related problem, cumulative sum also helps sometimes

### Reference: ###
Great One: https://medium.freecodecamp.org/coding-interviews-for-dummies-5e048933b82b