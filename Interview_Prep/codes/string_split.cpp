#include<iostream>
#include<vector>
#include<sstream>
#include<unordered_map>
using namespace std;

vector<string> splitString(string str, string delim){
    int prev = 0;
    vector<string>res;
    while(str.find(delim, prev) != std::string::npos){

        int cur = str.find(delim, prev);
        //cout<<cur<<endl;
        string curStr = str.substr(prev,cur-prev);
        res.push_back(curStr);
        prev = cur+1; //delim.size();
    }
    if(prev < str.size()-1){
        res.push_back(str.substr(prev, str.size()-prev));
    }
    return res;
}


//better one (uses char delimiter!!!)
vector<string> split2(string str, char delim)
{
    vector<string> res;
    stringstream ss(str);
    string token;
    while (std::getline(ss, token, delim)) {
        res.push_back(token);
    }
    return res;
}

// strtok
vector<string> split3(string str, char delim)
{
    vector<string>res;
    char *tok = strtok(str.c_str(), delim);

    while(tok != NULL)
    {
        string token(tok);
        res.push_back(token);
        tok = strtok(str, delim);
    }

    return res;
}



int main(){

    vector<string> strs = split2("cool",'_');
    for(int i=0;i<strs.size();i++){
        cout<<strs[i]<<endl;
    }
    // cout<<tolower('1')<<endl;
    unordered_map<int, int>myMap;
    cout<<myMap[1]<<endl;

    return 0;
}
