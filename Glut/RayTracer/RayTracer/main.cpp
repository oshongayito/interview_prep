#include <iostream>
#include <vector>
#include <math.h>
#include <stdlib.h>
#include<fstream>
#include <sstream>
#ifdef _WIN32
#include<windows.h>
#include<GL/glut.h>
#elif _WIN64
#include<windows.h>
#include<GL/glut.h>
#elif __APPLE__
#include<GLUT/glut.h>
#else
#include<GL/glut.h>
#endif
#define MAX_HEIGHT 70

#include "cube.cpp"




#include "camera.h"
#include "light.h"
#include "Ray.h"
#include "customtypes.h"
#include "Plane.h"
#include "sphere.h"

#include "bitmap_image.hpp"

using namespace std;
double const pi=acos(-1);

double cameraHeight;
double cameraAngle;
int drawgrid=1;
int drawaxes;
double angle;
double radius;
double pos,acce,dir;

int gridUnitSize=10;
Camera cam;

int recursionDepth,resolution,totalObjects,lights;


float dfx=100,dfy=100,dfz=50,dr=1.0,dg=1.0,db=1.0,diff_rad=50,diff_delta=5,diff_ang=0,delta=3.0;
float sx=100,sy=100,sz=50,ax=1.0,ay=1.0,az=1.0;


std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}


std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}



struct colorRGB
{
  double r;
  double g;
  double b;

};

vector<Object>scene_objects;

struct point
{
	double x,y,z;
		Color col;

	point(){}

	point(double a,double b,double c):x(a),y(b),z(c){}


	Vector3 getLightPosition()
	{
	    Vector3 temp;
	    temp.x=x;
	    temp.y=y;
	    temp.z=z;
	    return temp;
	}
	void setLightColor(double r,double g,double b)
	{
	    col.r=r;
	    col.g=g;
	    col.b=b;
	}
	Color getLightColor()
	{
	    return col;
	}
};

Color floorColor (1, 1, 1, 2);

Vector3 Y (0,1,0);
Plane checkerBoard(Y, -1, floorColor);
vector<point>light_sources;
vector<sphere>spheres;
vector<cube>cubes;

Color getColorAtSphere(Vector3 intersection_position, Vector3 intersecting_ray_direction, vector<sphere>spheress, int index_of_winning_object, vector<point>l_sources, double accuracy, double ambientlight);

int winningObjectIndex(vector<double> object_intersections) {
	// return the index of the winning intersection
	int index_of_minimum_value;

	// prevent unnessary calculations
	if (object_intersections.size() == 0) {
		// if there are no intersections
		return -1;
	}
	else if (object_intersections.size() == 1) {
		if (object_intersections.at(0) > 0) {
			// if that intersection is greater than zero then its our index of minimum value
			return 0;
		}
		else {
			// otherwise the only intersection value is negative
			return -1;
		}
	}
	else {
		// otherwise there is more than one intersection
		// first find the maximum value

		double max = 0;
		for (int i = 0; i < object_intersections.size(); i++) {
			if (max < object_intersections.at(i)) {
				max = object_intersections.at(i);
			}
		}

		// then starting from the maximum value find the minimum positive value
		if (max > 0) {
			// we only want positive intersections
			for (int index = 0; index < object_intersections.size(); index++) {
				if (object_intersections.at(index) > 0 && object_intersections.at(index) <= max) {
					max = object_intersections.at(index);
					index_of_minimum_value = index;
				}
			}

			return index_of_minimum_value;
		}
		else {
			// all the intersections were negative
			return -1;
		}
	}
}



Color getColorAt(Vector3 intersection_position, Vector3 intersecting_ray_direction, Plane board, int index_of_winning_object, vector<point>l_sources, double accuracy, double ambientlight) {

//	Color winning_object_color = scene_objects.at(index_of_winning_object)->getColor();
	Color winning_object_color = board.getColor();

	Vector3 winning_object_normal = board.getNormalAt(intersection_position);

	if (winning_object_color.getColorSpecial() == 2)
	{
		// checkered/tile floor pattern

		int square = (int)floor(intersection_position.x) + (int)floor(intersection_position.z);

		if ((square % 2) == 0){
			// black tile
			winning_object_color.setColorRed(0);
			winning_object_color.setColorGreen(0);
			winning_object_color.setColorBlue(0);
		}
		else
		{
			// white tile
			winning_object_color.setColorRed(1);
			winning_object_color.setColorGreen(1);
			winning_object_color.setColorRed(1);
		}
	}

	Color final_color = winning_object_color.colorScalar(ambientlight);

	if (winning_object_color.getColorSpecial() > 0 && winning_object_color.getColorSpecial() <= 1) {
		// reflection from objects with specular intensity
		double dot1 = winning_object_normal.dot(intersecting_ray_direction.neg());
		Vector3 scalar1 = winning_object_normal.Mult(dot1);
		Vector3 add1 = scalar1.vectAdd(intersecting_ray_direction);
		Vector3 scalar2 = add1.Mult(2);
		Vector3 add2 = intersecting_ray_direction.neg().vectAdd(scalar2);
		Vector3 reflection_direction = add2;
		reflection_direction.normalize();

		Ray reflection_ray (intersection_position, reflection_direction);

		// determine what the ray intersects with first
		vector<double> reflection_intersections;

//		for (int reflection_index = 0; reflection_index < scene_objects.size(); reflection_index++) {
			reflection_intersections.push_back(board.findIntersection(reflection_ray));
//		}

		int index_of_winning_object_with_reflection = winningObjectIndex(reflection_intersections);

		if (index_of_winning_object_with_reflection != -1) {
			// reflection ray missed everthing else
			if (reflection_intersections.at(index_of_winning_object_with_reflection) > accuracy) {
				// determine the position and direction at the point of intersection with the reflection ray
				// the ray only affects the color if it reflected off something

				Vector3 reflection_intersection_position = intersection_position.vectAdd(reflection_direction.Mult(reflection_intersections.at(index_of_winning_object_with_reflection)));
				Vector3 reflection_intersection_ray_direction = reflection_direction;

                if(index_of_winning_object_with_reflection==0){
				Color reflection_intersection_color = getColorAt(reflection_intersection_position, reflection_intersection_ray_direction, board, index_of_winning_object_with_reflection, light_sources, accuracy, ambientlight);
                				final_color = final_color.colorAdd(reflection_intersection_color.colorScalar(winning_object_color.getColorSpecial()));

                }else
                {
                    Color reflection_intersection_color = getColorAtSphere(reflection_intersection_position, reflection_intersection_ray_direction, spheres, index_of_winning_object_with_reflection, light_sources, accuracy, ambientlight);

				final_color = final_color.colorAdd(reflection_intersection_color.colorScalar(winning_object_color.getColorSpecial()));
                }
//				final_color = final_color.colorAdd(reflection_intersection_color.colorScalar(winning_object_color.getColorSpecial()));
			}
		}
	}

	for (int light_index = 0; light_index < light_sources.size(); light_index++) {
		Vector3 light_direction = light_sources.at(light_index).getLightPosition().vectAdd(intersection_position.neg());
		light_direction.normalize();

		float cosine_angle = winning_object_normal.dot(light_direction);

		if (cosine_angle > 0) {
			// test for shadows
			bool shadowed = false;

			Vector3 distance_to_light = light_sources.at(light_index).getLightPosition().vectAdd(intersection_position.neg());
			distance_to_light.normalize();
			float distance_to_light_magnitude = distance_to_light.magnitude();
            Vector3 temps = light_sources.at(light_index).getLightPosition().vectAdd(intersection_position.neg());
            temps.normalize();
			Ray shadow_ray (intersection_position,temps );

			vector<double> secondary_intersections;

			for (int object_index = 0; object_index < scene_objects.size() && shadowed == false; object_index++) {
				secondary_intersections.push_back(scene_objects.at(object_index).findIntersection(shadow_ray));
			}

			for (int c = 0; c < secondary_intersections.size(); c++) {
				if (secondary_intersections.at(c) > accuracy) {
					if (secondary_intersections.at(c) <= distance_to_light_magnitude) {
						shadowed = true;
					}
				}
				break;
			}

			if (shadowed == false) {
				final_color = final_color.colorAdd(winning_object_color.colorMultiply(light_sources.at(light_index).getLightColor()).colorScalar(cosine_angle));

				if (winning_object_color.getColorSpecial() > 0 && winning_object_color.getColorSpecial() <= 1) {
					// special [0-1]
					double dot1 = winning_object_normal.dot(intersecting_ray_direction.neg());
					Vector3 scalar1 = winning_object_normal.Mult(dot1);
					Vector3 add1 = scalar1.vectAdd(intersecting_ray_direction);
					Vector3 scalar2 = add1.Mult(2);
					Vector3 add2 = intersecting_ray_direction.neg().vectAdd(scalar2);
					Vector3 reflection_direction = add2;
					reflection_direction.normalize();

					double specular = reflection_direction.dot(light_direction);
					if (specular > 0) {
						specular = pow(specular, 10);
						final_color = final_color.colorAdd(light_sources.at(light_index).getLightColor().colorScalar(specular*winning_object_color.getColorSpecial()));
					}
				}

			}

		}
	}

	return final_color.clip();
}


Color getColorAtSphere(Vector3 intersection_position, Vector3 intersecting_ray_direction, vector<sphere>spheress, int index_of_winning_object, vector<point>l_sources, double accuracy, double ambientlight) {

	Color winning_object_color = spheress.at(index_of_winning_object-1).getColor();
	return winning_object_color;
//	winning_object_color.print();
//	Color winning_object_color = board.getColor();

	Vector3 winning_object_normal = spheress.at(index_of_winning_object-1).getNormalAt(intersection_position);


	Color final_color = winning_object_color.colorScalar(ambientlight);
//	Color final_color = winning_object_color;

	if (winning_object_color.getColorSpecial() > 0 && winning_object_color.getColorSpecial() <= 1) {
		// reflection from objects with specular intensity
		double dot1 = winning_object_normal.dot(intersecting_ray_direction.neg());
		Vector3 scalar1 = winning_object_normal.Mult(dot1);
		Vector3 add1 = scalar1.vectAdd(intersecting_ray_direction);
		Vector3 scalar2 = add1.Mult(2);
		Vector3 add2 = intersecting_ray_direction.neg().vectAdd(scalar2);
		Vector3 reflection_direction = add2;
		reflection_direction.normalize();

		Ray reflection_ray (intersection_position, reflection_direction);

		// determine what the ray intersects with first
		vector<double> reflection_intersections;

//		for (int reflection_index = 0; reflection_index < scene_objects.size(); reflection_index++) {
			reflection_intersections.push_back(spheress[index_of_winning_object-1].findIntersection(reflection_ray));
//		}

		int index_of_winning_object_with_reflection = winningObjectIndex(reflection_intersections);

		if (index_of_winning_object_with_reflection != -1) {
			// reflection ray missed everthing else
			if (reflection_intersections.at(index_of_winning_object_with_reflection) > accuracy) {
				// determine the position and direction at the point of intersection with the reflection ray
				// the ray only affects the color if it reflected off something

				Vector3 reflection_intersection_position = intersection_position.vectAdd(reflection_direction.Mult(reflection_intersections.at(index_of_winning_object_with_reflection)));
				Vector3 reflection_intersection_ray_direction = reflection_direction;

                if(index_of_winning_object_with_reflection==0)
				{
				    Color reflection_intersection_color = getColorAt(reflection_intersection_position, reflection_intersection_ray_direction, checkerBoard, index_of_winning_object_with_reflection, light_sources, accuracy, ambientlight);
				    				final_color = final_color.colorAdd(reflection_intersection_color.colorScalar(winning_object_color.getColorSpecial()));

				}
				else
				{
				        Color reflection_intersection_color = getColorAtSphere(reflection_intersection_position, reflection_intersection_ray_direction, spheres, index_of_winning_object_with_reflection, light_sources, accuracy, ambientlight);
								final_color = final_color.colorAdd(reflection_intersection_color.colorScalar(winning_object_color.getColorSpecial()));

				}

//				final_color = final_color.colorAdd(reflection_intersection_color.colorScalar(winning_object_color.getColorSpecial()));
			}
		}
	}

	for (int light_index = 0; light_index < light_sources.size(); light_index++) {
		Vector3 light_direction = light_sources.at(light_index).getLightPosition().vectAdd(intersection_position.neg());
		light_direction.normalize();

		float cosine_angle = winning_object_normal.dot(light_direction);

		if (cosine_angle > 0) {
			// test for shadows
			bool shadowed = false;

			Vector3 distance_to_light = light_sources.at(light_index).getLightPosition().vectAdd(intersection_position.neg());
			distance_to_light.normalize();
			float distance_to_light_magnitude = distance_to_light.magnitude();
            Vector3 tempw = light_sources.at(light_index).getLightPosition().vectAdd(intersection_position.neg());
            tempw.normalize();
			Ray shadow_ray (intersection_position,tempw);

			vector<double> secondary_intersections;

			for (int object_index = 0; object_index < scene_objects.size() && shadowed == false; object_index++) {
				secondary_intersections.push_back(scene_objects.at(object_index).findIntersection(shadow_ray));
			}

			for (int c = 0; c < secondary_intersections.size(); c++) {
				if (secondary_intersections.at(c) > accuracy) {
					if (secondary_intersections.at(c) <= distance_to_light_magnitude) {
						shadowed = true;
					}
				}
				break;
			}

			if (shadowed == false) {
				final_color = final_color.colorAdd(winning_object_color.colorMultiply(light_sources.at(light_index).getLightColor()).colorScalar(cosine_angle));

				if (winning_object_color.getColorSpecial() > 0 && winning_object_color.getColorSpecial() <= 1) {
					// special [0-1]
					double dot1 = winning_object_normal.dot(intersecting_ray_direction.neg());
					Vector3 scalar1 = winning_object_normal.Mult(dot1);
					Vector3 add1 = scalar1.vectAdd(intersecting_ray_direction);
					Vector3 scalar2 = add1.Mult(2);
					Vector3 add2 = intersecting_ray_direction.neg().vectAdd(scalar2);
					Vector3 reflection_direction = add2;
					reflection_direction.normalize();

					double specular = reflection_direction.dot(light_direction);
					if (specular > 0) {
						specular = pow(specular, 10);
						final_color = final_color.colorAdd(light_sources.at(light_index).getLightColor().colorScalar(specular*winning_object_color.getColorSpecial()));
					}
				}

			}

		}
	}

	return final_color.clip();
}


void drawAxes()
{
	if(drawaxes==1)
	{
		glColor3f(0, 0, 1);
		glBegin(GL_LINES);{
			glVertex3f( 100,0,0);
			glVertex3f(-100,0,0);
			glVertex3f(0,-100,0);
			glVertex3f(0, 100,0);
			glVertex3f(0,0, 100);
			glVertex3f(0,0,-100);
		}glEnd();
	}
}




void drawGrid()
{
	int i;
	if(drawgrid==1)
	{
		glColor3f(0.6, 0.6, 0.6);	//grey
		glBegin(GL_LINES);{
			for(i=-8;i<=8;i++){

				if(i==0)
					continue;	//SKIP the MAIN axes

				//lines parallel to Y-axis
				glVertex3f(i*10, -90, 0);
				glVertex3f(i*10,  90, 0);

				//lines parallel to X-axis
				glVertex3f(-90, i*10, 0);
				glVertex3f( 90, i*10, 0);
			}
		}glEnd();
	}
}






void keyboardListener(unsigned char key, int x,int y){

    switch(key)
        {
            case '1':
            cam.yaw(-delta);
            break; /// lookleft
        case '2':
            cam.yaw(delta);
            break; /// lookright
        case '3':
            cam.pitch(-delta);
            break; /// lookup
        case '4':
            cam.pitch(delta);
            break; /// lookdown
        case '5':
            cam.roll(delta);
            break; /// twistleft
        case '6':
            cam.roll(-delta);
            break; /// twistright
        case '9':
			dr = 1,dg=1,db=1;//set diffuse light to white
			break;
		case '0':
			dr = 0,dg=0,db=1;//set diffuse light to blue
			break;
        case 'a':
		    cam.slide(0,delta,0);
		    break; /// up
		case 's':
		    cam.slide(0,-delta,0);
		    break; /// down
		case '7':
			diff_ang+=diff_delta;
			dfx = diff_rad*cos(PI*diff_ang/180.);
			dfy = diff_rad*sin(PI*diff_ang/180.);
			break;
		case '8':
			diff_ang-=diff_delta;
			dfx = diff_rad*cos(PI*diff_ang/180.);
			dfy = diff_rad*sin(PI*diff_ang/180.);
			break;
					case 'i':
                sz+=10;
			break;
				case 'j':
                sz-=10;
			break;
            case 'k':
                ax=0.3;ay=0.3;az=0.3;
			break;
				case 'l':
                ax=1.0;ay=1.0;az=1.0;
			break;


			}
}



void specialKeyListener(int key, int x,int y){
	switch(key)
	{
		case GLUT_KEY_UP:
		    cam.slide(0,0,-delta);
		    //cameraAngle+=0.5;
		    break; /// forward
		case GLUT_KEY_DOWN:
		    cam.slide(0,0,delta);
		    //cameraAngle-=0.5;
		    break; /// backward
		case GLUT_KEY_LEFT:
		    cam.slide(-delta,0,0);
		    break; /// left
		case GLUT_KEY_RIGHT:
		    cam.slide(delta,0,0);
		    break; /// right
		case GLUT_KEY_PAGE_UP:
		    cam.slide(0,delta,0);
		    break; /// up
		case GLUT_KEY_PAGE_DOWN:
		    cam.slide(0,-delta,0);
		    break; /// down
	}
	glutPostRedisplay();
}









void drawsphere2(float radius,int slices,int stacks)
{
	struct point points[100][100];
		int i,j;
	double h,r;
	for(i=0;i<=stacks;i++)
	{
		h=radius*sin(((double)i/(double)stacks)*(pi/2))*.5;
		r= radius*cos(((double)i/(double)stacks)*(pi/2))*.55;
		//r=exp(-((h*h)/45))*sqrt(radius*radius-h*h);
        //if(i > (stacks/2))r=(r/h);
		for(j=0;j<=slices;j++)
		{
			points[i][j].x=r*cos(((double)j/(double)slices)*2*pi);
			points[i][j].y=r*sin(((double)j/(double)slices)*2*pi);
			points[i][j].z=h;
		}

	}
	for(i=0;i<stacks;i++)
	{
		for(j=0;j<slices;j++)
		{
			//glColor3f((double)i/(double)stacks,(double)i/(double)stacks,(double)i/(double)stacks);
			//glColor3f(0.6,0.6,0.6);
			 glColor3f(.5,.75,0.5);


			glBegin(GL_QUADS);{

				glVertex3f(points[i][j].x,points[i][j].y,points[i][j].z);
				glVertex3f(points[i][j+1].x,points[i][j+1].y,points[i][j+1].z);
				glVertex3f(points[i+1][j+1].x,points[i+1][j+1].y,points[i+1][j+1].z);
				glVertex3f(points[i+1][j].x,points[i+1][j].y,points[i+1][j].z);

			}glEnd();
		}

	}
}

void drawSpheres()
{
    for(int i=0;i<spheres.size();i++)
    {

        glPushMatrix();{


	glColor3f(spheres[i].colorR,spheres[i].colorG,spheres[i].colorB);
	glTranslatef(spheres[i].centerX,spheres[i].centerY,spheres[i].centerZ);
	glutSolidSphere(spheres[i].radius,100,80);

	}glPopMatrix();
    }

}

void drawCubes()
{
    for(int i=0;i<cubes.size();i++)
    {

        glPushMatrix();{




	glColor3f(cubes[i].colorR,cubes[i].colorG,cubes[i].colorB);
	glTranslatef(cubes[i].lwx,cubes[i].lwy,cubes[i].lwz);
	glutSolidCube(cubes[i].length);


	}glPopMatrix();
    }

}
void drawCheckerBoard(){


glPushMatrix();{

    glTranslatef(200,200,0);

for(int i=0;i<80;i++)
{

    for(int j=0;j<80;j++)
    {

            if(i%2){
                    if(j%2)glColor3f(0,0,0);
                    else glColor3f(1,1,1);
            }
            else
            {
                    if(j%2)glColor3f(1,1,1);
                    else glColor3f(0,0,0);

            }

            glPushMatrix();{
            glTranslatef(200-(i*gridUnitSize),200-(j*gridUnitSize),0);


            glBegin(GL_QUADS);{

                    glVertex3f(0,0,0);
                    glVertex3f(0,gridUnitSize,0);
                    glVertex3f( gridUnitSize, gridUnitSize,0);
                    glVertex3f(gridUnitSize,0,0);

                }glEnd();
                	}glPopMatrix();

        }
    }
	}glPopMatrix();
}

void drawWorld(){

    drawSpheres();
    drawCubes();


}

void setLightSources()
{
//    setAmbientLight(ax,ay,az);
setDiffuseLight(light_sources[0].x,light_sources[0].y,light_sources[0].z,dr,dg,db);
light_sources[0].setLightColor(dr,dg,db);

//	set_spot_light(100,100,50,0,-1,-1);

	setSpotLight(light_sources[1].x,light_sources[1].y,light_sources[1].z,1,-1,-1);

	light_sources[1].setLightColor(1,0,0);

}

void display(){

	//clear the display
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0,0,0,0);	//color black
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	/********************
     / set-up camera here
     ********************/
	//load the correct matrix -- MODEL-VIEW matrix
///glMatrixMode(GL_MODELVIEW);

	//initialize the matrix
	///glLoadIdentity();

	//now give three info
	//1. where is the camera (viewer)?
	//2. where is the camera looking?
	//3. Which direction is the camera's UP direction?

	//gluLookAt(100,100,100,	0,0,0,	0,0,1);
	///gluLookAt(100*cos(cameraAngle), 100*sin(cameraAngle), cameraHeight,		0,0,0,		0,0,1);
	//gluLookAt(0,-1,150,	0,0,0,	0,0,1);


	//again select MODEL-VIEW
	///glMatrixMode(GL_MODELVIEW);


setLightSources();


	drawAxes();
//	drawGrid();
	//~ glColor3f(1,0,0);
	/****************************
     / Add your objects from here
     ****************************/

	drawCheckerBoard();
	drawWorld();

	//ADD this line in the end --- if you use double buffer (i.e. GL_DOUBLE)
	glFlush();

	glutSwapBuffers();
}



void init(){
	//codes for initialization
	drawgrid=0;
	drawaxes=1;

    cameraHeight=100.0;
	cameraAngle=1.0;

	angle=0;

	radius = 5;
	pos = MAX_HEIGHT;
	//~ t=0;
	acce = 9.8;
	dir = -1;
	//clear the screen
	glClearColor(0,0,0,0);

	/************************
     / set-up projection here
     ************************/
	//load the PROJECTION matrix
	glMatrixMode(GL_PROJECTION);

	//initialize the matrix
	glLoadIdentity();

	//give PERSPECTIVE parameters
	gluPerspective(80,	9./7.,	1,	10000.0);
	//field of view in the Y (vertically)
	//aspect ratio that determines the field of view in the X direction (horizontally)
	//near distance
	//far distance
}

void initCamera()
{
    glViewport(0,0,1200,700);
    Vector3 up(0,0,1);
    Point3 eye(100.0f,10.0f,150.0f);
    Point3 look(10.0f,10.0f,10.0f);
    cam.setPos(3, 1.5, -4);
    cam.set(eye,look,up);
    cam.setShape(30.0f,64.0f/48.0f,0.5f,500.0f);
}

void animate(){
	angle+=0.05;
	//codes for any changes in Models, Camera
	glutPostRedisplay();
}


void reshapeHandler(int w,int h){
	if(!h)h=1;
	double rat = 1.0*w/h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0,0,w,h);
	gluPerspective(80,rat,1,10000);
	glMatrixMode(GL_MODELVIEW);
}

void parseInputData()
{
    ifstream  data("E:\\CODES\\Glut\\RayTracer\\RayTracer\\description.txt");
    string line;
    int flag=0;

    int newLineCount=0;

        //y.resize(4);



         for(int i=0,j=0;std::getline(data,line);i++)
        {
            if(flag==0)
            {
                if(line=="")
                {
                    newLineCount++;
                }


                if(line!="" && newLineCount==0)
                {
                    if(i==0)
                    {
                        recursionDepth=atoi(line.c_str());
                        cout<<"depth: "<<recursionDepth<<endl;
                    }
                    else if(i==1)
                    {
                        resolution=atoi(line.c_str());
                        cout<<"resolution: "<<resolution<<endl;
                    }
                }
                else if(line!=""&& newLineCount==1)
                {
                    totalObjects=atoi(line.c_str());
                    flag=totalObjects;
                }

                else if(line!=""&&newLineCount==3)
                {
                    lights= atoi(line.c_str());

                }
                else if(line!="" && newLineCount>3)//take light sources
                {
                    std::vector<std::string> x = split(line,' ');
                    point temp;
                    temp.x=atof(x[0].c_str());
                    temp.y=atof(x[1].c_str());
                    temp.z=atof(x[2].c_str());
                    light_sources.push_back(temp);



                }

            }
            else if(flag>0)//read objects details
            {
                if(line=="")
                {

                    int type=0;
                    sphere x;
                    cube y;
                    for(int k=0;k<6 && std::getline(data,line);k++)
                    {
                        if(line=="1" && k==0)
                        {
                            type=1;//sphere


                        }
                        else if(line=="2" && k==0)
                        {
                            type=2;//cube

                        }
                        if(type==1 && k>0)//reading spheres description
                        {
                            if(k==1)
                            {
                                std::vector<std::string> z = split(line,' ');
                                x.setCenter(atof(z[0].c_str()),atof(z[1].c_str()),atof(z[2].c_str()));

                            }
                            else if(k==2)
                            {
                                x.setRadius(atof(line.c_str()));
                            }
                            else if(k==3)
                            {
                                std::vector<std::string> z = split(line,' ');
                                x.setColor(atof(z[0].c_str()),atof(z[1].c_str()),atof(z[2].c_str()));

                            }
                            else if(k==4)
                            {
                                std::vector<std::string> z = split(line,' ');
                                x.setLightCoeff(atof(z[0].c_str()),atof(z[1].c_str()),atof(z[2].c_str()));

                            }
                            else if(k==5)
                            {
                                x.setShinness(atof(line.c_str()));
                                spheres.push_back(x);

                            }
                        }
                        else if(type==2 && k>0)//reading cubes description
                        {
                             if(k==1)
                            {
                                std::vector<std::string> z = split(line,' ');
                                y.setLowerCoord(atof(z[0].c_str()),atof(z[1].c_str()),atof(z[2].c_str()));

                            }
                            else if(k==2)
                            {
                                y.setLength(atof(line.c_str()));
                            }
                            else if(k==3)
                            {
                                std::vector<std::string> z = split(line,' ');
                                y.setColor(atof(z[0].c_str()),atof(z[1].c_str()),atof(z[2].c_str()));

                            }
                            else if(k==4)
                            {
                                std::vector<std::string> z = split(line,' ');
                                y.setLightCoeff(atof(z[0].c_str()),atof(z[1].c_str()),atof(z[2].c_str()));

                            }
                            else if(k==5)
                            {
                                y.setShinness(atof(line.c_str()));
                                cubes.push_back(y);

                            }

                        }



                    }
                }
                flag--;

            }




    }



    //printing for debug

 cout<<"Spheres: "<<spheres.size()<<" Cubes: "<<cubes.size()<<" Lights: "<<light_sources.size()<<endl;






}


void saveImage(const char *filename, int w, int h, int dpi, vector<colorRGB>data)
{
    cout<<"Creating the image......"<<endl;
    FILE *f;
	int k = w*h;
	int s = 4*k;
	int filesize = 54 + s;

	double factor = 39.375;
	int m = static_cast<int>(factor);

	int ppm = dpi*m;

	unsigned char bmpfileheader[14] = {'B','M', 0,0,0,0, 0,0,0,0, 54,0,0,0};
	unsigned char bmpinfoheader[40] = {40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0,24,0};

	bmpfileheader[ 2] = (unsigned char)(filesize);
	bmpfileheader[ 3] = (unsigned char)(filesize>>8);
	bmpfileheader[ 4] = (unsigned char)(filesize>>16);
	bmpfileheader[ 5] = (unsigned char)(filesize>>24);

	bmpinfoheader[ 4] = (unsigned char)(w);
	bmpinfoheader[ 5] = (unsigned char)(w>>8);
	bmpinfoheader[ 6] = (unsigned char)(w>>16);
	bmpinfoheader[ 7] = (unsigned char)(w>>24);

	bmpinfoheader[ 8] = (unsigned char)(h);
	bmpinfoheader[ 9] = (unsigned char)(h>>8);
	bmpinfoheader[10] = (unsigned char)(h>>16);
	bmpinfoheader[11] = (unsigned char)(h>>24);

	bmpinfoheader[21] = (unsigned char)(s);
	bmpinfoheader[22] = (unsigned char)(s>>8);
	bmpinfoheader[23] = (unsigned char)(s>>16);
	bmpinfoheader[24] = (unsigned char)(s>>24);

	bmpinfoheader[25] = (unsigned char)(ppm);
	bmpinfoheader[26] = (unsigned char)(ppm>>8);
	bmpinfoheader[27] = (unsigned char)(ppm>>16);
	bmpinfoheader[28] = (unsigned char)(ppm>>24);

	bmpinfoheader[29] = (unsigned char)(ppm);
	bmpinfoheader[30] = (unsigned char)(ppm>>8);
	bmpinfoheader[31] = (unsigned char)(ppm>>16);
	bmpinfoheader[32] = (unsigned char)(ppm>>24);

	f = fopen(filename,"wb");

	fwrite(bmpfileheader,1,14,f);
	fwrite(bmpinfoheader,1,40,f);

	for (int i = 0; i < k; i++) {
		colorRGB rgb = data[i];

		double red = (rgb.r)*255;
		double green = (rgb.g)*255;
		double blue = (rgb.b)*255;

		unsigned char color[3] = {(char)floor(blue),(char)floor(green),(char)floor(red)};

		fwrite(color,1,3,f);
	}

	fclose(f);

	    cout<<"Image Created successfully..."<<endl;

}

void generateImage()
{
    double ambientlight = 0.2;
	double accuracy = 0.00000001;
    int recdepth=1;
    int dpi=72;
	vector<colorRGB>pixels;
	pixels.resize(resolution*resolution);


    Vector3 O (0,0,0);
	Vector3 X (1,0,0);

	Vector3 Z (0,0,1);




    scene_objects.push_back(checkerBoard);

    for(int i=0;i<spheres.size();i++)scene_objects.push_back(spheres[i]);


	Vector3 campos = cam.getCameraLocation();
	campos.print();

	Vector3 look_at = cam.getCameraLookAt();


//	Vector3 diff (campos.x - look_at.x, campos.y - look_at.y, campos.z - look_at.z);

	Vector3 camdir = cam.n.neg();


	Vector3 camright = cam.u;

	Vector3 camdown = cam.v;

//	Camera scene_cam(campos, camdir, camright, camdown);

    double xamnt,yamnt;
    int aa_index;

    for(int i=0;i<resolution;i++)
    {
        for(int j=0;j<resolution;j++)
        {

            double tempRed[recdepth*recdepth];
			double tempGreen[recdepth*recdepth];
			double tempBlue[recdepth*recdepth];


int index=(i*resolution+j);

        for (int aax = 0; aax < recdepth; aax++) {//loop for reflection level
            for (int aay = 0; aay < recdepth; aay++) {
                    aa_index = aay*recdepth + aax;

                    xamnt = (i + 0.5)/resolution;
                    yamnt = ((resolution - j) + 0.5)/resolution;

//                    camright.print();
//                    camdown.print();
//                    camdir.print();
//                    cout<<endl;
                    Vector3 cam_ray_origin = cam.getCameraLocation();
					Vector3 cam_ray_direction = camdir.vectAdd(camright.Mult(xamnt - 0.5).vectAdd(camdown.Mult(yamnt - 0.5)));

					cam_ray_direction .normalize();
//                    cam_ray_direction.print();
//
//                    camdir.print();
//                    camright.print();
//                    camdown.print();
//                    cout<<"!!"<<endl;
					Ray eyeRay(cam_ray_origin, cam_ray_direction);



                    vector<double> intersections;

                    intersections.push_back(checkerBoard.findIntersection(eyeRay));

                    for(int l=0;l<spheres.size();l++)
                    {
                        intersections.push_back(spheres[l].findIntersection(eyeRay));
                    }

//                    cout<<intersections[0]<<endl;

                    int index_of_winning_object= winningObjectIndex(intersections);
                    if (index_of_winning_object == -1) {
						// set the backgroung black
						tempRed[aa_index] = 0;
						tempGreen[aa_index] = 0;
						tempBlue[aa_index] = 0;
					}
					else{
						// index coresponds to an object in our scene
						if (intersections.at(index_of_winning_object) > accuracy) {
							// determine the position and direction vectors at the point of intersection

							Vector3 intersection_position = cam_ray_origin.vectAdd(cam_ray_direction.Mult(intersections.at(index_of_winning_object)));
							Vector3 intersecting_ray_direction = cam_ray_direction;

                           if(index_of_winning_object==0)//plane
							{
							    Color intersection_color = getColorAt(intersection_position, intersecting_ray_direction, checkerBoard, index_of_winning_object, light_sources, accuracy, ambientlight);
//                            cout<<"color "<<intersection_color.r<<" "<<intersection_color.g<<" "<<intersection_color.b<<endl;
							tempRed[aa_index] = intersection_color.getColorRed();
							tempGreen[aa_index] = intersection_color.getColorGreen();
							tempBlue[aa_index] = intersection_color.getColorBlue();
							}
							else//otherObjects
							{
							    Color intersection_color = getColorAtSphere(intersection_position, intersecting_ray_direction, spheres, index_of_winning_object, light_sources, accuracy, ambientlight);
//                            cout<<"color "<<intersection_color.r<<" "<<intersection_color.g<<" "<<intersection_color.b<<endl;
//							intersection_color.print();
							tempRed[aa_index] = intersection_color.getColorRed();
							tempGreen[aa_index] = intersection_color.getColorGreen();
							tempBlue[aa_index] = intersection_color.getColorBlue();
							}
						}
					}
            }
        }




			// average the pixel color
			double totalRed = 0;
			double totalGreen = 0;
			double totalBlue = 0;

			for (int iRed = 0; iRed < recdepth*recdepth; iRed++) {
				totalRed = totalRed + tempRed[iRed];
			}
			for (int iGreen = 0; iGreen < recdepth*recdepth; iGreen++) {
				totalGreen = totalGreen + tempGreen[iGreen];
			}
			for (int iBlue = 0; iBlue < recdepth*recdepth; iBlue++) {
				totalBlue = totalBlue + tempBlue[iBlue];
			}

			double avgRed = totalRed/(recdepth*recdepth);
			double avgGreen = totalGreen/(recdepth*recdepth);
			double avgBlue = totalBlue/(recdepth*recdepth);
//            double avgRed  = totalRed;
//            double avgBlue = totalBlue;
//            double avgGreen = totalGreen;

            if(avgRed>1){avgRed=1;cout<<"aisi";}
            if(avgBlue>1)avgBlue=1;
            if(avgGreen>1)avgGreen=1;

            pixels[index].r=avgRed;
            pixels[index].g=avgGreen;
            pixels[index].b=avgBlue;
        }

    }

    saveImage("E:\\CODES\\Glut\\RayTracer\\RayTracer\\output.bmp",resolution,resolution,dpi,pixels);

}


void mouseListener(int button, int state, int x, int y){	//x, y is the x-y of the screen (2D)
	switch(button){
		case GLUT_LEFT_BUTTON:
			if(state == GLUT_DOWN){		// 2 times?? in ONE click? -- solution is checking DOWN or UP
				drawaxes=1-drawaxes;
			}
			break;

		case GLUT_RIGHT_BUTTON:
			//........
			generateImage();//my rayTracer

			break;

		case GLUT_MIDDLE_BUTTON:
			//........
			break;

		default:
			break;
	}
}


int main(int argc, char **argv){
	//~ freopen("in.txt","r",stdin);
	//freopen("out.txt","w",stdout);



	glutInit(&argc,argv);
	glutInitWindowSize(900, 700);
	glutInitWindowPosition(0, 0);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGB);	//Depth, Double buffer, RGB color

    parseInputData();
	glutCreateWindow("Bouncing Ball");

	init();

	initCamera();//init camera
	initLighting();//init lights



	glEnable(GL_DEPTH_TEST);	//enable Depth Testing

	glutDisplayFunc(display);	//display callback function
	glutIdleFunc(animate);		//what you want to do in the idle time (when no drawing is occuring)

	glutKeyboardFunc(keyboardListener);
	glutSpecialFunc(specialKeyListener);
	glutMouseFunc(mouseListener);

    glutReshapeFunc(reshapeHandler);
//    glutTimerFunc(20,bounce,0);


	glutMainLoop();		//The main loop of OpenGL



	return 0;
}
