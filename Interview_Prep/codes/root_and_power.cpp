/*
 *

 
 In this question we’ll implement a function root that calculates the n’th root
  of a number. The function takes a nonnegative number x and a positive integer n,
   and returns the positive n’th root of x within an error of 0.001 (i.e. suppose
    the real root is y, then the error is: |y-root(x,n)| and must satisfy 
    |y-root(x,n)| < 0.001).

Don’t be intimidated by the question. While there are many algorithms to 
calculate roots that require prior knowledge in numerical analysis (some 
of them are mentioned here), there is also an elementary method which doesn’t
 require more than guessing-and-checking. Try to think more in terms of the 
 latter.

Make sure your algorithm is efficient, and analyze its time and space
 complexities.

Examples:

input:  x = 7, n = 3
output: 1.913

input:  x = 9, n = 2
output: 3


 */



#include <iostream>

using namespace std;

double abs_d(double x, double y){
  return x-y>0?x-y:y-x;
}

double getNthPower(double x, int n){
	if(n==0)return 1;
  if(n%2==1)return x*getNthPower(x,n-1);
  else return getNthPower(x*x, n/2);
}



double root(double x, unsigned int n) 
{
  // your code goes here
  double low = 0.0;
	double high = x;
	double mid;
	while(low<=high){
		mid = low + (high-low)/2;
    //cout<<mid<<endl;
		double cur = getNthPower(mid,n);
		if(abs_d(cur, x)<=0.001){
     // cout<<mid<<endl;
			return mid;
		}
		else if(cur < x){
			low = mid;
		}
		else{
			high = mid;
		}
	}

}

int main() {
  
  return 0;
}

