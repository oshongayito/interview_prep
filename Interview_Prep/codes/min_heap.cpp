#include<iostream>
using namespace std;

#include<vector>
#include<cmath>

class minHeap{
private:
	int size;
	int capacity;
	long long negMax;
	vector<int>heap;

public:
	minHeap(int cap){
		heap.resize(cap);
		size=0;
		capacity=cap;
		negMax  = -pow(2,32);
	}
	int parent(int i){
		return (i-1)/2;
	}
	int left(int i){
		return (2*i)+1;
	}
	int right(int i){
		return (2*i)+2;
	}
	void insert(int val);
	void remove(int index);
	void minHeapify(int root);
	int extractMin();
	void decreaseKey(int ind, int newVal);

	void print();

};

void minHeap::print(){
	cout<<"capacity "<<capacity<<endl;
	for(int i=0;i<size;i++){
		cout<<heap[i]<<" ";
	}
	cout<<endl;
}


/*
insert goes from bottom to top
*/
void minHeap::insert(int val){
	cout<<val<<endl;
	if(size+1 > capacity){
		heap.push_back(val);
		capacity++;
	}
	else{
		heap[size]=val;
	}
	
	//if the newly inserted element is smaller than some other elment, 
	// then it should be moved to it's proper position.
	int i=size;
	while(i>0 && heap[i] < heap[parent(i)]){
		swap(heap[i],heap[parent(i)]);
		i=parent(i);
	}

	size++;
	return;
}

/*
In a min heap, every node is less than it's childs. 
Therefore, checking the smaller child and going in that direction is gonna yield in the correct 
order of the min heap.
minHeapify goes from top to bottom.
minHeapify is called when the minHeap property is distorted and needs to fixed.
*/

void minHeap::minHeapify(int root){
	int smaller = root;
	if(left(root)<size && heap[left(root)]<heap[root])smaller = left(root);
	if(right(root)<size && heap[right(root)]<heap[smaller]) smaller = right(root);

	if(smaller != root){
		swap(heap[root],heap[smaller]);
		minHeapify(smaller);
	}
}

int minHeap::extractMin(){
	if(size==0)return negMax;
	int res = heap[0];
	heap[0]=heap[size-1];
	size--;
	minHeapify(0);
	return res;

}

/*
update the value of a index using a new val.
Here we are decreasing the value, that's why the cur element can only go up not down.
However, the minHepify goes down. So we can't use that.
For the same reason, while inserting we are also going up, as the newly inserted value 
is inserted in the end and can only go up (if possible).

decreaseKey goes from bottom to top
*/
void minHeap::decreaseKey(int ind, int newVal){
	heap[ind] = newVal;

	//after updating we need to maintain the heap property
	while(ind>0 && heap[ind] < heap[parent(ind)]){
		swap(heap[ind], heap[parent(ind)]);
		ind = parent(ind);
	}
}

void minHeap::remove(int ind){
	decreaseKey(ind, negMax); //this will set the negMax in id and will move it to the root
	extractMin();//now just extract the root will do the removing
}


int main(){
	minHeap my_heap = minHeap(2);
	my_heap.insert(20);
	my_heap.insert(10);
	my_heap.insert(30);
	my_heap.insert(40);
	my_heap.insert(5);
	my_heap.print();
	cout<<"extractMin "<<my_heap.extractMin()<<endl;
	my_heap.print();
	//cout<<"extractMin "<<my_heap.extractMin()<<endl;
	cout<<"removing from ind 2"<<endl;
	my_heap.remove(2);
	my_heap.print();
	//cout<<"extractMin "<<my_heap.extractMin()<<endl;
	cout<<"removing from ind 2"<<endl;
	my_heap.remove(2);
	my_heap.print();


	priority_queue <int, vector<int>, greater<int> > pq;

	
	return 0;
}








class minHeap{
    int size, capacity;
    long long negMax;
    vector<ListNode*>heap;
    public:
    minHeap(int cap){
        size = 0;
        negMax = -pow(2,33);
        capacity = cap;
        heap.resize(cap);
    }
    int parent(int i){return (i-1)/2;}
    int left(int i){return (2*i)+1;}
    int right(int i){return (2*i)+2;}
    
    void insert(ListNode* node){
        if(size<capacity){
            heap[size] = node;
        }
        else{
            heap.push_back(node);
            capacity++;
        }    

        int i = size;
        while(i>0 && heap[i]->val < heap[parent(i)]->val){
            swap(heap[i],heap[parent(i)]);
            i = parent(i);
        }
        size++;
    }

    void minHeapify(int root){
        int smaller = root;
        if(left(root)<size && heap[left(root)]->val < heap[root]->val)smaller = left(root);
        if(right(root)<size && heap[right(root)]->val < heap[smaller]->val)smaller = right(root);
        if(smaller != root){
            swap(heap[smaller],heap[root]);
            minHeapify(smaller);
        }
    }
    
    ListNode* extractMin(){
        if(size<=0)return NULL;
        ListNode* res = heap[0];
        heap[0] = heap[size-1];
        size--;
        minHeapify(0);
        return res;
    }
    
    bool empty(){
        return size==0;
    }
    
};


ListNode* Solution::mergeKLists(vector<ListNode*> &A) {
    // Do not write main() function.
    // Do not read input, instead use the arguments to the function.
    // Do not print the output, instead return values as specified
    // Still have a doubt. Checkout www.interviewbit.com/pages/sample_codes/ for more details
    ListNode* head = NULL;
    ListNode* tail = NULL;
    minHeap myHeap = minHeap(A.size());
    for(int i=0;i<A.size();i++){
        myHeap.insert(A[i]);
    }
    
    if(!myHeap.empty()){
        head = myHeap.extractMin();
        tail = head;
        if(tail->next!=NULL)myHeap.insert(tail->next);
    }

    while(!myHeap.empty()){
        ListNode* temp = myHeap.extractMin();
        if(temp->next!=NULL)myHeap.insert(temp->next);
        tail->next = temp;
        tail = tail->next;
    }
    return head;

}

