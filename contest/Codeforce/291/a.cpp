    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
string reverse_string(string str)
{
    string r;
    for(int i=str.size()-1;i>=0;i--)
    {

        r+=str[i];
    }
    return r;
}

int main()
{
    ll x;
    cin>>x;
    string str;
    for(int i=0;x;i++)
    {
        if((x%10)>4)
        {

            str+='0'+9-(x%10);
        }
        else str+='0'+(x%10);
        x/=10;

    }
    str=reverse_string(str);
    if(str[0]=='0')str[0]='9';
    cout<<str<<endl;
    return 0;
}
