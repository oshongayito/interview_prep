    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000


int main()
{
    int res=0,x1,y1,x2,y2,i,j,k,n,tmpx,tmpy,lev;
    vc<int> tedit;
    cin>>n;
    tedit.resize(n);
    for(i=0;i<n;i++)cin>>tedit[i];
    cin>>x1>>y1>>x2>>y2;

    if(x1>x2){tmpx=x1;tmpy=y1;x1=x2;y1=y2;x2=tmpx;y2=tmpy;}
    for(i=x1,lev=y1;i<=x2;i++)
    {
        if(lev>tedit[i] && i>x1){res+=tedit[i]-lev;lev=tedit[i];}
        res++;
        if(i==x2)res+=abs(tedit[i]-lev);
    }

    cout<<res<<endl;

    return 0;
}
