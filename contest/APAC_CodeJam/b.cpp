    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>
    #include <fstream>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 1000000007LL
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))


int main()
{

    freopen ("B-large.in","r",stdin);
    ofstream myfile;
  myfile.open ("output.txt");



    ll t,n,a,b,p;
    vc< pair<ll,ll>>bus;
    vc<ll>home;

    cin>>t;

    for(int k=1;k<=t;k++)
    {
        cin>>n;
        bus.resize(n);
        for(int i=0;i<n;i++)
        {
            cin>>a>>b;
            bus[i]=mp(a,b);
        }

        cin>>p;
        home.resize(p);
        for(int i=0;i<p;i++)cin>>home[i];


        myfile<<"Case #"<<k<<": ";
        for(int i=0;i<p;i++)
        {
            ll count=0;
            for(int j=0;j<n;j++)
            {
                if(home[i]>=bus[j].first && home[i]<=bus[j].second)count++;
            }
            if(i>0)myfile<<" "<<count;
            else
                myfile<<count;
        }
        myfile<<endl;

        bus.clear();
        home.clear();
    }
    return 0;
}
