#include<iostream>
using namespace std;

long long int Max = 4000000;

long long int sum;

void get_fibo_even_sum(int x,int y)
{
    if(x>Max)return;

    if(x%2==0)sum+=x;
    get_fibo_even_sum(y,x+y);
}

int main()
{
   get_fibo_even_sum(1,2);
   cout<<sum<<endl;
    return 0;
}
