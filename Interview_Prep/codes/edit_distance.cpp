
/*
 * Related leetcode problem: https://leetcode.com/problems/edit-distance/
 */

/*
 * Problem description: Given two words word1 and word2, find the minimum 
 * number of operations required to convert word1 to word2.

 * You have the following 3 operations permitted on a word:

 * 1. Insert a character
 * 2. Delete a character
 * 3. Replace a character
 */

/* edit distance with insert, delete and replace operations */
int edit_distance(string A, string B){ 
	int n1 = A.size();
	int n2 = B.size();

	vector< vector<int> >DP;

	DP.resize(n1+1);
	for(int i =0; i<=n1;i++){
		DP[i].resize(n2+1,0);
	}
    
    for(int i=0;i<=n1;i++){
        DP[i][0] = i;
    }
    for(int j=0;j<=n2;j++){
        DP[0][j] = j;
    }
    
	for(int i=1;i<=n1;i++){
		for(int j=1;j<=n2;j++){
			if(A[i-1] == B[j-1]){ 			//if same characters, then do nothing
				DP[i][j] = DP[i-1][j-1];
			}
			else{
				DP[i][j] = 1+ min(				//else find minimum of insert, delete, and replace
						DP[i-1][j],		//insert (inserting ith element of A)
						min(DP[i-1][j-1],	//replace
						DP[i][j-1])		//delete (deleting jth elem of B)
					);
			}
		}
	}

	return DP[n1][n2];
}


/* edit distance with insert and delete  operations */
int edit_distance2(string A, string B){ 
	int n1 = A.size();
	int n2 = B.size();

	vector< vector<int> >DP;

	DP.resize(n1+1);
	for(int i =0; i<=n1;i++){
		DP[i].resize(n2+1,0);
	}
    
    for(int i=0;i<=n1;i++){
        DP[i][0] = i;
    }
    for(int j=0;j<=n2;j++){
        DP[0][j] = j;
    }
    
	for(int i=1;i<=n1;i++){
		for(int j=1;j<=n2;j++){
			if(A[i-1] == B[j-1]){
				DP[i][j] = DP[i-1][j-1];
			}
			else{
				DP[i][j] = 1+ min(
					DP[i-1][j],		//insert
					DP[i][j-1]		//delete
					);
			}

		}
	}

	return DP[n1][n2];
}



/*
 The second version of the ED can also be implemented using LCS.
 as no replace operation is allowed, we have to do only insert or delete. 
 we can count the number of inserts and deletes by subtracting the lcs from
 the maximum length of the two strings
*/


int edit_distance2_using_lcs(string A, string B){
	int n1 = A.size();
	int n2 = B.size();
	int lcs = LCS(A,B);
	return (n1-lcs)+	//number of inserts
			(n2-lcs);	//number of deletes
}






