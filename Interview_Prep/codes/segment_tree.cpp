/*
 * This is an implementation of segment tree to find minimum in a range (min range query)
 * */

#include<iostream>
#include<vector>
#include<cmath>
using namespace std;
#define max_val 100000000
//0 4 2
// 0 2 1 3 4
// 2 2



void update_s_tree_element(vector<int>&s_tree,vector<int>&arr, int ind, int new_val, int low, int high, int pos){
    int mid = (low+high)/2;
//    if(ind<low || ind>high)return;
    if(low == high){
        cout<<"mid: "<<mid<<" pos: "<<pos<<endl;
        arr[ind] = new_val;
        s_tree[pos] = new_val;
        return;
    }

    if(ind <= mid){
        update_s_tree_element(s_tree, arr, ind, new_val, low, mid, (2*pos)+1);
    }
    else{
        update_s_tree_element(s_tree, arr, ind, new_val, mid+1, high, (2*pos)+2);
    }
//    cout<<pos<<endl;
    s_tree[pos] = min(s_tree[(2*pos)+1], s_tree[(2*pos)+2]);
}

//O(logn)
int get_range_min(vector<int>&s_tree,int q_low, int q_high, int low, int high, int pos){
    //no overlap
    if((q_low<low && q_high<low) || (q_low>high && q_high>high)){
        return max_val;
    }
        //total overlap
    else if(low>=q_low && high<=q_high){
        return s_tree[pos];
    }
    //partial overlap
    else{
        int mid = (low+high)/2;
        return min(get_range_min(s_tree, q_low, q_high, low, mid, (2*pos)+1), get_range_min(s_tree, q_low, q_high, mid+1, high, (2*pos)+2));
    }
}


//Time Complexity O(n)
void construct_segment_tree(vector<int>&arr, vector<int>&s_tree, int low, int high, int pos){
    if(low == high){
        s_tree[pos] = arr[low];
        return;
    }
    int mid = (low+high)/2;
    construct_segment_tree(arr, s_tree, low, mid, (2*pos)+1);
    construct_segment_tree(arr, s_tree, mid+1, high, (2*pos)+2);
    s_tree[pos] = min(s_tree[(2*pos)+1], s_tree[(2*pos)+2]);
    return;
}

// 2 1 -1 3 5
//low = 0, high = 3, pos = 0, mid = 1   //s_tree[0] = 1
    // low = 0, high = 1, pos = 1, mid = 0      //s_tree[1] = 1
        // low = 0, high = 0, pos = 3, mid = 0          //s_tree[3] = 2
        // low = 1, high = 1, pos = 4, mid = 1          //s_tree[4] = 1

    // low = 2, high = 3, pos = 2, mid = 2      // s_tree[2] = 3
        //low = 2, high = 2, pos = 5, mid = 2           //s_tree[5] = -1
        //low = 3, high = 3, pos = 6, mid = 3           //s_tree[6] = 3


// so the segment tree will look like
// 1 1 3 2 1 7 3

void print_segment_tree(vector<int>s_tree){
    cout<<"the segment tree is:"<<endl;
    for(int i=0;i<s_tree.size();i++){
        cout<<s_tree[i]<<" ";
    }
    cout<<endl;
    return;
}

int main(){
    vector<int>arr;
    arr.push_back(2);
    arr.push_back(1);
    arr.push_back(-1);
    arr.push_back(3);
//    arr.push_back(5);

    int n = 2* pow(2,ceil(log2(arr.size())))-1;
    vector<int>s_tree;
    s_tree.resize(n,max_val);
    construct_segment_tree(arr, s_tree, 0, arr.size()-1, 0);
    print_segment_tree(s_tree);
    int low = 0, high = arr.size()-1;
    cout<<"minimum in range (0,3) is: "<<get_range_min(s_tree, 0, 3, low, high, 0)<<endl;
    cout<<"minimum in range (0,2) is: "<<get_range_min(s_tree, 0, 2, low, high, 0)<<endl;
    cout<<"minimum in range (0,1) is: "<<get_range_min(s_tree, 0, 1, low, high, 0)<<endl;
    cout<<"minimum in range (2,2) is: "<<get_range_min(s_tree, 2, 2, low, high, 0)<<endl;
    cout<<"minimum in range (2,3) is: "<<get_range_min(s_tree, 2, 3, low, high, 0)<<endl;
    cout<<"minimum in range (3,3) is: "<<get_range_min(s_tree, 3, 3, low, high, 0)<<endl;
//    cout<<"minimum in range (0,0) is: "<<get_range_min(s_tree, 0, 0, low, high, 0)<<endl;
//    cout<<"minimum in range (4,4) is: "<<get_range_min(s_tree, 4, 4, 0, 4, 0)<<endl;

//    s_tree[]
    //update
//    arr[2] = 7;
    update_s_tree_element(s_tree, arr, 2, 7, low, high, 0);
    print_segment_tree(s_tree);

    cout<<"minimum in range (0,3) is: "<<get_range_min(s_tree, 0, 3, low, high, 0)<<endl;
    cout<<"minimum in range (0,2) is: "<<get_range_min(s_tree, 0, 2, low, high, 0)<<endl;
    cout<<"minimum in range (0,1) is: "<<get_range_min(s_tree, 0, 1, low, high, 0)<<endl;
    cout<<"minimum in range (2,2) is: "<<get_range_min(s_tree, 2, 2, low, high, 0)<<endl;
    cout<<"minimum in range (2,3) is: "<<get_range_min(s_tree, 2, 3, low, high, 0)<<endl;
    cout<<"minimum in range (3,3) is: "<<get_range_min(s_tree, 3, 3, low, high, 0)<<endl;
//

    return 0;
}