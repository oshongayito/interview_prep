
/**
 * Recursive Versions of tree traversal
 */

void inOrderTraverse(TreeNode* root){
        if(root == NULL){
            return;
        }
        traverse(root->left);
        res.push_back(root->val); // res is vector<int> which is in global scope, or can be passed to the function
        traverse(root->right);
}

void preOrderTraverse(TreeNode* root){
        if(root == NULL){
            return;
        }
        res.push_back(root->val); // res is vector<int> which is in global scope, or can be passed to the function
        traverse(root->left);
        traverse(root->right);
}

void postOrderTraverse(TreeNode* root){
        if(root == NULL){
            return;
        }
        traverse(root->left);
        traverse(root->right);
        res.push_back(root->val); // res is vector<int> which is in global scope, or can be passed to the function
}


/**
 * Iterative Versions of tree traversal
 */
vector<int> inOrderTraversalIterative(TreeNode* root){
    stack<TreeNode*>stack;
    vector<int>res;
 
    while(root != NULL || !stack.empty()){
        while(root != NULL){
            stack.push(root);
            root = root->left;
        }
        root = stack.top();
        stack.pop();
        res.push_back(root->val);    
        root = root->right;
    }
    return res;
}


vector<int> postorderTraversalIterative(TreeNode* root) {
        stack<int> stack;
        vector<int>res;
        TreeNode* pre = NULL;
        TreeNode* current = root;
        while(current != NULL || !stack.empty()) {
            while(current != NULL) {
                stack.push(current);
                current = current->left;
            }
            current = stack.top();
            stack.pop();
            if(current->right != null && pre != current->right) {
                stack.push(current);
                current = current->right;
                continue;
            }

            res.push_back(current->val);
            pre = current;
            current = null;
        }
        return res;
}


vector<int> preOrderTraversalIterative(TreeNode* root){
    vector<int>res;
    stack<TreeNode*>rights; 
    while(root != NULL){
        res.push_back(root->val);
        if(root->right != NULL){
            rights.push(root->right); // keep the rights in the stack
        }
        
        root = root->left;
        if(root == NULL && !rights.empty()){
            root = rights.top();
            rights.pop();
        }
    }
    return res;
}
