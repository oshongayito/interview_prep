#include<iostream>
using namespace std;
#include<vector>

/*
* this implmentation can round to zero at some point
*/
long long combination(int n, int k) {
    long long num=1,den=1,res=1;
    int temp;
    if(n<k)return 0;

    if(n-k < k)k = n-k; //reducing the computations

    for(int i=1;i<=k;i++){
        
        if(i!=n-1)num*=(n+1-i);
        if(i!=0)den*=i;
        if(num%den == 0){
            num /= den;
            den =1;
        }
    }
    //cout<<num<<" "<<den<<endl;
    return (num/den);
}


vector<vector<int> >dp;
/*
* this one uses addition, which avoids rounding to zero.
So this is a better approach.
Explanation: https://stackoverflow.com/questions/12970897/can-you-explain-this-recursive-n-choose-k-code-to-me
case 1: We choose element #i
case 2: We don't choose element #i

this two cases are mutually exclusive. So, adding these two will give our result.

case 1: if we choose element #i, then we have to choose k-1 elements from n-1 options.
case 2: as we haven't choosen element #i, we have to choose k elements from n-1 options.
Hence, n choose k  = (n-1 choose k-1) + (n-1 choose k)
*/

long long nChooseKRecursive(int n, int k) {
    long long num=1,den=1;
    if(n<k)return 0LL;
    if(n <= 1)return 1LL;
    if(k == 0)return 1LL;
    if(n-k < k)k = n-k; //reducing the computations
    if(dp[n][k] != -1)return dp[n][k];
    dp[n][k] = (nChooseKRecursive(n-1,k-1)%1000000007 + nChooseKRecursive(n-1,k)%1000000007)%1000000007;
    return dp[n][k];
}


int main(){
    cout<<combination(100,30)<<endl;
    dp.resize(102, vector<int>(102,-1));
    cout<<nChooseKRecursive(100,30)<<endl;
    return 0;
}