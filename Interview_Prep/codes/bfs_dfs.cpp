#include<iostream>
#include<vector>
using namespace std;

/*
 * Problem:
	Suppose you have a 2D grid of 0s and 1s.
	0 1 1 0
	1 0 1 1
	0 0 1 1
	1 0 0 0

	Find the number of clusters of 1s. Two elements are neighbors if they are vertically or horizontally one hop away.
	Hence, here the output will be 3
 */

vector<pair<int,int> >dirs = {make_pair(-1,0), make_pair(1,0), make_pair(0,-1), make_pair(0,1)};


/*
* The complexity of this bfs function is O(V+E). Here, E can be at most 4 for a single vertex.
* Hence the complexity is O(V + 4V).
* Here V = O(m.n)
* Hence, the time complexity is O(m.n);
* Also the space complexity is O(m.n)
*/

void bfs(int x, int y, vector<vector<int> >&grid, int m, int n, vector<vector<bool> >&vis){
	queue<pair<int,int> >q;
	q.push(make_pair(x,y));
	vis[x][y] = true;

	while(!q.empty()){
		pair<int,int>cur = q.top();
		q.pop();

		for(int i=0; i<dirs.size(); i++) {
			int curX = cur.first + dirs[i].first;
			int curY = cur.second + dirs[i].second;
			if(curX >= 0 && curX < m && curY >= 0 && curY < n && !vis[curX][curY] && grid[curX][curY]  == 1){
				q.push(make_pair(curX,curY));
				vis[curX][curY] = true;
			}
		}
	}
}

/*
* The dfs time complexity is also O(V+E). Which will be O(mn), same as bfs.
*/
void dfs(int x, int y, vector<vector<int> >&grid, int m, int n, vector<vector<bool> >&vis){
	vis[x][y] = true;
	for(int i=0;i<dirs.size();i++){
		int curX = x + dirs[i].first;
		int curY = y + dirs[i].second;
		if(curX >= 0 && curX < m && curY >= 0 && curY < n && !vis[curX][curY] && grid[curX][curY] == 1){
			dfs(curX, curY, gird, m, n, vis);
		}
	}
}

/*
* Time complexity O(m.n), as the bfs will never be called with the same element twice.
* if for the first element all the m.n vertices are visited then,
* Time = mn + 1 + 1 + ... + 1 (mn-1 time 1s)
* Time = mn + (mn-1)
* Time = O(mn)
*/
int findClusters(vector<vector<int> >&grid){
	int res = 0;
	vector<vector<bool> > vis(grid.size(), vector<bool>(grid[0].size(), false));
	int m = grid.size();
	int n = grid[0].size();
	for(int i=0;i<grid.size();i++){
		for(int j=0;j<grid[i].size();j++){
			if(!vis[i][j]){
				bfs(i,j,grid,m,n,vis);
				res++;
			}
		}
	}
	return res;
}



/*
BFS vs DFS
when to use what?

If you know a solution is not far from the root of the tree, a breadth 
first search (BFS) might be better. If the tree is very deep and solutions are 
rare, depth first search (DFS) might take an extremely long time, but BFS could 
be faster.

If the tree is very wide, a BFS might need too much memory, so it might be 
completely impractical.

If solutions are frequent but located deep in the tree, BFS could be impractical.

If the search tree is very deep you will need to restrict the search depth for 
depth first search (DFS), anyway (for example with iterative deepening).

BFS:
for
1. finding shortest path
2. finding neighbors (1st level, 2nd level friends in linkedin, FB)
3. Use BFS - when you want to find the shortest path from a certain source node 
   to a certain destination. (Or more generally, the smallest number of steps 
   to reach the end state from a given initial state.)


DFS:
1. Searching for a target
2. Use DFS - when you want to exhaust all possibilities, and check which 
   one is the best/count the number of all possible ways.
3. Cycle detection


Iterative Deepening DFS:
-------------------------

In a depth-first search, you begin at some node in the graph and continuously
explore deeper and deeper into the graph while you can find new nodes that you 
haven't yet reached (or until you find the solution). Any time the DFS runs out
of moves, it backtracks to the latest point where it could make a different 
choice, then explores out from there. This can be a serious problem if your 
graph is extremely large and there's only one solution, since you might end 
up exploring the entire graph along one DFS path only to find the solution 
after looking at each node. Worse, if the graph is infinite (perhaps your graph
consists of all the numbers, for example), the search might not terminate. 
Moreover, once you find the node you're looking for, you might not have the
optimal path to it (you could have looped all over the graph looking for the 
solution even though it was right next to the start node!)

One potential fix to this problem would be to limit the depth of any one path 
taken by the DFS. For example, we might do a DFS search, but stop the search 
if we ever take a path of length greater than 5. This ensures that we never 
explore any node that's of distance greater than five from the start node, 
meaning that we never explore out infinitely or (unless the graph is extremely 
dense) we don't search the entire graph. However, this does mean that we might 
not find the node we're looking for, since we don't necessarily explore the 
entire graph.

The idea behind iterative deepening is to use this second approach but to keep 
increasing the depth at each level. In other words, we might try exploring using 
all paths of length one, then all paths of length two, then length three, etc. 
until we end up finding the node in question. This means that we never end up 
exploring along infinite dead-end paths, since the length of each path is capped 
by some length at each step. It also means that we find the shortest possible 
path to the destination node, since if we didn't find the node at depth d but 
did find it at depth d + 1, there can't be a path of length d (or we would have 
taken it), so the path of length d + 1 is indeed optimal.

The reason that this is different from a DFS is that it never runs into the case 
where it takes an extremely long and circuitous path around the graph without 
ever terminating. The lengths of the paths are always capped, so we never end 
up exploring unnecessary branches.

The reason that this is different from BFS is that in a BFS, you have to hold 
all of the fringe nodes in memory at once. This takes memory O(bd), where b is 
the branching factor. Compare this to the O(d) memory usage from iterative 
deepening (to hold the state for each of the d nodes in the current path). 
Of course, BFS never explores the same path multiple times, while iterative 
deepening may explore any path several times as it increases the depth limit. 
However, asymptotically the two have the same runtime. BFS terminates in O(bd) 
steps after considering all O(bd) nodes at distance d. Iterative deepening uses 
O(bd) time per level, which sums up to O(bd) overall, but with a higher constant 
factor.

In short:

DFS is not guaranteed to find an optimal path; iterative deepening is.
DFS may explore the entire graph before finding the target node; iterative 
deepening only does this if the distance between the start and end node is 
the maximum in the graph. BFS and iterative deepening both run in O(bd), but 
iterative deepening has a higher constant factor. BFS uses O(bd) memory, while 
iterative deepening uses only O(d).


Source: https://stackoverflow.com/questions/7395992/iterative-deepening-vs-depth-first-search


*/

