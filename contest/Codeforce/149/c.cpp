    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define ull unsigned long long
    #define vi vector<int>
    #define vll vector<ll>
    #define vvi vector<vector<int> >
    #define pii pair<int,int>
    #define vpii vector<pair<int,int> >
    #define vs vector<string>
    #define pb push_back
    #define mp make_pair
    #define PI acos(-1)
    #define all(a) a.begin(),a.end()
    #define mem(a,v) memset(a,v,sizeof(a))
    #define eps 1e-9
    #define repi(i,a,n) for(int i=(a);i<(n);i++)
    #define rep(i,n) for(int i=0;i<n;i++)
    #define len(a) ((int)a.size())
    #define repr(i,a,n) for(int i=(n);i>=(a);i--)
    template<class T> inline T gcd(T a,T b) {if(a<0)return gcd(-a,b);if(b<0)return gcd(a,-b);return (b==0)?a:gcd(b,a%b);}

    map<pii, bool >vv;
    map<pii,int >dist;
    map<pii,bool >vt;

    int ex,ey,sx,sy;
    int bfs(){
            queue<pii > qq;
            qq.push(mp(sx,sy));
            dist[mp(sx,sy)]=0;
            dist[mp(ex,ey)]=-1;
            while(!qq.empty()){
                    pii x=qq.front();
                    //~ cout<<x.first<<" "<<x.second<<" "<<dist[x]<<endl;
                    qq.pop();
                    if(x==mp(ex,ey)) break;
                    int tt=dist[x];
                    int a=x.first,b=x.second-1;
                    if(vv.count(mp(a,b)) && !vt[mp(a,b)]){
                            qq.push(mp(a,b));
                            vt[mp(a,b)]=true;
                            dist[mp(a,b)]=tt+1;
                    }
                    b=x.second+1;
                    if(vv.count(mp(a,b)) && !vt[mp(a,b)]){
                            qq.push(mp(a,b));
                            vt[mp(a,b)]=true;
                            dist[mp(a,b)]=tt+1;
                    }
                    a=x.first-1;
                    b=x.second;
                    if(vv.count(mp(a,b)) && !vt[mp(a,b)]){
                            qq.push(mp(a,b));
                            vt[mp(a,b)]=true;
                            dist[mp(a,b)]=tt+1;
                    }
                    a=x.first+1;
                    b=x.second;
                    if(vv.count(mp(a,b)) && !vt[mp(a,b)]){
                            qq.push(mp(a,b));
                            vt[mp(a,b)]=true;
                            dist[mp(a,b)]=tt+1;
                    }
                    a=x.first-1;
                    b=x.second-1;
                    if(vv.count(mp(a,b)) && !vt[mp(a,b)]){
                            qq.push(mp(a,b));
                            vt[mp(a,b)]=true;
                            dist[mp(a,b)]=tt+1;
                    }
                    a=x.first-1;
                    b=x.second+1;
                    if(vv.count(mp(a,b)) && !vt[mp(a,b)]){
                            qq.push(mp(a,b));
                            vt[mp(a,b)]=true;
                            dist[mp(a,b)]=tt+1;
                    }
                    a=x.first+1;
                    b=x.second-1;
                    if(vv.count(mp(a,b)) && !vt[mp(a,b)]){
                            qq.push(mp(a,b));
                            vt[mp(a,b)]=true;
                            dist[mp(a,b)]=tt+1;
                    }
                    a=x.first+1;
                    b=x.second+1;
                    if(vv.count(mp(a,b)) && !vt[mp(a,b)]){
                            qq.push(mp(a,b));
                            vt[mp(a,b)]=true;
                            dist[mp(a,b)]=tt+1;
                    }
            }
            return dist[mp(ex,ey)];
    }


    int main(void){

            cin>>sx>>sy>>ex>>ey;
            int n;
            cin>>n;
            int r,a,b;
            rep(i,n){
                    cin>>r>>a>>b;
                    repi(j,a,b+1)vv[mp(r,j)]=true;
            }
            cout<<bfs();
            return 0;
    }
