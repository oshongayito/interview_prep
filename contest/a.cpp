#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    long long t,n;
    cin>>t;
    while(t--)
    {
        cin>>n;
        if(n==1)cout<<0<<endl;
        else if(n%2==0)cout<<(n/2)*(n/2)<<endl;
        else cout<<(n/2)*((n/2)+1)<<endl;
    }
    return 0;
}

