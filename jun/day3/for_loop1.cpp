/* this is the code to print the array of age
 using for loop */

#include<stdio.h>
int main()
{
    int a[6];
    int i;
    a[0]=15;
    a[1]=16;
    a[2]=15;
    a[3]=16;
    a[4]=16;
    a[5]=16;

    // we will print the array numbers using for loop

    for(i=0;i<6;i++)
    {
        printf("%d ",a[i]);
    }
    return 0;
}
