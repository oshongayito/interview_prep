    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include<locale>
    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


string touppers(string s)
{
    locale loc;
    int l;
    l=s.size();
    for(int i=0;i<l;i++)
    {
        if(islower(s[i],loc))s[i]=toupper(s[i],loc);
    }
    return s;
}


string tolowers(string s)
{
    locale loc;
    int l;
    l=s.size();
    for(int i=0;i<l;i++)
    {
        if(isupper(s[i],loc))s[i]=tolower(s[i],loc);
    }
    return s;
}

int main()
{
    string s;
    locale loc;
    int l,x,y;
    cin>>s;
    l=s.size();
    for(int i=0;i<l;i++)
    {
        if(isupper(s[i],loc))y++;
        else x++;
    }
    if(y>x)
    {
        cout<<touppers(s);
    }
    else cout<<tolowers(s);

    return 0;
}
