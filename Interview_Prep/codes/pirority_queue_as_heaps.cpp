#include <iostream>
#include<queue>
using namespace std;

// The top of the priority queue is in the end. That's how the comparator are designed

bool compInt(int a, int b){
    return a>b;
}

bool compPair(pair<int,int> x, pair<int,int>y){
    return x.second < y.second;
}
bool compPairSum(pair<int,int>x, pair<int,int>y){
    return (x.first + x.second) < (y.first + y.second);
}

struct compPair2{
       bool operator()(const pair<int,int> x, const pair<int,int>y){
           return x.second < y.second;
       }
   };


int main() {
    std::cout << "Hello, World!" << std::endl;
    //max heap with int key
    std::priority_queue<int>maxHeap;
    maxHeap.push(1);
    maxHeap.push(2);
    maxHeap.push(1000);
    maxHeap.push(3);
    cout<<maxHeap.top()<<endl;

    //min heap with int key
    std::priority_queue<int, vector<int>, decltype(&compInt)>minHeap(compInt);
    minHeap.push(1);
    minHeap.push(2);
    minHeap.push(1000);
    minHeap.push(3);
    cout<<minHeap.top()<<endl;

    //max heap with pair (using first element as key)
    priority_queue<pair<int,int>>maxHeapPair;
    maxHeapPair.push(make_pair(1,2));
    maxHeapPair.push(make_pair(34,6));
    maxHeapPair.push(make_pair(4,1));
    cout<<maxHeapPair.top().first<<" "<<maxHeapPair.top().second<<endl;


    //max heap with pair (using second element as key)
    // priority_queue<pair<int,int>, vector<pair<int,int>>, decltype(&compPair) > maxHeapPairSecond(compPair); 
    priority_queue<pair<int,int>, vector<pair<int,int>>, decltype(compPair2) > maxHeapPairSecond;
    maxHeapPairSecond.push(make_pair(1,2));
    maxHeapPairSecond.push(make_pair(3,6));
    maxHeapPairSecond.push(make_pair(4,1));
    cout<<maxHeapPairSecond.top().first<<" "<<maxHeapPairSecond.top().second<<endl;


    //max heap with pair (using sum of elements as key)
    priority_queue<pair<int,int>, vector<pair<int,int>>, decltype(&compPairSum) > maxHeapPairSum(compPairSum);
    maxHeapPairSum.push(make_pair(1,2));
    maxHeapPairSum.push(make_pair(3,6));
    maxHeapPairSum.push(make_pair(4,10));
    cout<<maxHeapPairSum.top().first<<" "<<maxHeapPairSum.top().second<<endl;


    return 0;
}