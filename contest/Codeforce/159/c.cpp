    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define pi acos(-1)

    #define vc vector
    #define ll long long

    int main()
    {
        ll n,x,y,z,i,j,k;
        vector< pair<ll,ll> >cord;

        //complex<double> p(0,0);
        //complex<double> q(0,0);
        cin>>n;
        //cord.resize(n);
        for(i=0;i<n;i++){cin>>x>>y;cord.push_back(make_pair(x,y));}

        sort(cord.begin(),cord.end());

        complex<double> p(cord[0].first,cord[0].second);
        complex<double> q(cord[n-1].first,cord[n-1].second);
        cout<<(180/pi)*(q-p)<<endl;

        return 0;
    }


