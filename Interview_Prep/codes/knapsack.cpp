*#include<iostream>
using namespace std;
#include<vector>

//here we can use one item only once
//try to visualize the 2D array, it will help to understand how the algo works
int knapsack(vector<int>wts, vector<int>vals, int capacity){
    int n = wts.size();
    vector<vector<int> >dp;
    dp.resize(n+1);
    for(int i=0;i<=n;i++){
        dp[i].resize(capacity+1,0);
    }

    for(int i=1;i<=n;i++){
        for(int j=1;j<=capacity;j++){
            if(wts[i-1] > j){ //if weight of ith value is greater than current capacity
                dp[i][j] = dp[i-1][j];
            }
            else{
                /* otherwise, you can take the ith item or leave it. calculate the maximum between these two
                 * */
                dp[i][j] = max(vals[i-1]+dp[i-1][j-wts[i-1]], dp[i-1][j]);
            }
        }
    }
    return dp[n][capacity];
}


//here we can use any item repeatedly
//try to visualize the 1D array, it will help to understand how the algo works
int unbounded_knapsack(vector<int>wts, vector<int>vals, int capacity){
    vector<int>dp;
    /* we are taking an 1D dp array.
     * because, we will keep only the maximum possible value for all capacities using all the items.
     * Thats why we don't need the other dimension for number of items.
    * */
    dp.resize(capacity+1,0);

    int n = wts.size();

    for(int i=1;i<=capacity;i++){
        for(int j=0;j<n;j++){
            /*calculating the maximum possible value for capacity i using all the items
             * */
            if(wts[j] > i){
                dp[i] = dp[i]; // keep the previous value
            }
            else{
                /*otherwise take the current item. take the max between curr value
                 * and the value if you take the current item
                 * */
                dp[i] = max( dp[i], vals[j] + dp[i-wts[j]] );
            }
        }
    }
    return dp[capacity];
}




int main(){
    vector<int> wts ={1,3,4,5};
    vector<int> vals ={1,4,5,7};
    int capacity = 13;
//    wts.push_back(1);
//    wts.push_back(3);
//    wts.push_back(4);
//    wts.push_back(5);

//    vals.push_back(1);
//    vals.push_back(4);
//    vals.push_back(5);
//    vals.push_back(7);

    cout<<"Maximum possible value is (0/1 knapsack): "<<knapsack(wts, vals, capacity)<<endl;
    cout<<"Maximum possible value is (0/1 knapsack unbounded): "<<unbounded_knapsack(wts, vals, capacity)<<endl;
    return 0;
}