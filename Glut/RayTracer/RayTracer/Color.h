
class Color{

    public:
    double r,g,b,o;
    Color(){};
    Color(double a,double c,double d,double e)
    {
        r=a;
        g=c;
        b=d;
        o=e;
    };

    void set(double a,double c,double d,double e)
    {
        r=a;
        g=c;
        b=d;
        o=e;
    }
    void print()
    {
        cout<<"printing color: "<<r<<" "<<g<<" "<<b<<" "<<o<<endl;
    }

    void setColorRed(double y)
    {
        r=y;
    }
        void setColorGreen(double y)
    {
        g=y;
    }
        void setColorBlue(double y)
    {
        b=y;
    }
        void setColorSpecial(double y)
    {
        o=y;
    }

    double getColorRed()
    {
        return r;
    }

        double getColorGreen()
    {
        return g;
    }
        double getColorBlue()
    {
        return b;
    }
    double getColorSpecial()
    {
        return o;
    }

	double brightness() {
		return(r + g+ b)/3;
	}

	Color colorScalar(double scalar) {
		return Color (r*scalar, g*scalar, b*scalar, o);
	}

	Color colorAdd(Color color) {
		return Color (r + color.getColorRed(), g + color.getColorGreen(), b + color.getColorBlue(), o);
	}

	Color colorMultiply(Color color) {
		return Color (r*color.getColorRed(), g*color.getColorGreen(), b*color.getColorBlue(), o);
	}

	Color colorAverage(Color color) {
		return Color ((r + color.getColorRed())/2, (g + color.getColorGreen())/2, (b + color.getColorBlue())/2, o);
	}


    Color clip() {
		double alllight = r+ g + b;
		double excesslight = alllight - 3;
		if (excesslight > 0) {
			r = r + excesslight*(r/alllight);
			g = g + excesslight*(g/alllight);
			b = b + excesslight*(b/alllight);
		}
		if (r > 1) {r = 1;}
		if (g > 1) {g = 1;}
		if (b > 1) {b = 1;}
		if (r < 0) {r = 0;}
		if (g < 0) {g = 0;}
		if (b < 0) {b = 0;}

		return Color (r, g, b, o);
	};


};
