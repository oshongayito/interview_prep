#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cmath>
#include<string>
#include<sstream>

#define ll long long
using namespace std;

string to_str(ll n)
{
    stringstream sin;
    sin<<n;
    return sin.str();
}

ll to_int(string str)
{
    ll y;
    int i;
    for(i=0;i<str.size();i++)
    {
        if(i==0)y=str[i]-'0';
        else
        {
            y*=10;
            y+=str[i]-'0';
        }
    }

    return y;
}



int main()
{
    int n,i,j,l,t;
    ll x,y,z;
    bool flag=true;

    string str,s;

    cin>>t;
    for(j=1;j<=t;j++){

    cin>>str>>s;
    if(s[0]=='-')s.erase(0,1);
    l=s.size();

    y = to_int(s);



    for(i=0;str.size()>0;i++)
    {
        if(str[0]=='0' || str[0]=='-')
        {
            str.erase(0,1);
            continue;
        }



        if(str.size()<l)
        {
            flag=false;
            break;
        }



        z = to_int(str.substr(0,l));

        if((z/y)>0)
        {
            str.replace(0,l,to_str(z%y));

        }
        else if(str.size()>=(l+1))
        {
            z=to_int(str.substr(0,l+1));
            str.replace(0,l+1,to_str(z%y));
        }
        else
        {
            flag=false;
            break;
        }

        //cout<<z<<" "<<str<<endl;


    }
    if(flag)cout<<"Case "<<j<<": "<<"divisible"<<endl;
    else cout<<"Case "<<j<<": "<<"not divisible"<<endl;

    flag=true;

    }

    return 0;
}
