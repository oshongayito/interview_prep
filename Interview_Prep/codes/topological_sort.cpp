// https://www.geeksforgeeks.org/topological-sorting/

#include<iostream>
#include<vector>
using namespace std;

/*
 * Topological sorting for Directed Acyclic Graph (DAG) is a linear ordering of 
 * vertices such that for every directed edge uv, vertex u comes before v in the 
 * ordering. Topological Sorting for a graph is not possible if the graph is not
 * a DAG.
 */


/*
 * In DFS, we start from a vertex, we first print it and then recursively call DFS
 * for its adjacent vertices. In topological sorting, we use a temporary stack. 
 * We don’t print the vertex immediately, we first recursively call topological 
 * sorting for all its adjacent vertices, then push it to a stack. Finally, print
 * contents of stack. Note that a vertex is pushed to stack only when all of its 
 * adjacent vertices (and their adjacent vertices and so on) are already in stack.
 */

// A modified version of the DFS. Here we have an additional stack.
// The time complexity will be same as DFS (O(V+E))
void topologicalSort(int x, vector<vector<int>>&grid, vector<bool>&vis, stack<int>&myStack){
	vis[x] = true;
	for(int i=0;i<grid[x].size();i++){
		if(grid[x][i] == 1 && !vis[i]){
			topologicalSort(i, grid, vis, myStack);
		}
	}
	myStack.push(x);
}

/*
Here, the rows of the grid represents each vertex and their edges.  
This is a binary matrix, where a 1 represents a edge between the corresponding two elements.
*/
void printOrderedElems(vector<vector<int> > grid){
	vector<bool>vis(grid.size(),false);
	stack<int>myStack;
	for(int i=0;i<grid.size();i++){
		if(!vis[j]){
			topologicalSort(j, grid, vis, myStack);
		}
	}
	//at this point the stack will have the desired ordering. let's print it
	while(!myStack.empty()){
		cout<<myStack.top();
		myStack.pop();
	}

}