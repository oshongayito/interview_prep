#include<iostream>
#include<cstdio>
#include<cmath>
#include<vector>
#include<algorithm>
#include<cstring>

#define ll long long

using namespace std;

ll dyp[101][101][101];
ll knldge[101];
ll dec_fac[101];
ll comb[101][101];
int cnt[101];

ll nway[101];

ll T,n,f=-1,v;
ll int dp(int c,int i,int j){


    if(c==0){

        return 1;
        }
    if(i<0){return 0;}
    if(dyp[c][i][j]!=-1)
    {
        return dyp[c][i][j];
    }
    if(knldge[i]-(j*dec_fac[i])<=c && knldge[i]-(j*dec_fac[i])>0)
        return dyp[c][i][j]= dp(c-knldge[i]+(j*dec_fac[i]),i,j+1)  +  dp(c,i-1,0);

    else return dyp[c][i][j] = dp(c,i-1,0);
}

void dp_print(int c,int i,int j){


    if(c==0){
        for(int l=0;l<101;l++)
        {

            if(cnt[l]>0)
            {
                if(f==1)cout<<" + ";
                cout<<cnt[l]<<"("<<knldge[l]<<")";
                f=1;
            }
        }
        cout<<endl;
    dyp[c][i][j]=1;
    f=0;
        return ;
        }
    if(i<0)
    {
        dyp[c][i][j]=0;
        return ;
    }
    if(dyp[c][i][j]!=-1)
    {
        return;
    }
    if(knldge[i]-(j*dec_fac[i])<=c && knldge[i]-(j*dec_fac[i])>0)
    {

    cnt[i]++;
    dp_print(c-knldge[i]+(j*dec_fac[i]),i,j+1);
    cnt[i]--;
    dp_print(c,i-1,0);
    }
    else dp_print(c,i-1,0);


}




ll int itrtve_coin_change(){
    int c;
    nway[0]=1;
    for(int i=0;i<n;i++){
        c=knldge[i];
        for(int j=c;j<=T && c>0;j++)
        {
            nway[j]+=nway[j-c];
            c-=dec_fac[i];
        }
    }
    return nway[T];
}


int main(){

    ll tests,m,x,y,z,i,j,k,p;
     freopen("cg_input.txt","r",stdin);

    cin>>tests;
    for(p=1;p<=tests;p++){
    cin>>n>>T;



    for(i=0;i<101;i++)for(j=0;j<101;j++)memset(dyp[i][j],-1,100);

    for(i=0;i<n;i++)
    {
        cin>>knldge[i];

    }
    for(i=0;i<n;i++)
    {
        cin>>dec_fac[i];

    }
    if(p>1)cout<<"---------------"<<endl;

    cout<<endl<<dp(T,n-1,0)<<endl;

    for(i=0;i<101;i++)for(j=0;j<101;j++)memset(dyp[i][j],-1,100);


    cout<<"Valid Combinations"<<endl;
    dp_print(T,n-1,0);
    memset(cnt,0,101);

    }

    return 0;
}
