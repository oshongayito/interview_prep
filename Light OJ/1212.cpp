#include<iostream>
#include<vector>
#include<cstdio>
#include<cmath>

#define pb push_back
#define ppb pop_back

using namespace std;

int main()
{
    //vector<int > deq;
    int deq[100];
    string cmd;
    int a,b,t,c,d,i,j,k,l,n,m,pop,q;
    cin>>t;
    for(c=1;c<=t;c++){
    cin>>n>>m;
    cout<<"Case "<<c<<":"<<endl;


    for(i=0,l=0;i<m;i++)
    {
        cin>>cmd;
        if(cmd=="pushLeft")
        {
            cin>>d;
            if(l<n)
            {
                for(j=l;j>0;j--)
                {
                    deq[j]=deq[j-1];
                }
                deq[0]=d;
                l++;
                cout<<"Pushed in left: "<<d<<endl;
            }
            else cout<<"The queue is full"<<endl;

        }
        if(cmd== "pushRight")
        {
            cin>>d;
            if(l<n)
            {
                deq[l]=d;
                l++;
                cout<<"Pushed in right: "<<d<<endl;
            }
            else cout<<"The queue is full"<<endl;
        }
        if(cmd== "popLeft")
        {
            if(l>0)
            {
                q= deq[0];
                for(j=0;j<(l-1);j++)
                {
                    deq[j]=deq[j+1];
                }
                l--;
                cout<<"Popped from left: "<<q<<endl;
            }
            else cout<<"The queue is empty"<<endl;
        }
        if(cmd=="popRight")
        {
            if(l>0)
            {
                q=deq[l-1];
                l--;
                cout<<"Popped from right: "<<q<<endl;
            }
            else cout<<"The queue is empty"<<endl;
        }

    }

    }

    return 0;
}
