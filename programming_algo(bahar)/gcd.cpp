
int gcd(int n,int m)
{
	while(m>0)
	{
		n=n%m;
		n^=m;
		m^=n;
		n^=m;
	}
	return n;
}

int gcd(int n,int m)
{
	if(n%m==0) return m;
	else return gcd(m,n%m);
}
