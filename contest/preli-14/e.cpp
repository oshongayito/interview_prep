    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


int main()
{
    long long int t;
    int d1,m1,d2,m2,count=0;

    int arr[12]={31,28,31,30,31,30,31,31,30,31,30,31};
    //cin>>t;
    scanf("%lld",&t);

    for(int k=0;k<t;k++)
    {
        count=0;
        //cin>>d1>>m1>>d2>>m2;
        scanf("%d %d %d %d",&d1,&m1,&d2,&m2);



        if((d1>0 && d2>0 && m1>0 && m2>0)&&(d1<=12 && d1>0 && m1<=arr[d1-1]) && (d2<=12 && d2>0 && m2<=arr[d2-1]) && (d1<=d2))
        {
            if(d1<d2)count++;
            else if(d1==d2)
            {
                if(m1<m2)count++;
            }


        }

        if((d1>0 && d2>0 && m1>0 && m2>0)&&(m1<=12 && m1>0 && d1<=arr[m1-1]) && (m2<=12 && m2>0 && d2<=arr[m2-1]) && (m1<=m2))
        {
            if(m1<m2)count++;
            else if(m1==m2)
            {
                if(d1<d2)count++;
            }


        }

        if(count==0)
        {
            printf("Case %d: I am sure there is some kinda mistake!\n",k+1);
            //cout<<"Case "<<k+1<<": "<<"I am sure there is some kinda mistake!"<<endl;
        }

        else if(count==2)
        {
            printf("Case %d: Oh no it is ambiguous!\n",k+1);

            //cout<<"Case "<<k+1<<": "<<"Oh no it is ambiguous!"<<endl;
        }

        else if(count==1)
        {
            printf("Case %d: Okay got it!\n",k+1);

            //cout<<"Case "<<k+1<<": "<<"Okay got it!"<<endl;
        }
    }
    return 0;
}
