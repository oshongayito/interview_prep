#include <iostream>
#include <string>
#include <sstream>
using namespace std;
int main()
{
   string token, text("Here:is:some:text");
   istringstream iss(text);
   while ( getline(iss, token, ':') )
   {
      cout << token << endl;
   }
   return 0;
}
/* my output
Here
is
some
text
*/
