#include<iostream>
#include<string>
#include<cstring>
#include<cstdio>
using namespace std;
class symbol_data{
    public:
    string symbol;
    string symbol_type;
    symbol_data *next;
    symbol_data(string symbol,string symbol_type)
   {
       this->symbol=symbol;
       this->symbol_type=symbol_type;
       this->next=NULL;
    }
};
class symbol_info{
    public:
    symbol_data *data[20];
     symbol_info()
    {
        for (int i=0; i <20; i++) {
            data[i]=NULL;
        }
    }
    void insert(string symbol,string symbol_type)
    {
        int hash_val=hash_func(symbol);
        if(data[hash_val]==NULL)
        {
            data[hash_val]=new symbol_data(symbol,symbol_type);


        }
        else if(data[hash_val]!=NULL)
        {
            symbol_data *newdata=data[hash_val];
            while(newdata->next!=NULL)
            {
                newdata=newdata->next;
            }
             newdata->next=new symbol_data(symbol,symbol_type);


        }


    }

    bool lookup(string symbol)
    {
         int hash_val=hash_func(symbol);
         if(data[hash_val]==NULL)
         return false;
         else{
             if(data[hash_val]->symbol==symbol)
             return true;
             else
             {
                 symbol_data *newnode=data[hash_val];
                 while(newnode->next!=NULL)
                 {
                     if(newnode->symbol!=symbol)
                     {
                     newnode=newnode->next;
                     }
                     else if(newnode->symbol==symbol)
                     {
                         return true;
                     }
                 }
                 return false;
             }
         }

    }

    void dump()
    {
         cout << "\nHash table result:\n";

    cout << "\n.....................................\n";
    cout <<"index"<<" "<<"symbol"<<" "<<"symbol_Type"<<endl;
        for (int i=0; i <20; i++) {
            cout<<"\n";
            cout<<i<<"-->";
            while(data[i]!=NULL){
                 cout << data[i]->symbol<<",";
                cout << data[i]->symbol_type;
                cout<<"->";
                data[i]=data[i]->next;
            }
        }

            cout<<endl;
            cout << "-----------------------------------------------------\n";

    }

    int hash_func(string symbol)
    {
        int hash_val=0;
        for(int i=0;i<symbol.size();i++)
        {
            hash_val=hash_val+symbol[i];
        }
        return (hash_val%20);
    }
};
int main()
{

    symbol_info symbol_table;
    cout<<"choose from below"<<endl;
    cout<<"1.insert"<<endl;
    cout<<"2.lookup"<<endl;
    cout<<"3.dump"<<endl;
    cout<<"4 for exit "<<endl;
    char choice=0;

    while(choice!=4)
    {
    cin>>choice;
    choice-='0';
    if(choice==4) return 0;
    else if(choice==1)
    {

        bool b;
        cout<<"Enter symbol and symboltype:"<<endl;
        string s1,s2;
        cin>>s1>>s2;
        s1=s1.substr(0,s1.size());
        //b=symbol_table.lookup(s1);
        /*if(b)
        {
            cout<<"Symbol already exists in the symbol table"<<endl;
        }
        else{*/
            symbol_table.insert(s1,s2);
            cout<<"successfully inserted"<<endl;
        //}
    }

    else if(choice==2)
    {
        string s;
        bool b;
        cout<<"Enter symbol :"<<endl;
        cin>>s;
        b=symbol_table.lookup(s);
        if(b)
        {
            cout<<"Symbol exists in the symbol table"<<endl;
        }
        else{
            cout<<"symbol not found in the table"<<endl;
        }
    }

    else if(choice==3)
    {
        symbol_table.dump();
    }

    else cout<<"invalid choice"<<endl;

    }

    return 0;

}
