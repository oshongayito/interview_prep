#include<iostream>
#include<cstdio>
#include<string>
#include<cmath>
#include<vector>


using namespace std;

int main()
{
    int tests,i,j,k,l,a,x1,y1,x2,y2;
    cin>>tests;
    for(i=1;i<=tests;i++)
    {
        cin>>x1>>y1>>x2>>y2;
        if(((x1+y1)%2==0 && (x2+y2)%2==0)||((x1+y1)%2==1 && (x2+y2)%2==1))
        {
            if(abs((double)(x1-x2))==abs((double)(y1-y2)))cout<<"Case "<<i<<": 1"<<endl;
            else cout<<"Case "<<i<<": 2"<<endl;
        }
        else
        {
            cout<<"Case "<<i<<": impossible"<<endl;
        }
    }
    return 0;
}
