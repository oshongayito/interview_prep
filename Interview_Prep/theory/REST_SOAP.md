### REST ###
1. uses http only
2. Stateless
3. more suitable for scaling

### SOAP ###
1. uses http and other protocols
2. state dependent
3. less suitable for scaling
