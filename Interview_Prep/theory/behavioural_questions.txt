
1. What are your biggest weaknesses?

2. What are your biggest strengths?

3. Talk about a time you were criticized and how you handled it.

4. Give me an example of a time when you did not meet a client’s expectation. What happened, and how did you attempt to rectify the situation? What did you learn from it?

5. Tell the story of last time you had to apologize to someone.

6. Tell me about a time when you had to work on a project with unclear responsibilities.

7. Tell me about a time when you had to leave a task unfinished.

8. Do you collaborate well?

9. Tell me about a time when you invented something.

10. Tell me about a time when you gave a simple solution to a complex problem.

11. Tell about a time when u were wrong.

12. Tell me about a time when you had a change in paradigm, why you changed the paradigm and how did you cope with that?

13. Tell me about a time when you mentored someone.

14. Tell me about a time when you couldn’t meet your own expectations on a project.

15. Tell me about a time when you went way beyond the scope of the project and delivered.

16. Describe a time when you saw some problem and took the initiative to correct it rather than waiting for someone else to do it.

17. Tell me about a time when you had to work with limited time or resources.

18. Tell me about a time when you had to tell someone a harsh truth.

19. Give me two examples of when you did more than what was required in any job experience.

20. Tell me about something that you learnt recently in your role.

21. Give me an example of a time you faced a conflict while working on a team. How did you handle that?

22. What is the most difficult situation you have ever faced in your life? How did you handle it?

23. Give me an example of a time when you were 75% of the way through a project, and you had to pivot strategy–how were you able to make that into a success story?

24. Give me an example of a time when you made a data-driven decision?

25. Tell me about a challenge you’ve recently faced in your role. How did you tackle it, what was the outcome, and what did you learn? 

26. What’s an example of a difficult problem you solved? Be specific about how the problem was diagnosed and your process for approaching it.

27. Tell me about a time when you had to motivate your team to take a particular action.

28. Tell me about a setback that you faced recently.

29. Tell me about your greatest achievements.


