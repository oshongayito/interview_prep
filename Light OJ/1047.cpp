#include<iostream>
#include<cstdio>
#include<cmath>
#include<vector>
#include<algorithm>
#include<cstring>

using namespace std;

#define ll long long

int dp[20][3];

int main()
{
    int arr[20][3];
    int i,j,k,n,t;

    cin>>t;

    for(k=1;k<=t;k++){

    cin>>n;
    for(i=0;i<n;i++)
    {
        for(j=0;j<3;j++)
        {
            cin>>arr[i][j];
            if(i==0)
            dp[i][j]=arr[i][j];
        }

    }


    for(i=1;i<n;i++)
    {
        for(j=0;j<3;j++)
        {
            dp[i][j]=(arr[i][j]+min(dp[i-1][(j+1)%3],dp[i-1][(j+2)%3]));

        }

    }

    cout<<"Case "<<k<<": "<<min(dp[n-1][0],min(dp[n-1][1],dp[n-1][2]))<<endl;

    for(j=0;j<n;j++){
            memset(dp[j],0,3);
            memset(arr[j],0,3);
        }
    }


    return 0;

}
