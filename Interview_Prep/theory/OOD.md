0. Clarify!!! Clarify!!! Clarify!!!
1. Find entities
2. Use cases/ features
3. Classes 
4. Relationships among the classes
5. SOLID principles (https://alanbarber.com/post/solid-principles-five-principles-of-objectoriented-programming-and-design/)
6. Try to use design patterns (creational, structural, behavioral)


Design a online book reading system.

roles: reader, admin, books, library, book categories, authors

Features, usecases: 
1. Show books
2. Search books (by categories, names, authors)
3. Select a book
4. Start reading
5. Go to a particular page
6. Bookmark a page
7. Finish reading
8. upload new book
9. Modify meta data of existing books


reader -> library -> search book -> select book -> start reading -> bookmark -> finish reading

public enum categories
{
	HISTORY,
	SCIENCE FICTION,
	TRAVELLING,
	SELF HELP
}

class Author
{
	public String name;
	public String dob;
	public categories type;
	public List<book> books;
}


Class Book
{
	Public String name;
	Public Author author;
	Public categories type;
	public int count;

	public readBook();
	public finishReading();
	public isAvailable();
}

Class Library
{
	public HashMap<bookid, book>books;

	public book issueBook();
	public void addBook();
	public void restoreBook();
	public List<book> searchBook(Categories type);
	public List<book> searchBook(String name);
	public void startReading(Reader reader, Book book);
}

Class Reader
{
	Public string name;
	public List<book>myBooks;

}

