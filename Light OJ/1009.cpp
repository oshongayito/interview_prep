    #include<iostream>
    #include<vector>
    #include<cmath>
    #include<cstring>
    #include<cstdio>
    #include<queue>
    #include<map>
    #include <set>
    #include <stack>
    #include <sstream>
    #include <list>


    #define black 1
    #define white 0
    #define pb push_back
    #define ms memset
    using namespace std;


    vector<int> graph[20001];
    bool vis[20001];
    int dis[20001];
    int edges;

    bool exists [20000];
    map<int,int> mp;

    int bfs(int s)
    {
        queue <int > Q;
        int i,j,k,l=0,u,max=-1,vamp=1,lyk=0,flag=0;
        int where=white;

        Q.push(s);
        k=0;
        j=1;
        while(!Q.empty())
        {
            u = Q.front();
            Q.pop();


            for(i=0;i<graph[u].size();i++)
            {

                if(vis[(int)graph[u][i]]==white)
                {

                        Q.push((int)graph[u][i]);
                    //dis[i]=dis[u]+1;
                        dis[u]++;
                        vis[(int)graph[u][i]] =   black;
                        flag=1;
                }
            }

            vis[u]=black;


            if(where==black)
            {

                vamp+=dis[u];
                l+=dis[u];
                k++;
                if(k==j)
                {
                    where=white;
                    j=l;k=0;l=0;
                }


            }
            else{
            lyk+=dis[u];
            l+=dis[u];
            //j=dis[u];
            k++;
            if(k==j){
                where=black;
                j=l;
                k=0;
                l=0;
            }
            }

        }

        if(flag==0)vamp--;

        if(vamp>max)max=vamp;
        if(lyk>max)max=lyk;


        return max;

    }



    int main()
    {
        int i,j,x,y,max,tests,num=0;
        scanf("%d",&tests);

        for(j=1;j<=tests;j++)
        {
            num=1;
        scanf("%d",&edges);
        for(i=1;i<=edges;i++)
        {
            scanf("%d%d",&x,&y);
            if(mp[x]==white){mp[x]=num++;}
            if(mp[y]==white){mp[y]=num++;}
            graph[mp[x]].pb(mp[y]);
            graph[mp[y]].pb(mp[x]);

        }


        //max=bfs(1);
        for(max=0,i=1;i<num ;i++)
        {
            if(vis[i]==white)
                {
                max+=bfs(i);
                }
        }
        //cout<<"Case "<<j<<": "<<max<<endl; //Shows distance of any node from the source node
        printf("Case %d: %d\n",j,max);
        memset(vis,0,sizeof(vis));
        memset(dis,0,sizeof(dis));
        for(i=0;i<num;i++)graph[i].clear();
        memset(exists,0,sizeof(exists));
        mp.clear();

        num=0;



        }
        return 0;
    }



