#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#define pi (2*acos(0.0))

using namespace std;

int  main()
{
	int t,i;
	double a,b,c,p,q,r1,r2,h,res;
	cin>>t;
	for(i=1;i<=t;i++)
	{
		cin>>r1>>r2>>h>>p;
		a = r1-r2;
		b = (a/h)*p;
		q = r2+b;
		c = (h/a)*r1;
		c = c-(h-p);
		res = ((pi*q*q*c) - (pi*r2*r2*(c-p)))/3 + 0.0000000001;
		
		printf("Case %d: %.10lf\n",i,res);
	}
	return 0;
}

