    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))


ll count_bit(ll x,ll n)
{
    ll count=0;
    for(int i=0;i<(int)n;i++)
    {
        if(x&((ll)1<<i))count++;
    }
    return count;
}
int main()
{
    ll n,m,k,i,j,l,res=0;
    vc<ll>arr;
    cin>>n>>m>>k;


    arr.resize(m+1);

    for(i=0;i<m+1;i++)
    {
        cin>>arr[i];
    }

    //cout<<count_bit(5,3)<<" ";

    for(i=0;i<m;i++)
    {
        l=arr[m]^arr[i];

        if(count_bit(l,n)<=k)
        {
            res++;
        }
    }

    cout<<res<<endl;
    return 0;
}
