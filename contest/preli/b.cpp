
#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>

#define ll long long
#define pi (2*acos(0.0))
#define pb push_back

using namespace std;
char grid[4][6];
bool dp[4][6];
ll res=1,n;
bool possible=0;

bool king(ll x)
{
    int a,b,c,i;
    a=(x-1)%4;
    b=(x-1)/4;


        if(dp[a][b-1] && b-1>=0)
        {
            return 0;
        }
        if(dp[a][b+1] && b+1<n)
        {
            return 0;
        }
        if(dp[a-1][b] && a-1>=0)
        {
            return 0;
        }
        if(dp[a+1][b] && a+1<4)
        {
            return 0;
        }
        if(dp[a-1][b-1] && a-1>=0 && b-1>=0)
        {
            return 0;
        }
        if(dp[a-1][b+1] && a-1>=0 && b+1<n)
        {
            return 0;
        }
        if(dp[a+1][b-1] && a+1<4 && b-1>=0)
        {
            return 0;
        }
        if(dp[a+1][b+1] && a+1<4 && b+1<n)
        {
            return 0;
        }
        dp[a][b]=1;
        return 1;



}

bool knight(ll x)
{
    ll a,b,c,d,i,j,k;
    a=(x-1)%4;
    b=(x-1)/4;


        if(dp[a-2][b-1] && b-1>=0 && a-2>=0)
        {
            return 0;
        }
        if(dp[a-2][b+1] && b+1<n && a-2>=0)
        {
            return 0;
        }
        if(dp[a-1][b-2] && a-1>=0 && b-2>=0)
        {
            return 0;
        }
        if(dp[a-1][b+2] && a-1>=0 && b+2<n)
        {
            return 0;
        }
        if(dp[a+1][b-2] && a+1<4 && b-2>=0)
        {
            return 0;
        }
        if(dp[a+1][b+2] && a+1<4 && b+2<n)
        {
            return 0;
        }
        if(dp[a+2][b-1] && a+2<4 && b-1>=0)
        {
            return 0;
        }
        if(dp[a+2][b+1] && a+2<4 && b+1<n)
        {
            return 0;
        }
        dp[a][b]=1;
        return 1;


}

bool rook(ll x)
{
    ll a,b,c,d,i,j;
    a=(x-1)%4;
    b=(x-1)/4;
    for(i=0;i<n;i++)
    {
        if(dp[a][i])return 0;
    }
    for(i=0;i<4;i++)
    {
        if(dp[i][b])return 0;
    }
    dp[a][b]=1;
    return 1;
}


void dyp(ll x,int y)
{
    cout<<"$"<<endl;
    if(x>(4*n))
    {
        cout<<"  $$"<<endl;
        res = (res+1)%3851919;
        return;
    }
    if(grid[(x-1)%4][(x-1)/4]=='x' || dp[(x-1)%4][(x-1)/4]){cout<<" &";dyp(x+1,y);cout<<"     &&"<<endl;}
    else
    {
        if(y==0)
        {
            possible = king(x);
            dyp(x+1,0);
            dp[(x-1)%4][(x-1)/4]=0;

            dyp(x+1,1);
            dp[(x-1)%4][x/4]=0;

            dyp(x+1,2);
            dp[(x-1)%4][x/4]=0;
            cout<<"1retd"<<endl;
        }
        else if(y==1)
        {
            possible = knight(x);
            dyp(x+1,1);
            dp[(x-1)%4][(x-1)/4]=0;

            dyp(x+1,0);
            dp[(x-1)%4][(x-1)/4]=0;

            dyp(x+1,2);
            dp[(x-1)%4][(x-1)/4]=0;
        }
        else if(y==2)
        {
            possible = rook(x);
            dyp(x+1,2);
            dp[(x-1)%4][(x-1)/4]=0;

            dyp(x+1,0);
            dp[(x-1)%4][(x-1)/4]=0;

            dyp(x+1,1);
            dp[(x-1)%4][(x-1)/4]=0;
        }




    }


}

int main()
{
    ll t,x1,y1,x2,y2,i,j,k,l,a,b,c,d;
    cin>>n;
    for(i=0;i<4;i++)
    {
        for(j=0;j<n;j++)
        {
            cin>>grid[i][j];
        }
    }
    dyp(1,0);
    cout<<res<<endl;
    return 0;
}

