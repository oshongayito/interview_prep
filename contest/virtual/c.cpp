#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#include<cstring>
#define ll long long
#define pi (2*acos(0.0))
#define pb push_back

using namespace std;

int main()
{
    char num[1000006];
    cin>>num;
    ll l=strlen(num),i,j,k;
    ll sum=0;
    ll mn=2;
    for(i=0;i<l;i++)
    {
        if(num[i]>='0' && num[i]<='9')
        {
            sum+=num[i]-'0';
            if(num[i]-'0'>mn-1) mn=num[i]-'0'+1;
        }
        else
        {
            sum+=num[i]-55;
            if(num[i]-55>mn-1) mn=num[i]-55+1;
        }
    }
    ll ans=0;
    for(i=mn;i<=36;i++)
    {
        if(sum%(i-1)==0)
        {
            ans=i;
            break;
        }
    }
    if(ans) cout<<ans<<endl;
    else cout<<"No solution."<<endl;
    return 0;
}
