    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)




class symbolInfo{
    public:
    string symbol;
    string symbol_type;
    symbolInfo *next;
    symbolInfo(string symbol,string symbol_type)
   {
       this->symbol=symbol;
       this->symbol_type=symbol_type;
       this->next=NULL;
    }
};


class symbolTable{
	
	


public:
symbolInfo *table[20];
/*
void symbolTable(){
	table_init();
}*/

void table_init()
{
    for (int i=0; i <20; i++) {
            table[i]=NULL;
        }
}

void insert(string symbol,string symbol_type)
{
    int val;
    val = hash_func(symbol);
    if(table[val]==NULL)
    {
        table[val]=new symbolInfo(symbol,symbol_type);
    }
    else{
        symbolInfo *tmp;
        tmp=table[val];
        while(tmp->next!=NULL)
        {
            tmp=tmp->next;
        }
        tmp->next=new symbolInfo(symbol,symbol_type);
    }
}

int hash_func(string symbol)
    {
        int hash_val=0;
        for(int i=0;i<symbol.size();i++)
        {
            hash_val=(hash_val+symbol[i]*i)%47;
        }
        return (hash_val%20);
    }

bool lookup(string s)
{
    symbolInfo *temp;
    int hash_val=0;
    hash_val = hash_func(s);
    temp=table[hash_val];
    do
        {
            if(temp==NULL)
            {
                return 0;
            }
            else if(temp->symbol!=s){
            temp=temp->next;
            }
            else if(temp->symbol==s)
            {
                return 1;
            }
        }
        while(temp!=NULL);
    return 0;
}


void view()
{
    bool flag=0;
    symbolInfo *temp;
    for(int i=0;i<20;i++)
    {

        temp=table[i];

        while(temp!=NULL)
        {

            if(temp!=NULL){
                if(!flag)
                    cout<<i<<": ";
            flag=1;
            cout<<temp->symbol<<" "<<temp->symbol_type<<" -> ";
            temp=temp->next;
            }
        }
        if(flag){cout<<endl;flag=0;}


        //cout<<endl<<endl;


    }
    cout<<endl;
}



};







int main()
{
    symbolTable oprtn;

    string s1,s2,s3,choice;

    oprtn.table_init();

    while(1){//while starts

        cout<<"1 for insert"<<endl;
        cout<<"2 for lookup"<<endl;
        cout<<"3 for dump"<<endl;
        cout<<"Enter your Choice: ";


    cin>>choice;

    if(choice=="1")
    {
        cin>>s1>>s2;
        oprtn.insert(s1,s2);
    }
    else if(choice=="2")
    {
        cout<<"enter the symbol for lookup in the table: ";
        cin>>s3;
        if(oprtn.lookup(s3))
        {
            cout<<"Symbol already Exists in the Symbol Table."<<endl<<endl;
        }
        else cout<<"Symbol Doesn't Exist in the Symbol Table!!!"<<endl<<endl;
    }
    else if(choice=="3")
    {
        oprtn.view();
    }
    else cout<<"invalid choice"<<endl<<endl;


    }//while ends
}
