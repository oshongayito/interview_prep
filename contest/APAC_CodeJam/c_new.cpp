// A C++ program to print topological sorting of a DAG
    #include <list>
    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>
    #include <fstream>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 1000000007LL
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))


// Class to represent a graph
class Graph
{
    int V;    // No. of vertices'

    // Pointer to an array containing adjacency listsList
    list<int> *adj;

    // A function used by topologicalSort
    void topologicalSortUtil(int v, bool visited[], stack<int> &Stack);
public:
    Graph(int V);   // Constructor

     // function to add an edge to graph
    void addEdge(int v, int w);

    // prints a Topological Sort of the complete graph
    void topologicalSort();
};

Graph::Graph(int V)
{
    this->V = V;
    adj = new list<int>[V];
}

void Graph::addEdge(int v, int w)
{
    adj[v].push_back(w); // Add w to v�s list.
}

// A recursive function used by topologicalSort
void Graph::topologicalSortUtil(int v, bool visited[], stack<int> &Stack)
{
    // Mark the current node as visited.
    visited[v] = true;

    // Recur for all the vertices adjacent to this vertex
    list<int>::iterator i;
    for (i = adj[v].begin(); i != adj[v].end(); ++i)
        if (!visited[*i])
            topologicalSortUtil(*i, visited, Stack);

    // Push current vertex to stack which stores result
    Stack.push(v);
}

// The function to do Topological Sort. It uses recursive topologicalSortUtil()
void Graph::topologicalSort()
{
    stack<int> Stack;

    // Mark all the vertices as not visited
    bool *visited = new bool[V];
    for (int i = 0; i < V; i++)
        visited[i] = false;

    // Call the recursive helper function to store Topological Sort
    // starting from all vertices one by one
    for (int i = 0; i < V; i++)
      if (visited[i] == false)
        topologicalSortUtil(i, visited, Stack);

    // Print contents of stack
    while (Stack.empty() == false)
    {

        cout << Stack.top() << " ";
        Stack.pop();
    }
}

// Driver program to test above functions
int main()
{
    // Create a graph given in the above diagram
    //freopen ("C-small-attempt0.in","r",stdin);
    //ofstream myfile;
  //myfile.open ("output.txt");

ll t,n,count;
    map<string,ll>nodes;
    map<ll,string>i_nodes;

    string start,end;

    queue<ll>q;

    vc<string>res;
    vc<int>resi;
    vc<ll>in_deg;

    bool adj[1001][1001];

cin>>t;
    for(int k=1;k<=t;k++){
        cin>>n;
        //in_deg.resize(n*2+1);
        Graph g((2*n)+1);
        //adj.resize(n*2);
       // for(int j=0;j<n*2;j++)adj[j].resize(n*2);

        count=1;
        for(int i=0;i<n;i++)
        {


        cin>>start>>end;


        if(!nodes[start])
        {
            nodes[start]=count;
            i_nodes[count]=start;
            count++;
        }

        if(!nodes[end])
        {
            nodes[end]=count;
            i_nodes[count]=end;
            count++;
        }
        //adj[nodes[start]][nodes[end]]=1;
        g.addEdge(nodes[start], nodes[end]);

       // adj[mp(nodes[start],nodes[end])]=1;
        //in_deg[nodes[end]]++;

        }

        cout << "Following is a Topological Sort of the given graph \n";
        //cout<<count<<endl;
        g.topologicalSort();
    res.clear();
    resi.clear();
    nodes.clear();
    i_nodes.clear();

    }




    return 0;
}
