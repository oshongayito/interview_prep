#include<iostream>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <map>
#include <set>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
#include <string>
#include <sstream>
#include <list>
#define ms(a,b) memset(a, b, sizeof(a))
#define pb(a) push_back(a)
#define pi (2*acos(0))
#define oo 1<<20
#define dd double
#define ll long long
#define ff float
#define EPS 10E-10
#define fr first
#define sc second
#define MAXX 100
#define SZ(a) (int)a.size()
#define all(a) a.begin(),a.end()
#define intlim 2147483648
#define rtintlim 46340
#define llim 9223372036854775808
#define rtllim 3037000499
#define ull unsigned long long

using namespace std;

ll color[200005], A, B;
map<ll,ll>mp;
vector<ll>v[200005];

void bfs(ll x)
{
    ll now;
    color[x]=1;
    A++;
    queue<ll>Q;
    Q.push(x);
    while(!Q.empty())
    {
        now=Q.front();Q.pop();
        if(color[now]==1)
        {
            for(ll p=0; p<(ll)v[now].size();p++)
            {
                if(color[v[now][p]]==0)
                {
                    color[v[now][p]] = 2;
                    B++;
                }
            }
        }

        else if(color[now]==2)
        {
            for(ll p=0; p<(ll)v[now].size();p++)
            {
                if(color[v[now][p]]==0)
                {
                    color[v[now][p]] = 1;
                    A++;
                }
            }
        }
    }
    return ;
}

int main()
{
    ll cas , i, t, a, b, n, j, cnt1, cnt2,cnt=1, sol=0, z=1;
    scanf("%lld", &t);
    while(t--)
    {
        cnt=1;
        scanf("%lld", &n);
        for(i=0;i<=(2*n);i++)
            v[i].clear();
        mp.clear();
        for(i=0;i<n;i++)
        {
            scanf("%lld%lld", &a, &b);
            if(mp[a]==0) mp[a]=cnt++;
            if(mp[b]==0) mp[b]=cnt++;
            v[mp[a]].pb(mp[b]);v[mp[b]].pb(mp[a]);
        }
        ms(color, 0);sol=0;
        for(i=1;i<cnt;i++)
        {
            if(color[i]==0)
            {
                A=0, B=0;
                bfs(i);
                sol += max(A, B);
            }
        }
        //cout<<sol-1<<endl;
        printf("Case %lld: %lld\n", z++, sol);
    }
    return 0;
}
