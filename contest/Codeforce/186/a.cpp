    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)

int main()
{
    ll x,y,z,i,j,k;
    string str,s1,s2;

    cin>>str;
    x=atol(str.c_str());
    //cout<<x<<endl;

    if(x>=0)cout<<x<<endl;
    else
    {
        k=str.size();
        s1=str.substr(0,k-1);
        s2=str.substr(0,s1.size()-1);
        s2.pb(str[k-1]);
        y=atol(s1.c_str());
        z=atol(s2.c_str());
        if(y>z)cout<<y<<endl;
        else cout<<z<<endl;
    }
    //cout<<s1<<" "<<s2<<endl;
    return 0;
}
