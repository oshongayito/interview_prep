
class point{
	public:
	int i;
	int j;
	};

int n,m;
vector<string> graph(110);
bool visit[110][110];

int check(int x,int y)
{
	if(x>=0 && x<m && y>=0 && y<n) return 1;
	return 0;
}

int move[][2]={{0,1},{0,-1},{1,0},{-1,0}};

void dfs_visit(int i,int j)
{
	stack<point> s;
	point p,q;
	p.i=i;
	p.j=j;
	visit[i][j]=1;
	s.push(p);
	while (!s.empty())
	{
		p=s.top();
		s.pop();
		for (int i = 0; i < 4; i++)
		{
			q.i=p.i+move[i][0];
			q.j=p.j+move[i][1];
			if(check(q.i,q.j) && visit[q.i][q.j]==0 && graph[q.i][q.j]=='@')
			{
				visit[q.i][q.j]=1;
				s.push(q);
			}
		}
		
	}
	
	return;
}

int dfs()
{
	int count=0;
	memset(visit,0,sizeof(visit));
	for (int i = 0; i < m; i++)
	{
		for (int j = 0; j < n ; j++)
		{
			if(visit[i][j]==0 && graph[i][j]=='@')
			{
				count++;
				dfs_visit(i,j);
			}
		}
	}
	
	return count;
}

