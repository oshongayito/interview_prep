    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))

int n,m;
vc<int>goal;


bool isGoal(vc<int>pos)
{
    for(int i=0;i<pos.size();i++)
    {
        if(pos[i]!=goal[i])return false;
    }
    return true;
}
int nMax(vc<int>cur_pos)
{
    vc<int>index;



    int step=0;
    int length=cur_pos.size();
    index.resize(length);



    for(int i=0;i<length;i++)
    {
        int k=cur_pos[i];
        index[k]=i;


    }

    for(int i=0;i<index.size();i++)
    {
        if(i%m==0)cout<<endl;
        cout<<cur_pos[i];
    }

    int temp;
    while(!isGoal(cur_pos))
    {

        //if(step>=length )break;
        int i1=index[0];
        int i2=index[index[0]];

        temp = cur_pos[i1];
        cur_pos[i1]= cur_pos[i2];
        cur_pos[i2]=temp;

        temp = i1;
        index[0]=i2;
        index[temp] = temp;
        step++;
        cout<<temp<<endl;
        for(int i=0;i<length;i++){if(i%m==0)cout<<endl;cout<<cur_pos[i];}
        cout<<endl;
    }

    return step;
}

int manhattan(vc<int>cur_pos)
{
    int sum=0;
    vc<int>index;

    index.resize(cur_pos.size());

    for(int i=0;i<cur_pos.size();i++)
    {
        int k=cur_pos[i];
        index[k]=i;


    }
    int r1,r2,c1,c2;
    for(int i=0;i<cur_pos.size();i++)
    {
        r1=index[i]/m;
        r2=goal[i]/m;
        c1=index[i]%m;
        c2=goal[i]%m;

        sum+=(abs(r1-r2)+abs(c1-c2));
    }
    return sum;
}

int tiles_out(vc<int>cur_pos)
{
    int row=0,col=0;
    vc<int>index;

    index.resize(cur_pos.size());

    for(int i=0;i<cur_pos.size();i++)
    {
        int k=cur_pos[i];
        index[k]=i;


    }
    for(int i=1;i<cur_pos.size();i++)
    {
        if((index[i]/m)!=(i/m))row++;
        if((index[i]%m)!=(i%m))col++;
    }

    return row+col;
}

class Board{


    public:

    vc<int>position;
    int f,g,h;
    string parent;
    vc<string>moves;

    Board()
    {
        position.resize(n*m);
    }
    Board(vc<int>pos)
    {

        position.resize(pos.size());
        for(int i=0;i<pos.size();i++)position[i]=pos[i];
        g=0;
        //h=max(nMax(position),manhattan(position));
        //h = manhattan(position);
        //h=nMax(position);
        h=tiles_out(position);
        //h=max(manhattan(position),tiles_out(position));
        f=g+h;
        parent="head";

    }


    Board(vc<int>pos,int blank_pos,vc<string>mvs,string dir)
    {
        int temp;
        for(int i=0;i<mvs.size();i++)moves.pb(mvs[i]);
        if(dir=="left")
        {
            temp=pos[blank_pos];
            pos[blank_pos]=pos[blank_pos-1];
            pos[blank_pos-1]=temp;
            parent="left";
            moves.pb("left");

        }
        else if(dir=="right")
        {
            temp=pos[blank_pos];
            pos[blank_pos]=pos[blank_pos+1];
            pos[blank_pos+1]=temp;
            parent="right";
                        moves.pb("right");
        }
        else if(dir=="up")
        {
            temp=pos[blank_pos];
            pos[blank_pos]=pos[blank_pos-m];
            pos[blank_pos-m]=temp;
            parent="up";
                        moves.pb("up");
        }
        else if(dir=="down")
        {
            temp=pos[blank_pos];
            pos[blank_pos]=pos[blank_pos+m];
            pos[blank_pos+m]=temp;
            parent = "down";
                        moves.pb("down");
        }

        position.resize(n*m);
        for(int i=0;i<pos.size();i++)position[i]=pos[i];
        //g=prev+1;

        //f=moves.size()+max(nMax(position),manhattan(position));
        //f=moves.size()+manhattan(position);
        //f=moves.size()+nMax(position);
        f=moves.size()+tiles_out(position);
        //f=moves.size()+max(manhattan(position),tiles_out(position));
    }

    bool goal_found()
    {

        for(int i=0;i<position.size();i++)
        {
            if(position[i]!=goal[i]){return false;}
        }

        return true;
    }

    int get_cost()
    {
        return f;
    }

    int get_blank_pos()
    {
        for(int i=0;i<position.size();i++)
        {
            if(position[i]==0){return i;break;}
        }
        return 0;
    }

    vc<Board> nextBoards()
    {
        vc<Board>boards;

        int blank_pos=get_blank_pos();
        if((blank_pos%m)>0)
        {
            boards.pb(Board(position,blank_pos,this->moves,"left"));
        }
        if((blank_pos%m)<2)
        {
            boards.pb(Board(position,blank_pos,this->moves,"right"));

        }
        if((blank_pos/m)>0)
        {
                 boards.pb(Board(position,blank_pos,this->moves,"up"));

        }
        if((blank_pos/m)<2)
        {
                 boards.pb(Board(position,blank_pos,this->moves,"down"));

        }
        //boards.pb(Board(this->position,blank_pos,"left",g));
        return boards;
    }

    bool operator< ( const Board& node ) const
{

      if(node.f<this->f)return true;
    return false;
}

bool operator> ( const Board& node ) const
{


      if(node.f>this->f)return true;

    return false;
}

void printBoard()
{
    for(int i=0;i<position.size();i++)cout<<position[i];
    cout<<endl;
}

};

class CompareCost {
public:
    bool operator()(const Board& t1,const Board& t2)
    {
       if (t1.f >= t2.f) return true;
       return false;
    }
};

class Search{

    public:
    int step;
    vc<string>moves;
    vc<int>start_pos;
    vc<int>goal_pose();

    set< vc<int> >visited;
    vc<Board>expanded;


    Board start;

    priority_queue< Board, vector < Board >, CompareCost > pq;

    Search(vc<int>pos)
    {
        step=0;
        start = Board(pos);

        start.printBoard();
        visited.insert(start.position);

        pq.push(start);

        do_search();
        //printSearch();
    }


    void do_search(){
        Board cur_board=getNextBoard();
       // vc<Board>nextBoards = cur_board.nextBoards();
        //cout<<cur_board.goal_found()<<endl;
       while(!cur_board.goal_found())
       //while(step<6)
       {
          // cout<<"hi"<<endl;
            vc<Board>NextBoards = cur_board.nextBoards();

            for(int i=0;i<NextBoards.size();i++)
            {
                    //cout<<"aisi be"<<endl;

                //if(true)
                if(!isVisited(NextBoards[i].position))
                {
                    //cout<<"aisi"<<endl;
                    pq.push(NextBoards[i]);
                    visited.insert(NextBoards[i].position);
                }
                //NextBoards[i].printBoard();
            }
            expanded.pb(cur_board);
            cur_board=getNextBoard();
           // moves.pb(cur_board.parent);
            step++;
       }
      cout<<visited.size()<<" "<<step;
      cout<<"steps "<<cur_board.moves.size()<<endl;
      cout<<"the Steps are: "<<endl;
      for(int i=0;i<cur_board.moves.size();i++)
      {
          cout<<cur_board.moves[i]<<endl;
      }
    }

    Board getNextBoard()
    {
        Board node = pq.top();
        pq.pop();
        return node;
    }

    bool isVisited( vc<int> node )
    {
        if(visited.count(node)==0)return false;
        else return true;
    }

    void printSearch()
    {
        cout<<"Total Steps Required to reach the goal is : "<<moves.size()<<endl;
        cout<<"The steps are: "<<endl;
        for(int i=0;i<moves.size();i++)
        {
            cout<<moves[i]<<endl;
        }
    }
};

int main()
{
    vc<int>position;
    vc<int>index;
    vc<bool>flag;

    int k,count,step=0;

    cin>>n>>m;
    count=n*m;

    position.resize(n*m);
    goal.resize(n*m);
    cout<<"Enter the initial State:"<<endl;
    for(int i=0;i<(n*m);i++)
    {
        cin>>k;
        position[i]=k;

    }

    cout<<"Enter the Goal State: "<<endl;
    for(int i=0;i<(n*m);i++)
    {
        cin>>goal[i];
    }

    Search* x = new Search(position);

    //cout<<tiles_out(position)<<endl;



    return 0;
}



