#include<bits/stdc++.h>

#ifdef _WIN32
#include<windows.h>
#include<GL/glut.h>
#elif _WIN64
#include<windows.h>
#include<GL/glut.h>
#elif __APPLE__
#include<GLUT/glut.h>
#else
#include<GL/glut.h>
#include<GL/gl.h>
#endif


using namespace std;

typedef long long ll;
typedef unsigned long long ull;
typedef pair<int,int> pii;
typedef vector<int> vi;
typedef vector<long long> vll;
typedef vector<vector<int> > vvi;
typedef vector<string> vs;
typedef vector<pair<int,int> > vpii;

#define pb push_back
#define mp make_pair
//~ #define PI acos(-1)
//~ #define all(a) (a).begin(),(a).end()
//~ #define len(a) ((int)(a).size())
//~ #define mem(a,n) memset(a,n,sizeof(a))
#define eps 1e-9
//~ #define rep(i,n) for(int i=0;i<(n);i++)
//~ #define repi(i,a,n) for(int i=(a);i<(n);i++)
//~ #define repr(i,a,n) for(int i=(n);i>=(a);i--)









double angle;

GLuint sidewall1,sidewall2,ground,sky;



struct point
{
	double x,y,z;
	point(){}
	point(double a,double b,double c):x(a),y(b),z(c){}
};




//~ void keyboardListener(unsigned char key, int x,int y){
	//~ switch(key){
            //~ 
		//~ case '1':
			//~ drawgrid=1-drawgrid;
			//~ break;
            //~ 
		//~ default:
			//~ break;
	//~ }
//~ }


//~ void specialKeyListener(int key, int x,int y){
	//~ switch(key){
		//~ case GLUT_KEY_DOWN:		//down arrow key
			//~ cameraHeight -= 3.0;
			//~ break;
		//~ case GLUT_KEY_UP:		// up arrow key
			//~ cameraHeight += 3.0;
			//~ break;
            //~ 
		//~ case GLUT_KEY_RIGHT:
			//~ cameraAngle += 0.03;
			//~ break;
		//~ case GLUT_KEY_LEFT:
			//~ cameraAngle -= 0.03;
			//~ break;
            //~ 
		//~ case GLUT_KEY_PAGE_UP:
			//~ break;
		//~ case GLUT_KEY_PAGE_DOWN:
			//~ break;
            //~ 
		//~ case GLUT_KEY_INSERT:
			//~ break;
            //~ 
		//~ case GLUT_KEY_HOME:
			//~ break;
		//~ case GLUT_KEY_END:
            //~ exit(0);
			//~ break;
            //~ 
		//~ default:
			//~ break;
	//~ }
//~ }



void drawRect(double a,double b){
	glBegin(GL_QUADS);{
		glVertex3d(0,a/2,b/2);
		glVertex3d(0,-a/2,b/2);
		glVertex3d(0,-a/2,-b/2);
		glVertex3d(0,a/2,-b/2);
	}glEnd();
}


void drawRectBar(double a,double b,double c,double d,double h){

	glBegin(GL_QUADS);{
		
		
		glColor3f(0.8,0.8,0.8);
		glVertex3d(a/2,b/2,h/2);
		glVertex3d(-a/2,b/2,h/2);
		glVertex3d(-c/2,d/2,-h/2);
		glVertex3d(c/2,d/2,-h/2);
		
		glColor3f(0.8,0.8,0.8);
		glVertex3d(-a/2,b/2,h/2);
		glVertex3d(-a/2,-b/2,h/2);
		glVertex3d(-c/2,-d/2,-h/2);
		glVertex3d(-c/2,d/2,-h/2);
		
		glColor3f(0.8,0.8,0.8);
		glVertex3d(-a/2,-b/2,h/2);
		glVertex3d(a/2,-b/2,h/2);
		glVertex3d(c/2,-d/2,-h/2);
		glVertex3d(-c/2,-d/2,-h/2);
		
		glColor3f(0.8,0.8,0.8);
		glVertex3d(a/2,-b/2,h/2);
		glVertex3d(a/2,b/2,h/2);
		glVertex3d(c/2,d/2,-h/2);
		glVertex3d(c/2,-d/2,-h/2);
		
		
		///make it a solid cube
		
		glColor3f(0.8,0.8,0.8);
		glVertex3d(a/2,b/2,h/2);
		glVertex3d(-a/2,b/2,h/2);
		glVertex3d(-a/2,-b/2,h/2);
		glVertex3d(a/2,-b/2,h/2);
		
		glVertex3d(c/2,d/2,-h/2);
		glVertex3d(-c/2,d/2,-h/2);
		glVertex3d(-c/2,-d/2,-h/2);
		glVertex3d(c/2,-d/2,-h/2);
		
		//~ glVertex3d(c*x[0],d*y[0],0);
		//~ glVertex3d(c*x[1],d*y[1],0);
		//~ glVertex3d(a*x[1],b*y[1],h);
		//~ glVertex3d(a*x[0],b*y[0],h);
		
		

	}glEnd();
}

void drawHexBar(double a,double b,double h){
	const double c = sqrt(3)/2.;
	double x[]={1,0.5,-0.5,-1,-0.5,0.5,1};
	double y[]={0,c,c,0,-c,-c,0};
	
	glBegin(GL_QUADS);{
		rep(i,6){
			glVertex3d(a*x[i],a*y[i],0);
			glVertex3d(a*x[i+1],a*y[i+1],0);
			glVertex3d(b*x[i+1],b*y[i+1],h);
			glVertex3d(b*x[i],b*y[i],h);
		}
	}glEnd();
	
	
}


double column_angle=4,column_bas_pos=6;

void drawHexColumns(){
	glColor3d(0.8,0.8,0.8);
	double a=80,b=7,c=10;
	double base_width=3.2;
	glPushMatrix();{
		glTranslated(-column_bas_pos,0,0);
		glRotated(column_angle,0,1,0);
		drawHexBar(base_width,1,a);
	};glPopMatrix();
	glPushMatrix();{
		glTranslated(column_bas_pos,0,0);
		glRotated(-column_angle,0,1,0);
		drawHexBar(base_width,1,a);
	};glPopMatrix();
	
	glPushMatrix();{
		glTranslated(-0.5,0,a);
		glRotated(3,0,1,0);
		drawHexBar(1,0.5,b);
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslated(0.5,0,a);
		glRotated(-3,0,1,0);
		drawHexBar(1,0.5,b);
	}glPopMatrix();
	
    glPushMatrix();{
		glTranslated(0,0,a+b);
		drawHexBar(0.5,0.05,c);
	}glPopMatrix();
}


void drawColumnStrip(double width=1,double radius=15){
	vector<point> v1,v2;
	vector<point> v3,v4;
	double radius2 = radius-1;
	repi(i,-77,45){
		v1.pb(point(-width/2,1.4*radius*sin(PI*i/180.),radius*cos(PI*i/180)));
		v2.pb(point(width/2,1.4*radius*sin(PI*i/180.),radius*cos(PI*i/180)));
		
		v3.pb(point(-width/2,1.32*radius2*sin(PI*i/180.),radius2*cos(PI*i/180)));
		v4.pb(point(width/2,1.32*radius2*sin(PI*i/180.),radius2*cos(PI*i/180)));
		//~ cout<<v1.back().x<<" "<<v1.back().y<<" "<<v1.back().z<<endl;
	}
	//~ exit(0);
	
	glColor3d(1,0,0);
	glRotated(-90,1,0,0);
	glBegin(GL_QUADS);{
		rep(i,len(v1)-1){
			//~ glColor3d(0,1,0);
			glColor3d(1,1,1);
			///outer strip
			glVertex3f(v1[i].x,v1[i].y,v1[i].z);
			glVertex3f(v1[i+1].x,v1[i+1].y,v1[i+1].z);
			glVertex3f(v2[i+1].x,v2[i+1].y,v2[i+1].z);
			glVertex3f(v2[i].x,v2[i].y,v2[i].z);
			
			///inner strip
			glVertex3f(v3[i].x,v3[i].y,v3[i].z);
			glVertex3f(v3[i+1].x,v3[i+1].y,v3[i+1].z);
			glVertex3f(v4[i+1].x,v4[i+1].y,v4[i+1].z);
			glVertex3f(v4[i].x,v4[i].y,v4[i].z);
			
			///right side
			//~ glColor3d(1,0,0);
			glVertex3f(v1[i].x,v1[i].y,v1[i].z);
			glVertex3f(v1[i+1].x,v1[i+1].y,v1[i+1].z);
			glVertex3f(v3[i+1].x,v3[i+1].y,v3[i+1].z);
			glVertex3f(v3[i].x,v3[i].y,v3[i].z);
			
			///left side
			glVertex3f(v2[i+1].x,v2[i+1].y,v2[i+1].z);
			glVertex3f(v2[i].x,v2[i].y,v2[i].z);
			glVertex3f(v4[i].x,v4[i].y,v4[i].z);
			glVertex3f(v4[i+1].x,v4[i+1].y,v4[i+1].z);
		}
	}glEnd();
}


double curve_col_ang=40;

void drawCurveColumns(){
	double ang = curve_col_ang;
	glPushMatrix();{
		glTranslated(0.5,0,0);
		glRotated(-ang,0,0,1);
		glPushMatrix();{
			glTranslated(0,-12,13.5);
			glRotated(-37,1,0,0);
			drawRectBar(1.25,0.4,1.25,15,55);
		};glPopMatrix();
		
		
		glTranslated(0,2,82);
		glRotated(-18,1,0,0);
		glTranslated(0,-4.5,-37);
		drawColumnStrip(1.25,22);
	}glPopMatrix();
	
	glPushMatrix();{
		glTranslated(-0.5,0,0);
		glRotated(ang,0,0,1);
		glPushMatrix();{
			glTranslated(0,-12,13.5);
			glRotated(-37,1,0,0);
			drawRectBar(1.25,0.4,1.25,15,55);
		};glPopMatrix();
		
		
		glTranslated(0,2,82);
		glRotated(-18,1,0,0);
		glTranslated(0,-4.5,-37);
		drawColumnStrip(1.25,22);
	}glPopMatrix();
		//~ 
	//~ glPushMatrix();{
		//~ //glTranslated(0,0,25);
		//~ drawRectBar(1.5,0.5,1.5,7.5,50);
	//~ }glPopMatrix();
}





void drawMiddleBars(){
	double gap=11;
	double w=1.5,l=6;
	glPushMatrix();{
		glTranslated(0,0,22.5);
		drawRectBar(l,w,l,w,w);
		glTranslated(0,0,gap);
		drawRectBar(l,w,l,w,w);
		glTranslated(0,0,gap);
		drawRectBar(l,w,l,w,w);
		glTranslated(0,0,gap);
		drawRectBar(l,w,l,w,w);
		
	};glPopMatrix();
}


double cylinder_gap=2.75,cylinder_base_h=41;

void drawCylinder(double sign,double r=0.5){
	
	double h=cylinder_base_h;
	GLUquadricObj *q;
	q = gluNewQuadric();
	
	
	double d;
	d = column_bas_pos-h/tan(PI*(90-column_angle)/180);
	double lengths[] ={4.8,5.8,6.5,7.2,7.6,7.6,7.5,7,6.5,5.5,4};
	
	glColor3d(0.8,0.8,0.8);
	//~ glTranslated(0,0,h);
	
	glPushMatrix();{
		glTranslated(sign*d,0,h-cylinder_gap);
		glRotated(-sign*column_angle,0,1,0);
		
		
		
		rep(i,8){
			glTranslated(0,0,cylinder_gap);
			glPushMatrix();{
				
				glRotated(-sign*(curve_col_ang-(10-i)*1.3),0,0,1);
				glRotated(-90,1,0,0);
				gluCylinder(q,r,r,lengths[i],5,60);
			};glPopMatrix();
		}
		repi(i,8,11){
			glTranslated(0,0,cylinder_gap);
			glPushMatrix();{
				
				glRotated(-sign*(curve_col_ang-(10-i)*1.3),0,0,1);
				glRotated(-90,1,0,0);
				gluCylinder(q,1.1*r,1.1*r,lengths[i],5,60);
			};glPopMatrix();
		}
	}glPopMatrix();
	
	
}


void drawRoundCylinder(double ang,double r,double r1=0.75,double r2=0.25){
	//~ double r1=0.75,r2=0.25;
	vector<point> v;
	repi(i,0,360){
		v.pb(point(0,r1*cos(PI*i/180),r2*sin(PI*i/180)));
		i+=20;
	}
	
	double w = 1;
	glPushMatrix();{
		glRotated(-ang/2,0,0,1);
		rep(i,ang){
			glRotated(1,0,0,1);
			glPushMatrix();{
				glTranslated(0,r,0);
				glBegin(GL_QUADS);{
					rep(j,len(v)){
						int k =(j+1)%len(v);
						glVertex3d(v[j].x,v[j].y,v[j].z);
						glVertex3d(v[j].x+w,v[j].y,v[j].z);
						glVertex3d(v[k].x+w,v[k].y,v[k].z);
						glVertex3d(v[k].x,v[k].y,v[k].z);
					}
				}glEnd();
			}glPopMatrix();
		}
	}glPopMatrix();
	
}

void drawTopFloorPlane(double ang,double r){
	vector<point> v;
	rep(i,ang){
		v.pb(point(r*cos(PI*i/180),r*sin(PI*i/180),0));
		i+=2;
	}
	
	glPushMatrix();{
		glRotated(ang/2,0,0,1);
		glBegin(GL_TRIANGLES);{
			rep(i,len(v)-1){
				glVertex3f(0,0,0);
				glVertex3f(v[i].x,v[i].y,v[i].z);
				glVertex3f(v[i+1].x,v[i+1].y,v[i+1].z);
				
			}
		}glEnd();
	}glPopMatrix();
}


void drawTopFloors(){
	double rad[] = {8.3,7.7,6.8,5.5};
	double w=0.25;
	double f = cylinder_base_h+cylinder_gap*7;
	glTranslated(0,0,f-w);
	drawTopFloorPlane(curve_col_ang*2+15,rad[0]);
	glTranslated(0,0,2*w);
	drawTopFloorPlane(curve_col_ang*2+15,rad[0]);
	
	glTranslated(0,0,cylinder_gap-w);
	drawTopFloorPlane(curve_col_ang*2+15,rad[1]);
	glTranslated(0,0,2*w);
	drawTopFloorPlane(curve_col_ang*2+15,rad[1]);
	
	glTranslated(0,0,cylinder_gap-w);
	drawTopFloorPlane(curve_col_ang*2+15,rad[2]);
	glTranslated(0,0,2*w);
	drawTopFloorPlane(curve_col_ang*2+15,rad[2]);
	
}

void drawRoundRailing(point p,double radius,double bheight,double ang,double lang,double rang,double l1){
	GLUquadricObj *q;
	q = gluNewQuadric();
	glColor3d(0.5,0.5,0.5);
	double ln = 1.3;
	glPushMatrix();{
		repi(i,lang+ang,rang+ang){
			glPushMatrix();{
				glTranslated(0,radius,bheight);
				gluCylinder(q,0.1,0.1,1,10,10);
				glTranslated(0,0,1);
				glRotated(-90,0,1,0);
				gluCylinder(q,0.1,0.1,ln,10,10);
			}glPopMatrix();
			glRotated(-ang,0,0,1);
			i+=ang-1;
		}
		
		glTranslated(0,radius,bheight);
		rep(i,l1){
			glPushMatrix();{
				glTranslated(-ln,0,0);
				gluCylinder(q,0.1,0.1,1,10,10);
				glTranslated(0,0,1);
				glRotated(-90,0,1,0);
				gluCylinder(q,0.1,0.1,ln,10,10);
			}glPopMatrix();
		}
	}glPopMatrix();
	
}


void drawBaseRound(double radius=18,double height=2.5,double l1=10,double l2=10,double lang=-90,double rang=91){
	vector<point> v;
	double ang=5;
	repi(i,lang,rang){
		v.pb(point(radius*cos(PI*i/180.),radius*sin(PI*i/180.),0));
		i+=ang-1;
	}
	//for railing co-ordinates
	vector<point> tmp=v;
	
	glPushMatrix();{
		//sides
		glColor3f(0.8,.8,0.8);
		rep(i,len(v)-1){
			glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D,sidewall1);
				glNormal3f(1.0,0.0,0.0);
				glBegin(GL_QUADS);{
					glTexCoord2f(0,0);glVertex3d(v[i].x,v[i].y,v[i].z);
					glTexCoord2f(1,0);glVertex3d(v[i].x,v[i].y,v[i].z+height);
					glTexCoord2f(1,1);glVertex3d(v[i+1].x,v[i+1].y,v[i+1].z+height);
					glTexCoord2f(0,1);glVertex3d(v[i+1].x,v[i+1].y,v[i+1].z);		
				}glEnd();
			glDisable(GL_TEXTURE_2D);
		}
		//rooftop
		glBegin(GL_TRIANGLES);{
			rep(i,len(v)-1){
				
				glVertex3d(0,0,height);
				glVertex3d(v[i].x,v[i].y,v[i].z+height);
				glVertex3d(v[i+1].x,v[i+1].y,v[i+1].z+height);
			}
		}glEnd();
		
		
		double x = v.back().x,y=v.back().y,z=v.back().z;
		double inc = 1;
		//extended right sides
		rep(i,l1){
			glColor3f(0.8,.8,0.8);
			glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D,sidewall1);
				glNormal3f(1.0,0.0,0.0);
				glBegin(GL_QUADS);{
					tmp.pb(point(x,y,0));
					glVertex3d(x,y,z);
					glVertex3d(x,y,z+height);
					glVertex3d(x-inc,y,z+height);
					glVertex3d(x-inc,y,z);
					x -= inc;
					//~ y += inc;
				}glEnd();
			glDisable(GL_TEXTURE_2D);
		}
		
		//extended right top
		x = v.back().x,y=v.back().y,z=v.back().z;
		rep(i,l1){
			glColor3f(0.8,.8,0.8);
			glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D,ground);
				glNormal3d(1,0,0);
				glBegin(GL_TRIANGLES);{
					glVertex3d(0,0,height);
					glVertex3d(x,y,z+height);
					glVertex3d(x-inc,y,z+height);
					x -= inc;
					//~ y += inc;
				}glEnd();
			glDisable(GL_TEXTURE_2D);
		}
		
		x = v[0].x,y=v[0].y,z=v[0].z;
		
		//extended left side
		rep(i,l2){
			glColor3f(0.8,.8,0.8);
			glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D,sidewall1);
				glNormal3f(1.0,0.0,0.0);
				glBegin(GL_QUADS);{
					glVertex3d(x,y,z);
					glVertex3d(x,y,z+height);
					glVertex3d(x+0.5*inc,y-inc,z+height);
					glVertex3d(x+0.5*inc,y-inc,z);
					y-=inc;
					x+=0.5*inc;
				}glEnd();
			glDisable(GL_TEXTURE_2D);
		}
		
		if(lang>-80)
			drawRoundRailing(v.back(),radius,height,ang,lang,rang,l1);
		
		if(lang>-80){//upper
			glBegin(GL_QUADS);{
				glVertex3d(v[0].x,v[0].y+1.5,height);
				glVertex3d(v[0].x-7,v[0].y+0.5,height);
				glVertex3d(v.back().x-25,v.back().y-1.5,height);
				glVertex3d(v.back().x,v.back().y-1.5,height);
			}glEnd();
		}else{
			glBegin(GL_QUADS);{
				glVertex3d(v[0].x,v[0].y,height);
				glVertex3d(v[0].x-10,v[0].y,height);
				glVertex3d(v.back().x-10,v.back().y,height);
				glVertex3d(v.back().x,v.back().y,height);
				
				//leftside extention
				glVertex3d(x,y,height);
				glVertex3d(x-7,y,height);
				glVertex3d(v[0].x-7,v[0].y,height);
				glVertex3d(v[0].x,v[0].y,height);
			}glEnd();
		}
		
	}glPopMatrix();
	
}




void drawRailing(double length,double height,double bheight,double cols){
	GLUquadricObj *q;
	q = gluNewQuadric();
	double dist = length/cols;
	
	glPushMatrix();{
		glTranslated(length/2,0,bheight+height);
		glRotated(-90,0,1,0);
		gluCylinder(q,0.1,0.1,length,10,10);
	}glPopMatrix();
	glPushMatrix();{
		glTranslated(-length/2-dist,0,bheight);
		rep(i,cols){
			glTranslated(dist,0,0);
			gluCylinder(q,0.1,0.1,height,10,10);
		}
		glTranslated(dist,0,0);
		//~ glTranslated(length-dist*(cols-1),0,0);
		gluCylinder(q,0.1,0.1,height,2,10);
	}glPopMatrix();
	
}

void drawRoom(double height=5,bool rail=false){
	
	double d;
	glPushMatrix();{
		//front
		glTranslated(0,12,3.5);
		d = 4.55;
		if(rail)drawRailing(2*d,1,height,10);
		
		glBegin(GL_QUADS);{
			glVertex3f(-d,0,0);
			glVertex3f(d,0,0);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			
			glVertex3f(d,-18,height);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			glVertex3f(-d,-18,height);
			
		}glEnd();
		
	}glPopMatrix();
	
	
	
	//leftSide
	glPushMatrix();{
		glTranslated(8,12/2,3.5);
		glRotated(120,0,0,1);
		d = 14.2/2;
		if(rail)drawRailing(2*d,1,height,10);
		glBegin(GL_QUADS);{
			glVertex3f(-d,0,0);
			glVertex3f(d,0,0);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			
			glVertex3f(1.2*d,10,height);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			glVertex3f(-1.2*d,10,height);
			
		}glEnd();
	}glPopMatrix();
	//rightSide
	glPushMatrix();{
		glTranslated(-8,12/2,3.5);
		glRotated(-120,0,0,1);
		d = 14.2/2;
		if(rail)drawRailing(2*d,1,height,10);
		glBegin(GL_QUADS);{
			glVertex3f(-d,0,0);
			glVertex3f(d,0,0);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			
			glVertex3f(1.5*d,10,height);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			glVertex3f(-1.5*d,10,height);
		}glEnd();
	}glPopMatrix();
	
	
	//leftSide
	glPushMatrix();{
		glTranslated(10.9,-1.4,3.5);
		glRotated(-120,0,0,1);
		d = 3./2;
		if(rail)drawRailing(2*d,1,height,10);
		glBegin(GL_QUADS);{
			glVertex3f(-d,0,0);
			glVertex3f(d,0,0);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			
			glVertex3f(1.5*d,-10,height);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			glVertex3f(-1.5*d,-10,height);
		}glEnd();
	}glPopMatrix();
	//rightSide
	glPushMatrix();{
		glTranslated(-10.9,-1.4,3.5);
		glRotated(120,0,0,1);
		d = 3./2;
		if(rail)drawRailing(2*d,1,height,10);
		glBegin(GL_QUADS);{
			glVertex3f(-d,0,0);
			glVertex3f(d,0,0);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			
			glVertex3f(1.5*d,-10,height);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			glVertex3f(-1.5*d,-10,height);
		}glEnd();
	}glPopMatrix();
	
	
	//leftSide
	glPushMatrix();{
		glTranslated(9.8,-5.8,3.5);
		glRotated(85,0,0,1);
		d = 6.8/2+0.1;
		if(rail)drawRailing(2*d,1,height,10);
		glBegin(GL_QUADS);{
			glVertex3f(-d,0,0);
			glVertex3f(d,0,0);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			
			glVertex3f(1.2*d,16,height);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			glVertex3f(-1.2*d,16,height);
		}glEnd();
	}glPopMatrix();
	//rightSide
	glPushMatrix();{
		glTranslated(-9.8,-5.8,3.5);
		glRotated(-85,0,0,1);
		d = 6.8/2+0.1;
		if(rail)drawRailing(2*d,1,height,10);
		glBegin(GL_QUADS);{
			glVertex3f(-d,0,0);
			glVertex3f(d,0,0);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			
			glVertex3f(1.2*d,16,height);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			glVertex3f(-1.2*d,16,height);
		}glEnd();
	}glPopMatrix();
	
	
	//leftSide
	glPushMatrix();{
		glTranslated(8.5,-9.9,3.5);
		glRotated(35,0,0,1);
		d = 2.3/2;
		if(rail)drawRailing(2*d,1,height,10);
		glBegin(GL_QUADS);{
			glVertex3f(-d,0,0);
			glVertex3f(d,0,0);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			
			glVertex3f(d,10,height);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			glVertex3f(-d,10,height);
		}glEnd();
	}glPopMatrix();
	//rightSide
	glPushMatrix();{
		glTranslated(-8.5,-9.9,3.5);
		glRotated(-35,0,0,1);
		d = 2.3/2;
		if(rail)drawRailing(2*d,1,height,10);
		glBegin(GL_QUADS);{
			glVertex3f(-d,0,0);
			glVertex3f(d,0,0);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			
			glVertex3f(d,10,height);
			glVertex3f(d,0,height);
			glVertex3f(-d,0,height);
			glVertex3f(-d,10,height);
		}glEnd();
	}glPopMatrix();
	
	
	
	//~ //roof
	//~ double x[10] = {};
	//~ double y[10] = {};
	//~ glBegin(GL_POLYGON);{
		//~ rep(i,10){
			//~ glVertex3d(x[i],y[i],height);
		//~ }
	//~ }glEnd();
	
}

void drawBackRoom(double radius = 20,double height=7.6){
	vector<point> v;
	repi(i,-40-90,40-90){
		v.pb(point(radius*cos(PI*i/180.),radius*sin(PI*i/180),height));
		i+=10;
	}
	
	glBegin(GL_POLYGON);{
		glVertex3f(0,0,height);
		rep(i,len(v)){
			glVertex3f(v[i].x,v[i].y,v[i].z);
		}
	}glEnd();
	
	glColor3f(1,1,1);
	glBegin(GL_QUADS);{
		rep(i,len(v)-1){
			glVertex3f(v[i].x,v[i].y,v[i].z);
			glVertex3f(v[i+1].x,v[i+1].y,v[i+1].z);
			glVertex3f(v[i+1].x,v[i+1].y,0);
			glVertex3f(v[i].x,v[i].y,0);
		}
	}glEnd();
}



void drawLowerCylinder(){
	glPushMatrix();{
		double r = 11;
		glColor3f(1,1,1);
		glTranslated(0,3,7.15);
		drawRoundCylinder(225,r,0.25,0.25);
		double ln = 22;
		glTranslated(0,1,0);
		drawRectBar(ln,0.4,ln,0.4,0.4);
		ln = 20;
		glTranslated(0,4,0);
		drawRectBar(ln,0.4,ln,0.4,0.4);
		ln = 13;
		glTranslated(0,3.5,0);
		drawRectBar(ln,0.4,ln,0.4,0.4);
		
		
		ln = 4.5;
		glPushMatrix();{
			glTranslated(-3,0,0);
			glRotated(-90,0,0,1);
			drawRectBar(ln,0.4,ln,0.4,0.4);
		}glPopMatrix();
		glPushMatrix();{
			glTranslated(3,0,0);
			glRotated(-90,0,0,1);
			drawRectBar(ln,0.4,ln,0.4,0.4);
		}glPopMatrix();
	}glPopMatrix();
	
	//~ double x = r*cos(0)
}



void drawBase(){
	glPushMatrix();{
		glRotatef(75,0,0,1);
		drawBaseRound(20,2.5,10,10);
		glTranslated(-0.5,0,2.5);
		
		glColor3f(0.8,.8,0.8);
		drawBaseRound(15.5,1,6,0,-50);
	}glPopMatrix();
	
	glPushMatrix();{
		glColor3f(0.8,0.80,0.8);
		drawBackRoom(22.5,7.6);
		glColor3f(0,.3,0);
		drawRoom(2.7);
		glTranslated(0,0,2.7);
		glColor3f(0.8,.8,0.8);
		drawRoom(0.7);
		glTranslated(0,0,0.7);
		glColor3f(0.8,.8,0.8);
		drawRoom(0.7,true);
	}glPopMatrix();
	
	
	
	drawLowerCylinder();
	glColor3d(1,1,1);
	glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,ground);
		glNormal3d(1,0,0);
		glBegin(GL_QUADS);{
			glTexCoord2d(0,0);glVertex3d(100,100,0);
			glTexCoord2d(1,0);glVertex3d(-100,100,0);
			glTexCoord2d(1,1);glVertex3d(-100,-100,0);
			glTexCoord2d(0,1);glVertex3d(100,-100,0);
		}glEnd();
	glDisable(GL_TEXTURE_2D);
	
	
}


GLuint LoadTexture( const char * filename )
{
	GLuint texture;

	int width, height;

	unsigned char * data;

	FILE * file;

	file = fopen( filename, "rb" );

	if ( file == NULL ) return 0;
	width = 1024;
	height = 1024;
	data = (unsigned char *)malloc( width * height * 3 );
	//int size = fseek(file,);
	fread( data, width * height * 3, 1, file );
	fclose( file );

	for(int i = 0; i < width * height ; ++i)
	{
		int index = i*3;
		unsigned char B,R;
		B = data[index];
		R = data[index+2];

		data[index] = R;
		data[index+2] = B;

	}


	glGenTextures( 1, &texture );
	glBindTexture( GL_TEXTURE_2D, texture );
	glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,GL_MODULATE );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR_MIPMAP_NEAREST );


	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT );
	gluBuild2DMipmaps( GL_TEXTURE_2D, 3, width, height,GL_RGB, GL_UNSIGNED_BYTE, data );
	free( data );
	cout<<"hise"<<endl;

	return texture;
}

void draw(){
	drawHexColumns();
	drawMiddleBars();
	drawCurveColumns();
	drawBase();	
	drawCylinder(1);
	drawCylinder(-1);
	
	
	glColor3f(0.9,0.9,0.9);
	
	double rad[] = {6.6,7.45,8.1,8.55,8.8,8.9,8.7,8.3,7.7,6.8,5.5};
	glPushMatrix();{
		glTranslated(0,0,cylinder_base_h);
		drawRoundCylinder(2*curve_col_ang,rad[0]);
		rep(i,7){
			glTranslated(0,0,cylinder_gap);
			drawRoundCylinder(2*curve_col_ang,rad[i+1]);
		}
		rep(i,2){
			glTranslated(0,0,cylinder_gap);
			drawRoundCylinder(2*curve_col_ang,rad[i+8],.75,.5);
		}
		
		glTranslated(0,0,cylinder_gap);
		drawRoundCylinder(2*curve_col_ang,rad[10]);
	}glPopMatrix();
	
	
	glColor3f(0.8,0.8,0.8);
	glPushMatrix();{
		drawTopFloors();
	}glPopMatrix();
	
}


void init(){
	//codes for initialization

	angle=0;
	
	
	sidewall1 = LoadTexture("sidewall1.bmp");
	sidewall2 = LoadTexture("sidewall2.bmp");
	ground = LoadTexture("ground.bmp");
	sky = LoadTexture("sky.bmp");
	

	
}



void reshapeHandler(int w,int h){
	if(!h)h=1;
	double rat = 1.0*w/h;
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0,0,w,h);
	gluPerspective(80,rat,1,10000);
	glMatrixMode(GL_MODELVIEW);
}



