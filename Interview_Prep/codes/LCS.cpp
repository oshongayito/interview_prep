
/*
 * Related leetcode problem: https://leetcode.com/problems/longest-common-subsequence/
 */


/*
 * Given two strings text1 and text2, return the length of their longest 
 * common subsequence.

 * A subsequence of a string is a new string generated from the original string
 * with some characters(can be none) deleted without changing the relative order
 * of the remaining characters. (eg, "ace" is a subsequence of "abcde" while "aec"
 * is not). A common subsequence of two strings is a subsequence that is common 
 * to both strings.
 */

int LCS(string A, string B){
	vector< vector<int> > DP;
	int n1 = A.size();
	int n2 = B.size();
	
	DP.resize(n1+1);
	for(int i=0;i<n1+1;i++){
		DP[i].resize(n2+1,0);
	}

	for(int i=1;i<=n1;i++){
		for(int j=1;j<=n2;j++){
			if(A[i-1] == B[j-1]){
				DP[i][j] = 1+DP[i-1][j-1];
			}
			else{
				DP[i][j] = max(DP[i-1][j], DP[i][j-1]);
			}
		}
	}

	return DP[n1][n2];
}
