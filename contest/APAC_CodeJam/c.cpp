    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>
    #include <fstream>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 1000000007LL
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))





int main()
{
    freopen ("C-small-attempt0.in","r",stdin);
    ofstream myfile;
  myfile.open ("output.txt");


    ll t,n,count;
    map<string,ll>nodes;
    map<ll,string>i_nodes;

    string start,end;

    queue<ll>q;

    vc<string>res;
    vc<int>resi;
    vc<ll>in_deg;

    bool adj[1001][1001];
    //map< pair<int,int>,bool>adj;
    cin>>t;
    for(int k=1;k<=t;k++){
        cin>>n;
        in_deg.resize(n*2+1);
        //adj.resize(n*2);
       // for(int j=0;j<n*2;j++)adj[j].resize(n*2);

        count=1;
        for(int i=0;i<n;i++)
        {


        cin>>start>>end;


        if(!nodes[start])
        {
            nodes[start]=count;
            i_nodes[count]=start;
            count++;
        }

        if(!nodes[end])
        {
            nodes[end]=count;
            i_nodes[count]=end;
            count++;
        }
        adj[nodes[start]][nodes[end]]=1;
       // adj[mp(nodes[start],nodes[end])]=1;
        in_deg[nodes[end]]++;

        }

    //cout<<count<<endl;
        //for(int i=1;i<count;i++)cout<<" "<<in_deg[i]<<endl;
        for(int i=1;i<count;i++)
        {
            if(in_deg[i]==0)
            {
                q.push(i);
            }
        }

        while(!q.empty())
        {
            ll x=q.front();
           // cout<<x<<" ";
            q.pop();
            res.pb(i_nodes[x]);
            resi.pb(x);
            for(int i=1;i<count;i++)
            {
                if(adj[x][i]==1)
                {
                    adj[x][i]=0;
                    in_deg[i]--;
                    if(in_deg[i]==0)q.push(i);
                }
            }
        }


    myfile<<"Case #"<<k<<": ";
    for(int i=0;i<res.size()-1;i++)
    {
        if(i==res.size()-2)
        myfile<<res[i]<<"-"<<res[i+1];
        else
        myfile<<res[i]<<"-"<<res[i+1]<<" ";
    }
    myfile<<endl;

    res.clear();
    resi.clear();
    nodes.clear();
    i_nodes.clear();

    for(int i=0;i<1001;i++)
    {
        for(int j=0;j<1001;j++)adj[i][j]=0;
    }
    in_deg.clear();




    }

fclose (stdin);
myfile.close();

    return 0;
}
