### Categories of Design Patterns ###
--------------------------------------
Design patterns can be classified in three categories: Creational, Structural and Behavioral patterns.

Creational Patterns - These design patterns provide a way to create objects while hiding the creation logic, rather than instantiating objects directly using new opreator. This gives program more flexibility in deciding which objects need to be created for a given use case.

Structural Patterns - These design patterns concern class and object composition. Concept of inheritance is used to compose interfaces and define ways to compose objects to obtain new functionalities.

Behavioral Patterns - These design patterns are specifically concerned with communication between objects.


### Singleton Patern ###
--------------------------
Singleton pattern is a creational pattern which allows only one instance of a class to be created which will be available to the whole application. The major advantage of Singleton design pattern is its saves memory because the single instance is reused again and again; there is no need to create a new object at each request. For example, in our application, we can use a single database connection shared by multiple objects, instead of creating a database connection for every request.


### Why use a factory class to instantiate a class when we can use new operator? ###
--------------------------------------------------------------------------------------
Factory classes provide flexibility in terms of design. Below are some of the benefits of factory class:
•Factory design pattern results in more decoupled code as it allows us to hide creational logic from dependant code
•It allows us to introduce an Inversion of Control container
•It gives you a lot more flexibility when it comes time to change the application as our creational logic is hidden from dependent code


### What is observer design pattern in Java? ###
---------------------------------------------------
Observer design pattern is one of the **behavioral design patterns** which defines one-to-many dependencies between objects & is useful when we are interested in a state of an object and we want to get notified when there is any change in state of Object. In Observer design pattern, when one object changes its state, all its dependent objects are automatically notified, the object is called Subject and dependants are called Observers. Java provides libraries to implement Observer design pattern using java.util.Observable class & java.util.Observer interface.


### Give an example of decorator design pattern? ###
------------------------------------------------------
The decorator pattern, also known as a **structural pattern** is used to add additional functionality to a particular object at runtime. It wraps the original object through decorator object. For example, when you are buying a burger, you can customize it by adding extra filling and sauces, now the cost of these items have to be added to the final price. The customization will differ from customer to customer and offer from a shop. Creating different classes of burger with different fillings will end up creating a lot of classes. Decorator solves this problem by extending the functionality of single Burger class at runtime based on customer request.


#### References ####
1. https://www.tutorialspoint.com/design_pattern/design_pattern_interview_questions.htm
2. https://www.educba.com/design-pattern-interview-questions/


