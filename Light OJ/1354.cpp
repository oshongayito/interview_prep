#include<iostream>
#include<vector>
#include<cmath>
#include<cstring>
#include<cstdio>
#include<queue>

#define black 1
#define white 0

using namespace std;


vector<bool> graph[20001];
bool vis[20001];
int dis[20001];
int edges;

bool exists [20000];
int w;

int bfs(int s)
{
    queue <int > Q;
    int i,j,k,l=0,u,max=-1,vamp=1,lyk=0,flag=0;
    int where=white;

    Q.push(s);
    k=0;
    j=1;
    while(!Q.empty())
    {
        u = Q.front();
        Q.pop();


        for(i=1;i<=graph[u].size();i++)
        {
            if(graph[u][i]==1 && vis[i]==white)
            {

                    Q.push(i);
                //dis[i]=dis[u]+1;
                    dis[u]++;
                    vis[i] =   black;
                    flag=1;
            }
        }

        vis[u]=black;


        if(where==black)
        {

            vamp+=dis[u];
            l+=dis[u];
            k++;
            if(k==j)
            {
                where=white;
                j=l;k=0;l=0;
            }


        }
        else{
        lyk+=dis[u];
        l+=dis[u];
        //j=dis[u];
        k++;
        if(k==j){
            where=black;
            j=l;
            k=0;
            l=0;
        }
        }

    }

    if(flag==0)vamp--;
    else w++;
    if(vamp>max)max=vamp;
    if(lyk>max)max=lyk;


    return max;

}



int main()
{
    int i,j,x,y,max,tests,num=0;
    cin>>tests;

    for(j=1;j<=tests;j++)
    {
    cin>>edges;
    for(i=1;i<=edges;i++)
    {
        cin>>x>>y;
        if(exists[x]==white){num++;exists[x]=black;}
        if(exists[y]==white){num++;exists[y]=black;}
        graph[x].push_back(x);
        graph[y].push_back(y);

    }


    //max=bfs(1);
    for(max=0,i=1;i<=20000 && w<=num ;i++)
    {
        if(vis[i]==white)
            {


            max+=bfs(i);
            }
    }
    cout<<"Case "<<j<<": "<<max<<endl; //Shows distance of any node from the source node
    memset(vis,0,sizeof(vis));
    memset(dis,0,sizeof(dis));
    for(i=0;i<20001;i++)graph[i].clear();
    memset(exists,0,sizeof(exists));
    num=0;
    w=0;


    }
    return 0;
}
