int a[102][102];

int max_submatrix(int n,int m)
{
	
	int maximum=0;
	
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<m;j++)
		{
			int b[n][n];
			memset(b,0,sizeof(b));
			
			
			b[i][j]=a[i][j];
			
			for(int l=i+1;l<n;l++)
			{
				b[l][j]=b[l-1][j]+a[l][j];
				if(b[l][j]>maximum) maximum=b[l][j];
			}
			for(int l=j+1;l<m;l++)
			{
				b[i][l]=b[i][l-1]+a[i][l];
				if(b[i][l]>maximum) maximum=b[i][l];
			}
			for(int k=i+1;k<n;k++)
			{
				for(int l=j+1;l<m;l++)
				{
					b[k][l]=b[k-1][l]+b[k][l-1]-b[k-1][l-1]+a[k][l];
					if(b[k][l]>maximum) maximum=b[k][l];
				}		
			}
		}
	}
	
	return maximum;
}
