/*
 * For backtracking, remember this 3 key points.
 * 1. Choices 
 * 2. Constraings
 * 3. Goal (base case)
 * 
 * Example:
 * 
 * Restore valid ip addresses from a given string (https://leetcode.com/problems/restore-ip-addresses/)
 * "25525511135" => ["255.255.11.135", "255.255.111.35"]
 *
 * 1. Choices: We have to put 3 dots. The position of the dots are our choice. 
 * 				we can choose any index in the string to put our dot.
 *
 * 2. Constraint: However, not all our choices will be valid. That means, in this 
 * 				scenario, only few position of the dot will lead to valid ip.
 *
 * 3. Goal/Base: We need to define our goal which is the terminating case of our 
 * 				recursion. In this case, when we have already put the 3 dots we will
 * 				be done. Also we need to think of some other terminating cases that
 *				may arise.
 * 
 */