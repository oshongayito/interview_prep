    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 10000000
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))

int main(){

    int n,i,j,k,l,flag=0;
    vc<vc<char> >grid;


    cin>>n;
    grid.resize(n);
    for(i=0;i<n;i++)grid[i].resize(n);


    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++){
            cin>>grid[i][j];
        }
    }

    for(i=0;i<n && flag==0;i++)
    {
        for(j=0,k=0;j<n;j++)
        {
            if(i-1>=0 && grid[i-1][j]=='o')k++;
            if(i+1<n && grid[i+1][j]=='o')k++;
            if(j-1>=0 && grid[i][j-1]=='o')k++;
            if(j+1<n && grid[i][j+1]=='o')k++;

            if((k%2)!=0){
                flag=1;
                break;
            }

        }

    }
    if(flag==0)cout<<"YES"<<endl;
    else cout<<"NO"<<endl;
    return 0;
}
