
https://drive.google.com/file/d/1fsJ9ztvlbpCQaMdXqqQxHSCWX-8LeuLg/view?usp=sharing

### Vertical vs Horizontal Scaling ###

Horizontal vs. Vertical Scaling: Horizontal scaling means that you scale by adding more servers into your pool of resources whereas Vertical scaling means that you scale by adding more power (CPU, RAM, Storage, etc.) to an existing server.


The distinction of horizontal vs vertical comes from the traditional tabular view of a database. A database can be split **vertically** — storing different tables & columns in a separate database, or  **horizontally ** — storing rows of a same table in multiple database nodes.


 **Vertical scaling ** needs to be done at  **application level ** (specify in the code where should the query look at)
 **Horizontal scaling ** can be done either in the  **application level or in the database level**

 **Sharding ** provides  **horizontal scaling**


Sharding Process: https://drive.google.com/file/d/1wlhNBOLBk_o1cOkBWRsTrEOHpQbYcUC9/view?usp=sharing





