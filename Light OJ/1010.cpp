    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector

    int main()
    {
        int t,m,n,i,j,k;

        cin>>t;

        for(i=1;i<=t;i++)
        {

            scanf("%d%d",&m,&n);
            if(m==1 || n==1)
            {

                k=m*n;
            }
            else if(m==2 || n==2)
            {
                if(m==2)
                {
                    if(n%4==1 || n%4==2)
                    k=(4*(n/4))+((n%4)*2);
                    else if(n%4==3)k=(4*((n-1)/4))+(((n-1)%4)*2);
                    else k=(4*((n-2)/4))+(((n-2)%4)*2);
                }
                else
                {
                    if(m%4==1 || m%4==2)
                    k=(4*(m/4))+((m%4)*2);
                    else if(m%4==3)k=(4*((m-1)/4))+(((m-1)%4)*2);
                    else k=(4*((m-2)/4))+(((m-2)%4)*2);
                }
            }
            else
            {
                k=(m*n);

                if(k%2==1)k=((k+1)/2);
                else k=(k/2);
            }

            //cout<<"Case "<<i<<": "<<k<<endl;
            printf("Case %d: %d\n",i,k);

        }
        return 0;
    }
