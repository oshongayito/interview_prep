    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>
    #include <iomanip>
    #include <fstream>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector
    #define pi acos(-1)


    #define MAX 1000000007LL
#define is_it(n) (flag[n>>6]&(1<<((n>>1)&31)))
#define set_it(n) (flag[n>>6]|=(1<<((n>>1)&31)))


int main()
{
    freopen ("A-large.in","r",stdin);
    ofstream myfile;
  myfile.open ("output.txt");

 /// printf ("This sentence is redirected to a file.");
 // fclose (stdout);

  ll t,s,max,room;
  vc< vc<ll>>arr;
  map<ll,pair<int,int>>maps;


  cin>>t;

  for(int k=0;k<t;k++)
  {
      cin>>s;
      arr.resize(s);
      for(int i=0;i<s;i++)arr[i].resize(s);


        max=-1;
        room=MAX;

      for(int j=0;j<s;j++)
      {
          for(int l=0;l<s;l++)
          {
              cin>>arr[j][l];
              maps[arr[j][l]]=mp(j,l);
          }
      }

      for(ll i=(s*s);i>0;i--)
      {
          if(i==(s*s))arr[maps[i].first][maps[i].second]=1;
          else
          {
              if(maps[i].first==maps[i+1].first && maps[i].second-1==maps[i+1].second)
              {
                  arr[maps[i].first][maps[i].second]=arr[maps[i+1].first][maps[i+1].second]+1;
              }
              else if(maps[i].first==maps[i+1].first && maps[i].second+1==maps[i+1].second)
              {
                  arr[maps[i].first][maps[i].second]=arr[maps[i+1].first][maps[i+1].second]+1;
              }
              else if(maps[i].first-1==maps[i+1].first && maps[i].second==maps[i+1].second)
              {
                  arr[maps[i].first][maps[i].second]=arr[maps[i+1].first][maps[i+1].second]+1;
              }
              else if(maps[i].first+1==maps[i+1].first && maps[i].second==maps[i+1].second)
              {
                  arr[maps[i].first][maps[i].second]=arr[maps[i+1].first][maps[i+1].second]+1;
              }
              else
              {
                  arr[maps[i].first][maps[i].second]=1;
              }

          }

          if(arr[maps[i].first][maps[i].second]>=max)
          {
              max=arr[maps[i].first][maps[i].second];
              if(i<room)room=i;

          }
      }

      myfile<<"Case #"<<k+1<<": "<<room<<" "<<max<<endl;
  }
fclose (stdin);
myfile.close();
    return 0;
}
