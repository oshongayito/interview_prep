
def pow_mod(x, y, p):
    res = 1

    x = x % p

    while y > 0:  # O(log(y)) time
        if y & 1:
            res = ((res % p) * (x % p)) % p

        x = (x * x) % p
        y >>= 1
    return res % p

def main():
    print(pow_mod(5,3,7))

if __name__ == '__main__':
    main()
