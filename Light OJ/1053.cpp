#include<iostream>
#include<cstdio>
#include<cmath>

using namespace std;

int main()
{
    long long int tests,x,y,z,i;

    cin>>tests;

    for(i=1;i<=tests;i++)
    {

        cin>>x>>y>>z;
        if(((x*x)+(y*y)==(z*z))||((x*x)+(z*z)==(y*y))||((y*y)+(z*z)==(x*x)))cout<<"Case "<<i<<": "<<"yes"<<endl;
        else cout<<"Case "<<i<<": "<<"no"<<endl;
    }

    return 0;

}
