    #include<iostream>
    #include<vector>
    #include<cmath>
    #include<cstring>
    #include<cstdio>
    #include<queue>
    #include<map>
    #include <set>
    #include <stack>
    #include <sstream>
    #include <list>
    #include<cstdlib>
    #include<string>

    #define black 1
    #define white 0
    #define pb push_back
    #define ms memset

    using namespace std;


    int main()
    {
        map <string,int> mp;
        //char mnth1[20],mnth2[20],day1[5],day2[5];
        string mnth1,mnth2,day1,day2;
        long long int year1,year2,cases,x,y,i,j,k,dy1,dy2,m1,m2,reslt=0,range;
        bool flag=0,fl=0;
        mp.clear();
        mp["January"]=1;
        mp["February"]=2;
        mp["March"]=3;
        mp["April"]=4;
        mp["May"]=5;
        mp["June"]=6;
        mp["July"]=7;
        mp["August"]=8;
        mp["September"]=9;
        mp["Ocotober"]=10;
        mp["November"]=11;
        mp["December"]=12;


        //scanf("%lld",&cases);
        cin>>cases;
        for(i=1;i<=cases;i++)
        {
            //scanf("%s%s%lld",mnth1,day1,&year1);
            //scanf("%s%s%lld",mnth2,day2,&year2);
            cin>>mnth1>>day1>>year1;
            cin>>mnth2>>day2>>year2;

            dy1=atoi(day1.c_str());
            dy2=atoi(day2.c_str());
            m1 = mp[mnth1];
            m2 = mp[mnth2];

            //printf("%lld %llld %lld , %lld %lld %lld\n",dy1,m1,year1,dy2,m2,year2);



            if((year1%4==0)&& m1<3)
            {
                if(year1%100==0 && year1%400!=0)
                {
                    reslt = reslt;
                }
                else
                {
                    reslt++;
                }
                fl=1;
            }

            if((year2%4==0)&& m2>=2)
            {
                if(year2%100==0 && year2%400!=0)
                {
                    reslt = reslt;
                }
                else
                {
                    reslt++;
                    if(m2==2 && dy2<29)reslt--;

                }
            }
            for(j=year1 ; j<year2 && fl==0; j++)
                {
                    if(j%4==0 )
                    {
                        if(j>year1)
                        {
                            reslt++;
                        if(j%100==0 && j%400!=0)reslt--;
                        }
                         break;
                    }

                }
                //cout<<"@> "<<reslt<<endl;
            if(fl==1)j=year1;
            range = year2-j-1;
            k=j;
            //cout<<"k> "<<k<<endl;
            reslt += (range/4);
            //cout<<"reslt> "<<reslt<<endl;
            if(k%100==0)k-=100;

            for(j=(k)+(100-k%100); j<year2 ;j+=100)
            {
                //cout<<"j> "<<j;
                if(j%400!=0){if(j>year1 && k%100!=0)reslt--;}
                else {flag=1;break;}
            }
            if(flag==1)
            reslt-=((year2-1-j)/100 - (year2-1-j)/400);

            //cout<<"year2-1-j> "<<year2-1-j<<endl;

            if(year1==year2)reslt--;
            if(reslt<0)reslt=0;

            printf("Case %lld: %lld\n",i,reslt);


            reslt=0;range=0;j=0;flag=0;fl=0,k=0;


        }

        return 0;

    }
