 ### Common data structures to use ###

1. Array (C++: vector)
2. Hash table (C++: unordered_map<T,T>, map<T,T>)
3. Queue (C++: queue<T>)
4. Stack (sometimes recursion helps better than stack) (C++: stack<T>)
5. Priority Q (C++: priority_queue<T>)
6. Heap (using priority queue. C++: priority_queue<int, vector<int>, greater<int>>)
7. List (doubly link list) (C++: list<T>)
8. Set (C++: unordered_set)
9. Map (C++: unordered_map<T,T>, map<T,T>)
10. Tree 
11. BST 
12. Multiset (like unordered_set but supports duplicate) (C++: multiset<T>)
13. Disjoint Set (Union Find)
14. Segment Tree
15. Iterators

