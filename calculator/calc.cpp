#include<iostream>
#include<cstdio>
#include<cstdlib>
#include<cmath>
#include<cstring>


using namespace std;

int main(int argc, char *argv[])
{

	if(argc==4)
	{
		float x,y;
		char control;
		x=atof(argv[1]);
		y=atof(argv[3]);
		 control = argv[2][0];
		switch(control){
			case '+':
			cout<<x+y<<endl;
			break;
			
			case '-':
			cout<<x-y<<endl;
			break;
			
			case '*':
			cout<<x*y<<endl;
			break;
			
			case '/':
			cout<<x/y<<endl;
			break;
			
			default:
			cout<<"invalid syntax"<<endl;
			break;
		}
	}
	else if(argc==5)
	{
		float x,y;
		x= atof(argv[1]);
		y= atof(argv[4]);
		cout<<x*y<<endl; 
	}
	
	
	exit(0);
}

