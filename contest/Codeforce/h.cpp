

#include<iostream>
#include<cstdio>
#include<string>
#include<vector>
#include<algorithm>
#include<map>
#include<cmath>
#define pi (2*acos(0.0))
#define pb push_back

using namespace std;

bool bisearch(double a1, double b1, double a2, double b2, double x1, double y1,double x2,double y2)
{

    int fl=0;
    double ux,uy,lx,ly,cx,cy,i,j;
    if(a1>=x1&&a1<=x2&&b1>=y2&&b1<=y1)return 1;
    if(a2>=x1&&a2<=x2&&b2>=y2&&b2<=y1)return 1;

    else
    {
        if(a1<a2)
        {ux=a1;uy=b1;lx=a2;ly=b2;}
        else if(a1==a2)
        {
            if(b1>=b2)
            {
                ux=a1;uy=b1;lx=a2;ly=b2;
            }
        }
        else {ux=a2;uy=b2;lx=a1;ly=b1;}


        cx= (ux+lx)/2;
        cy= (uy+ly)/2;

        while((abs(ux-cx)>0.00001)||(abs(cx-lx)>0.00001))
        {
            //cout<<cx<<" "<<cy<<endl;
            if(cx>=x1&&cx<=x2&&cy>=y2&&cy<=y1){fl=1;break;}
            else if(cx>x2){lx=cx;ly=cy;}
            else if(cx<x1){ux=cx;uy=cy;}
            else if(cy>y1){ux=cx;uy=cy;}
            else if(cy<y2){lx=cx;ly=cy;}
            cx=(ux+lx)/2;
            cy=(uy+ly)/2;

        }
     }
     if(fl==1){return 1;}
     else return 0;
}

int  main()
{
    int t,i,f=0;
    double a1,a2,b1,b2,x1,x2,y1,y2,l,m,n,p,q,r1,r2,r3,r4,s1,s2,s3,s4;
    cin>>t;
    for(i=1;i<=t;i++){
    cin>>a1>>b1>>a2>>b2>>x1>>y1>>x2>>y2;

   if(bisearch(a1,b1,a2,b2,x1,y1,x2,y2))cout<<"T"<<endl;
   else cout<<"F"<<endl;
    }
	return 0;
}
