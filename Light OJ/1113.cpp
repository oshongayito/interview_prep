    #include<cstdio>
    #include<cstring>
    #include<cstdlib>
    #include<cmath>
    #include<cctype>
    #include<climits>
    #include<ctime>
    #include<iostream>
    #include<sstream>
    #include<string>
    #include<vector>
    #include<map>
    #include<stack>
    #include<queue>
    #include<algorithm>
    #include<set>
    #include<bitset>
    #include<complex>
    #include<numeric>
    #include<valarray>

    using namespace std;

    #define ll long long
    #define pb push_back
    #define mp make_pair
    #define vc vector

    int main()
    {
        string cmd,adrs,cur;
        vc<string> bstr,fstr;
        int x=0,y=0,i,j,k,t;
        bool f=0;
        cin>>t;
        for(i=1;i<=t;i++){
        cur="http://www.lightoj.com/";
        for(j=0; ;j++)
        {
            cin>>cmd;
            if(j==0)
            cout<<"Case "<<i<<":"<<endl;

            if(cmd.compare("QUIT")==0)break;
            if(cmd.compare("VISIT")==0)
            {

                cin>>adrs;
                bstr.pb(cur);
                fstr.clear();
                cur=adrs;
            }
            else if(cmd.compare("BACK")==0)
            {
                if(bstr.size()>0){
                fstr.pb(cur);
                adrs=bstr.back();
                bstr.pop_back();
                cur=adrs;

                }
                else f=1;
            }
            else if(cmd.compare("FORWARD")==0)
            {
                if(fstr.size()>0)
                {
                    bstr.pb(cur);
                    adrs=fstr.back();
                    fstr.pop_back();
                    cur=adrs;
                }
                else f=1;
            }

            if(f==0)
            {
                cout<<cur<<endl;
            }
            else
            {
                cout<<"Ignored"<<endl;
                //cur = adrs;
            f=0;
            }
        }

        f=0;
        bstr.clear();
        fstr.clear();
        adrs.clear();
        cur.clear();

        }
        return 0;
    }
