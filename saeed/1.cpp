#include <set>
#include <map>
#include <cmath>
#include <ctime>
#include <queue>
#include <stack>
#include <vector>
#include <string>
#include <cctype>
#include <cstdio>
#include <iomanip>
#include <sstream>
#include <cstdlib>
#include <cassert>
#include <climits>
#include <complex>
#include <numeric>
#include <valarray>
#include <iostream>
#include <memory.h>
#include <algorithm>
using namespace std;
#define all(a) a.begin(),a.end()
#define pb push_back
#define mp make_pair
#define rep(i,n) for(int i(0),_n(n);i<_n;++i)
#define repi(i,a,b) for(int i(a),_b(b);i<_b;++i)
template<class T> int len(const T&c){return (int)c.size();}
typedef long long ll;
#define repe(it,c) for(__typeof((c).begin()) it=(c).begin();it!=(c).end();++it)
typedef vector<int> vi;
typedef vector<string> vs;
#define mem(x,a) memset(x,a,sizeof(x))
template<class T> void cmin(T &a,T b){if(b<a) a=b;}
template<class T> void cmax(T &a,T b){if(b>a) a=b;}
vector<int> v;
vector<vector<int> > arr;


int pow(int n){
		int p=0;
		while((1<<p)<=n){
				p++;

		}
		return p-1;
}

void	Rmq(vector<int> t){
		int n,sqn;
		int i,j,l;
		v=t;
		n=len(v);
		sqn=log(n)/log(2);
		arr=vector<vi> (n,vi(sqn+1));
		rep(k,n)arr[k][0]=k;
		for(j=1;(1<<j)<=n;j++){
			for(i=0;i+(1<<j)-1<n;i++){
				if(v[arr[i][j-1]]<v[arr[i+(1<<(j-1))][j-1]])arr[i][j]=arr[i][j-1];
				else arr[i][j]=arr[i+(1<<(j-1))][j-1];
			}
		}
	}

int rmq(int f,int s){
	int l;
	//l=log(s-f+1)/log(2);
	l=pow(s-f+1);
	int l1=v[arr[s-(1<<l)+1][l]],l2=v[arr[f][l]];
	if(l1<=l2)return l1;
	else return l2;
}


int dp[560][560];


int main(){
	int n=500,k=500,t=500;
	cin>>n>>k>>t;
    vector<int> r(n+1);
    for(int i=0;i<n;i++){
			//r[i+1]=rand()%100;
			cin>>r[i+1];
    }

	Rmq(r);


    for(int i=1;i<=n;i++){
    	for(int j=1;j<=k;j++)s{
			for(int l=1;l<=min(t,n-k+1);l++){
				if(i-l>=0){
					dp[i][j]=max(dp[i][j],dp[i-l][j]);
					dp[i][j]=max(dp[i][j],dp[i-l][j-1]+l*(rmq(i-l+1,i)));

				}
				else break;
			}
    	}
    }
	cout<<dp[n][k]<<endl;

	//cout<<1.0*clock()/CLOCKS_PER_SEC;

}
/*

10 2 4
7
3
12
11
13
4
8
6
6
20

ans=57

10 3 4
7
3
12
11
13
4
8
6
6
20

ans=71

10 1 4
7
30
12
11
13
8
8
6
6
20

*/
