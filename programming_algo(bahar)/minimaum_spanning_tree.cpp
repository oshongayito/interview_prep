#include<iostream>
#include<cstdio>
#include<set>
#include<queue>
#include<string>
#include<cstring>
#include<vector>
#include<map>
#include<cstdlib>
#include<climits>
using namespace std;


class edge{
	public:
	int to;
	int weight;
	edge(){}
	edge(int tt,int ww)
	{
		to=tt;
		weight=ww;
	}
    bool operator<(const edge &f) const
	{
		 return weight>f.weight;
	}
};

int mst(int source,int destination,vector< vector<edge> > &graph,vector<edge> &list)
{
	int length=0;
	vector<int> distance(graph.size(),INT_MAX);
	vector<int> visit(graph.size(),0);
	distance[source]=0;
	priority_queue<edge> q; 
		
	q.push(edge(source,0));

	while(!q.empty())
	{
		int node=q.top().to;
		int dist=q.top().weight;
		
		q.pop();
		
		if(dist>distance[node]) continue;
		
		length+=dist;
		list.push_back(edge(node,dist));
		visit[node]=1;
		
		for(int i=0;i<(int)graph[node].size();i++)
		{
			int to=graph[node][i].to;
			int weight=graph[node][i].weight;
			
			if(!visit[to] && weight<distance[to])
			{
				distance[to]=weight;
				q.push(edge(to,distance[to]));
			}
		}
	}
	
	return length;
}

int main()
{
		
	int n,m,s,t,u,v,e,d;
	
	cin>>t;
	
	for(int p=1;p<=t;p++)
	{
    
    cin>>n>>m>>s>>d;
    
    vector< vector<edge> > graph(n);
    vector<edge> list;

    for(int i=0;i<m;i++)
    {
	    cin>>u>>v>>e;
	    
	    graph[u].push_back(edge(v,e));
	    graph[v].push_back(edge(u,e));
	    
	} 	
	
	
	int length=mst(s,d,graph,list);
	
	cout<<"Case #"<<p<<": "<<length<<endl;
	
	}
	
	return 0;
}

